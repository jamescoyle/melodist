﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.PhraseEditors
{
    /// <summary>
    /// Interaction logic for PhraseInvertPopup.xaml
    /// </summary>
    public partial class PhraseInvertPopup : Popup
    {

        public PhraseInvertPopup(decimal reflectionPitch = 60, Scale originalScale = null)
        {
            InitializeComponent();
            ReflectionPitch.Value = Math.Round(reflectionPitch * 2) / 2;

            if (originalScale != null)
                OriginalScaleLabel.Content = originalScale.ToString();

            Thumb thumb = new Thumb
            {
                Width = 0,
                Height = 0,
            };
            ContentCanvas.Children.Add(thumb);

            MouseDown += (sender, e) =>
            {
                thumb.RaiseEvent(e);
            };

            thumb.DragDelta += (sender, e) =>
            {
                HorizontalOffset += e.HorizontalChange;
                VerticalOffset += e.VerticalChange;
            };
        }


        public void SetNewScale(Scale newScale)
        {
            NewScaleLabel.Content = (newScale == null) ? string.Empty : newScale.ToString();
        }



        public event PhraseInvertPopupEventHandler ReflectionPitchChanged;

        private void ReflectionPitch_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            decimal pitchDecimal = (decimal)ReflectionPitch.Value;
            Pitch pitch1 = new Pitch((int)Math.Floor(pitchDecimal));
            Pitch pitch2 = new Pitch((int)Math.Ceiling(pitchDecimal));

            if (pitch1.Value == pitch2.Value)
                PitchName.Content = pitch1.ToString(false, true);
            else
            {
                if (pitch1.Octave == pitch2.Octave)
                    PitchName.Content = pitch1.ToString(false) + "/" + pitch2.ToString(false, true);
                else
                    PitchName.Content = pitch1.ToString(false, true) + "/" + pitch2.ToString(false, true);
            }

            ReflectionPitchChanged?.Invoke(this, new PhraseInvertPopupEventArgs(pitchDecimal));
        }



        public event PhraseInvertPopupEventHandler Confirmed;

        private void ConfirmBtn_Click(object sender, RoutedEventArgs e)
        {
            Confirmed?.Invoke(this, new PhraseInvertPopupEventArgs((decimal)ReflectionPitch.Value));
        }

    }



    public delegate void PhraseInvertPopupEventHandler(object sender, PhraseInvertPopupEventArgs e);


    public class PhraseInvertPopupEventArgs : EventArgs
    {

        public decimal ReflectionPitch { get; private set; }

        public PhraseInvertPopupEventArgs(decimal reflectionPitch)
        {
            ReflectionPitch = reflectionPitch;
        }

    }
}
