﻿using GeneralMidi;
using HarmonyEngine;
using Jacobi.Vst.Core;
using MathPlus;
using MidiPlayback;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSTHosting;

namespace NewMelodist.PhraseEditors
{
    public class PhraseEditorWaveProvider : VSTWaveProvider
    {

        public PhraseEditor PhraseEditor { get; private set; }


        public override Rational LoopStart { get { return PhraseEditor.Phrase.StartBeat; } }

        public override Rational LoopEnd { get { return PhraseEditor.Phrase.EndBeat; } }

        public override bool Loop { get { return true; } }

        public override Rational Tempo { get { return PhraseEditor.Phrase.Tempo; } }



        public PhraseEditorWaveProvider(PhraseEditor phraseEditor, string pluginPath, int sampleRate, int channels)
            : base(pluginPath, sampleRate, channels)
        {
            PhraseEditor = phraseEditor;
            Playhead = PreviousPlayhead = phraseEditor.Phrase.StartBeat;
        }

        
        protected override global::Jacobi.Vst.Core.VstEvent[] GetVstEvents(int offset, int count)
        {
            Rational framesPerBeat = new Rational(60 * WaveFormat.SampleRate, 1) / Tempo;
            int samplesRequired = count;

            Rational startBeat = Playhead;
            Rational endBeat = startBeat + (samplesRequired / framesPerBeat);
            
            List<VstEvent> vstEvents = new List<VstEvent>();

            Rational beatOffset = 0;

            for (int i = 0; i < 2; i++)
            {
                foreach (Note note in PhraseEditor.Phrase.Notes.ToList())
                {
                    if (note.EndBeat > startBeat && note.EndBeat <= endBeat)
                        vstEvents.Add(NoteOffEvent(1, (byte)note.Pitch.Value, ((note.EndBeat - startBeat + beatOffset) * framesPerBeat).Round()));

                    if (note.StartBeat >= startBeat && note.StartBeat < endBeat)
                        vstEvents.Add(NoteOnEvent(1, (byte)note.Pitch.Value, 100, (note.Duration * framesPerBeat).Round(), ((note.StartBeat - startBeat + beatOffset) * framesPerBeat).Round()));
                }

                foreach(Chord chord in PhraseEditor.Phrase.Chords.ToList())
                {
                    if (chord.EndBeat > startBeat && chord.EndBeat <= endBeat)
                    {
                        foreach(Pitch pitch in chord.Pitches)
                            vstEvents.Add(NoteOffEvent(2, (byte)(pitch.Value + 48), ((chord.EndBeat - startBeat + beatOffset) * framesPerBeat).Round()));
                    }

                    if (chord.StartBeat >= startBeat && chord.StartBeat < endBeat)
                    {
                        foreach(Pitch pitch in chord.Pitches)
                            vstEvents.Add(NoteOnEvent(2, (byte)(pitch.Value + 48), 100, (chord.Duration * framesPerBeat).Round(), ((chord.StartBeat - startBeat + beatOffset) * framesPerBeat).Round()));
                    }
                }

                if (!Loop || endBeat < LoopEnd)
                    break;

                beatOffset = endBeat - LoopEnd;
                endBeat -= LoopEnd - LoopStart;
                startBeat = LoopStart;
            }

            PreviousPlayhead = Playhead;
            Playhead = endBeat;

            return vstEvents.OrderBy(x => x.DeltaFrames).ToArray();
        }

    }



    public class PhraseEditorGMPlayer : GMPlayer
    {

        public PhraseEditor PhraseEditor { get; private set; }


        public override bool Loop { get { return true; } }

        public override Rational LoopStart { get { return PhraseEditor.Phrase.StartBeat; } }

        public override Rational LoopEnd { get { return PhraseEditor.Phrase.EndBeat; } }

        public override Rational Tempo { get { return PhraseEditor.Phrase.Tempo; } }


        public PhraseEditorGMPlayer(PhraseEditor phraseEditor, MidiDevice outputDevice = null)
            : base(outputDevice)
        {
            PhraseEditor = phraseEditor;
        }


        public void UpdateInstrument()
        {
            OutputDevice.SendProgramChange(1, PhraseEditor.InstrumentPicker.SelectedInstrument.Index);
        }


        protected override void Read(Rational startBeat, Rational endBeat)
        {
            if (Loop && startBeat > endBeat && startBeat < LoopEnd && endBeat > LoopStart)
            {
                Read(startBeat, LoopEnd);
                Read(LoopStart, endBeat);
                return;
            }

            List<Note> notes = PhraseEditor.Phrase.Notes.ToList();
            List<Chord> chords = PhraseEditor.Phrase.Chords.ToList();

            foreach (Note note in notes)
            {
                if (note.EndBeat > startBeat && note.EndBeat <= endBeat)
                    OutputDevice.SendNoteOff(1, note.Pitch, 127);
            }

            foreach(Chord chord in chords)
            {
                if(chord.EndBeat > startBeat && chord.EndBeat <= endBeat)
                {
                    foreach (Pitch pitch in chord.Pitches)
                        OutputDevice.SendNoteOff(1, pitch.Value + 48, 127);
                }
            }

            foreach(Note note in notes)
            {
                if (note.StartBeat >= startBeat && note.StartBeat < endBeat)
                    OutputDevice.SendNoteOn(1, note.Pitch, 127);
            }

            foreach(Chord chord in chords)
            {
                if (chord.StartBeat >= startBeat && chord.StartBeat < endBeat)
                {
                    foreach (Pitch pitch in chord.Pitches)
                        OutputDevice.SendNoteOn(1, pitch.Value + 48, 127);
                }
            }
        }

    }
}
