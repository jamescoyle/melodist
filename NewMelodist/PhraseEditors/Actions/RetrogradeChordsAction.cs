﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.PhraseEditors.Actions
{
    public class RetrogradeChordsAction : PhraseEditorAction, IUndoable
    {

        public bool IsUndone { get; private set; }

        public List<Chord> Chords { get; private set; }

        public Rational RangeStart { get; private set; }

        public Rational RangeEnd { get; private set; }



        public RetrogradeChordsAction(PhraseEditor phraseEditor)
            : base(phraseEditor)
        {
            Chords = PhraseEditor.PhraseChordViewer.GetSelectedChordViewers().Select(x => x.Chord).ToList();

            if (Chords.Count <= 1 || Chords.Count == PhraseEditor.Phrase.Chords.Count)
            {
                Chords = PhraseEditor.Phrase.Chords.ToList();
                RangeStart = PhraseEditor.Phrase.StartBeat;
                RangeEnd = PhraseEditor.Phrase.EndBeat;
            }
            else
            {
                RangeStart = Chords.Min(x => x.StartBeat);
                RangeEnd = Chords.Max(x => x.EndBeat);
            }

            Redo();
            Finished = true;
        }


        public void Undo()
        {
            foreach (Chord chord in Chords)
                chord.StartBeat = RangeStart + (RangeEnd - chord.EndBeat);

            IsUndone = true;
        }

        public void Redo()
        {
            foreach (Chord chord in Chords)
                chord.StartBeat = RangeStart + (RangeEnd - chord.EndBeat);

            IsUndone = false;
        }

    }
}
