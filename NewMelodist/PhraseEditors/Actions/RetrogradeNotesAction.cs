﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.PhraseEditors.Actions
{
    public class RetrogradeNotesAction : PhraseEditorAction, IUndoable
    {

        public bool IsUndone { get; private set; }

        public List<Note> Notes { get; private set; }

        public Rational RangeStart { get; private set; }

        public Rational RangeEnd { get; private set; }



        public RetrogradeNotesAction(PhraseEditor phraseEditor)
            : base(phraseEditor)
        {
            Notes = PhraseEditor.PhraseNotesViewer.GetSelectedNoteViewers().Select(x => x.Note).ToList();
            if(Notes.Count <= 1 || Notes.Count == PhraseEditor.Phrase.Notes.Count)
            {
                Notes = PhraseEditor.Phrase.Notes.ToList();
                RangeStart = PhraseEditor.Phrase.StartBeat;
                RangeEnd = PhraseEditor.Phrase.EndBeat;
            }
            else
            {
                RangeStart = Notes.Min(x => x.StartBeat);
                RangeEnd = Notes.Max(x => x.EndBeat);
            }

            Redo();
            Finished = true;
        }


        public void Undo()
        {
            foreach (Note note in Notes)
                note.StartBeat = RangeStart + (RangeEnd - note.EndBeat);

            IsUndone = true;
        }

        public void Redo()
        {
            foreach (Note note in Notes)
                note.StartBeat = RangeStart + (RangeEnd - note.EndBeat);

            IsUndone = false;
        }

    }
}
