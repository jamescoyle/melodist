﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.PhraseEditors.Actions
{
    public abstract class PhraseEditorAction
    {

        public PhraseEditor PhraseEditor { get; private set; }

        public bool Finished { get; protected set; }


        public PhraseEditorAction(PhraseEditor phraseEditor)
        {
            PhraseEditor = phraseEditor;
        }

    }
}
