﻿using CSharpExtras;
using HarmonyEngine;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist.PhraseEditors.Actions
{
    public class NewChordsAction : PhraseEditorAction, IUndoable
    {

        public bool IsUndone { get; private set; }

        public List<Chord> OldChords { get; private set; }

        public List<Chord> NewChords { get; private set; }



        public NewChordsAction(PhraseEditor phraseEditor) 
            : base(phraseEditor)
        {
            ChordProgressioner progressioner = new ChordProgressioner()
            {
                MaxDissonance = 8,
                MaxScaleDistance = 5,
                MaxChordMovement = 4,
                ChordDefs = ChordDef.ChordDefs
            };
            Phrase newPhrase = progressioner.Run(phraseEditor.Phrase);

            OldChords = phraseEditor.Phrase.Chords.ToList();
            NewChords = newPhrase.Chords.ToList();
            Redo();
            Finished = true;
        }



        public void Undo()
        {
            foreach (Chord chord in NewChords)
                PhraseEditor.Phrase.RemoveChord(chord);

            foreach (Chord chord in OldChords)
                PhraseEditor.Phrase.AddChord(chord);

            IsUndone = true;
        }

        public void Redo()
        {
            foreach (Chord chord in OldChords)
                PhraseEditor.Phrase.RemoveChord(chord);

            foreach (Chord chord in NewChords)
                PhraseEditor.Phrase.AddChord(chord);

            IsUndone = false;
        }

    }
}
