﻿using CSharpExtras;
using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.PhraseEditors.Actions
{
    public class InvertNotesAction : PhraseEditorAction, IUndoable
    {

        public bool IsUndone { get; private set; }

        public List<Note> Notes { get; private set; }

        private List<Note> _notesBackup;

        public decimal ReflectionPitch { get; private set; }



        public InvertNotesAction(PhraseEditor phraseEditor)
            : base(phraseEditor)
        {
            IsUndone = false;

            Notes = PhraseEditor.PhraseNotesViewer.GetSelectedNoteViewers().Select(x => x.Note).ToList();

            if (Notes.Count <= 1 || Notes.Count == PhraseEditor.Phrase.Notes.Count)
                Notes = PhraseEditor.Phrase.Notes.ToList();

            if (Notes.Count == 0)
            {
                Finished = true;
                return;
            }

            //Create backup copy of the notes as they were when the action started
            _notesBackup = new List<Note>();
            Notes.ForEach(x => _notesBackup.Add(new Note(x.Pitch, x.StartBeat, x.Duration)));

            //Calculate which pitch to default to
            Scale phraseScale = PhraseEditor.Phrase.CalculateScale();

            decimal baseReflection = Math.Round((decimal)Notes.Average(x => x.Pitch.Value) * 2) / 2;
            decimal bestReflection = baseReflection;
            Scale bestReflectionScale = GetReflectionScale(bestReflection);
            double bestDistance = phraseScale.Distance(bestReflectionScale);

            for (decimal i = 0; Math.Abs(i) < 6; i *= -1)
            {
                if (i >= 0)
                    i += 0.5M;

                Scale reflectionScale = GetReflectionScale(baseReflection + i);
                double distance = phraseScale.Distance(reflectionScale);
                if (distance < bestDistance)
                {
                    bestReflection = baseReflection + i;
                    bestReflectionScale = reflectionScale;
                    bestDistance = distance;
                }
            }
            ReflectionPitch = bestReflection;

            //Create new popup
            PhraseInvertPopup popup = new PhraseInvertPopup(ReflectionPitch, bestReflectionScale);
            popup.SetNewScale(GetReflectionScale(ReflectionPitch, Notes));
            popup.ReflectionPitchChanged += Popup_ReflectionPitchChanged;
            popup.Confirmed += Popup_Closed;
            popup.Closed += Popup_Closed;
            popup.IsOpen = true;
        }



        private void Popup_ReflectionPitchChanged(object sender, PhraseInvertPopupEventArgs e)
        {
            ReflectionPitch = e.ReflectionPitch;
            PhraseInvertPopup popup = (PhraseInvertPopup)sender;
            popup.SetNewScale(GetReflectionScale(ReflectionPitch, Notes));
        }


        private void Popup_Closed(object sender, EventArgs e)
        {
            PhraseInvertPopup popup = (PhraseInvertPopup)sender;
            popup.ReflectionPitchChanged -= Popup_ReflectionPitchChanged;
            popup.Confirmed -= Popup_Closed;
            popup.Closed -= Popup_Closed;
            popup.IsOpen = false;

            Finished = true;
        }


        private void UpdateNotes(decimal reflectionPitch, List<Note> listToUpdate = null)
        {
            if (listToUpdate == null)
                listToUpdate = new List<Note>();

            if (listToUpdate.Count == 0)
                _notesBackup.ForEach(x => listToUpdate.Add(new Note(x.Pitch, x.StartBeat, x.Duration)));

            for (int i = 0; i < _notesBackup.Count; i++)
            {
                try
                {
                    Note x = _notesBackup[i];
                    listToUpdate[i].Pitch = new Pitch((int)((2 * reflectionPitch) - x.Pitch.Value));
                }
                catch { }
            }
        }


        private Scale GetReflectionScale(decimal reflectionPitch, List<Note> notes = null)
        {
            if (notes == null)
                notes = new List<Note>();

            UpdateNotes(reflectionPitch, notes);

            return Scale.GetBestFit(notes);
        }



        public void Undo()
        {
            for (int i = 0; i < Notes.Count; i++)
                Notes[i].Pitch = _notesBackup[i].Pitch;

            IsUndone = true;
        }


        public void Redo()
        {
            UpdateNotes(ReflectionPitch, Notes);

            IsUndone = false;
        }

    }
}
