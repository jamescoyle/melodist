﻿using CSharpExtras;
using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist.PhraseEditors.Actions
{
    public class InvertChordsAction : PhraseEditorAction, IUndoable
    {

        public bool IsUndone { get; private set; }

        public List<Chord> Chords { get; private set; }

        public List<Chord> _chordsBackup;

        public decimal ReflectionPitch { get; private set; }



        public InvertChordsAction(PhraseEditor phraseEditor)
            : base(phraseEditor)
        {
            IsUndone = false;

            Chords = PhraseEditor.PhraseChordViewer.GetSelectedChordViewers().Select(x => x.Chord).ToList();

            if (Chords.Count <= 1 || Chords.Count == PhraseEditor.Phrase.Chords.Count)
                Chords = PhraseEditor.Phrase.Chords.ToList();

            if (Chords.Count == 0)
            {
                Finished = true;
                return;
            }

            //Create backup copy of the notes as they were when the action started
            _chordsBackup = new List<Chord>();
            Chords.ForEach(x => _chordsBackup.Add(new Chord(x.ChordDef, x.Root, x.StartBeat, x.Duration)));

            //Calculate which reflection pitch to default to
            Scale phraseScale = PhraseEditor.Phrase.CalculateScale();

            decimal baseReflection = 60;
            decimal bestReflection = baseReflection;
            Scale bestReflectionScale = GetReflectionScale(bestReflection);
            double bestDistance = phraseScale.Distance(bestReflectionScale);

            for (decimal i = 0; Math.Abs(i) < 6; i *= -1)
            {
                if (i >= 0)
                    i += 0.5M;

                Scale reflectionScale = GetReflectionScale(baseReflection + i);
                double distance = phraseScale.Distance(reflectionScale);
                if (distance < bestDistance)
                {
                    bestReflection = baseReflection + i;
                    bestReflectionScale = reflectionScale;
                    bestDistance = distance;
                }
            }
            ReflectionPitch = bestReflection;

            //Create new popup
            PhraseInvertPopup popup = new PhraseInvertPopup(ReflectionPitch, bestReflectionScale);
            popup.SetNewScale(GetReflectionScale(ReflectionPitch, Chords));
            popup.ReflectionPitchChanged += Popup_ReflectionPitchChanged;
            popup.Confirmed += Popup_Closed;
            popup.Closed += Popup_Closed;
            popup.IsOpen = true;
        }



        private void Popup_ReflectionPitchChanged(object sender, PhraseInvertPopupEventArgs e)
        {
            ReflectionPitch = e.ReflectionPitch;
            PhraseInvertPopup popup = (PhraseInvertPopup)sender;
            popup.SetNewScale(GetReflectionScale(ReflectionPitch, Chords));
        }


        private void Popup_Closed(object sender, EventArgs e)
        {
            PhraseInvertPopup popup = (PhraseInvertPopup)sender;
            popup.ReflectionPitchChanged -= Popup_ReflectionPitchChanged;
            popup.Confirmed -= Popup_Closed;
            popup.Closed -= Popup_Closed;
            popup.IsOpen = false;

            Finished = true;
        }


        private void UpdateChords(decimal reflectionPitch, List<Chord> listToUpdate = null)
        {
            if (listToUpdate == null)
                listToUpdate = new List<Chord>();

            if (listToUpdate.Count == 0)
                _chordsBackup.ForEach(x => listToUpdate.Add(new Chord(x.ChordDef, x.Root, x.StartBeat, x.Duration)));

            for (int i = 0; i < _chordsBackup.Count; i++)
            {
                try
                {
                    Chord chord = _chordsBackup[i];
                    List<Pitch> invertPitches = new List<Pitch>();
                    chord.Pitches.ForEach(x => invertPitches.Add(new Pitch((int)((2 * reflectionPitch) - x.Value) % 12)));
                    Chord invertChord = Chord.Retrieve(invertPitches, chord.StartBeat, chord.Duration);
                    listToUpdate[i].ChordDef = invertChord.ChordDef;
                    listToUpdate[i].Root = invertChord.Root;
                }
                catch { }
            }
        }


        private Scale GetReflectionScale(decimal reflectionPitch, List<Chord> chords = null)
        {
            if (chords == null)
                chords = new List<Chord>();

            UpdateChords(reflectionPitch, chords);

            return Scale.GetBestFit(chords);
        }


        public void Undo()
        {
            UpdateChords(ReflectionPitch, Chords);

            IsUndone = true;
        }

        public void Redo()
        {
            Chords.Clear();
            _chordsBackup.ForEach(x => Chords.Add(x));

            IsUndone = false;
        }

    }
}
