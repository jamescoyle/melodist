﻿using CSharpExtras;
using HarmonyEngine;
using Microsoft.Win32;
using MidiPlayback;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NewMelodist.PhraseEditors.Actions;
using NewMelodist.MidiImports;
using MathPlus;

namespace NewMelodist.PhraseEditors
{
    /// <summary>
    /// Interaction logic for PhraseEditor.xaml
    /// </summary>
    public partial class PhraseEditor : UserControl
    {

        private Phrase _indirectPhrase;
        /// <summary>
        /// Setting IndirectPhrase results in a duplicate being produced in the Phrase property which gets modified by the editor while IndirectPhrase remains unchanged
        /// If the changes to phrase are satisfactory, they can then be confirmed to IndirectPhrase
        /// </summary>
        public Phrase IndirectPhrase
        {
            get
            {
                return _indirectPhrase;
            }
            set
            {
                if (value == null)
                {
                    _indirectPhrase = null;
                    return;
                }

                Phrase copyPhrase = new Phrase();
                copyPhrase.CopyProperties(value);
                value.Notes.ForEach(x => copyPhrase.AddNote(x.Copy()));
                value.Chords.ForEach(x => copyPhrase.AddChord(x.Copy()));
                Phrase = copyPhrase;
                _indirectPhrase = value;
            }
        }



        private Phrase _phrase;
        /// <summary>
        /// Phrase is the object that gets modified by the editor
        /// </summary>
        public Phrase Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                _phrase = value;
                _indirectPhrase = null;
                PhraseNotesViewer.Phrase = _phrase;
                PhraseChordViewer.Phrase = _phrase;
                PhraseDurationViewer.Phrase = _phrase;
                DataContext = _phrase;
            }
        }


        public UndoableStack PreviousActions { get; private set; }

        public PlayerManager Player { get; private set; }

        private DispatcherTimer PlaybackUIUpdater { get; set; }



        public PhraseEditor()
        {
            InitializeComponent();
            Phrase = new Phrase();

            InstrumentPicker.LoadVSTs(MelodistDB.Instance);

            PreviousActions = new UndoableStack();
            PhraseNotesViewer.PreviousActions.Pushed += OnActionPushed;
            PhraseChordViewer.PreviousActions.Pushed += OnActionPushed;

            PlaybackUIUpdater = new DispatcherTimer();
            PlaybackUIUpdater.Interval = TimeSpan.FromMilliseconds(30);
            PlaybackUIUpdater.Tick += (sender, args) =>
            {
                PhraseNotePlayhead.UpdatePlayhead(Player.Playhead);
            };
        }


        public void ConfirmChangesToIndirectPhrase()
        {
            if (IndirectPhrase == null || Phrase == null)
                return;

            IndirectPhrase.CopyProperties(Phrase);
            IndirectPhrase.ClearNotes();
            IndirectPhrase.ClearChords();
            Phrase.Notes.ForEach(x => IndirectPhrase.AddNote(new Note(x.Pitch, x.StartBeat, x.Duration)));
            Phrase.Chords.ForEach(x => IndirectPhrase.AddChord(new Chord(x.ChordDef, x.Root, x.StartBeat, x.Duration)));
        }



        private void OnActionPushed(object sender, UndoableStackEventArgs args)
        {
            //When detect undoable action pushed to one of the child UndoableStacks, add it to this one and remove it from the child's
            PreviousActions.Push(args.Item);
            ((UndoableStack)sender).Clear();
        }


        private void PhraseDurationViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }


        private void PhraseChordViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }


        private void PianoScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //If the piano-roll tries to scroll, force it back to the scroll position of the note viewer
            if (PianoScrollViewer.VerticalOffset != NoteScrollViewer.VerticalOffset)
                PianoScrollViewer.ScrollToVerticalOffset(NoteScrollViewer.VerticalOffset);
        }


        private void NoteScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //Update the horizontal and vertical scrolls of the controls based on the scrolling of the Notes Viewer
            if (DurationScrollViewer.HorizontalOffset != e.HorizontalOffset)
                DurationScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);

            if (ChordScrollViewer.HorizontalOffset != e.HorizontalOffset)
                ChordScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);

            if (PianoScrollViewer.VerticalOffset != e.VerticalOffset)
                PianoScrollViewer.ScrollToVerticalOffset(e.VerticalOffset);
        }


        private void StartBeatPicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.OldValue == null || e.NewValue == null)
                return;

            int oldValue = (int)e.OldValue;
            int newValue = (int)e.NewValue;
            Phrase.StartBeat += newValue - oldValue;
        }


        private void BeatCountPicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
                return;
            
            int newValue = (int)e.NewValue;
            Phrase.EndBeat = Rational.Min(Phrase.EndBeat, newValue);
        }


        #region Playback methods
        private void OnPlay(object sender, EventArgs e)
        {
            Play();
        }

        public void Play()
        {
            if (Player != null)
                return;

            try
            {
                if (InstrumentPicker.SelectedInstrument == null)
                {
                    MessageBox.Show("No instrument selected");
                    return;
                }

                InstrumentPicker.IsEnabled = false;

                LoadPlayer();
                Player.Play();
                PlaybackUIUpdater.Start();
            }
            catch
            {
                MessageBox.Show("Failed to start playback");
                InstrumentPicker.IsEnabled = true;
            }
        }


        private void OnStop(object sender, EventArgs e)
        {
            Stop();
        }

        public void Stop()
        {
            if (Player == null)
                return;

            PlaybackUIUpdater.Stop();
            PhraseNotePlayhead.RemovePlayhead();

            InstrumentPicker.IsEnabled = true;
            Player.Stop();
            Player = null;
        }


        private void ShowVstBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!InstrumentPicker.VstSelected)
            {
                MessageBox.Show("Please select a VST instrument");
                return;
            }
            try
            {
                LoadPlayer();
                VSTPluginWindow.Show(Player.VstPlayer.WaveProviders[0].VstContext);
            }
            catch
            {
                MessageBox.Show("Failed to show VST editor");
            }
        }


        private void LoadPlayer()
        {
            if (Player != null)
            {
                if (Player.GMPlayer != null)
                    ((PhraseEditorGMPlayer)Player.GMPlayer).UpdateInstrument();
                return;
            }

            VstPlayer vstPlayer = null;
            PhraseEditorGMPlayer gmPlayer = null;
            if(InstrumentPicker.VstSelected)
            {
                vstPlayer = new VstPlayer(new PhraseEditorWaveProvider(this, InstrumentPicker.SelectedVSTInstrument.FilePath, 44100, 2));
                vstPlayer.Playhead = Phrase.StartBeat;
            }
            else if(InstrumentPicker.SelectedInstrument != null)
            {
                gmPlayer = new PhraseEditorGMPlayer(this, Tools.GetSelectedMidiDevice());
                gmPlayer.UpdateInstrument();
                gmPlayer.Playhead = Phrase.StartBeat;
            }

            Player = new PlayerManager(vstPlayer, gmPlayer);
        }
        #endregion Playback methods



        #region File menu event handlers
        private void FileMenuLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog()
            {
                DefaultExt = ".phr",
                InitialDirectory = Project.Instance.FilePath,
                Filter = "Phrase documents (.phr)|*.phr"
            };
            if (openDialog.ShowDialog() == true)
                IndirectPhrase = Phrase.Load(openDialog.FileName);
        }


        private void FileMenuSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Phrase phrase = (IndirectPhrase != null) ? IndirectPhrase : Phrase;
                string phrasePath = phrase.FilePath;
                if (string.IsNullOrEmpty(phrasePath))
                {
                    SaveFileDialog saveDialog = new SaveFileDialog()
                    {
                        FileName = "My Phrase",
                        DefaultExt = ".phr",
                        InitialDirectory = Project.Instance.FilePath,
                        Filter = "Phrase documents (.phr)|*.phr"
                    };
                    if (saveDialog.ShowDialog() != true)
                        return;
                    phrasePath = saveDialog.FileName;
                }

                if (IndirectPhrase != null)
                    ConfirmChangesToIndirectPhrase();

                phrase.Save(phrasePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }


        private void FileMenuClear_Click(object sender, RoutedEventArgs e)
        {
            Phrase.ClearNotes();
            Phrase.ClearChords();
        }


        private void FileMenuExportMidi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveDialog = new SaveFileDialog()
                {
                    FileName = Phrase.Name,
                    DefaultExt = ".mid",
                    Filter = "MIDI Files (.mid)|*.mid"
                };
                if (saveDialog.ShowDialog() != true)
                    return;

                Rational startBeat = 0;
                while (startBeat > Phrase.StartBeat)
                    startBeat -= Phrase.BeatsPerBar;

                int ticksPerBeat = 120;
                NAudio.Midi.MidiEventCollection midiTracks = new NAudio.Midi.MidiEventCollection(1, ticksPerBeat);

                IList<NAudio.Midi.MidiEvent> metaTrack = midiTracks.AddTrack();
                NAudio.Midi.TempoEvent tempoEvent = new NAudio.Midi.TempoEvent(1, 0);
                tempoEvent.Tempo = Phrase.Tempo;
                metaTrack.Add(tempoEvent);
                metaTrack.Add(new NAudio.Midi.TimeSignatureEvent(0, Phrase.BeatsPerBar, 4, ticksPerBeat, 8));
                metaTrack.Add(new NAudio.Midi.MetaEvent(NAudio.Midi.MetaEventType.EndTrack, 0, ((Phrase.EndBeat - startBeat) * ticksPerBeat).Round()));

                IList<NAudio.Midi.MidiEvent> melodyTrack = midiTracks.AddTrack();
                melodyTrack.Add(new NAudio.Midi.PatchChangeEvent(0, 1, InstrumentPicker.IsGMInstrumentSelected ? InstrumentPicker.SelectedInstrument.Index : 1));
                foreach (Note note in Phrase.Notes)
                {
                    melodyTrack.Add(new NAudio.Midi.NoteOnEvent(((note.StartBeat - startBeat) * ticksPerBeat).Round(), 1, note.Pitch.Value, note.Velocity, (note.Duration * ticksPerBeat).Round()));
                    melodyTrack.Add(new NAudio.Midi.NoteOnEvent(((note.EndBeat - startBeat) * ticksPerBeat).Round(), 1, note.Pitch.Value, 0, 0));
                }
                melodyTrack.Add(new NAudio.Midi.MetaEvent(NAudio.Midi.MetaEventType.EndTrack, 0, ((Phrase.EndBeat - startBeat) * ticksPerBeat).Round()));

                IList<NAudio.Midi.MidiEvent> chordTrack = midiTracks.AddTrack();
                chordTrack.Add(new NAudio.Midi.PatchChangeEvent(0, 2, InstrumentPicker.IsGMInstrumentSelected ? InstrumentPicker.SelectedInstrument.Index : 1));
                foreach (Chord chord in Phrase.Chords)
                {
                    foreach (Pitch pitch in chord.Pitches)
                    {
                        chordTrack.Add(new NAudio.Midi.NoteOnEvent(((chord.StartBeat - startBeat) * ticksPerBeat).Round(), 2, (pitch.Value % 12) + 48, 104, (chord.Duration * ticksPerBeat).Round()));
                        chordTrack.Add(new NAudio.Midi.NoteOnEvent(((chord.EndBeat - startBeat) * ticksPerBeat).Round(), 2, (pitch.Value % 12) + 48, 0, 0));
                    }
                }
                chordTrack.Add(new NAudio.Midi.MetaEvent(NAudio.Midi.MetaEventType.EndTrack, 0, ((Phrase.EndBeat - startBeat) * ticksPerBeat).Round()));

                NAudio.Midi.MidiFile.Export(saveDialog.FileName, midiTracks);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        #endregion File menu event handlers



        #region Edit menu event handlers
        private void EditMenuUndo_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Undo();
        }


        private void EditMenuRedo_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Redo();
        }


        private void EditMenuNoteRetrograde_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Push(new RetrogradeNotesAction(this));
        }


        private void EditMenuChordRetrograde_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Push(new RetrogradeChordsAction(this));
        }


        private void EditMenuNoteInvert_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Push(new InvertNotesAction(this));
        }


        private void EditMenuChordInvert_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Push(new InvertChordsAction(this));
        }


        private void EditMenuAutoHarmonise_Click(object sender, RoutedEventArgs e)
        {
            PreviousActions.Push(new NewChordsAction(this));
        }
        #endregion Edit menu event handlers



        #region View menu event handlers
        private void ViewMenuZoomIn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatWidth = (int)Math.Round(PhraseNotesViewer.BeatWidth * 1.2);
            PhraseNotesViewer.TrackHeight = (int)Math.Round(PhraseNotesViewer.TrackHeight * 1.2);
        }

        private void ViewMenuZoomOut_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatWidth = (int)Math.Round(PhraseNotesViewer.BeatWidth / 1.2);
            PhraseNotesViewer.TrackHeight = (int)Math.Round(PhraseNotesViewer.TrackHeight / 1.2);
        }
        #endregion View menu event handlers



        private void UserControl_Drop(object sender, DragEventArgs e)
        {
            DataObject dataObject = e.Data as DataObject;
            if (dataObject.GetDataPresent(typeof(Phrase)))
            {
                Phrase phrase = dataObject.GetData(typeof(Phrase)) as Phrase;
                IndirectPhrase = phrase;
                e.Handled = true;
            }
            else if(dataObject.GetDataPresent(typeof(string)))
            {
                try
                {
                    Phrase phrase = Phrase.Load(dataObject.GetData(typeof(string)) as string);
                    IndirectPhrase = phrase;
                    e.Handled = true;
                }
                catch { }
            }
        }


        private void LowerBeatCountBtn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatDuration = Rational.Max(1, PhraseNotesViewer.BeatDuration - 1);
        }

        private void HigherBeatCountBtn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatDuration += 1;
        }

        private void LowerStartBeatBtn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.StartBeat -= 1;
        }

        private void HigherStartBeatBtn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.StartBeat = Rational.Min(0, PhraseNotesViewer.StartBeat + 1);
        }
        
    }
}
