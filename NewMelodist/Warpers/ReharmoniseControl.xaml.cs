﻿using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.Warpers
{
    /// <summary>
    /// Interaction logic for ReharmoniseControl.xaml
    /// </summary>
    public partial class ReharmoniseControl : UserControl, IPhraseWarper
    {

        public ReharmoniseControl(PhraseWarpActionFrame frame)
        {
            InitializeComponent();
            frame.WarpActionName = "Reharmoniser";
            frame.WarpActionControl = this;
        }


        public Phrase Run(Phrase input)
        {
            try
            {
                Phrase harmony = null;
                if (HarmonyText.DataContext is Phrase)
                    harmony = (Phrase)HarmonyText.DataContext;
                else
                    harmony = Phrase.Load(HarmonyText.Text);

                HarmonyWarper reharmoniser = new HarmonyWarper(harmony, (int)MaxCorrectionPicker.Value, (double)StepPenaltyPicker.Value);
                return reharmoniser.Run(input);
            }
            catch (Exception)
            {
                return input;
            }
        }

    }
}
