﻿using MathPlus;
using OxyPlot;
using OxyPlot.Wpf;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.Warpers
{
    /// <summary>
    /// Interaction logic for RerhythmiserControl.xaml
    /// </summary>
    public partial class RerhythmiserControl : UserControl, IPhraseWarper
    {

        private double _suggestedTargetDistance = 0;
        private double _suggestedDistanceAllowance = 0;


        private BackgroundWorker RhythmDistanceAnalysisWorker { get; set; }

        private RhythmDistanceContext _rhythmDistanceContext;



        public RerhythmiserControl(PhraseWarpActionFrame frame)
        {
            InitializeComponent();
            frame.WarpActionName = "Rhythm Modifier";
            frame.WarpActionControl = this;

            //Disable scrolling on distance graph
            foreach (OxyPlot.Wpf.Axis axis in RhythmDistanceAnalysisPlot.Axes)
            {
                axis.IsZoomEnabled = false;
                axis.IsPanEnabled = false;
            }

            RhythmDistanceAnalysisWorker = new BackgroundWorker();
            RhythmDistanceAnalysisWorker.DoWork += RhythmDistanceAnalysisWorker_DoWork;
            RhythmDistanceAnalysisWorker.RunWorkerCompleted += RhythmDistanceAnalysisWorker_RunWorkerCompleted;
        }


        public void SetInputPhraseChangeSubscriber(Action inputPhraseChangeSubscriber)
        {
            inputPhraseChangeSubscriber();
        }


        private RhythmWarper GetRhythmWarper()
        {
            try
            {
                Phrase modifier = null;
                if (ModifierText.DataContext is Phrase)
                    modifier = (Phrase)ModifierText.DataContext;
                else
                    modifier = Phrase.Load(ModifierText.Text);

                double targetDistance = (double)TargetDistance.Value;
                double distanceAllowance = (double)DistanceAllowance.Value;
                decimal maxReplacementLength = (decimal)MaxSwapLength.Value;
                return new RhythmWarper(modifier, Rational.FromDecimal(maxReplacementLength), targetDistance, distanceAllowance);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public Phrase Run(Phrase input)
        {
            try
            {
                RhythmWarper rhythmWarper = GetRhythmWarper();
                if (rhythmWarper == null)
                    return input;
                return rhythmWarper.Run(input);
            }
            catch (Exception)
            {
                return input;
            }
        }


        private bool _updateDistanceFieldsAfterAnalysis = false;
        private void CalculateDistancesBtn_Click(object sender, RoutedEventArgs e)
        {
            if (_suggestedTargetDistance == 0 && _suggestedDistanceAllowance == 0)
            {
                _updateDistanceFieldsAfterAnalysis = true;
                ModifierText_TextChanged(null, null);
            }
            else
            {
                TargetDistance.Value = (decimal)_suggestedTargetDistance;
                DistanceAllowance.Value = (decimal)_suggestedDistanceAllowance;
                if (RhythmDistanceAnalysisWorker.IsBusy)
                    _updateDistanceFieldsAfterAnalysis = true;
            }
        }


        private void ShowRhythmAnalysisBtn_Click(object sender, RoutedEventArgs e)
        {
            if (RhythmDistanceAnalysisPlot.Visibility == Visibility.Collapsed)
            {
                RhythmDistanceAnalysisPlot.Visibility = Visibility.Visible;
                ShowRhythmAnalysisBtnText.Text = "Hide Analysis";
                if (_rhythmDistanceContext != null)
                {
                    RhythmDistanceAnalysisPlot.DataContext = _rhythmDistanceContext;
                    Axis xAxis = RhythmDistanceAnalysisPlot.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Bottom);
                    xAxis.Maximum = (_rhythmDistanceContext.Points.Count == 0) ? 1 : _rhythmDistanceContext.Points.Max(x => x.X);

                    Axis yAxis = RhythmDistanceAnalysisPlot.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Left);
                    yAxis.Maximum = (_rhythmDistanceContext.Points.Count == 0) ? 1 : _rhythmDistanceContext.Points.Max(x => x.Y * 1.1);
                }
            }
            else
            {
                RhythmDistanceAnalysisPlot.Visibility = Visibility.Collapsed;
                ShowRhythmAnalysisBtnText.Text = "Show Analysis";
            }
        }


        public string InputPhraseFile { get; set; }

        public void InputPhraseChanged(string inputPhraseFile)
        {
            if (!System.IO.File.Exists(inputPhraseFile))
                return;

            Phrase inputPhrase = null;
            try { inputPhrase = Phrase.Load(inputPhraseFile); } catch { return; }
            InputPhraseFile = inputPhraseFile;

            RhythmWarper rhythmWarper = GetRhythmWarper();
            if (rhythmWarper == null)
                return;

            UpdateRhythmAnalysis(inputPhrase, rhythmWarper);
        }


        private void ModifierText_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (!System.IO.File.Exists(ModifierText.Text) || InputPhraseFile == null)
                    return;

                InputPhraseChanged(InputPhraseFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void UpdateRhythmAnalysis(Phrase inputPhrase, RhythmWarper rhythmWarper)
        {
            if (!RhythmDistanceAnalysisWorker.IsBusy)
                RhythmDistanceAnalysisWorker.RunWorkerAsync(new Tuple<RhythmWarper, Phrase>(rhythmWarper, inputPhrase));
        }

        private void RhythmDistanceAnalysisWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Tuple<RhythmWarper, Phrase> args = (Tuple<RhythmWarper, Phrase>)e.Argument;
            List<double> distances = args.Item1.GetDistanceAnalysis(args.Item2);
            e.Result = new RhythmDistanceContext(distances);
        }

        private void RhythmDistanceAnalysisWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                return;

            _rhythmDistanceContext = (RhythmDistanceContext)e.Result;
            if (RhythmDistanceAnalysisPlot.Visibility == Visibility.Visible)
                RhythmDistanceAnalysisPlot.DataContext = _rhythmDistanceContext;

            List<DataPoint> calcPoints = _rhythmDistanceContext.Points.Where(x => x.X > 0).ToList();
            if (calcPoints.Count > 0)
            {
                //Analyse data, find where to set target distance, use the rule that we should aim for the bottom non-zero 10%
                double targetArea = calcPoints.Sum(y => y.Y) * 0.1;

                int startIndex = 0;
                for (startIndex = 0; startIndex < calcPoints.Count; startIndex++)
                {
                    if (calcPoints[startIndex].X != 0)
                        break;
                }

                DataPoint start = calcPoints[startIndex];
                DataPoint end = calcPoints[startIndex];

                double area = 0;
                for (int i = startIndex; i < calcPoints.Count; i++)
                {
                    area += calcPoints[i].Y;
                    if (area >= targetArea)
                    {
                        end = calcPoints[i];
                        break;
                    }
                }

                _suggestedTargetDistance = Math.Round(((end.X + start.X) / 2), 2);
                _suggestedDistanceAllowance = Math.Max(0.01, Math.Round(((end.X - start.X) / 2), 2));
                if (_updateDistanceFieldsAfterAnalysis)
                {
                    TargetDistance.Value = (decimal)_suggestedTargetDistance;
                    DistanceAllowance.Value = (decimal)_suggestedDistanceAllowance;
                    _updateDistanceFieldsAfterAnalysis = false;
                }
            }
        }



        private class RhythmDistanceContext : INotifyPropertyChanged
        {
            public List<DataPoint> Points { get; private set; }

            public RhythmDistanceContext(List<double> distances)
            {
                Points = new List<DataPoint>();

                if (distances.Count == 0)
                    return;

                distances.Sort();
                double range = distances[distances.Count - 1] - distances[0];
                double percentile = range / 100;

                double pointEnd = distances[0] + percentile;
                int count = 0;
                for (int i = 0; i < distances.Count; i++)
                {
                    if (distances[i] <= pointEnd)
                        count++;
                    else
                    {
                        Points.Add(new DataPoint(pointEnd - (percentile / 2), count));
                        count = 0;
                        pointEnd += percentile;
                        i--;
                    }
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
        }

    }
}
