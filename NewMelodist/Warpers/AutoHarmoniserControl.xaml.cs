﻿using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.Warpers
{
    /// <summary>
    /// Interaction logic for AutoHarmoniserControl.xaml
    /// </summary>
    public partial class AutoHarmoniserControl : UserControl
    {

        public AutoHarmoniserControl(PhraseWarpActionFrame frame)
        {
            InitializeComponent();
            frame.WarpActionName = "Auto-harmoniser";
            frame.WarpActionControl = this;
        }


        public Phrase Run(Phrase input)
        {
            try
            {
                Phrase chordRhythm = null;
                if (ChordRhythmText.DataContext is Phrase)
                    chordRhythm = (Phrase)ChordRhythmText.DataContext;
                else if (!string.IsNullOrEmpty(ChordRhythmText.Text))
                    chordRhythm = Phrase.Load(ChordRhythmText.Text);

                ChordProgressioner chordProgressioner = new ChordProgressioner()
                {
                    MinScaleDistance = ScaleDistanceSlider.LowerValue,
                    MaxScaleDistance = ScaleDistanceSlider.HigherValue,
                    MinDissonance = DissonanceSlider.LowerValue,
                    MaxDissonance = DissonanceSlider.HigherValue,
                    MinChordMovement = (int)Math.Round(MovementSlider.LowerValue),
                    MaxChordMovement = (int)Math.Round(MovementSlider.HigherValue),
                    HarmonyRhythm = chordRhythm
                };

                return chordProgressioner.Run(input);
            }
            catch (Exception)
            {
                return input;
            }
        }

    }
}
