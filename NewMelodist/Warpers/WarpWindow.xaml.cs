﻿using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.Warpers
{
    /// <summary>
    /// Interaction logic for WarpWindow.xaml
    /// </summary>
    public partial class WarpWindow : UserControl
    {

        public WarpWindow()
        {
            InitializeComponent();
        }


        private void AddWarpAction_Click(object sender, RoutedEventArgs e)
        {
            if (AddWarpActionType.SelectedIndex < 0)
            {
                MessageBox.Show("No action selected to add");
                return;
            }

            PhraseWarpActionFrame warpFrame = new PhraseWarpActionFrame();

            string actionName = ((ComboBoxItem)AddWarpActionType.SelectedItem).Content.ToString();
            UserControl warpControl = null;
            switch (actionName)
            {
                case "Contour Modifier":
                    warpControl = new RecontouriseControl(warpFrame);
                    break;
                case "Reharmoniser":
                    warpControl = new ReharmoniseControl(warpFrame);
                    break;
                case "Rhythm Modifier":
                    RerhythmiserControl rerhythmiser = new RerhythmiserControl(warpFrame);
                    rerhythmiser.InputPhraseFile = InputPhraseText.Text;
                    rerhythmiser.SetInputPhraseChangeSubscriber(() => { InputPhraseText.TextChanged += (object sender1, TextChangedEventArgs e1) => { rerhythmiser.InputPhraseChanged(InputPhraseText.Text); }; });
                    warpControl = rerhythmiser;
                    break;
                case "Auto-harmoniser":
                    warpControl = new AutoHarmoniserControl(warpFrame);
                    break;
                default:
                    MessageBox.Show("Failed to create action");
                    return;
            }

            WarpActions.Children.Add(warpFrame);
            warpFrame.UpArrowClick += WarpFrame_UpArrowClick;
            warpFrame.DownArrowClick += WarpFrame_DownArrowClick;
            warpFrame.Close += WarpFrame_Close;

            AddWarpActionType.SelectedIndex = -1;
        }

        private void WarpFrame_Close(object sender, EventArgs e)
        {
            PhraseWarpActionFrame frame = (PhraseWarpActionFrame)sender;
            WarpActions.Children.Remove(frame);
        }

        private void WarpFrame_DownArrowClick(object sender, EventArgs e)
        {
            PhraseWarpActionFrame frame = (PhraseWarpActionFrame)sender;
            int index = WarpActions.Children.IndexOf(frame);
            if (index >= 0 && index < WarpActions.Children.Count - 1)
            {
                WarpActions.Children.RemoveAt(index);
                if (index + 1 == WarpActions.Children.Count)
                    WarpActions.Children.Add(frame);
                else
                    WarpActions.Children.Insert(index + 1, frame);
            }
        }

        private void WarpFrame_UpArrowClick(object sender, EventArgs e)
        {
            PhraseWarpActionFrame frame = (PhraseWarpActionFrame)sender;
            int index = WarpActions.Children.IndexOf(frame);
            if (index >= 1 && index <= WarpActions.Children.Count - 1)
            {
                WarpActions.Children.RemoveAt(index);
                WarpActions.Children.Insert(index - 1, frame);
            }
        }



        public event PhraseEventHandler WarpCompleted;

        private void RunBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Phrase phrase = null;
                if (InputPhraseText.DataContext is Phrase)
                    phrase = (Phrase)InputPhraseText.DataContext;
                else
                    phrase = Phrase.Load(InputPhraseText.Text);

                foreach (PhraseWarpActionFrame warpFrame in WarpActions.Children.OfType<PhraseWarpActionFrame>())
                {
                    if (warpFrame.Active)
                        phrase = warpFrame.WarpAction.Run(phrase);
                }

                if (WarpCompleted != null)
                    WarpCompleted.Invoke(this, new PhraseEventArgs(phrase));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred: " + ex.Message);
            }
        }

    }
}
