﻿using MathPlus;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.Warpers
{
    /// <summary>
    /// Interaction logic for RecontouriseControl.xaml
    /// </summary>
    public partial class RecontouriseControl : UserControl, IPhraseWarper
    {

        public RecontouriseControl(PhraseWarpActionFrame frame)
        {
            InitializeComponent();
            frame.WarpActionName = "Contour Modifier";
            frame.WarpActionControl = this;
        }


        public Phrase Run(Phrase input)
        {
            try
            {
                Phrase contourPhrase = null;
                if (ContourText.DataContext is Phrase)
                    contourPhrase = (Phrase)ContourText.DataContext;
                else
                    contourPhrase = Phrase.Load(ContourText.Text);

                ContourWarper recontouriser = new ContourWarper(
                    contourPhrase, 
                    Rational.FromDecimal((decimal)SampleSizePicker.Value),
                    (int)MaxCorrectionPicker.Value,
                    (double)StepPenaltyPicker.Value
                );
                return recontouriser.Run(input);
            }
            catch (Exception)
            {
                return input;
            }
        }


        private bool _sampleSizeChangeInProgress = false;
        private void SampleSizePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (_sampleSizeChangeInProgress)
                return;

            try
            {
                _sampleSizeChangeInProgress = true;
                decimal oldValue = (decimal)e.OldValue;
                decimal newValue = (decimal)e.NewValue;
                if(oldValue < newValue)
                {
                    if (oldValue < 1)
                        SampleSizePicker.Value = oldValue * 2;
                    else
                        SampleSizePicker.Value = oldValue + 1;
                }
                else
                {
                    if (oldValue <= 1)
                        SampleSizePicker.Value = Math.Max(0.25M, oldValue / 2);
                    else
                        SampleSizePicker.Value = oldValue - 1;
                }
            }
            catch
            {
            }
            finally
            {
                _sampleSizeChangeInProgress = false;
            }
        }

    }
}
