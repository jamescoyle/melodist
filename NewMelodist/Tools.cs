﻿using GeneralMidi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class Tools
    {

        public static MidiDevice GetSelectedMidiDevice()
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.DefaultGmDevice))
                return new MidiDevice(Properties.Settings.Default.DefaultGmDevice);
            return MidiDevice.Default;
        }

    }
}
