﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NewMelodist.Converters
{
    public class RationalToInverseIntConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Rational)
                return ((Rational)value).B;
            throw new ArgumentException();
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
                return new Rational(1, (int)value);
            throw new ArgumentException();
        }

    }
}
