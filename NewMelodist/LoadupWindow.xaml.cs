﻿using GeneralMidi;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for LoadupWindow.xaml
    /// </summary>
    public partial class LoadupWindow : Window
    {

        public LoadupWindow()
        {
            InitializeComponent();
            MidiDevice.Latency = Properties.Settings.Default.GmLatencySeconds;
        }


        private void NewProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            NewProjectForm.Height = new GridLength(56);
            NewProjectNameText.Focus();
        }



        private void LoadProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            NewProjectForm.Height = new GridLength(0);
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.InitialDirectory = Project.ProjectsFolder;
            openDialog.Filter = "Melodist Project Files (*.mld)|*.mld";
            if (openDialog.ShowDialog() == true)
            {
                Project.Load(openDialog.FileName);
                Close();
            }
        }



        private void NewProjectStartBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(NewProjectNameText.Text))
            {
                NewProjectNameText.Focus();
                return;
            }
            string projectFolder = System.IO.Path.Combine(Project.ProjectsFolder, NewProjectNameText.Text);
            string projectPath = System.IO.Path.Combine(projectFolder, "Project.mld");
            if (Directory.Exists(projectFolder))
            {
                MessageBox.Show("Project Folder already exists");
                NewProjectNameText.Focus();
                return;
            }
            Directory.CreateDirectory(projectFolder);
            Project.StartNew(projectPath, NewProjectNameText.Text);
            Project.Instance.Save();
            Close();
        }

    }
}
