﻿using CSharpExtras;
using MelodistControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace NewMelodist.SongEditors
{
    public partial class SongTrackHeadsViewer : MultiTrackViewer
    {

        public List<SongTrackHeadViewer> SongTrackHeadViewers { get; private set; }


        private Song _song;
        public Song Song
        {
            get { return _song; }
            set
            {
                if (_song != null)
                {
                    _song.Tracks.Added -= SongTracks_Added;
                    _song.Tracks.Removed -= SongTracks_Removed;
                    _song.Tracks.ForEach(x => RemoveSongTrackHeadViewer(x));
                }

                _song = value;

                _song.Tracks.Added += SongTracks_Added;
                _song.Tracks.Removed += SongTracks_Removed;
                _song.Tracks.ForEach(x => AddSongTrackHeadViewer(x));
            }
        }



        public SongTrackHeadsViewer()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;

            BackgroundCanvas.Background = new SolidColorBrush(Colors.DarkGray);

            SongTrackHeadViewers = new List<SongTrackHeadViewer>();

            Painter = new SongTrackHeadsViewerPainter(this);

            TrackHeight = 60;
            TrackCount = 4;

            TrackHeightChanged += OnUpdateRequired;
            SizeChanged += OnUpdateRequired;
        }


        #region Phrase Track Heads Management Logic
        private void SongTracks_Added(object sender, MemberListEventArgs<SongTrack> e)
        {
            AddSongTrackHeadViewer(e.Item);
        }


        private void SongTracks_Removed(object sender, MemberListEventArgs<SongTrack> e)
        {
            RemoveSongTrackHeadViewer(e.Item);
        }


        private void OnUpdateRequired(object sender, EventArgs e)
        {
            foreach (SongTrackHeadViewer trackHeadViewer in SongTrackHeadViewers)
                UpdateSongTrackHeadViewer(trackHeadViewer);
        }


        protected void AddSongTrackHeadViewer(SongTrack songTrack)
        {
            SongTrackHeadViewer phraseTrackHeadViewer = new SongTrackHeadViewer(songTrack, (track) => Song.Tracks.IndexOf(track));
            ForegroundCanvas.Children.Add(phraseTrackHeadViewer);
            SongTrackHeadViewers.Add(phraseTrackHeadViewer);
            UpdateSongTrackHeadViewer(phraseTrackHeadViewer);
        }


        protected void UpdateSongTrackHeadViewer(SongTrackHeadViewer songTrackHeadViewer)
        {
            Canvas.SetLeft(songTrackHeadViewer, 0);
            songTrackHeadViewer.Width = ActualWidth;
            Canvas.SetTop(songTrackHeadViewer, songTrackHeadViewer.Track * TrackHeight);
            songTrackHeadViewer.Height = TrackHeight;
        }


        protected void RemoveSongTrackHeadViewer(SongTrack songTrack)
        {
            SongTrackHeadViewer phraseTrackHeadViewer = SongTrackHeadViewers.FirstOrDefault(x => x.SongTrack == songTrack);
            if (phraseTrackHeadViewer != null)
            {
                SongTrackHeadViewers.Remove(phraseTrackHeadViewer);
                ForegroundCanvas.Children.Remove(phraseTrackHeadViewer);
            }
        }
        #endregion Phrase Track Heads Management Logic


        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewers()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            SongTrackHeadViewers.ForEach(x => output.Add(x));
            return output;
        }

        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewersUnderMouse()
        {
            return new List<FiniteTimeTrackObjectViewer>();
        }

        public override List<FiniteTimeTrackObjectViewer> GetSelectedFiniteTimeTrackObjectViewers()
        {
            return new List<FiniteTimeTrackObjectViewer>();
        }

    }



    public class SongTrackHeadsViewerPainter : MultiTrackViewerPainter
    {

        public SongTrackHeadsViewerPainter(SongTrackHeadsViewer songTrackHeadsViewer)
            : base(songTrackHeadsViewer)
        {
        }

        public override void Paint()
        {
        }

        public override void PaintBack()
        {
        }
    }
}
