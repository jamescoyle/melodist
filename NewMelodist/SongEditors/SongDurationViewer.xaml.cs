﻿using CSharpExtras;
using MathPlus;
using MelodistControls.DurationViewers;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist.SongEditors
{
    public partial class SongDurationViewer : DurationViewer
    {

        private Song _song;
        public Song Song
        {
            get { return _song; }
            set
            {
                if (_song != null)
                {
                    _song.TimeSignatures.Added -= OnTimeSignatureAdded;
                    _song.TimeSignatures.Removed -= OnTimeSignatureRemoved;
                    _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Remove(x));
                }

                _song = value;

                Rational maxEndBeat = Rational.Max(100, Song.LoopEndBeat.Ceiling());
                foreach (SongTrack track in Song.Tracks)
                    maxEndBeat = Rational.Max(maxEndBeat, (track.Phrases.Count > 0) ? track.Phrases.Max(x => x.EndBeat).Ceiling() : 0);

                StartBeat = 0;
                EndBeat = maxEndBeat;

                BeatRangeViewer.BeatRange.StartBeat = Song.LoopStartBeat;
                BeatRangeViewer.BeatRange.EndBeat = Song.LoopEndBeat;

                _song.TimeSignatures.Added += OnTimeSignatureAdded;
                _song.TimeSignatures.Removed += OnTimeSignatureRemoved;
                _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Add(x));
            }
        }


        public SongDurationViewer()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();

            BeatRangeViewer.BeatRange.StartBeatChanged += (sender, e) =>
            {
                if (Song != null)
                    Song.LoopStartBeat = BeatRangeViewer.BeatRange.StartBeat;
            };

            BeatRangeViewer.BeatRange.EndBeatChanged += (sender, e) =>
            {
                if (Song != null)
                    Song.LoopEndBeat = BeatRangeViewer.BeatRange.EndBeat;
            };
        }


        private void OnTimeSignatureAdded(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Add(e.Item);
        }

        private void OnTimeSignatureRemoved(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Remove(e.Item);
        }

    }
}
