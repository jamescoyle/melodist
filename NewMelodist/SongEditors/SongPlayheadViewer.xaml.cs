﻿using MathPlus;
using MelodistControls.PlayheadViewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist.SongEditors
{
    public partial class SongPlayheadViewer : PlayheadViewer
    {

        private Song _song;
        public Song Song
        {
            get
            {
                return _song;
            }
            set
            {
                _song = value;

                Rational maxEndBeat = Rational.Max(100, Song.LoopEndBeat.Ceiling());
                foreach (SongTrack track in Song.Tracks)
                    maxEndBeat = Rational.Max(maxEndBeat, (track.Phrases.Count > 0) ? track.Phrases.Max(x => x.EndBeat).Ceiling() : 0);

                StartBeat = 0;
                EndBeat = maxEndBeat;
            }
        }


        public SongPlayheadViewer()
            : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();
        }

    }
}
