﻿using GeneralMidi;
using HarmonyEngine;
using Jacobi.Vst.Core;
using MathPlus;
using MidiPlayback;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSTHosting;

namespace NewMelodist.SongEditors
{
    public class SongEditorWaveProvider : VSTWaveProvider
    {

        public SongEditor SongEditor { get; private set; }

        public SongTrack Track { get; private set; }


        public override Rational LoopStart { get { return SongEditor.Song.LoopStartBeat; } }

        public override Rational LoopEnd { get { return SongEditor.Song.LoopEndBeat; } }

        public override bool Loop { get { return true; } }

        public override Rational Tempo { get { return SongEditor.Song.GetTempoAt(Playhead); } }



        public SongEditorWaveProvider(SongEditor songEditor, SongTrack track, string pluginPath, int sampleRate, int channels)
            : base(pluginPath, sampleRate, channels)
        {
            SongEditor = songEditor;
            Track = track;
        }


        protected override global::Jacobi.Vst.Core.VstEvent[] GetVstEvents(int offset, int count)
        {
            Rational framesPerBeat = new Rational(60 * WaveFormat.SampleRate, 1) / Tempo;
            int samplesRequired = count;

            Rational startBeat = Playhead;
            Rational endBeat = startBeat + (samplesRequired / framesPerBeat);

            List<VstEvent> vstEvents = new List<VstEvent>();

            Rational beatOffset = 0;

            for (int i = 0; i < 2; i++)
            {
                foreach(PhrasePosition phrase in Track.Phrases)
                {
                    if (phrase.StartBeat > endBeat || phrase.EndBeat < startBeat)
                        continue;

                    foreach (Note note in phrase.GetNotes())
                    {
                        if (note.EndBeat > startBeat && note.EndBeat <= endBeat)
                            vstEvents.Add(NoteOffEvent(1, (byte)note.Pitch.Value, ((note.EndBeat - startBeat + beatOffset) * framesPerBeat).Round()));

                        if (note.StartBeat >= startBeat && note.StartBeat < endBeat)
                            vstEvents.Add(NoteOnEvent(1, (byte)note.Pitch.Value, 100, (note.Duration * framesPerBeat).Round(), ((note.StartBeat - startBeat + beatOffset) * framesPerBeat).Round()));
                    }
                }

                if (!Loop || endBeat < LoopEnd)
                    break;

                beatOffset = endBeat - LoopEnd;
                endBeat -= LoopEnd - LoopStart;
                startBeat = LoopStart;
            }

            PreviousPlayhead = Playhead;
            Playhead = endBeat;

            return vstEvents.OrderBy(x => x.DeltaFrames).ToArray();
        }

    }



    public class SongEditorGMPlayer : GMPlayer
    {

        public SongEditor SongEditor { get; private set; }


        public override bool Loop { get { return true; } }

        public override Rational LoopStart { get { return SongEditor.Song.LoopStartBeat; } }

        public override Rational LoopEnd { get { return SongEditor.Song.LoopEndBeat; } }

        public override Rational Tempo { get { return SongEditor.Song.GetTempoAt(Playhead); } }

        private List<Tuple<SongTrack, int>> _trackChannels;


        public SongEditorGMPlayer(SongEditor songEditor, MidiDevice outputDevice = null)
            : base(outputDevice)
        {
            SongEditor = songEditor;
            _trackChannels = new List<Tuple<SongTrack, int>>();
        }


        public void UpdateInstruments()
        {
            _trackChannels.Clear();
            foreach(SongTrackHeadViewer trackHead in SongEditor.TrackHeadListViewer.SongTrackHeadViewers)
            {
                if(!trackHead.InstrumentPicker.VstSelected && trackHead.InstrumentPicker.SelectedInstrument != null)
                {
                    int gmChannel = _trackChannels.Count + 1;
                    OutputDevice.SendProgramChange(gmChannel, trackHead.InstrumentPicker.SelectedInstrument.Index);
                    _trackChannels.Add(new Tuple<SongTrack, int>(trackHead.SongTrack, gmChannel));
                }
            }
        }


        protected override void Read(Rational startBeat, Rational endBeat)
        {
            if (Loop && startBeat > endBeat && startBeat < LoopEnd && endBeat > LoopStart)
            {
                Read(startBeat, LoopEnd);
                Read(LoopStart, endBeat);
                return;
            }

            foreach(Tuple<SongTrack, int> track in _trackChannels)
            {
                foreach(PhrasePosition phrase in track.Item1.Phrases)
                {
                    if (phrase.StartBeat > endBeat || phrase.EndBeat < startBeat)
                        continue;

                    List<Note> notes = phrase.GetNotes();
                    foreach(Note note in notes)
                    {
                        if (note.EndBeat > startBeat && note.EndBeat <= endBeat)
                            OutputDevice.SendNoteOff(track.Item2, note.Pitch, 127);
                    }
                    foreach(Note note in notes)
                    {
                        if (note.StartBeat >= startBeat && note.StartBeat < endBeat)
                            OutputDevice.SendNoteOn(track.Item2, note.Pitch, 127);
                    }
                }
            }
        }

    }
}
