﻿using CSharpExtras;
using MathPlus;
using MelodistControls.PhrasePositionsViewers;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.SongEditors.PhrasePositionsActions
{
    public class PasteAction : SongPhrasePositionsViewerAction, IUndoable
    {

        private List<PhrasePosition> _phrasePositions;

        public bool IsUndone { get; private set; }


        public PasteAction(SongPhrasePositionsViewer songPhrasePositionsViewer)
            : base(songPhrasePositionsViewer)
        {
            IsUndone = false;

            List<PhrasePosition> phrasePositions = MelodistControls.MusicViewerActions.CopyAction.Clipboard.OfType<PhrasePosition>().ToList();
            if(phrasePositions.Count == 0)
            {
                Finished = true;
                return;
            }

            Rational pasteStartBeat = SongPhrasePositionsViewer.GetMouseBeat();
            Rational notesStartBeat = phrasePositions.Min(x => x.StartBeat);
            Rational pasteNotesDuration = phrasePositions.Max(x => x.EndBeat) - notesStartBeat;
            pasteStartBeat = Rational.Max(pasteStartBeat, 0);
            pasteStartBeat = Rational.Min(pasteStartBeat, SongPhrasePositionsViewer.EndBeat - pasteNotesDuration);
            Rational pasteBeatOffset = pasteStartBeat - notesStartBeat;

            _phrasePositions = new List<PhrasePosition>();
            foreach (PhrasePosition phrasePosition in phrasePositions)
                _phrasePositions.Add(new PhrasePosition(phrasePosition.Phrase, phrasePosition.StartBeat + pasteBeatOffset, phrasePosition.LoopCount));

            Redo();
            Finished = true;
        }


        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }



        public void Undo()
        {
            if (_phrasePositions == null)
                return;

            foreach (PhrasePosition phrasePosition in _phrasePositions)
                SongPhrasePositionsViewer.Song.GetPhrasePositionTrack(phrasePosition).RemovePhrase(phrasePosition);
            IsUndone = true;
        }


        public void Redo()
        {
            SongPhrasePositionsViewer.PhrasePositionViewers.ForEach(x => x.Selected = false);
            foreach (PhrasePosition phrasePosition in _phrasePositions)
            {
                SongPhrasePositionsViewer.Song.GetPhrasePositionTrack(phrasePosition).AddPhrase(phrasePosition);
                PhrasePositionViewer phrasePositionViewer = SongPhrasePositionsViewer.PhrasePositionViewers.FirstOrDefault(x => x.PhrasePosition == phrasePosition);
                if (phrasePositionViewer != null)
                    phrasePositionViewer.Selected = true;
            }
            IsUndone = false;
        }

    }
}
