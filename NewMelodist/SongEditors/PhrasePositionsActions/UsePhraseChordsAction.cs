﻿using CSharpExtras;
using HarmonyEngine;
using MelodistControls.PhrasePositionsViewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.SongEditors.PhrasePositionsActions
{
    public class UsePhraseChordsAction : SongPhrasePositionsViewerAction, IUndoable
    {

        private List<Chord> _addedChords = new List<Chord>();

        public bool IsUndone { get; private set; }


        public UsePhraseChordsAction(SongPhrasePositionsViewer songPhrasePositionsViewer) 
            : base(songPhrasePositionsViewer)
        {
            List<PhrasePositionViewer> selectedPhrases = SongPhrasePositionsViewer.GetSelectedPhrasePositionViewers();
            if (selectedPhrases.Count != 1)
                throw new Exception("Error, expecting one phrase to use");

            List<Chord> chords = selectedPhrases[0].PhrasePosition.GetChords();
            bool chordOverlap = false;
            foreach(Chord chord in chords)
            {
                if (songPhrasePositionsViewer.Song.Chords.Any(x => x.StartBeat < chord.EndBeat && x.EndBeat > chord.StartBeat))
                    chordOverlap = true;
                else
                {
                    SongPhrasePositionsViewer.Song.AddChord(chord);
                    _addedChords.Add(chord);
                }
            }
        }


        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }


        public void Undo()
        {
            foreach (Chord chord in _addedChords)
                SongPhrasePositionsViewer.Song.RemoveChord(chord);
            IsUndone = true;
        }

        public void Redo()
        {
            foreach (Chord chord in _addedChords)
                SongPhrasePositionsViewer.Song.AddChord(chord);
            IsUndone = false;
        }

    }
}
