﻿using CSharpExtras;
using MelodistControls.PhrasePositionsViewers;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewMelodist.SongEditors.PhrasePositionsActions
{
    public class DeletePhraseAction : SongPhrasePositionsViewerAction, IUndoable
    {

        private List<Tuple<PhrasePosition, SongTrack>> _phrases = new List<Tuple<PhrasePosition, SongTrack>>();

        public bool IsUndone { get; private set; }



        public DeletePhraseAction(SongPhrasePositionsViewer songPhrasePositionsViewer)
            : base(songPhrasePositionsViewer)
        {
            List<PhrasePositionViewer> selectedItems = SongPhrasePositionsViewer.GetSelectedPhrasePositionViewers();
            _phrases = new List<Tuple<PhrasePosition, SongTrack>>();
            selectedItems.ForEach(x => _phrases.Add(new Tuple<PhrasePosition, SongTrack>(x.PhrasePosition, SongPhrasePositionsViewer.Song.Tracks.First(y => y.Phrases.Contains(x.PhrasePosition)))));
            Redo();
            Finished = true;
        }


        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }


        public void Undo()
        {
            _phrases.ForEach(x => x.Item2.AddPhrase(x.Item1));
            IsUndone = true;
        }

        public void Redo()
        {
            _phrases.ForEach(x => x.Item2.RemovePhrase(x.Item1));
            IsUndone = false;
        }
    }
}
