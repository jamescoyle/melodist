﻿using MidiPlayback;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace NewMelodist.SongEditors
{
    /// <summary>
    /// Interaction logic for SongEditor.xaml
    /// </summary>
    public partial class SongEditor : UserControl
    {

        private Song _song;
        public Song Song
        {
            get
            {
                return _song;
            }
            set
            {
                if(_song != null)
                {
                    _song.TempoChanged -= OnTempoChanged;
                    _song.TimeSignatureChanged -= OnTimeSignatureChanged;
                }

                _song = value;
                SongViewer.Song = _song;
                SongDurationViewer.Song = _song;
                TrackHeadListViewer.Song = _song;
                SongChordViewer.Song = _song;
                SongPlayhead.Song = _song;
                DataContext = _song;
                _song.TempoChanged += OnTempoChanged;
                OnTempoChanged(null, null);
                _song.TimeSignatureChanged += OnTimeSignatureChanged;
                OnTimeSignatureChanged(null, null);
            }
        }


        public PlayerManager Player { get; private set; }

        private DispatcherTimer PlaybackUIUpdater { get; set; }



        public SongEditor()
        {
            InitializeComponent();
            if (Project.Instance != null)
                Song = Project.Instance.Song;

            PlaybackUIUpdater = new DispatcherTimer();
            PlaybackUIUpdater.Interval = TimeSpan.FromMilliseconds(30);
            PlaybackUIUpdater.Tick += (sender, args) =>
            {
                SongPlayhead.UpdatePlayhead(Player.Playhead);
            };

            TempoPicker.ValueChanged += (sender, args) =>
            {
                if (Song != null && TempoPicker.Value != null)
                    Song.SetTempoAt(0, (int)TempoPicker.Value);
            };
            
            BeatsPerBarPicker.ValueChanged += (sender, args) =>
            {
                if (Song != null && BeatsPerBarPicker.Value != null)
                    Song.SetTimeSignatureAt(0, (int)BeatsPerBarPicker.Value, 4);
            };
        }


        private void SongScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (DurationScrollViewer.HorizontalOffset != SongScrollViewer.HorizontalOffset)
                DurationScrollViewer.ScrollToHorizontalOffset(SongScrollViewer.HorizontalOffset);
            
            if (ChordScrollViewer.HorizontalOffset != SongScrollViewer.HorizontalOffset)
                ChordScrollViewer.ScrollToHorizontalOffset(SongScrollViewer.HorizontalOffset);

            if (TrackHeadScrollViewer.VerticalOffset != SongScrollViewer.VerticalOffset)
                TrackHeadScrollViewer.ScrollToVerticalOffset(SongScrollViewer.VerticalOffset);
        }


        private void SongChordViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }


        private void OnTempoChanged(object sender, TempoChangeEventArgs e)
        {
            if (Song.TempoChanges.Count > 0)
                TempoPicker.Value = Song.TempoChanges[0].Tempo;
        }


        private void OnTimeSignatureChanged(object sender, TimeSignatureEventArgs e)
        {
            if (Song.TimeSignatures.Count > 0)
                BeatsPerBarPicker.Value = Song.TimeSignatures[0].QuarterBeatsPerBar.Round();
        }


        #region Edit menu event handlers
        private void EditMenuUndo_Click(object sender, EventArgs e)
        {
            SongViewer.PreviousActions.Undo();
        }


        private void EditMenuRedo_Click(object sender, EventArgs e)
        {
            SongViewer.PreviousActions.Redo();
        }
        #endregion Edit menu event handlers



        #region View menu event handlers
        private void ViewMenuZoomIn_Click(object sender, RoutedEventArgs e)
        {
            SongViewer.BeatWidth = (int)(Math.Round(SongViewer.BeatWidth * 1.2));
        }

        private void ViewMenuZoomOut_Click(object sender, RoutedEventArgs e)
        {
            SongViewer.BeatWidth = (int)(Math.Round(SongViewer.BeatWidth / 1.2));
        }
        #endregion View menu event handlers



        private void PlayBtn_Click(object sender, EventArgs e)
        {
            Play();
        }

        public void Play()
        {
            if (Player != null)
                return;

            try
            {
                if (TrackHeadListViewer.SongTrackHeadViewers.Count(x => x.InstrumentPicker.SelectedInstrument != null) == 0)
                {
                    MessageBox.Show("No instruments selected");
                    return;
                }

                LoadPlayer();
                Player.Play();
                PlaybackUIUpdater.Start();
            }
            catch
            {
                MessageBox.Show("Failed to start playback");
            }
        }


        private void StopBtn_Click(object sender, EventArgs e)
        {
            Stop();
        }

        public void Stop()
        {
            if (Player == null)
                return;

            PlaybackUIUpdater.Stop();
            SongPlayhead.RemovePlayhead();

            Player.Stop();
            Player = null;
        }


        private void LoadPlayer()
        {
            if (Player != null)
            {
                if (Player.GMPlayer != null)
                    ((SongEditorGMPlayer)Player.GMPlayer).UpdateInstruments();
                return;
            }

            VstPlayer vstPlayer = null;
            List<SongEditorWaveProvider> waveProviders = new List<SongEditorWaveProvider>();
            foreach(SongTrackHeadViewer trackHeadViewer in TrackHeadListViewer.SongTrackHeadViewers)
            {
                if (trackHeadViewer.InstrumentPicker.VstSelected)
                {
                    waveProviders.Add(new SongEditorWaveProvider(
                        this, trackHeadViewer.SongTrack, trackHeadViewer.InstrumentPicker.SelectedVSTInstrument.FilePath, 44100, 2));
                }
            }
            if (waveProviders.Count > 0)
            {
                vstPlayer = new VstPlayer(waveProviders.ToArray());
                vstPlayer.Playhead = Song.LoopStartBeat;
            }
            
            SongEditorGMPlayer gmPlayer = null;
            if(TrackHeadListViewer.SongTrackHeadViewers.Count(x => x.InstrumentPicker.VstSelected == false && x.InstrumentPicker.SelectedInstrument != null) > 0)
            {
                gmPlayer = new SongEditorGMPlayer(this, Tools.GetSelectedMidiDevice());
                gmPlayer.UpdateInstruments();
                gmPlayer.Playhead = Song.LoopStartBeat;
            }

            Player = new PlayerManager(vstPlayer, gmPlayer);
        }


        private void AddTrackBtn_Click(object sender, RoutedEventArgs e)
        {
            Song.AddTrack(new SongTrack("Track " + (Song.Tracks.Count + 1)));
        }

    }
}
