﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using MelodistControls;
using MelodistControls.MultiTrackViewerActions;
using MelodistControls.PhrasePositionsViewers;
using NewMelodist.SongEditors.PhrasePositionsActions;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace NewMelodist.SongEditors
{
    public partial class SongPhrasePositionsViewer : PhrasePositionsViewer
    {

        private Song _song;
        public Song Song
        {
            get
            {
                return _song;
            }
            set
            {
                if (_song != value)
                {
                    if (_song != null)
                    {
                        foreach (SongTrack track in _song.Tracks)
                        {
                            track.Phrases.Added -= SongPhrasePositions_Added;
                            track.Phrases.Removed -= SongPhrasePositions_Removed;
                            track.Phrases.ForEach(x => RemovePhrasePositionViewer(x));
                        }
                        BindingOperations.ClearAllBindings(BackgroundCanvas);

                        _song.TimeSignatures.Added -= SongTimeSignatures_Added;
                        _song.TimeSignatures.Removed -= SongTimeSignatures_Removed;
                        _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Remove(x));

                        _song.Tracks.Added -= SongTracks_Added;
                        _song.Tracks.Removed -= SongTracks_Removed;
                    }

                    _song = value;
                    
                    Rational maxEndBeat = Rational.Max(100, Song.LoopEndBeat.Ceiling());
                    foreach (SongTrack track in Song.Tracks)
                        maxEndBeat = Rational.Max(maxEndBeat, (track.Phrases.Count > 0) ? track.Phrases.Max(x => x.EndBeat).Ceiling() : 0);

                    StartBeat = 0;
                    EndBeat = maxEndBeat;
                    TrackCount = _song.Tracks.Count;

                    foreach (SongTrack track in _song.Tracks)
                    {
                        track.Phrases.Added += SongPhrasePositions_Added;
                        track.Phrases.Removed += SongPhrasePositions_Removed;
                        track.Phrases.ForEach(x => AddSongPhrasePositionViewer(x));
                    }

                    _song.TimeSignatures.Added += SongTimeSignatures_Added;
                    _song.TimeSignatures.Removed += SongTimeSignatures_Removed;
                    _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Add(x));

                    _song.Tracks.Added += SongTracks_Added;
                    _song.Tracks.Removed += SongTracks_Removed;
                }
            }
        }


        private bool? _dropPhrases = null;
        public bool DropPhrases
        {
            get { return (_dropPhrases == null) ? false : (bool)_dropPhrases; }
            set
            {
                if(_dropPhrases == null || _dropPhrases != value)
                {
                    AllowDrop = value;
                    if (value)
                        Drop += SongPhrasePositionsViewer_Drop;
                    else if(_dropPhrases != null)
                        Drop -= SongPhrasePositionsViewer_Drop;
                    _dropPhrases = value;
                }
            }
        }
        

        public SongPhrasePositionsViewer()
            : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            DropPhrases = false;
            Init();
        }


        private void SongTimeSignatures_Added(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Add(e.Item);
        }

        private void SongTimeSignatures_Removed(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Remove(e.Item);
        }


        private void SongPhrasePositions_Added(object sender, MemberListEventArgs<PhrasePosition> args)
        {
            AddSongPhrasePositionViewer(args.Item);
        }

        protected void AddSongPhrasePositionViewer(PhrasePosition phrasePosition)
        {
            AddPhrasePositionViewer(phrasePosition, (item) => Song.Tracks.IndexOf(Song.Tracks.FirstOrDefault(x => x.Phrases.Contains(item))));
        }

        private void SongPhrasePositions_Removed(object sender, MemberListEventArgs<PhrasePosition> args)
        {
            RemovePhrasePositionViewer(args.Item);
        }


        private void SongTracks_Added(object sender, MemberListEventArgs<SongTrack> e)
        {
            TrackCount = Song.Tracks.Count;
        }
        
        private void SongTracks_Removed(object sender, MemberListEventArgs<SongTrack> e)
        {
            TrackCount = Song.Tracks.Count;
        }


        public override MultiTrackViewerAction NewMoveAction()
        {
            return new MoveAction<PhrasePosition>(
                this,
                (phrasePosition) => Song.Tracks.FindIndex(x => x.Phrases.Contains(phrasePosition)),
                (phrasePosition, track) =>
                {
                    if (track < 0 || track >= Song.Tracks.Count)
                        return;
                    SongTrack currentTrack = Song.Tracks.FirstOrDefault(x => x.Phrases.Contains(phrasePosition));
                    if (Song.Tracks[track] == currentTrack)
                        return;
                    if (currentTrack != null)
                        currentTrack.RemovePhrase(phrasePosition);
                    Song.Tracks[track].AddPhrase(phrasePosition);
                    PhrasePositionViewer phrasePositionViewer = PhrasePositionViewers.FirstOrDefault(x => x.PhrasePosition == phrasePosition);
                    if(phrasePositionViewer != null)
                        phrasePositionViewer.Selected = true;
                }
            );
        }


        public override MusicViewerAction NewDeleteAction()
        {
            return new DeletePhraseAction(this);
        }

        public override MusicViewerAction NewPasteAction()
        {
            return new PasteAction(this);
        }


        protected override void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(GetSelectedPhrasePositionViewers().Count <= 1 && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && GetPhrasePositionViewersUnderMouse().Count > 0)
            {
                DragDrop.DoDragDrop(this, GetPhrasePositionViewersUnderMouse()[0].PhrasePosition.Phrase, DragDropEffects.Copy);
                return;
            }
            base.OnMouseDown(sender, e);
        }


        private void SongPhrasePositionsViewer_Drop(object sender, DragEventArgs e)
        {
            DataObject dataObject = e.Data as DataObject;
            Phrase phrase = null;
            try
            {
                if (dataObject.GetDataPresent(typeof(Phrase)))
                    phrase = dataObject.GetData(typeof(Phrase)) as Phrase;

                else if (dataObject.GetDataPresent(typeof(string)))
                    phrase = Phrase.Load(dataObject.GetData(typeof(string)) as string);

                else
                    return;
                
                Point mousePosition = e.GetPosition(this);
                SongTrack track = Song.Tracks[GetDragMouseTrack(mousePosition)];
                Rational beat = GetDragMouseBeat(mousePosition);
                track.AddPhrase(new PhrasePosition(phrase, beat));
                e.Handled = true;
            }
            catch
            {
            }
        }


        private Rational GetDragMouseBeat(Point p)
        {
            return Rational.FromDecimal((decimal)(Math.Round(p.X) / BeatWidth));
        }

        private int GetDragMouseTrack(Point p)
        {
            return (int)Math.Floor(p.Y / TrackHeight);
        }



        #region Context menu event handlers
        private void ForegroundCanvas_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            List<PhrasePositionViewer> phrases = GetPhrasePositionViewersUnderMouse();
            if (phrases.Count == 0)
            {
                e.Handled = true;
                return;
            }
            phrases[0].Selected = true;
            for (int i = 1; i < phrases.Count; i++)
                phrases[i].Selected = false;
        }


        private void PhraseContextMenu_EditClick(object sender, RoutedEventArgs e)
        {
            List<PhrasePositionViewer> phrases = GetSelectedPhrasePositionViewers();
            if (phrases.Count != 1)
            {
                MessageBox.Show("Error, expected a phrase to edit");
                return;
            }

            DockWindow.Instance.PhraseEditorPane.Show();
            DockWindow.Instance.PhraseEditor.Phrase = phrases[0].PhrasePosition.Phrase;
        }


        private void PhraseContextMenu_UseChordsClick(object sender, RoutedEventArgs e)
        {
            try
            {
                UsePhraseChordsAction action = new UsePhraseChordsAction(this);
                PreviousActions.Push(action);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion Context menu event handlers

    }



    public abstract class SongPhrasePositionsViewerAction : MultiTrackViewerAction
    {

        public SongPhrasePositionsViewer SongPhrasePositionsViewer { get; private set; }


        public SongPhrasePositionsViewerAction(SongPhrasePositionsViewer songPhrasePositionsViewer)
            : base(songPhrasePositionsViewer)
        {
            SongPhrasePositionsViewer = songPhrasePositionsViewer;
        }

    }
}
