﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using MelodistControls;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.SongEditors
{
    /// <summary>
    /// Interaction logic for SongChordsViewer.xaml
    /// </summary>
    public partial class SongChordsViewer : MelodistControls.ChordsViewers.ChordsViewer
    {

        private Song _song;
        public Song Song
        {
            get
            {
                return _song;
            }
            set
            {
                if(_song != value)
                {
                    if(_song != null)
                    {
                        _song.Chords.Added -= SongChords_Added;
                        _song.Chords.Removed -= SongChords_Removed;
                        _song.Chords.ForEach(x => RemoveChordViewer(x));

                        BindingOperations.ClearAllBindings(BackgroundCanvas);

                        _song.TimeSignatures.Added -= SongTimeSignatures_Added;
                        _song.TimeSignatures.Removed -= SongTimeSignatures_Removed;
                        _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Remove(x));
                    }

                    _song = value;

                    Rational maxEndBeat = Rational.Max(100, Song.LoopEndBeat.Ceiling());
                    foreach (SongTrack track in Song.Tracks)
                        maxEndBeat = Rational.Max(maxEndBeat, (track.Phrases.Count > 0) ? track.Phrases.Max(x => x.EndBeat).Ceiling() : 0);

                    StartBeat = 0;
                    EndBeat = maxEndBeat;

                    _song.Chords.Added += SongChords_Added;
                    _song.Chords.Removed += SongChords_Removed;
                    _song.Chords.ForEach(x => AddChordViewer(x));

                    _song.TimeSignatures.Added += SongTimeSignatures_Added;
                    _song.TimeSignatures.Removed += SongTimeSignatures_Removed;
                    _song.TimeSignatures.ForEach(x => TimeSignaturesManager.Add(x));
                }
            }
        }


        
        public SongChordsViewer()
            : base()
        {
            InitializeComponent();
            Painter = new SongChordsViewerPainter(this);
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();
        }


        private void SongChords_Added(object sender, MemberListEventArgs<Chord> args)
        {
            AddChordViewer(args.Item);
        }

        private void SongChords_Removed(object sender, MemberListEventArgs<Chord> args)
        {
            RemoveChordViewer(args.Item);
        }


        private void SongTimeSignatures_Added(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Add(e.Item);
        }

        private void SongTimeSignatures_Removed(object sender, MemberListEventArgs<TimeSignature> e)
        {
            TimeSignaturesManager.Remove(e.Item);
        }

    }



    public abstract class SongChordsViewerAction : MusicViewerAction
    {

        public SongChordsViewer SongChordsViewer { get; private set; }


        public SongChordsViewerAction(SongChordsViewer songChordsViewer)
            : base(songChordsViewer)
        {
            SongChordsViewer = songChordsViewer;
        }

    }



    public class SongChordsViewerPainter : MusicViewerPainter
    {

        public SongChordsViewerPainter(SongChordsViewer songChordsViewer)
            : base(songChordsViewer)
        {
            Style beatLineStyle = new Style(typeof(Line));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.SteelBlue)));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(1, beatLineStyle);

            BarLineStyle = new Style(typeof(Line));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.LightSteelBlue)));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
        }


        public override void PaintBack()
        {
        }

    }
}
