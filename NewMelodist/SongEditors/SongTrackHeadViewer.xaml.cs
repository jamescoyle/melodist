﻿using MelodistControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.SongEditors
{
    /// <summary>
    /// Interaction logic for SongTrackHeadViewer.xaml
    /// </summary>
    public partial class SongTrackHeadViewer : FiniteTimeTrackObjectViewer
    {
        
        public SongTrack SongTrack { get; private set; }


        public Func<SongTrack, int> TrackGetter { get; private set; }

        public override int Track { get { return TrackGetter(SongTrack); } }


        public SongTrackHeadViewer(SongTrack songTrack, Func<SongTrack, int> trackGetter)
        {
            InitializeComponent();
            DataContext = songTrack;
            SongTrack = songTrack;
            TrackGetter = trackGetter;
            InstrumentPicker.LoadVSTs(MelodistDB.Instance);
        }

    }
}
