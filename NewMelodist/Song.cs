﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class Song : INotifyPropertyChanged
    {

        private object _lock = new object();


        private MemberListManager<SongTrack> TracksMngr;
        public MemberList<SongTrack> Tracks { get { return TracksMngr.List; } }


        private MemberListManager<Chord> ChordsMngr;
        public MemberList<Chord> Chords { get { return ChordsMngr.List; } }


        private string _name;
        public event EventHandler NameChanged;
        public string Name
        {
            get { return _name; }
            set
            {
                if(_name != value)
                {
                    lock (_lock) { _name = value; }
                    NameChanged?.Invoke(this, null);
                    OnPropertyChanged("Name");
                }
            }
        }


        private MemberListManager<TimeSignature> TimeSignaturesMngr;
        public MemberList<TimeSignature> TimeSignatures { get { return TimeSignaturesMngr.List; } }


        private MemberListManager<TempoChange> TempoChangesMngr;
        public MemberList<TempoChange> TempoChanges { get { return TempoChangesMngr.List; } }


        private Rational _loopStartBeat;
        public event EventHandler LoopStartBeatChanged;
        public Rational LoopStartBeat
        {
            get { lock (_lock) { return _loopStartBeat; } }
            set
            {
                if(_loopStartBeat != value)
                {
                    lock (_lock) { _loopStartBeat = value; }
                    LoopStartBeatChanged?.Invoke(this, null);
                    OnPropertyChanged("LoopStartBeat");
                }
            }
        }


        private Rational _loopDuration;
        public event EventHandler LoopDurationChanged;
        public Rational LoopDuration
        {
            get
            {
                lock (_lock) { return _loopDuration; }
            }
            set
            {
                if (value <= 0)
                    throw HarmonyEngineException.NegativeDuration();
                if (_loopDuration != value)
                {
                    lock (_lock) { _loopDuration = value; }
                    LoopDurationChanged?.Invoke(this, null);
                    OnPropertyChanged("LoopDuration");
                }
            }
        }


        public Rational LoopEndBeat
        {
            get
            {
                return LoopStartBeat + LoopDuration;
            }
            set
            {
                LoopDuration = value - LoopStartBeat;
            }
        }



        public Song(string name = null, int tempo = 120, int beatsPerBar = 4)
        {
            Name = name;
            TracksMngr = new MemberListManager<SongTrack>();
            for (int i = 1; i <= 4; i++)
                TracksMngr.Add(new SongTrack("Track " + i));
            ChordsMngr = new MemberListManager<Chord>();
            TimeSignaturesMngr = new MemberListManager<TimeSignature>();
            TempoChangesMngr = new MemberListManager<TempoChange>();
            TimeSignaturesMngr.Add(new TimeSignature(0, beatsPerBar, 4));
            TempoChangesMngr.Add(new TempoChange(0, tempo));
            LoopStartBeat = 0;
            LoopDuration = 16;
        }


        public Song(SongParse song)
        {
            Name = song.Name;
            LoopStartBeat = song.LoopStartBeat.Load();
            LoopDuration = song.LoopDuration.Load();

            TracksMngr = new MemberListManager<SongTrack>();
            song.Tracks.ForEach(x => TracksMngr.Add(x.Load()));

            ChordsMngr = new MemberListManager<Chord>();
            song.Chords.ForEach(x => ChordsMngr.Add(x.Load()));

            TimeSignaturesMngr = new MemberListManager<TimeSignature>();
            song.TimeSignatures.ForEach(x => TimeSignaturesMngr.Add(x.Load()));

            TempoChangesMngr = new MemberListManager<TempoChange>();
            song.TempoChanges.ForEach(x => TempoChangesMngr.Add(x.Load()));
        }



        public void AddTrack(SongTrack track, int index = -1)
        {
            if (index < 0)
                TracksMngr.Add(track);
            else
                TracksMngr.Insert(index, track);
        }

        public void RemoveTrack(SongTrack track)
        {
            TracksMngr.Remove(track);
        }


        public void AddChord(Chord chord)
        {
            ChordsMngr.Add(chord);
        }

        public void RemoveChord(Chord chord)
        {
            ChordsMngr.Remove(chord);
        }


        public event TempoChangeEventHandler TempoChanged;
        public void SetTempoAt(Rational beat, int tempo)
        {
            for(int i = 0; i < TempoChanges.Count; i++)
            {
                if(TempoChanges[i].Beat == beat && TempoChanges[i].Tempo != tempo)
                {
                    TempoChangesMngr.RemoveAt(i);
                    TempoChange tempoChange = new TempoChange(beat, tempo);
                    TempoChangesMngr.Insert(i, tempoChange);
                    TempoChanged?.Invoke(this, new TempoChangeEventArgs(tempoChange));
                    return;
                }
                else if(TempoChanges[i].Beat < beat)
                {
                    TempoChange tempoChange = new TempoChange(beat, tempo);
                    TempoChangesMngr.Insert(i + 1, tempoChange);
                    TempoChanged?.Invoke(this, new TempoChangeEventArgs(tempoChange));
                    return;
                }
            }
        }


        public int GetTempoAt(Rational beat)
        {
            foreach(TempoChange tempoChange in TempoChanges)
            {
                if (tempoChange.Beat <= beat)
                    return tempoChange.Tempo;
            }
            return 120;
        }


        public event TimeSignatureEventHandler TimeSignatureChanged;
        public void SetTimeSignatureAt(Rational beat, int numerator, int denominator)
        {
            for(int i = 0; i < TimeSignatures.Count; i++)
            {
                if(TimeSignatures[i].Beat == beat && (TimeSignatures[i].Numerator != numerator || TimeSignatures[i].Denominator != denominator))
                {
                    TimeSignaturesMngr.RemoveAt(i);
                    TimeSignature timeSig = new TimeSignature(beat, numerator, denominator);
                    TimeSignaturesMngr.Insert(i, timeSig);
                    TimeSignatureChanged?.Invoke(this, new TimeSignatureEventArgs(timeSig));
                    return;
                }
                else if(TimeSignatures[i].Beat < beat)
                {
                    TimeSignature timeSig = new TimeSignature(beat, numerator, denominator);
                    TimeSignaturesMngr.Insert(i + 1, timeSig);
                    TimeSignatureChanged?.Invoke(this, new TimeSignatureEventArgs(timeSig));
                    return;
                }
            }
        }


        public SongTrack GetPhrasePositionTrack(PhrasePosition phrasePosition)
        {
            foreach(SongTrack track in Tracks)
            {
                if (track.Phrases.Any(x => x.LoopCount == phrasePosition.LoopCount && x.Phrase == phrasePosition.Phrase))
                    return track;
            }
            return null;
        }


        public int GetPhrasePositionTrackIndex(PhrasePosition phrasePosition)
        {
            for(int i = 0; i < Tracks.Count; i++)
            {
                if (Tracks[i].Phrases.Any(x => x.LoopCount == phrasePosition.LoopCount && x.Phrase == phrasePosition.Phrase))
                    return i;
            }
            return -1;
        }


        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion INotifyPropertyChanged implementation

    }



    public class SongParse
    {
        public List<SongTrackParse> Tracks;
        public List<ChordParse> Chords;
        public string Name;
        public List<TimeSignatureParse> TimeSignatures;
        public List<TempoChangeParse> TempoChanges;
        public RationalParse LoopStartBeat;
        public RationalParse LoopDuration;

        public SongParse()
        {
        }

        public SongParse(Song song)
        {
            Tracks = new List<SongTrackParse>();
            song.Tracks.ForEach(x => Tracks.Add(new SongTrackParse(x)));

            Chords = new List<ChordParse>();
            song.Chords.ForEach(x => Chords.Add(new ChordParse(x)));

            Name = song.Name;

            TimeSignatures = new List<TimeSignatureParse>();
            song.TimeSignatures.ForEach(x => TimeSignatures.Add(new TimeSignatureParse(x)));

            TempoChanges = new List<TempoChangeParse>();
            song.TempoChanges.ForEach(x => TempoChanges.Add(new TempoChangeParse(x)));

            LoopStartBeat = new RationalParse(song.LoopStartBeat);
            LoopDuration = new RationalParse(song.LoopDuration);
        }
    }
}
