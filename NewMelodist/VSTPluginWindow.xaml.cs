﻿using Jacobi.Vst.Interop.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for VSTPluginWindow.xaml
    /// </summary>
    public partial class VSTPluginWindow : Window
    {
        public VSTPluginWindow()
        {
            InitializeComponent();
        }


        public static VSTPluginWindow Show(VstPluginContext vstPluginContext)
        {
            VSTPluginWindow vstPluginWindow = new VSTPluginWindow();
            vstPluginWindow.Show();
            vstPluginContext.PluginCommandStub.EditorOpen(new WindowInteropHelper(vstPluginWindow).Handle);
            System.Drawing.Rectangle pluginRect;
            vstPluginContext.PluginCommandStub.EditorGetRect(out pluginRect);
            vstPluginWindow.Width = pluginRect.Width;
            vstPluginWindow.Height = pluginRect.Height + SystemParameters.WindowCaptionHeight;
            vstPluginWindow.ResizeMode = ResizeMode.NoResize;
            return vstPluginWindow;
        }
    }
}
