﻿using CSharpExtras;
using GeneralMidi;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class SongTrack : INotifyPropertyChanged
    {

        private object _lock = new object();


        private string _name;
        public event EventHandler NameChanged;
        public string Name
        {
            get { return _name; }
            set
            {
                if(_name != value)
                {
                    lock (_lock) { _name = value; }
                    NameChanged?.Invoke(this, null);
                    OnPropertyChanged("Name");
                }
            }
        }


        private MemberListManager<PhrasePosition> PhrasesMngr;
        public MemberList<PhrasePosition> Phrases { get { return PhrasesMngr.List; } }


        private GMInstrument _instrument;
        public event EventHandler InstrumentChanged;
        public GMInstrument Instrument
        {
            get { lock (_lock) { return _instrument; } }
            set
            {
                if(_instrument != value)
                {
                    lock (_lock) { _instrument = value; }
                    InstrumentChanged?.Invoke(this, null);
                    OnPropertyChanged("Instrument");
                }
            }
        }



        public SongTrack(string name)
        {
            Name = name;
            PhrasesMngr = new MemberListManager<PhrasePosition>();
        }



        public void AddPhrase(PhrasePosition phrase)
        {
            PhrasesMngr.Add(phrase);
        }

        public void RemovePhrase(PhrasePosition phrase)
        {
            PhrasesMngr.Remove(phrase);
        }



        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion INotifyPropertyChanged implementation

    }



    public class SongTrackParse
    {
        public string Name;
        public List<PhrasePositionParse> Phrases;
        public int GMInstrumentIndex = -1;
        public int VSTInstrumentIndex = -1;

        public SongTrackParse()
        {
        }

        public SongTrackParse(SongTrack songTrack)
        {
            Name = songTrack.Name;
            Phrases = new List<PhrasePositionParse>();
            songTrack.Phrases.ForEach(x => Phrases.Add(new PhrasePositionParse(x)));
            if(songTrack.Instrument != null)
            {
                if (songTrack.Instrument is VSTInstrument)
                    VSTInstrumentIndex = songTrack.Instrument.Index;
                else
                    GMInstrumentIndex = songTrack.Instrument.Index;
            }
        }

        public SongTrack Load()
        {
            List<PhrasePosition> phrasePositions = new List<PhrasePosition>();
            SongTrack songTrack = new SongTrack(Name);
            Phrases.ForEach(x => songTrack.AddPhrase(x.Load()));
            if (GMInstrumentIndex >= 0)
                songTrack.Instrument = GMInstrument.Instruments[GMInstrumentIndex];
            else if (VSTInstrumentIndex >= 0)
                songTrack.Instrument = VSTInstrument.Get(MelodistDB.Instance, VSTInstrumentIndex);
            return songTrack;
        }
    }
}
