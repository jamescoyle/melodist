﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist.MidiImports
{
    public class MidiFile
    {

        private MemberListManager<MidiChannel> ChannelsMngr;
        public MemberList<MidiChannel> Channels { get { return ChannelsMngr.List; } }

        private MemberListManager<TimeSignature> TimeSignaturesMngr;
        public MemberList<TimeSignature> TimeSignatures { get { return TimeSignaturesMngr.List; } }

        private MemberListManager<TempoChange> TempoChangesMngr;
        public MemberList<TempoChange> TempoChanges { get { return TempoChangesMngr.List; } }
        
        public Rational EndBeat { get; private set; }



        public MidiFile(string midiFilePath)
        {
            NAudio.Midi.MidiFile midiFile = new NAudio.Midi.MidiFile(midiFilePath);

            List<Tuple<int, Note>> channeledNotes = new List<Tuple<int, Note>>();
            TimeSignaturesMngr = new MemberListManager<TimeSignature>();
            TempoChangesMngr = new MemberListManager<TempoChange>();
            List<Tuple<int, MidiProgramChange>> channeledProgramChanges = new List<Tuple<int, MidiProgramChange>>();

            foreach(List<NAudio.Midi.MidiEvent> track in midiFile.Events)
            {
                foreach(NAudio.Midi.MidiEvent midiEvent in track)
                {
                    if(midiEvent is NAudio.Midi.NoteOnEvent)
                    {
                        NAudio.Midi.NoteOnEvent noteOnEvent = (NAudio.Midi.NoteOnEvent)midiEvent;
                        if(noteOnEvent.Velocity > 0 && noteOnEvent.OffEvent != null)
                        {
                            Note note = new Note(
                                pitch: new Pitch(noteOnEvent.NoteNumber),
                                startBeat: new Rational(noteOnEvent.AbsoluteTime, midiFile.DeltaTicksPerQuarterNote),
                                duration: new Rational(noteOnEvent.NoteLength, midiFile.DeltaTicksPerQuarterNote)
                            );
                            channeledNotes.Add(new Tuple<int, Note>(noteOnEvent.Channel, note));
                            if (note.EndBeat > EndBeat)
                                EndBeat = note.EndBeat;
                        }
                    }
                    else if(midiEvent is NAudio.Midi.TimeSignatureEvent)
                    {
                        NAudio.Midi.TimeSignatureEvent timeSigEvent = (NAudio.Midi.TimeSignatureEvent)midiEvent;
                        TimeSignaturesMngr.Add(new TimeSignature(
                            beat: new Rational(timeSigEvent.AbsoluteTime, midiFile.DeltaTicksPerQuarterNote),
                            numerator: timeSigEvent.Numerator,
                            denominator: (int)Math.Pow(2, timeSigEvent.Denominator)
                        ));
                    }
                    else if(midiEvent is NAudio.Midi.TempoEvent)
                    {
                        NAudio.Midi.TempoEvent tempoEvent = (NAudio.Midi.TempoEvent)midiEvent;
                        TempoChangesMngr.Add(new TempoChange(
                            beat: new Rational(tempoEvent.AbsoluteTime, midiFile.DeltaTicksPerQuarterNote), 
                            tempo: (int)tempoEvent.Tempo
                        ));
                    }
                    else if(midiEvent is NAudio.Midi.PatchChangeEvent)
                    {
                        NAudio.Midi.PatchChangeEvent patchEvent = (NAudio.Midi.PatchChangeEvent)midiEvent;
                        channeledProgramChanges.Add(new Tuple<int, MidiProgramChange>(patchEvent.Channel, new MidiProgramChange(
                            beat: new Rational(patchEvent.AbsoluteTime, midiFile.DeltaTicksPerQuarterNote),
                            programNumber: patchEvent.Patch
                        )));
                    }
                }
            }

            EndBeat += 4;

            ChannelsMngr = new MemberListManager<MidiChannel>();
            for(int i = 0; i <= 16; i++)
            {
                List<Note> channelNotes = channeledNotes.Where(x => x.Item1 == i).Select(x => x.Item2).ToList();
                List<MidiProgramChange> channelProgramChanges = channeledProgramChanges.Where(x => x.Item1 == i).Select(x => x.Item2).ToList();
                if (channelNotes.Count > 0 || channelProgramChanges.Count > 0)
                    ChannelsMngr.Add(new MidiChannel(i, channelNotes, channelProgramChanges));
            }

            TimeSignaturesMngr = new MemberListManager<TimeSignature>(TimeSignatures.OrderBy(x => x.Beat));
            TempoChangesMngr = new MemberListManager<TempoChange>(TempoChanges.OrderBy(x => x.Beat));
        }


        public int TempoAt(Rational beat)
        {
            TempoChange tempo = TempoChanges.LastOrDefault(x => x.Beat <= beat);
            return (tempo == null) ? 120 : tempo.Tempo;
        }

        public Rational BeatsPerBarAt(Rational beat)
        {
            TimeSignature timeSig = TimeSignatures.LastOrDefault(x => x.Beat <= beat);
            return (timeSig == null) ? 4 : timeSig.QuarterBeatsPerBar;
        }

    }



    public class MidiChannel
    {

        private MemberListManager<Note> NotesMngr;
        public MemberList<Note> Notes { get { return NotesMngr.List; } }

        public int ChannelNumber { get; private set; }

        private MemberListManager<MidiProgramChange> ProgramChangesMngr;
        public MemberList<MidiProgramChange> ProgramChanges { get { return ProgramChangesMngr.List; } }

        public event EventHandler MuteChanged;
        private bool _mute;
        public bool Mute
        {
            get { return _mute; }
            set
            {
                if (_mute == value)
                    return;
                _mute = value;
                MuteChanged?.Invoke(this, null);
            }
        }


        public MidiChannel(int channel, List<Note> notes = null, List<MidiProgramChange> programChanges = null)
        {
            ChannelNumber = channel;
            NotesMngr = new MemberListManager<Note>(notes);
            ProgramChangesMngr = new MemberListManager<MidiProgramChange>(programChanges);
            Mute = false;
        }

    }



    public class MidiProgramChange
    {
        public Rational Beat { get; private set; }

        public int ProgramNumber { get; private set; }


        public MidiProgramChange(Rational beat, int programNumber)
        {
            Beat = beat;
            ProgramNumber = programNumber;
        }
    }
}
