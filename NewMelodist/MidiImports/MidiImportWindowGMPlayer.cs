﻿using GeneralMidi;
using MidiPlayback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathPlus;
using HarmonyEngine;

namespace NewMelodist.MidiImports
{
    public class MidiImportWindowGMPlayer : GMPlayer
    {

        public MidiImportWindow MidiImportWindow { get; private set; }


        public override bool Loop { get { return true; } }

        public override Rational LoopStart { get { return MidiImportWindow.PhraseDurationViewer.Phrase.StartBeat; } }

        public override Rational LoopEnd { get { return MidiImportWindow.PhraseDurationViewer.Phrase.EndBeat; } }

        public override Rational Tempo { get { return MidiImportWindow.MidiFile.TempoAt(Playhead); } }


        public MidiImportWindowGMPlayer(MidiImportWindow midiImportWindow, MidiDevice outputDevice = null)
            : base(outputDevice)
        {
            MidiImportWindow = midiImportWindow;
            foreach(MidiChannel channel in MidiImportWindow.MidiFile.Channels)
                channel.MuteChanged += OnMuteChanged;
        }


        private void OnMuteChanged(object sender, EventArgs e)
        {
            MidiChannel channel = (MidiChannel)sender;
            if (channel.Mute)
                OutputDevice.SendAllNotesOff(channel.ChannelNumber - 1);
        }


        public void UpdateInstruments()
        {
            foreach(MidiChannel channel in MidiImportWindow.MidiFile.Channels)
            {
                MidiProgramChange programChange = channel.ProgramChanges.LastOrDefault(x => x.Beat < MidiImportWindow.PhraseDurationViewer.Phrase.StartBeat);
                if (programChange != null)
                    OutputDevice.SendProgramChange(channel.ChannelNumber - 1, programChange.ProgramNumber);
            }
        }


        protected override void Read(Rational startBeat, Rational endBeat)
        {
            if (Loop && startBeat > endBeat && startBeat < LoopEnd && endBeat > LoopStart)
            {
                Read(startBeat, LoopEnd);
                OutputDevice.SendAllNotesOff();
                UpdateInstruments();
                Read(LoopStart, endBeat);
                return;
            }

            foreach (MidiChannel channel in MidiImportWindow.MidiFile.Channels)
            {
                foreach (MidiProgramChange programChange in channel.ProgramChanges)
                {
                    if (programChange.Beat >= startBeat && programChange.Beat < endBeat)
                        OutputDevice.SendProgramChange(channel.ChannelNumber - 1, programChange.ProgramNumber);
                }

                if (!channel.Mute)
                {
                    foreach (Note note in channel.Notes)
                    {
                        if (note.EndBeat >= startBeat && note.EndBeat < endBeat)
                            OutputDevice.SendNoteOff(channel.ChannelNumber - 1, note.Pitch, 104);
                    }

                    foreach (Note note in channel.Notes)
                    {
                        if (note.StartBeat >= startBeat && note.StartBeat < endBeat)
                            OutputDevice.SendNoteOn(channel.ChannelNumber - 1, note.Pitch, 104);
                    }
                }
            }
        }


        public override void Stop()
        {
            foreach (MidiChannel channel in MidiImportWindow.MidiFile.Channels)
                channel.MuteChanged -= OnMuteChanged;
            base.Stop();
        }

    }
}
