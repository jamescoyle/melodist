﻿using MelodistControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist.MidiImports
{
    /// <summary>
    /// Interaction logic for MidiChannelTrackHead.xaml
    /// </summary>
    public partial class MidiChannelTrackHead : UserControl
    {

        private int _channelNumber;
        public int ChannelNumber
        {
            get
            {
                return _channelNumber;
            }
            set
            {
                _channelNumber = value;
                ChannelNameText.Content = "Channel " + _channelNumber;
            }
        }

        public Color ChannelColour
        {
            get { return ((SolidColorBrush)ChannelColourCanvas.Background).Color; }
            set { ChannelColourCanvas.Background = new SolidColorBrush(value); }
        }

        public string ChannelDescription
        {
            get { return ChannelDescriptionText.Content.ToString(); }
            set { ChannelDescriptionText.Content = value; }
        }


        public MidiChannelTrackHead()
        {
            InitializeComponent();
        }

        public event MidiChannelTrackHeadEventHandler Show;
        private void ShowCheck_Click(object sender, RoutedEventArgs e)
        {
            Show?.Invoke(this, new MidiChannelTrackHeadEventArgs((bool)ShowCheck.IsChecked));
        }

        public event MidiChannelTrackHeadEventHandler Mute;
        private void MuteCheck_Click(object sender, RoutedEventArgs e)
        {
            if (MuteCheck.IsChecked == true && SoloCheck.IsChecked == true)
                SoloCheck.IsChecked = false;
            Mute?.Invoke(this, new MidiChannelTrackHeadEventArgs((bool)MuteCheck.IsChecked));
        }

        public event MidiChannelTrackHeadEventHandler Solo;
        private void SoloCheck_Click(object sender, RoutedEventArgs e)
        {
            if (MuteCheck.IsChecked == true && SoloCheck.IsChecked == true)
                MuteCheck.IsChecked = false;
            Solo?.Invoke(this, new MidiChannelTrackHeadEventArgs((bool)SoloCheck.IsChecked));
        }

    }


    public delegate void MidiChannelTrackHeadEventHandler(object sender, MidiChannelTrackHeadEventArgs e);

    public class MidiChannelTrackHeadEventArgs : EventArgs
    {
        public bool Active { get; private set; }

        public MidiChannelTrackHeadEventArgs(bool active)
        {
            Active = active;
        }
    }
}
