﻿using GeneralMidi;
using HarmonyEngine;
using MathPlus;
using MelodistControls.NotesViewers;
using Microsoft.Win32;
using MidiPlayback;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace NewMelodist.MidiImports
{
    /// <summary>
    /// Interaction logic for MidiImportWindow.xaml
    /// </summary>
    public partial class MidiImportWindow : Window
    {

        public PlayerManager Player { get; private set; }

        private DispatcherTimer PlaybackUIUpdater { get; set; }

        public MidiFile MidiFile { get; private set; }


        private static List<Color> TrackColours;

        static MidiImportWindow()
        {
            TrackColours = new List<Color>()
            {
                Color.FromRgb(4, 154, 100),
                Color.FromRgb(252, 254, 4),
                Color.FromRgb(204, 2, 100),
                Color.FromRgb(52, 154, 156),
                Color.FromRgb(204, 254, 4),
                Color.FromRgb(204, 2, 4),
                Color.FromRgb(52, 102, 156),
                Color.FromRgb(156, 206, 52),
                Color.FromRgb(252, 50, 4),
                Color.FromRgb(52, 50, 156),
                Color.FromRgb(100, 154, 52),
                Color.FromRgb(252, 154, 4),
                Color.FromRgb(100, 50, 156),
                Color.FromRgb(52, 154, 4),
                Color.FromRgb(252, 206, 52),
                Color.FromRgb(156, 50, 156)
            };
        }



        public MidiImportWindow(MidiFile midiFile)
        {
            MidiFile = midiFile;
            InitializeComponent();

            foreach (MidiChannel channel in midiFile.Channels)
            {
                MidiChannelTrackHead trackHead = new MidiChannelTrackHead();
                trackHead.ChannelNumber = channel.ChannelNumber;
                trackHead.ChannelColour = TrackColours[channel.ChannelNumber - 1];
                trackHead.Show += TrackHead_Show;
                trackHead.Mute += TrackHead_Mute;
                trackHead.Solo += TrackHead_Solo;
                TrackHeadsStack.Children.Add(trackHead);
                if (channel.ChannelNumber == 10)
                {
                    trackHead.ChannelDescription = "Drums";
                    channel.Notes.ForEach(x => x.Duration = Rational.Max(x.Duration, new Rational(1, 16)));
                }
                else if (channel.ProgramChanges.Count > 0)
                    trackHead.ChannelDescription = GMInstrument.Get(channel.ProgramChanges[0].ProgramNumber).Name;
            }

            List<Note> notes = new List<Note>();
            midiFile.Channels.ForEach(x => notes.AddRange(x.Notes));

            List<Note> harmonyNotes = new List<Note>();
            midiFile.Channels.Where(x => x.ChannelNumber != 10).ToList().ForEach(x => harmonyNotes.AddRange(x.Notes));

            ChordDetector chordDetector = new ChordDetector();
            List<Chord> chords = chordDetector.Run(harmonyNotes, midiFile.TimeSignatures.ToList());

            Rational beatsPerBar = midiFile.BeatsPerBarAt(0);
            int tempo = midiFile.TempoAt(0);

            Phrase phrase = new Phrase(beatsPerBar.Round(), tempo);
            notes.ForEach(x => phrase.AddNote(x));
            chords.ForEach(x => phrase.AddChord(x));
            phrase.EndBeat = MidiFile.EndBeat;

            PhraseNotesViewer.NoteViewerAdded += PhraseNoteViewer_NoteViewerAdded;
            PhraseNotesViewer.ReadOnly = true;
            PhraseNotesViewer.Phrase = phrase;

            PhraseChordViewer.ReadOnly = true;
            PhraseChordViewer.Phrase = phrase;

            PhraseDurationViewer.Phrase = phrase;

            PlaybackUIUpdater = new DispatcherTimer();
            PlaybackUIUpdater.Interval = TimeSpan.FromMilliseconds(30);
            PlaybackUIUpdater.Tick += PlaybackUIUpdate;
        }


        private void TrackHead_Show(object sender, MidiChannelTrackHeadEventArgs e)
        {
            MidiChannelTrackHead trackHead = (MidiChannelTrackHead)sender;
            List<Note> channelNotes = MidiFile.Channels.FirstOrDefault(x => x.ChannelNumber == trackHead.ChannelNumber)?.Notes.ToList();
            if (channelNotes == null)
                return;
            foreach (Note note in channelNotes)
            {
                if (e.Active)
                {
                    if (!PhraseNotesViewer.Phrase.Notes.Contains(note))
                        PhraseNotesViewer.Phrase.AddNote(note);
                }
                else
                    PhraseNotesViewer.Phrase.RemoveNote(note);
            }
        }


        private void TrackHead_Mute(object sender, MidiChannelTrackHeadEventArgs e)
        {
            MidiChannelTrackHead trackHead = (MidiChannelTrackHead)sender;
            MidiChannel channel = MidiFile.Channels.FirstOrDefault(x => x.ChannelNumber == trackHead.ChannelNumber);
            if(channel != null)
            {
                if (e.Active == true)
                    channel.Mute = true;
                else if(TrackHeadsStack.Children.OfType<MidiChannelTrackHead>().Count(x => x.SoloCheck.IsChecked == true) == 0)
                    channel.Mute = false;
            }
        }


        private void TrackHead_Solo(object sender, MidiChannelTrackHeadEventArgs e)
        {
            MidiChannelTrackHead trackHead = (MidiChannelTrackHead)sender;
            MidiChannel channel = MidiFile.Channels.FirstOrDefault(x => x.ChannelNumber == trackHead.ChannelNumber);
            if (channel != null)
            {
                if (e.Active == true)
                {
                    channel.Mute = false;
                    List<MidiChannelTrackHead> tracksToMute = TrackHeadsStack.Children.OfType<MidiChannelTrackHead>().Where(x => x.SoloCheck.IsChecked == false).ToList();
                    foreach(MidiChannelTrackHead muteTrackHead in tracksToMute)
                    {
                        MidiChannel muteChannel = MidiFile.Channels.FirstOrDefault(x => x.ChannelNumber == muteTrackHead.ChannelNumber);
                        muteChannel.Mute = true;
                    }
                }
                else if (TrackHeadsStack.Children.OfType<MidiChannelTrackHead>().Count(x => x.SoloCheck.IsChecked == true) > 0)
                    channel.Mute = true;

            }
        }


        private void PhraseNoteViewer_NoteViewerAdded(object sender, NoteViewerEventArgs e)
        {
            //Get the track that the note belongs to
            foreach (MidiChannel track in MidiFile.Channels)
            {
                if (track.ChannelNumber > 0 && track.ChannelNumber <= 16 && track.Notes.Contains(e.NoteViewer.Note))
                {
                    //Colour the note according to the track channel
                    e.NoteViewer.Colour = TrackColours[track.ChannelNumber - 1];
                }
            }
        }


        private void PhraseNoteViewer_SelectionChanged(object sender, EventArgs e)
        {
            List<NoteViewer> selection = PhraseNotesViewer.GetSelectedNoteViewers();
            if (selection.Count == 0)
                return;

            PhraseDurationViewer.Phrase.StartBeat = selection.Min(x => x.StartBeat);

            TimeSignature lastTimeSignature = MidiFile.TimeSignatures.LastOrDefault(x => x.Beat <= PhraseDurationViewer.Phrase.StartBeat);
            int beatsPerBar = (lastTimeSignature == null) ? 4 : ((Rational)lastTimeSignature.QuarterBeatsPerBar).Round();

            Rational endBeat = selection.Max(x => x.EndBeat);
            Rational duration = endBeat - PhraseDurationViewer.Phrase.StartBeat;
            duration = (duration / beatsPerBar).Ceiling() * beatsPerBar;
            PhraseDurationViewer.Phrase.Duration = duration;
        }



        private void FileMenuSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog()
            {
                FileName = "My Phrase",
                DefaultExt = ".phr",
                InitialDirectory = System.IO.Path.GetDirectoryName(Project.Instance.FilePath),
                Filter = "Phrase documents (.phr)|*.phr"
            };
            if (saveDialog.ShowDialog() != true)
                return;
            string phraseFile = saveDialog.FileName;

            List<Note> notes = PhraseNotesViewer.GetSelectedNoteViewers().Select(x => x.Note).ToList();
            if (notes.Count == 0)
            {
                MessageBox.Show("No notes selected");
                return;
            }

            Rational startBeat = notes.Min(x => x.StartBeat);
            Rational endBeat = notes.Max(x => x.EndBeat);

            int beatsPerBar = MidiFile.BeatsPerBarAt(startBeat).Round();
            int tempo = MidiFile.TempoAt(startBeat);

            Rational duration = endBeat - startBeat;
            duration = (duration / beatsPerBar).Ceiling() * beatsPerBar;

            TimeSignature lastTimeSignature = MidiFile.TimeSignatures.LastOrDefault(x => x.Beat <= startBeat);
            lastTimeSignature = (lastTimeSignature == null) ? new TimeSignature(0, 4, 4) : lastTimeSignature;
            Rational startBar = lastTimeSignature.Beat;
            while (startBar + lastTimeSignature.QuarterBeatsPerBar <= startBeat)
                startBar += lastTimeSignature.QuarterBeatsPerBar;
            if ((startBar + lastTimeSignature.QuarterBeatsPerBar - startBeat).Abs() < (startBar - startBeat).Abs())
                startBar += lastTimeSignature.QuarterBeatsPerBar;

            Phrase phrase = new Phrase(beatsPerBar, tempo);
            foreach (Note note in notes)
                phrase.AddNote(new Note(note.Pitch, note.StartBeat - startBar, note.Duration));

            phrase.StartBeat = startBeat - startBar;
            phrase.Duration = duration;

            List<Chord> chords = PhraseChordViewer.Phrase.Chords.Where(x => x.StartBeat < endBeat && x.EndBeat > startBeat).ToList();
            foreach (Chord chord in chords)
            {
                Rational chordStart = Rational.Max(chord.StartBeat - startBar, phrase.StartBeat);
                Rational chordEnd = Rational.Min(chord.EndBeat - startBar, phrase.EndBeat);
                phrase.AddChord(new Chord(chord.ChordDef, chord.Root, chordStart, chordEnd - chordStart));
            }

            phrase.Save(phraseFile);
        }


        private void OnPlay(object sender, EventArgs e)
        {
            Play();
        }

        public void Play()
        {
            if (Player != null)
                return;

            try
            {
                LoadPlayer();
                Player.Play();
                PlaybackUIUpdater.Start();
            }
            catch
            {
                MessageBox.Show("Failed to start playback");
            }
        }


        private Rational _previousPlayheadBeat;
        private void PlaybackUIUpdate(object sender, EventArgs e)
        {
            PhraseNotePlayhead.UpdatePlayhead(Player.Playhead);
            foreach (MidiChannel channel in MidiFile.Channels)
            {
                MidiProgramChange programChange = channel.ProgramChanges.LastOrDefault(x => x.Beat >= _previousPlayheadBeat && x.Beat < Player.Playhead);
                if (programChange != null)
                {
                    MidiChannelTrackHead trackHead = TrackHeadsStack.Children.OfType<MidiChannelTrackHead>().FirstOrDefault(x => x.ChannelNumber == channel.ChannelNumber);
                    if (trackHead != null)
                        trackHead.ChannelDescription = GMInstrument.Get(programChange.ProgramNumber).Name;
                }
            }
            _previousPlayheadBeat = Player.Playhead;
        }


        private void OnStop(object sender, EventArgs e)
        {
            Stop();
        }

        public void Stop()
        {
            if (Player == null)
                return;

            PlaybackUIUpdater.Stop();
            PhraseNotePlayhead.RemovePlayhead();

            Player.Stop();
            Player = null;
        }


        private void LoadPlayer()
        {
            if (Player != null)
            {
                ((MidiImportWindowGMPlayer)Player.GMPlayer).UpdateInstruments();
                return;
            }

            MidiImportWindowGMPlayer gmPlayer = new MidiImportWindowGMPlayer(this, Tools.GetSelectedMidiDevice());
            gmPlayer.Playhead = PhraseDurationViewer.Phrase.StartBeat;
            gmPlayer.UpdateInstruments();
            Player = new PlayerManager(null, gmPlayer);
        }


        private void ViewMenuZoomIn_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatWidth = (int)Math.Round(PhraseNotesViewer.BeatWidth * 1.2);
            PhraseNotesViewer.TrackHeight = (int)Math.Round(PhraseNotesViewer.TrackHeight * 1.2);
        }

        private void ViewMenuZoomOut_Click(object sender, RoutedEventArgs e)
        {
            PhraseNotesViewer.BeatWidth = (int)Math.Round(PhraseNotesViewer.BeatWidth / 1.2);
            PhraseNotesViewer.TrackHeight = (int)Math.Round(PhraseNotesViewer.TrackHeight / 1.2);
        }


        /// <summary>
        /// Prevents the PhraseChordViewer from trying to bring itself into view within its scrollviewer, resulting in its scroll going out of sync with that of the PhraseNoteViewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhraseChordViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }

        private void PhraseDurationViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }


        private void NoteScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (DurationScrollViewer.HorizontalOffset != e.HorizontalOffset)
                DurationScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);

            if (ChordScrollViewer.HorizontalOffset != e.HorizontalOffset)
                ChordScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);

            if (PianoScrollViewer.VerticalOffset != e.VerticalOffset)
                PianoScrollViewer.ScrollToVerticalOffset(e.VerticalOffset);
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Stop();
        }
        
    }
}
