﻿using GeneralMidi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class VSTInstrument : GMInstrument, IDatabaseModel
    {

        public string FilePath { get; private set; }



        public VSTInstrument(string name, string filePath, int index = -1)
            : base(index, name)
        {
            FilePath = filePath;
        }


        public static void CreateTable(MelodistDB db)
        {
            db.NonQuery("CREATE TABLE IF NOT EXISTS Vst ([name] VARCHAR, [filePath] VARCHAR);");
        }

        public int Save(MelodistDB db)
        {
            if(Index <= 0)
            {
                int index = Convert.ToInt32(db.SingleQuery("INSERT INTO Vst ([name], [filePath]) VALUES ('" + Name + "', '" + FilePath + "'); SELECT last_insert_rowid();"));
                Index = index;
                return Index;
            }
            return db.NonQuery("UPDATE Vst SET [name] = '" + Name + "', [filePath] = '" + FilePath + "' WHERE [rowid] = " + Index + ";");
        }

        public int Delete(MelodistDB db)
        {
            return db.NonQuery("DELETE Vst WHERE [rowid] = " + Index + ";");
        }

        private static List<VSTInstrument> Retrieve(DataTable dt)
        {
            List<VSTInstrument> vsts = new List<VSTInstrument>();
            foreach(DataRow dr in dt.Rows)
                vsts.Add(new VSTInstrument(dr["name"].ToString(), dr["filePath"].ToString(), Convert.ToInt32(dr["rowid"])));
            return vsts;
        }

        public static List<VSTInstrument> GetAll(MelodistDB db)
        {
            return Retrieve(db.Query("SELECT rowid, name, filePath FROM Vst;"));
        }

        public static VSTInstrument Get(MelodistDB db, int index)
        {
            return Retrieve(db.Query("SELECT rowid, name, filePath FROM Vst WHERE rowid = " + index + ";")).FirstOrDefault();
        }

    }
}
