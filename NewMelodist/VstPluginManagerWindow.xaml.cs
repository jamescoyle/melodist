﻿using Jacobi.Vst.Interop.Host;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VSTHosting;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for VstPluginManagerWindow.xaml
    /// </summary>
    public partial class VstPluginManagerWindow : Window
    {

        public VstPluginManagerWindow()
        {
            InitializeComponent();

            RefreshList();
        }


        private void RefreshList()
        {
            PluginList.ItemsSource = VSTInstrument.GetAll(MelodistDB.Instance);
        }


        private void FilePathText_LostFocus(object sender, RoutedEventArgs e)
        {
            string filePath = FilePathText.Text;
            if (File.Exists(filePath) && string.IsNullOrWhiteSpace(PluginNameText.Text))
                PluginNameText.Text = System.IO.Path.GetFileNameWithoutExtension(filePath);
        }


        private void AddPluginBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(PluginNameText.Text))
            {
                MessageBox.Show("Please enter a name for the plugin");
                return;
            }

            VstPluginContext context = null;
            Exception exception = null;
            try
            {
                context = VSTWaveProvider.OpenPlugin(FilePathText.Text, new HostCommandStub());
            }
            catch (Exception ex) { exception = ex; }

            if (context == null)
            {
                MessageBox.Show("Failed to load plugin" + ((exception != null) ? Environment.NewLine + exception?.Message : ""));
                return;
            }

            List<VSTInstrument> existingPlugins = VSTInstrument.GetAll(MelodistDB.Instance);
            if (existingPlugins.Where(x => x.FilePath == FilePathText.Text).ToList().Count > 0)
            {
                MessageBox.Show("This plugin has already been loaded");
                return;
            }
            if (existingPlugins.Where(x => x.Name == PluginNameText.Text).ToList().Count > 0)
            {
                MessageBox.Show("A plugin already exists with the same name");
                return;
            }

            VSTInstrument vst = new VSTInstrument(PluginNameText.Text, FilePathText.Text);
            vst.Save(MelodistDB.Instance);

            RefreshList();
        }


        private void FilePathText_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Dynamic-link Libraries (*.dll)|*.dll";
            if (openDialog.ShowDialog() == true)
                FilePathText.Text = openDialog.FileName;
        }


        private void RemovePluginsBtn_Click(object sender, RoutedEventArgs e)
        {
            List<VSTInstrument> removeVsts = PluginList.SelectedItems.OfType<VSTInstrument>().ToList();
            if (MessageBox.Show("Are you sure you want to remove the selected " + removeVsts.Count + " plugins?", "Are you sure?", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            foreach (VSTInstrument vst in removeVsts)
                vst.Delete(MelodistDB.Instance);

            RefreshList();
        }

    }
}
