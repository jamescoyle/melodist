﻿using HarmonyEngine;
using MathPlus;
using Microsoft.Win32;
using NewMelodist.MidiImports;
using NewMelodist.Warpers;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class DockWindow : Window
    {
        
        public static DockWindow Instance { get; private set; }

        

        public DockWindow()
        {
            try
            {
                if (Instance != null)
                    throw new Exception("An instance of Melodist DockWindow already exists");
                Instance = this;

                LoadupWindow loadupWindow = new LoadupWindow();
                loadupWindow.ShowDialog();
                if (Project.Instance == null)
                {
                    Application.Current.Shutdown();
                    return;
                }

                InitializeComponent();

                SequencerPane.IsSelectedChanged += WindowPane_IsSelectedChanged;
                PhraseEditorPane.IsSelectedChanged += WindowPane_IsSelectedChanged;
                PhraseListPane.IsSelectedChanged += WindowPane_IsSelectedChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void WindowPane_IsSelectedChanged(object sender, EventArgs e)
        {
            Keyboard.ClearFocus();
        }



        #region WarpWindow
        private WarpWindow _warpWindow;
        public WarpWindow WarpWindow
        {
            get
            {
                if (_warpWindow == null)
                    BuildWarpWindow();
                return _warpWindow;
            }
        }

        private LayoutAnchorable _warpWindowPane;
        public LayoutAnchorable WarpWindowPane
        {
            get
            {
                if (_warpWindowPane == null)
                    BuildWarpWindow();
                return _warpWindowPane;
            }
        }

        private void BuildWarpWindow()
        {
            _warpWindowPane = new LayoutAnchorable();
            _warpWindowPane.Title = "Melody Warper";
            _warpWindowPane.IsActive = true;
            _warpWindowPane.IsSelected = true;
            _warpWindow = new WarpWindow();
            _warpWindow.WarpCompleted += _warpWindow_WarpCompleted;
            _warpWindowPane.Content = _warpWindow;
            _warpWindowPane.ContentId = "warpWindow";
            _warpWindowPane.FloatingWidth = 500;
            _warpWindowPane.FloatingHeight = 350;
            _warpWindowPane.AddToLayout(DockingManager, AnchorableShowStrategy.Top);
            _warpWindowPane.Float();
            _warpWindowPane.IsSelectedChanged += WindowPane_IsSelectedChanged;
        }

        private void _warpWindow_WarpCompleted(object sender, PhraseManipulation.PhraseEventArgs e)
        {
            PhraseEditorPane.Show();
            PhraseEditor.Phrase = e.Phrase;
        }
        #endregion WarpWindow



        #region Song Editor
        private void SequencerPane_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SongEditor.Stop();
        }
        #endregion Song Editor



        #region Phrase Editor
        private void PhraseEditorPane_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PhraseEditor.Stop();
        }
        #endregion Phrase Editor



        #region Menu Options
        private void FileMenuSave_Click(object sender, RoutedEventArgs e)
        {
            Project.Instance.Save();
        }

        private void PlaybackMenuManageVstPlugins_Click(object sender, RoutedEventArgs e)
        {
            new VstPluginManagerWindow().ShowDialog();
        }

        private void PlaybackMenuGmSettings_Click(object sender, RoutedEventArgs e)
        {
            new GmSettings.GmSettingsPage().ShowDialog();
        }

        private void SongEditorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SequencerPane.Show();
        }

        private void PhraseEditorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            PhraseEditorPane.Show();
        }

        private void PhraseLibraryMenuItem_Click(object sender, RoutedEventArgs e)
        {
            PhraseListPane.Show();
        }

        private void WarpWindowMenuItem_Click(object sender, RoutedEventArgs e)
        {
            WarpWindowPane.Show();
        }

        private void FileMenuImportMidi_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog()
            {
                DefaultExt = ".mid",
                InitialDirectory = Project.Instance.FilePath,
                Filter = "MIDI files (.mid)|*.mid"
            };
            if (openDialog.ShowDialog() == true)
            {
                MidiFile midiFile = new MidiFile(openDialog.FileName);
                MidiImportWindow importWindow = new MidiImportWindow(midiFile);
                importWindow.Show();
            }
        }

        private void FileMenuExportMidi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Song song = Project.Instance.Song;

                SaveFileDialog saveDialog = new SaveFileDialog()
                {
                    FileName = song.Name,
                    DefaultExt = ".mid",
                    Filter = "MIDI Files (.mid)|*.mid"
                };
                if (saveDialog.ShowDialog() != true)
                    return;

                Rational endBeat = 0;
                foreach(SongTrack track in song.Tracks)
                    track.Phrases.ForEach(x => endBeat = Rational.Max(endBeat, x.EndBeat));

                int ticksPerBeat = 120;
                NAudio.Midi.MidiEventCollection midiTracks = new NAudio.Midi.MidiEventCollection(1, ticksPerBeat);

                IList<NAudio.Midi.MidiEvent> metaTrack = midiTracks.AddTrack();
                foreach(TempoChange tempoChange in song.TempoChanges)
                {
                    NAudio.Midi.TempoEvent tempoEvent = new NAudio.Midi.TempoEvent(1, (tempoChange.Beat * ticksPerBeat).Round());
                    tempoEvent.Tempo = tempoChange.Tempo;
                    metaTrack.Add(tempoEvent);
                }
                foreach(TimeSignature timeSig in song.TimeSignatures)
                    metaTrack.Add(new NAudio.Midi.TimeSignatureEvent(0, timeSig.Numerator, timeSig.Denominator, ticksPerBeat, 8));

                metaTrack.Add(new NAudio.Midi.MetaEvent(NAudio.Midi.MetaEventType.EndTrack, 0, (endBeat * ticksPerBeat).Round()));

                int channelNum = 1;
                foreach(SongEditors.SongTrackHeadViewer trackHead in SongEditor.TrackHeadListViewer.SongTrackHeadViewers)
                {
                    if (trackHead.InstrumentPicker.SelectedInstrument == null || trackHead.SongTrack.Phrases.Count == 0)
                        continue;
                    
                    IList<NAudio.Midi.MidiEvent> midiTrack = midiTracks.AddTrack();
                    midiTrack.Add(new NAudio.Midi.PatchChangeEvent(0, channelNum, trackHead.InstrumentPicker.IsGMInstrumentSelected ? trackHead.InstrumentPicker.SelectedInstrument.Index : 1));
                    foreach(PhrasePosition phrasePos in trackHead.SongTrack.Phrases)
                    {
                        foreach(Note note in phrasePos.GetNotes())
                        {
                            midiTrack.Add(new NAudio.Midi.NoteOnEvent((note.StartBeat * ticksPerBeat).Round(), channelNum, note.Pitch.Value, note.Velocity, (note.Duration * ticksPerBeat).Round()));
                            midiTrack.Add(new NAudio.Midi.NoteOnEvent((note.EndBeat * ticksPerBeat).Round(), channelNum, note.Pitch.Value, 0, 0));
                        }
                    }
                    midiTrack.Add(new NAudio.Midi.MetaEvent(NAudio.Midi.MetaEventType.EndTrack, 0, (endBeat * ticksPerBeat).Round()));
                    channelNum++;
                }

                NAudio.Midi.MidiFile.Export(saveDialog.FileName, midiTracks);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        #endregion Menu Options

    }
}
