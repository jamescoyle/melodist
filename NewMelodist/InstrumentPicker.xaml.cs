﻿using GeneralMidi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for InstrumentPicker.xaml
    /// </summary>
    public partial class InstrumentPicker : UserControl
    {
        
        public bool VstSelected { get; private set; }
        

        public event EventHandler SelectedInstrumentChanged;
        public static DependencyProperty SelectedInstrumentProperty = DependencyProperty.Register("SelectedInstrument", typeof(GMInstrument), typeof(InstrumentPicker), new PropertyMetadata(null, OnSelectedInstrumentDPChanged));
        private static void OnSelectedInstrumentDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            InstrumentPicker instance = (InstrumentPicker)d;
            instance.SetSelectedInstrumentSideEffects();
        }
        public GMInstrument SelectedInstrument
        {
            get { return (GMInstrument)GetValue(SelectedInstrumentProperty); }
            set
            {
                if(value != SelectedInstrument)
                {
                    SetValue(SelectedInstrumentProperty, value);
                    SetSelectedInstrumentSideEffects();
                }
            }
        }
        private void SetSelectedInstrumentSideEffects()
        {
            MenuHead.Header = (SelectedInstrument == null) ? "--None--" : SelectedInstrument.Name;
        }


        public VSTInstrument SelectedVSTInstrument
        {
            get
            {
                if (SelectedInstrument is VSTInstrument)
                    return (VSTInstrument)SelectedInstrument;
                return null;
            }
        }


        public bool IsGMInstrumentSelected
        {
            get { return (SelectedInstrument != null && SelectedInstrument.GetType() == typeof(GMInstrument)); }
        }

        public bool IsVSTInstrumentSelected
        {
            get { return (SelectedInstrument != null && SelectedInstrument is VSTInstrument); }
        }



        public InstrumentPicker()
        {
            InitializeComponent();
            LoadGMs();
        }


        private void LoadGMs()
        {
            foreach (GMInstrument instrument in GMInstrument.Instruments)
            {
                MenuItem menuItem = new MenuItem() { Header = instrument.Name, DataContext = instrument };
                menuItem.Click += (sender, args) =>
                {
                    VstSelected = false;
                    SelectedInstrument = (GMInstrument)(((MenuItem)sender).DataContext);
                };
                GmHead.Items.Add(menuItem);
            }
        }


        public void LoadVSTs(MelodistDB db)
        {
            foreach(VSTInstrument instrument in VSTInstrument.GetAll(db))
            {
                MenuItem menuItem = new MenuItem() { Header = instrument.Name, DataContext = instrument };
                menuItem.Click += (sender, args) =>
                {
                    VstSelected = true;
                    SelectedInstrument = (GMInstrument)(((MenuItem)sender).DataContext);
                };
                VstHead.Items.Add(menuItem);
            }
        }


        private void None_Click(object sender, RoutedEventArgs e)
        {
            VstSelected = false;
            SelectedInstrument = null;
        }

    }
}
