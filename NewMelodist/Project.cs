﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class Project
    {

        public static Project Instance { get; private set; }

        public Song Song { get; private set; }

        public string FilePath { get; private set; }


        public static string ProjectsFolder
        {
            get
            {
                if(string.IsNullOrWhiteSpace(Properties.Settings.Default.ProjectsFolder))
                {
                    Properties.Settings.Default.ProjectsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Melodist Projects");
                    Properties.Settings.Default.Save();
                }
                return Properties.Settings.Default.ProjectsFolder;
            }
            set
            {
                Properties.Settings.Default.ProjectsFolder = value;
                Properties.Settings.Default.Save();
            }
        }


        private Project()
        {
            MelodistDB db = MelodistDB.Instance;
        }


        public static Project StartNew(string filePath, string songName)
        {
            Project project = new Project();
            project.FilePath = filePath;
            project.Song = new Song(songName);
            Instance = project;
            project.Save();
            return project;
        }


        public static Project Load(string filePath)
        {
            string fileContents = File.ReadAllText(filePath);
            ProjectParse parse = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectParse>(fileContents);
            parse.FilePath = filePath;
            return Load(parse);
        }


        public static Project Load(ProjectParse projectParse)
        {
            Project project = new Project();
            project.FilePath = projectParse.FilePath;
            project.Song = new Song(projectParse.Song);
            Instance = project;
            return project;
        }


        public void Save(string filePath = null)
        {
            filePath = string.IsNullOrEmpty(filePath) ? FilePath : filePath;
            ProjectParse saveObj = new ProjectParse(this);
            string saveString = Newtonsoft.Json.JsonConvert.SerializeObject(saveObj);
            File.WriteAllText(filePath, saveString);
        }

    }



    public class ProjectParse
    {
        public SongParse Song;
        public string FilePath;

        public ProjectParse()
        {
        }

        public ProjectParse(Project project)
        {
            FilePath = project.FilePath;
            Song = new SongParse(project.Song);
        }
    }
}
