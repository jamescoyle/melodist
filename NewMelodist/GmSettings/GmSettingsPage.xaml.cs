﻿using CSCore.CoreAudioAPI;
using GeneralMidi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewMelodist.GmSettings
{
    /// <summary>
    /// Interaction logic for GmSettingsPage.xaml
    /// </summary>
    public partial class GmSettingsPage : Window
    {

        public GmSettingsPage()
        {
            InitializeComponent();

            LatencyText.Text = Properties.Settings.Default.GmLatencySeconds.ToString();

            foreach(string midiDeviceName in MidiDevice.GetDeviceNames())
            {
                MidiDeviceCombo.Items.Add(midiDeviceName);
                if (midiDeviceName == Properties.Settings.Default.DefaultGmDevice)
                    MidiDeviceCombo.SelectedItem = midiDeviceName;
            }
        }


        private void CalcLatencyBtn_Click(object sender, RoutedEventArgs e)
        {
            CalcLatencyBtn.IsEnabled = false;
            MessageBox.Show("Before using this, please ensure there are no sounds playing on your computer");

            BackgroundWorker backgroundWorker = new BackgroundWorker();

            backgroundWorker.DoWork += (obj, args) =>
            {
                using (MidiDevice inputDevice = Tools.GetSelectedMidiDevice())
                {
                    using (MMDevice outputDevice = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Console))
                    {
                        using (AudioMeterInformation outputMeter = AudioMeterInformation.FromDevice(outputDevice))
                        {
                            Stopwatch stopwatch = new Stopwatch();

                            inputDevice.SendProgramChange(1, 0);
                            inputDevice.SendNoteOn(1, 60);

                            stopwatch.Start();
                            while (stopwatch.ElapsedMilliseconds < 2000)
                            {
                                if (outputMeter.PeakValue > 0)
                                {
                                    stopwatch.Stop();
                                    break;
                                }
                            }

                            inputDevice.SendAllNotesOff(1);

                            args.Result = stopwatch.ElapsedMilliseconds;
                        }
                    }
                }
            };

            backgroundWorker.RunWorkerCompleted += (obj, args) =>
            {
                CalcLatencyBtn.IsEnabled = true;
                if (args.Error != null)
                    return;
                long latencyMilliseconds = (long)args.Result;
                LatencyText.Text = (latencyMilliseconds / 1000M).ToString();
            };

            backgroundWorker.RunWorkerAsync();
        }


        private void ConfirmBtn_Click(object sender, RoutedEventArgs e)
        {
            decimal latencySeconds = 0;
            if(decimal.TryParse(LatencyText.Text, out latencySeconds))
                Properties.Settings.Default.GmLatencySeconds = latencySeconds;

            Properties.Settings.Default.DefaultGmDevice = MidiDeviceCombo.Text;
            Properties.Settings.Default.Save();
            MidiDevice.Latency = Properties.Settings.Default.GmLatencySeconds;
            Close();
        }

    }
}
