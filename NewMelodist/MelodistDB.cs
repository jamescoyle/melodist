﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMelodist
{
    public class MelodistDB
    {

        private static string DbFile
        {
            get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Melodist", "database.sqlite"); }
        }


        private static MelodistDB _instance;
        public static MelodistDB Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (!File.Exists(DbFile))
                        SQLiteConnection.CreateFile(DbFile);
                    _instance = new MelodistDB();

                    SQLiteConnection conn = _instance.NewConnection();
                    try
                    {
                        conn.Open();
                        VSTInstrument.CreateTable(_instance);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                return _instance;
            }
        }


        #region Querying methods
        private SQLiteConnection NewConnection()
        {
            return new SQLiteConnection(@"Data Source=" + DbFile + ";Version=3;");
        }
        
        public int NonQuery(string query)
        {
            SQLiteCommand command = NewConnection().CreateCommand();
            command.CommandText = query;
            return NonQuery(command);
        }

        public int NonQuery(SQLiteCommand command)
        {
            try
            {
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
            finally
            {
                command.Connection.Close();
            }
        }

        public DataTable Query(string query)
        {
            SQLiteCommand command = NewConnection().CreateCommand();
            command.CommandText = query;
            return Query(command);
        }

        public DataTable Query(SQLiteCommand command)
        {
            try
            {
                command.Connection.Open();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                return dataTable;
            }
            finally
            {
                command.Connection.Close();
            }
        }

        public object SingleQuery(string query)
        {
            SQLiteCommand command = NewConnection().CreateCommand();
            command.CommandText = query;
            return SingleQuery(command);
        }

        public object SingleQuery(SQLiteCommand command)
        {
            try
            {
                command.Connection.Open();
                return command.ExecuteScalar();
            }
            finally
            {
                command.Connection.Close();
            }
        }
        #endregion Querying methods

    }



    public interface IDatabaseModel
    {
        int Save(MelodistDB db);

        int Delete(MelodistDB db);
    }
}
