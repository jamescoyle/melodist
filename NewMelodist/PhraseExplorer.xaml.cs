﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewMelodist
{
    /// <summary>
    /// Interaction logic for PhraseExplorer.xaml
    /// </summary>
    public partial class PhraseExplorer : UserControl
    {
        public PhraseExplorer()
        {
            InitializeComponent();
            if (Project.Instance == null)
                return;

            ProjectView.RootPath = Project.Instance.FilePath;
        }
    }
}
