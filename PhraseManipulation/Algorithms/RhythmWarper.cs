﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhraseManipulation.Algorithms.Tools;

namespace PhraseManipulation.Algorithms
{
    public class RhythmWarper : IPhraseWarper
    {

        public Phrase Modifier { get; set; }


        public double TargetDistancePerNote { get; set; }


        public double DistanceAllowance { get; set; }


        public Rational MaxReplacementLength { get; set; }



        public RhythmWarper(Phrase modifier, Rational maxReplacementLength, double targetDistancePerNote = 0, double distanceAllowance = 0)
        {
            Modifier = modifier;
            MaxReplacementLength = maxReplacementLength;
            TargetDistancePerNote = targetDistancePerNote;
            DistanceAllowance = distanceAllowance;
        }



        public Phrase Run(Phrase input)
        {
            List<RhythmReplacement> replacements = InputToModifierComparison(input, new RunPlugin(this)).Replacements.OrderBy(x => x.StartIndex).ToList();

            //Use rhythm and replacements to compile output rhythm
            Phrase phrase = new Phrase();
            phrase.CopyProperties(input);
            ContourStepLine contourLine = new ContourStepLine(input);

            List<Note> inputNotes = input.Notes.OrderBy(x => x.StartBeat).ToList();
            for (int i = 0; i < inputNotes.Count; i++)
            {
                RhythmReplacement replacement = replacements.FirstOrDefault(x => x.StartIndex == i);
                if (replacement == null)
                    phrase.AddNote(inputNotes[i].Copy());
                
                else
                {
                    Rational replacementStartBeat = inputNotes[replacement.StartIndex].StartBeat;
                    Rational replacementOffset = replacement.Replacement[0].StartBeat;

                    replacement.Replacement.ForEach(x => phrase.AddNote(new Note(
                        new Pitch(contourLine.GetModePitchAt(replacementStartBeat + x.StartBeat - replacementOffset, replacementStartBeat + x.EndBeat - replacementOffset)),
                        replacementStartBeat + x.StartBeat - replacementOffset,
                        x.Duration
                    )));
                    i = replacement.EndIndex;
                }
            }
            foreach (Chord chord in input.Chords)
                phrase.AddChord(chord.Copy());
            
            return phrase;
        }


        public List<double> GetDistanceAnalysis(Phrase input)
        {
            return InputToModifierComparison(input, new DistanceAnalysisPlugin()).Distances;
        }
        

        private T InputToModifierComparison<T>(Phrase input, T plugin) where T : AlgorithmPlugin
        {
            //Make sure both phrases are homophonic
            if (input.IsPolyphonic || Modifier.IsPolyphonic)
                throw PhraseException.ExpectedMonophonicPhrase();

            Phrase modifier = CommonMethods.MultiplyPhraseTempo(Modifier, input.Tempo);

            List<Note> inputNotes = input.Notes.OrderBy(x => x.StartBeat).ToList();
            List<Note> modifierNotes = modifier.Notes.OrderBy(x => x.StartBeat).ToList();

            //Loop through allowable rhythm note counts in decreasing order
            for (int noteCount = inputNotes.Count; noteCount >= 3; noteCount--)
            {
                //Loop through rhythm start note indexes
                for (int startIndex = 0; startIndex < inputNotes.Count; startIndex++)
                {
                    //Make sure we're not going out of the rhythm index bounds
                    int endIndex = startIndex + noteCount - 1;
                    if (endIndex >= inputNotes.Count)
                        break;

                    if (plugin.TrySkipInputSection(startIndex, endIndex))
                        continue;

                    Rational startBeat = inputNotes[startIndex].StartBeat;
                    Rational minBeatCount = inputNotes[endIndex].EndBeat - startBeat;
                    Rational maxBeatCount = (endIndex + 1 >= inputNotes.Count) ?
                        input.EndBeat - startBeat :
                        inputNotes[endIndex + 1].StartBeat - startBeat;

                    if (minBeatCount > MaxReplacementLength)
                        continue;

                    Rhythm subRhythm = new Rhythm(inputNotes.GetRange(startIndex, noteCount).OfType<FiniteTimeObject>().ToList());

                    plugin.BeforeModifierLoop();

                    //Loop through modifier sub-rhythm start notes
                    for (int k = 0; k <= modifierNotes.Count - 3; k++)
                    {
                        Rational modStartBeat = modifierNotes[k].StartBeat;

                        //Loop through modifier sub-rhythm end notes in reverse order, to go longest to shortest
                        for (int l = modifierNotes.Count - 1; l >= k + 2; l--)
                        {
                            Rational modEndBeat = modifierNotes[l].EndBeat;

                            //Make sure mod sub rhythm beat count falls within min & max beat count
                            if (modEndBeat - modStartBeat < minBeatCount || modEndBeat - modStartBeat > Rational.Min(maxBeatCount, MaxReplacementLength))
                                continue;

                            //Record per note distance between sub-rhythm and mod sub-rhythm
                            Rhythm modSubRhythm = new Rhythm(modifierNotes.GetRange(k, l - k + 1).OfType<FiniteTimeObject>().ToList());
                            double perNoteDistance = subRhythm.Distance(modSubRhythm) / subRhythm.Pulses.Count;

                            plugin.UseSubRhythmDistance(subRhythm, modSubRhythm, perNoteDistance);
                        }
                    }

                    plugin.AfterModifierLoop(startIndex, endIndex);
                }
            }
            return plugin;
        }


        public void UseRecommendedDistanceParameters(List<double> distances)
        {
            distances.Sort();

            int nonZeroCount = distances.Count(x => x > 0);
            if(nonZeroCount == 0)
            {
                TargetDistancePerNote = 0;
                DistanceAllowance = 0;
                return;
            }

            int firstNonZeroIndex = distances.Count - nonZeroCount;
            int endIndex = firstNonZeroIndex + (int)Math.Round(nonZeroCount * 0.1);

            double min = distances[firstNonZeroIndex];
            double max = distances[endIndex];

            TargetDistancePerNote = (min + max) / 2;
            DistanceAllowance = (max - min) / 2;
        }


        public void UseRecommendedDistanceParameters(Phrase input)
        {
            UseRecommendedDistanceParameters(GetDistanceAnalysis(input));
        }



        private abstract class AlgorithmPlugin
        {
            public abstract bool TrySkipInputSection(int inputStartIndex, int inputEndIndex);

            public abstract void BeforeModifierLoop();

            public abstract void UseSubRhythmDistance(Rhythm inputSubrhythm, Rhythm modSubrhythm, double perNoteDistance);

            public abstract void AfterModifierLoop(int inputStartIndex, int inputEndIndex);
        }


        private class RunPlugin : AlgorithmPlugin
        {
            public List<RhythmReplacement> Replacements { get; private set; }

            private RhythmWarper _rhythmWarper;

            private double _bestDistance;
            private Rhythm _bestMatch;

            public RunPlugin(RhythmWarper rhythmWarper)
            {
                Replacements = new List<RhythmReplacement>();
                _rhythmWarper = rhythmWarper;
            }

            public override bool TrySkipInputSection(int inputStartIndex, int inputEndIndex)
            {
                return (Replacements.FirstOrDefault(x => x.EndIndex >= inputStartIndex && x.StartIndex <= inputEndIndex) != null);
            }

            public override void BeforeModifierLoop()
            {
                _bestDistance = double.MaxValue / 2;
                _bestMatch = null;
            }

            public override void UseSubRhythmDistance(Rhythm inputSubrhythm, Rhythm modSubrhythm, double perNoteDistance)
            {
                if (Math.Abs(perNoteDistance - _rhythmWarper.TargetDistancePerNote) < Math.Abs(_bestDistance - _rhythmWarper.TargetDistancePerNote))
                {
                    _bestDistance = perNoteDistance;
                    _bestMatch = modSubrhythm;
                }
            }

            public override void AfterModifierLoop(int inputStartIndex, int inputEndIndex)
            {
                if (_bestMatch != null && Math.Abs(_bestDistance - _rhythmWarper.TargetDistancePerNote) < _rhythmWarper.DistanceAllowance)
                    Replacements.Add(new RhythmReplacement(inputStartIndex, inputEndIndex, _bestMatch.Pulses.ToList()));
            }
        }


        private class DistanceAnalysisPlugin : AlgorithmPlugin
        {
            public List<double> Distances { get; private set; }

            public DistanceAnalysisPlugin()
            {
                Distances = new List<double>();
            }

            public override bool TrySkipInputSection(int inputStartIndex, int inputEndIndex)
            {
                return false;
            }

            public override void BeforeModifierLoop()
            {
            }

            public override void UseSubRhythmDistance(Rhythm inputSubrhythm, Rhythm modSubrhythm, double perNoteDistance)
            {
                Distances.Add(perNoteDistance);
            }

            public override void AfterModifierLoop(int inputStartIndex, int inputEndIndex)
            {
            }
        }



        private class RhythmReplacement
        {
            public int StartIndex { get; private set; }
            public int EndIndex { get; private set; }
            public List<FiniteTimeObject> Replacement { get; private set; }

            public RhythmReplacement(int startIndex, int endIndex, List<FiniteTimeObject> replacement)
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
                Replacement = replacement;
            }
        }

    }
}
