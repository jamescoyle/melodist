﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    public class ChordSuggester2
    {

        public double MinMelodyDissonance { get; set; }

        public double? MaxMelodyDissonance { get; set; }


        public double MinScaleDistance { get; set; }

        public double? MaxScaleDistance { get; set; }



        public ChordSuggester2(double minMelodyDissonance = 0, double? maxMelodyDissonance = null, double minScaleDistance = 0, double? maxScaleDistance = null)
        {
            MinMelodyDissonance = minMelodyDissonance;
            MaxMelodyDissonance = maxMelodyDissonance;
            MinScaleDistance = minScaleDistance;
            MaxScaleDistance = maxScaleDistance;
        }


        public List<ChordSuggestion2> Run(Phrase input, Chord chord)
        {
            List<ChordSuggestion2> output = new List<ChordSuggestion2>();

            foreach(ChordDef chordDef in ChordDef.ChordDefs)
            {
                for(int i = 0; i < 12; i++)
                {
                    Chord newChord = new Chord(chordDef, i, chord.StartBeat, chord.Duration);

                    double scaleDistance = input.Scale.Distance(newChord);
                    if (scaleDistance < MinScaleDistance || (MaxScaleDistance != null && scaleDistance > MaxScaleDistance.Value))
                        continue;

                    double melodyDissonance = 0;
                    foreach (Note note in input.Notes)
                    {
                        Rational noteOverlap = chord.BeatOverlap(note);
                        if(noteOverlap > 0)
                            melodyDissonance += newChord.Distance(note.Pitch) * noteOverlap;
                    }

                    if (melodyDissonance < MinMelodyDissonance || (MaxMelodyDissonance != null && melodyDissonance > MaxMelodyDissonance.Value))
                        continue;

                    output.Add(new ChordSuggestion2(newChord, melodyDissonance, scaleDistance));
                }
            }
            return output;
        }

    }



    public class ChordSuggestion2
    {
        public Chord Chord { get; private set; }

        public double MelodyDissonance { get; private set; }

        public double ScaleDistance { get; private set; }


        public ChordSuggestion2(Chord chord, double melodyDissonance, double scaleDistance)
        {
            Chord = chord;
            MelodyDissonance = melodyDissonance;
            ScaleDistance = scaleDistance;
        }
    }
}
