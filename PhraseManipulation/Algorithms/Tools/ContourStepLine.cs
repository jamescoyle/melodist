﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms.Tools
{
    /// <summary>
    /// Represents the contour of a monphonic phrase as a graph of horizontal 'steps'
    /// </summary>
    public class ContourStepLine
    {

        public Phrase Phrase { get; private set; }

        public List<ContourLinePoint> Points { get; private set; }



        public ContourStepLine(Phrase phrase)
        {
            if (phrase.IsPolyphonic)
                throw PhraseException.ExpectedMonophonicPhrase();

            Phrase = phrase;

            Points = new List<ContourLinePoint>();
            foreach (Note note in Phrase.Notes)
                Points.Add(new ContourLinePoint(note.StartBeat, note.Pitch.Value));
            Points = Points.OrderBy(x => x.Beat).ToList();

            if (Points.Count == 0)
                return;

            Points[0].Beat = Phrase.StartBeat;
            for (int i = Points.Count - 1; i > 0; i--)
            {
                if (Points[i].Pitch == Points[i - 1].Pitch)
                    Points.RemoveAt(i);
            }
        }



        public int GetPitchAt(Rational beat)
        {
            beat = Phrase.MapBeat(beat);

            if (Points.Count == 1)
                return Points[0].Pitch;

            for (int i = 1; i < Points.Count; i++)
            {
                if (beat < Points[i].Beat)
                    return Points[i - 1].Pitch;
            }
            return Points[Points.Count - 1].Pitch;
        }



        public int GetModePitchAt(Rational startBeat, Rational endBeat)
        {
            startBeat = Phrase.MapBeat(startBeat);
            endBeat = Phrase.MapBeat(endBeat);

            List<ContourLinePoint> pitchDurations = null;
            if (startBeat <= endBeat)
                pitchDurations = GetPitchDurations(startBeat, endBeat);

            else
            {
                pitchDurations = GetPitchDurations(startBeat, Phrase.EndBeat);
                pitchDurations.AddRange(GetPitchDurations(Phrase.StartBeat, endBeat));
            }

            return pitchDurations.OrderByDescending(x => x.Beat).ToList()[0].Pitch;
        }



        private List<ContourLinePoint> GetPitchDurations(Rational startBeat, Rational endBeat)
        {
            //Build a list of contour line points to work from
            List<ContourLinePoint> pointsInRange = new List<ContourLinePoint>();
            pointsInRange.Add(new ContourLinePoint(startBeat, GetPitchAt(startBeat)));
            Points.Where(x => x.Beat > startBeat && x.Beat < endBeat).ToList()
                .ForEach(x => pointsInRange.Add(x));

            //Create a second list of contour line points to record the duration of each pitch
            List<ContourLinePoint> pitchDurations = new List<ContourLinePoint>();
            for (int i = 0; i < pointsInRange.Count; i++)
            {
                ContourLinePoint pitchDuration = pitchDurations.FirstOrDefault(x => x.Pitch == pointsInRange[i].Pitch);
                Rational pointEndBeat = (i == pointsInRange.Count - 1) ? endBeat : pointsInRange[i + 1].Beat;

                if (pitchDuration == null)
                    pitchDurations.Add(new ContourLinePoint(pointEndBeat - pointsInRange[i].Beat, pointsInRange[i].Pitch));

                else
                    pitchDuration.Beat += pointEndBeat - pointsInRange[i].Beat;
            }
            return pitchDurations;
        }

    }



    public class ContourLinePoint
    {
        public Rational Beat { get; set; }
        public int Pitch { get; set; }

        public ContourLinePoint(Rational beat, int pitch)
        {
            Beat = beat;
            Pitch = pitch;
        }
    }
}
