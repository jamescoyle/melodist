﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    internal class CommonMethods
    {

        private static Random _random = new Random();


        internal static double NoteChordsDistance(Note note, List<Chord> chords)
        {
            double distance = 0;
            foreach(Chord chord in chords)
            {
                Rational chordLength = chord.BeatOverlap(note);
                distance += chord.Distance(note.Pitch) * chordLength / note.Duration;
            }
            return distance;
        }


        internal static double ChordNotesDistance(Chord chord, List<Note> notes)
        {
            double distance = 0;
            foreach(Note note in notes)
            {
                Rational noteLength = chord.BeatOverlap(note);
                distance += chord.Distance(note.Pitch) * noteLength / chord.Duration;
            }
            return distance;
        }


        internal static List<Pitch> GetAllowedPitchesOverChords(List<Chord> chords, Scale scale)
        {
            int[] allowedPitches = new int[12];
            foreach (Chord noteChord in chords)
            {
                foreach (Pitch scaleDegree in scale.GetClosestScaleWithChord(noteChord).Pitches)
                    allowedPitches[scaleDegree.Value % 12]++;
            }
            List<Pitch> output = new List<Pitch>();
            for(int i = 0; i < allowedPitches.Length; i++)
            {
                if (allowedPitches[i] == chords.Count)
                    output.Add(i);
            }
            return output;
        }


        internal static Phrase LoopPhraseToLCMDuration(Phrase input, params Phrase[] durations)
        {
            if (durations.Length == 0)
                return input.CreateLoop(1);

            List<Rational> rationals = durations.Select(x => x.Duration).ToList();
            rationals.Add(input.Duration);

            Rational lcm = NumberTheory.LCM(rationals);
            return input.CreateLoop((lcm / input.Duration).Round());
        }


        internal static Note ReharmoniseNote(Note note, List<Chord> chords, Scale scale, double targetNoteChordsDistance, int maxPitchMovement, double pitchStepPenalty)
        {
            List<Pitch> allowedPitches = GetAllowedPitchesOverChords(chords, scale);

            List<int> pitchOffsets = new List<int>();
            for (int i = 1; i <= maxPitchMovement; i++)
            {
                pitchOffsets.Add(i);
                pitchOffsets.Add(-i);
            }
            if (_random.Next(2) == 0)
            {
                for (int i = 0; i < pitchOffsets.Count; i++)
                    pitchOffsets[i] *= -1;
            }

            Note bestNote = new Note(note.Pitch, note.StartBeat, note.Duration, note.Velocity);
            double bestDistance = NoteChordsDistance(bestNote, chords);

            foreach (int pitchOffset in pitchOffsets)
            {
                Note newNote = new Note(note.Pitch + pitchOffset, note.StartBeat, note.Duration, note.Velocity);
                if (allowedPitches.Any(x => x.Value == newNote.Pitch.Value % 12))
                {
                    double newNoteDistance = CommonMethods.NoteChordsDistance(newNote, chords);
                    newNoteDistance *= Math.Pow(pitchStepPenalty, Math.Abs(pitchOffset));
                    if (Math.Abs(newNoteDistance - targetNoteChordsDistance) < Math.Abs(bestDistance - targetNoteChordsDistance))
                    {
                        bestNote = newNote;
                        bestDistance = newNoteDistance;
                    }
                }
            }

            return bestNote;
        }


        internal static Phrase MultiplyPhraseTempo(Phrase phrase, int targetTempo)
        {
            Rational multiplication;
            if(phrase.Tempo < targetTempo)
                multiplication = new Rational(targetTempo, phrase.Tempo).Round();
            else
            {
                int division = new Rational(phrase.Tempo, targetTempo).Round();
                multiplication = new Rational(1, division);
            }

            Phrase output = new Phrase();
            output.CopyProperties(phrase);
            output.Duration *= multiplication;
            output.StartBeat *= multiplication;
            output.Tempo = (output.Tempo * multiplication).Round();
            phrase.Notes.ForEach(x => output.AddNote(new Note(x.Pitch, x.StartBeat * multiplication, x.Duration * multiplication, x.Velocity)));
            phrase.Chords.ForEach(x => output.AddChord(new Chord(x.ChordDef, x.Root, x.StartBeat * multiplication, x.Duration * multiplication)));
            return output;
        }

    }
}
