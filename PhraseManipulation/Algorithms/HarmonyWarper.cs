﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    public class HarmonyWarper : IPhraseWarper
    {

        public Phrase Harmony { get; set; }


        public int MaxTonalAdjustment { get; set; }


        public double PitchStepPenalty { get; set; }



        public HarmonyWarper(Phrase harmony, int maxTonalAdjustment, double pitchStepPenalty)
        {
            Harmony = harmony;
            MaxTonalAdjustment = maxTonalAdjustment;
            PitchStepPenalty = pitchStepPenalty;
        }



        public Phrase Run(Phrase input)
        {
            //Make sure that the harmony and input have the same beats per bar
            Phrase harmony = CommonMethods.MultiplyPhraseTempo(Harmony, input.Tempo).MapBeatsPerBar(input.BeatsPerBar);

            //Make sure the input phrase length is a multiple of the harmony phrase length
            input = CommonMethods.LoopPhraseToLCMDuration(input, harmony);

            //Find which scale the harmony is being played in
            Scale scale = Scale.GetBestFit(harmony.Chords.ToList());

            //Set up the phrase which will be written to
            Phrase output = new Phrase();
            output.CopyProperties(input);

            //Populate chords of output phrase from the harmony phrase
            for(int i = 0; true; i++)
            {
                bool chordAdded = false;

                foreach(Chord harmonyChord in harmony.Chords)
                {
                    Rational chordStart = harmonyChord.StartBeat + (i * harmony.Duration);
                    chordStart = output.MapBeat(chordStart);
                    Rational chordEnd = Rational.Min(chordStart + harmonyChord.Duration, output.EndBeat);

                    if (!output.Chords.Any(x => x.StartBeat == chordStart && x.EndBeat == chordEnd))
                    {
                        output.AddChord(new Chord(harmonyChord.ChordDef, harmonyChord.Root, chordStart, chordEnd - chordStart));
                        chordAdded = true;
                    }

                    if(chordStart + harmonyChord.Duration > output.EndBeat)
                    {
                        Rational chordDuration = chordStart + harmonyChord.Duration - output.EndBeat;
                        chordStart = output.StartBeat;
                        chordEnd = Rational.Min(chordStart + chordDuration, output.EndBeat);

                        if(!output.Chords.Any(x => x.StartBeat == chordStart && x.EndBeat == chordEnd))
                        {
                            output.AddChord(new Chord(harmonyChord.ChordDef, harmonyChord.Root, chordStart, chordEnd - chordStart));
                            chordAdded = true;
                        }
                    }
                }

                if (!chordAdded)
                    break;
            }
            
            //Reharmonise the input notes to the new harmony
            foreach (Note inputNote in input.Notes)
            {
                List<Chord> oldNoteChords = input.GetChordsInBeatRange(inputNote.StartBeat, inputNote.EndBeat);
                List<Chord> newNoteChords = output.GetChordsInBeatRange(inputNote.StartBeat, inputNote.EndBeat);
                double targetDistance = CommonMethods.NoteChordsDistance(inputNote, oldNoteChords);

                Note newNote = CommonMethods.ReharmoniseNote(inputNote, newNoteChords, scale, targetDistance, MaxTonalAdjustment, PitchStepPenalty);
                output.AddNote(newNote);
            }

            return output;
        }

    }
}
