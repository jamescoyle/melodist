﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    public class ChordProgressioner : IPhraseWarper
    {

        public double MinScaleDistance { get; set; }

        public double MaxScaleDistance { get; set; }

        public double MinDissonance { get; set; }

        public double MaxDissonance { get; set; }

        public int MinChordMovement { get; set; }

        public int MaxChordMovement { get; set; }

        public Phrase HarmonyRhythm { get; set; }

        public List<ChordDef> ChordDefs { get; set; }



        public Phrase Run(Phrase input)
        {
            //Prepare phrase that we will generate new chords for, including placeholder chords in the positions where they will appear
            Phrase output = new Phrase();
            output.CopyProperties(input);
            input.Notes.ForEach(x => output.AddNote(x.Copy()));
            if(HarmonyRhythm != null)
            {
                Phrase harmonyRhythm = CommonMethods.MultiplyPhraseTempo(HarmonyRhythm, input.Tempo);
                if (harmonyRhythm.BeatsPerBar != input.BeatsPerBar)
                    harmonyRhythm = HarmonyRhythm.MapBeatsPerBar(input.BeatsPerBar);
                foreach (Chord chord in harmonyRhythm.Chords.OrderBy(x => x.StartBeat).ToList())
                    output.AddChord(new Chord(ChordDef.Retrieve("M"), 0, chord.StartBeat, chord.Duration));
            }
            else if(input.Chords.Count > 0)
            {
                foreach (Chord chord in input.Chords.OrderBy(x => x.StartBeat).ToList())
                    output.AddChord(new Chord(ChordDef.Retrieve("M"), 0, chord.StartBeat, chord.Duration));
            }
            else
            {
                Rational i = input.StartBeat;
                while (i < input.EndBeat)
                {
                    Rational endBeat = (i < 0) ? 0 : Rational.Min(input.EndBeat, i + input.BeatsPerBar);
                    output.AddChord(new Chord(ChordDef.Retrieve("M"), 0, i, endBeat - i));
                    i = endBeat;
                }
            }

            //Get list of acceptable chords that can be used
            List<Chord> allowableChords = new List<Chord>();
            foreach(ChordDef chordDef in ChordDefs)
            {
                for(int i = 0; i < 12; i++)
                {
                    Chord chord = new Chord(chordDef, i, 0, 1);
                    double scaleDistance = input.Scale.Distance(chord);
                    if (scaleDistance >= MinScaleDistance && scaleDistance <= MaxScaleDistance)
                        allowableChords.Add(chord);
                }
            }

            //Loop through chords in output phrase, trying to find replacements for each one
            Random random = new Random();
            for (int i = 0; i < output.Chords.Count; i++)
            {
                List<Chord> possibleChords = new List<Chord>();
                foreach(Chord chord in allowableChords)
                {
                    chord.StartBeat = output.Chords[i].StartBeat;
                    chord.Duration = output.Chords[i].Duration;
                    double dissonance = CommonMethods.ChordNotesDistance(chord, input.Notes.ToList());
                    if(dissonance >= MinDissonance && dissonance <= MaxDissonance)
                    {
                        int movement = -1;
                        if (i > 0)
                            movement = ChordMovement(output.Chords[i - 1], chord);
                        if (movement < 0 || (movement >= MinChordMovement && movement <= MaxChordMovement))
                            possibleChords.Add(chord);
                    }
                }
                if (possibleChords.Count == 0)
                    throw new Exception("Overly strict parameters resulted in no chord progression being found");

                Chord newChord = possibleChords[random.Next(0, possibleChords.Count)];
                output.Chords[i].ChordDef = newChord.ChordDef;
                output.Chords[i].Root = newChord.Root;
            }

            return output;
        }



        private int ChordMovement(Chord chordA, Chord chordB)
        {
            if (chordA.Pitches.Count < chordB.Pitches.Count)
                return ChordMovement(chordB, chordA);

            int count = 0;
            foreach(Pitch pitchA in chordA.Pitches)
            {
                int bestMovement = int.MaxValue;
                foreach(Pitch pitchB in chordB.Pitches)
                {
                    int movement = Math.Abs(pitchA.Value - pitchB.Value) % 12;
                    if (movement > 6)
                        movement = 12 - movement;
                    if (movement < bestMovement)
                        bestMovement = movement;
                }
                count += bestMovement;
            }
            return count;
        }

    }
}
