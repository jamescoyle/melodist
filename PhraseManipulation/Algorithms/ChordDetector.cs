﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    public class ChordDetector
    {

        public List<ChordDef> ChordDefs { get; set; }



        public ChordDetector(List<ChordDef> chordDefs = null)
        {
            ChordDefs = (chordDefs == null) ? ChordDef.ChordDefs : chordDefs;
        }



        public List<Chord> Run(List<Note> notes, List<TimeSignature> timeSignatures)
        {
            timeSignatures = timeSignatures.OrderBy(x => x.Beat).ToList();
            TimeSignature lastTimeSignature = timeSignatures.FirstOrDefault();

            List<Chord> chords = new List<Chord>();

            Scale scale = Scale.GetBestFit(notes);

            Rational iStart = (lastTimeSignature == null) ? 0 : lastTimeSignature.Beat;
            Rational iFinal = notes.Max(x => x.EndBeat);

            while (iStart < iFinal)
            {
                lastTimeSignature = timeSignatures.LastOrDefault(x => x.Beat < iStart);
                Rational beatStep = (lastTimeSignature == null) ? 4 : (Rational)lastTimeSignature.QuarterBeatsPerBar;
                Rational iEnd = iStart + beatStep;

                //Collect all the notes that were playing in the iStart - iEnd time period, get the proportions of pitches being played
                List<Note> rangeNotes = notes.Where(x => x.StartBeat < iEnd && x.EndBeat > iStart).ToList();

                List<Note> pitchDurations = new List<Note>();
                foreach (Note note in rangeNotes)
                {
                    Note pitchDuration = pitchDurations.FirstOrDefault(x => Math.Abs(x.Pitch.Value - note.Pitch.Value) % 12 == 0);
                    if (pitchDuration == null)
                    {
                        pitchDuration = new Note(new Pitch(note.Pitch % 12), 0, 0);
                        pitchDurations.Add(pitchDuration);
                    }
                    pitchDuration.Duration += Rational.Min(iEnd, note.EndBeat) - Rational.Max(iStart, note.StartBeat);
                }

                List<Tuple<Pitch, double>> pitchWeights = new List<Tuple<Pitch, double>>();
                pitchDurations.ForEach(x => pitchWeights.Add(new Tuple<Pitch, double>(x.Pitch, (double)x.Duration)));
                double weightSum = pitchWeights.Sum(x => x.Item2);
                for (int i = pitchWeights.Count - 1; i >= 0; i--)
                {
                    if (pitchWeights[i].Item2 < weightSum / 10)
                        pitchWeights.RemoveAt(i);
                }

                Chord bestChord = GetBestChord(pitchWeights, scale);
                bestChord.StartBeat = iStart;
                bestChord.Duration = beatStep;
                chords.Add(bestChord);

                iStart += beatStep;
            }

            for (int i = 0; i < chords.Count - 1; i++)
            {
                if (chords[i].ChordDef.Equals(chords[i + 1].ChordDef) && chords[i].Root.Equals(chords[i + 1].Root))
                {
                    chords[i].Duration += chords[i + 1].Duration;
                    chords.RemoveAt(i + 1);
                }
            }

            return chords;
        }



        private Chord GetBestChord(List<Tuple<Pitch, double>> pitchWeights, Scale scale)
        {
            //Try to find if there are any major or minor triads contained within the pitch weights
            List<Tuple<ChordDef, List<Tuple<Pitch, double>>>> triads = new List<Tuple<ChordDef, List<Tuple<Pitch, double>>>>();
            foreach (Tuple<Pitch, double> root in pitchWeights)
            {
                Tuple<Pitch, double> fifth = pitchWeights.FirstOrDefault(x => x.Item1.Value % 12 == (root.Item1.Value + 7) % 12);
                if (fifth != null)
                {
                    Tuple<Pitch, double> minorThird = pitchWeights.FirstOrDefault(x => x.Item1.Value % 12 == (root.Item1.Value + 3) % 12);
                    if (minorThird != null)
                        triads.Add(new Tuple<ChordDef, List<Tuple<Pitch, double>>>(ChordDef.Retrieve(0, 3, 7), new List<Tuple<Pitch, double>>() { root, minorThird, fifth }));

                    Tuple<Pitch, double> majorThird = pitchWeights.FirstOrDefault(x => x.Item1.Value % 12 == (root.Item1.Value + 4) % 12);
                    if (majorThird != null)
                        triads.Add(new Tuple<ChordDef, List<Tuple<Pitch, double>>>(ChordDef.Retrieve(0, 4, 7), new List<Tuple<Pitch, double>>() { root, majorThird, fifth }));

                }
            }


            List<ChordDef> chordDefs = ChordDefs;
            List<int> roots = Enumerable.Range(0, 12).ToList();


            if (triads.Count > 0)
            {
                //Of all the triads found, get the one with the strongest presence
                Tuple<ChordDef, List<Tuple<Pitch, double>>> bestTriad = null;
                double bestTriadPresence = double.MinValue;
                foreach (Tuple<ChordDef, List<Tuple<Pitch, double>>> triad in triads)
                {
                    double presence = triad.Item2.Sum(x => x.Item2);
                    if (bestTriad == null || presence >= bestTriadPresence)
                    {
                        if (presence == bestTriadPresence)
                        {
                            Chord triadChord = new Chord(triad.Item1, triad.Item2[0].Item1, 0, 1);
                            Chord bestTriadChord = new Chord(bestTriad.Item1, bestTriad.Item2[0].Item1, 0, 1);
                            if (scale.Distance(triadChord) < scale.Distance(bestTriadChord))
                            {
                                bestTriad = triad;
                                bestTriadPresence = presence;
                            }
                        }
                        else
                        {
                            bestTriad = triad;
                            bestTriadPresence = presence;
                        }
                    }
                }

                //Limit the list of chordDefs to search through to only those that contain our preferred triad, also limit roots to only the root of the preferred triad
                chordDefs = ChordDefs.Where(x => x.Contains(bestTriad.Item1)).ToList();
                roots = new List<int>() { bestTriad.Item2[0].Item1.Value % 12 };
            }


            //Find which chord best fits the notes being played
            Chord bestChord = null;
            double bestDistance = double.MaxValue;
            foreach (ChordDef chordDef in chordDefs)
            {
                foreach (int root in roots)
                {
                    Chord chord = new Chord(chordDef, root, 0, 1);
                    double distance = GetChordDistance(chord, pitchWeights, scale);
                    if (bestChord == null || distance < bestDistance)
                    {
                        bestChord = chord;
                        bestDistance = distance;
                    }
                }
            }
            return bestChord;
        }



        private double GetChordDistance(Chord chord, List<Tuple<Pitch, double>> pitchWeights, Scale scale)
        {
            double output = 0;
            foreach (Tuple<Pitch, double> pitchWeight in pitchWeights)
                output += (chord.Distance(pitchWeight.Item1) + 1) * pitchWeight.Item2;

            output *= (scale.Distance(chord) + 1);
            return output;
        }

    }
}
