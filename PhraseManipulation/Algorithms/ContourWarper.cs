﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation.Algorithms
{
    public class ContourWarper : IPhraseWarper
    {

        /// <summary>
        /// The contour which is used to apply the change
        /// </summary>
        public Phrase Contour { get; set; }


        /// <summary>
        /// The number of beats that each block of notes is made up of
        /// </summary>
        public Rational BeatsPerSample { get; set; }


        /// <summary>
        /// The maximum number of steps up or down that will be made when trying to fine-tune a newly contoured note
        /// </summary>
        public int MaxTonalAdjustment { get; set; }


        /// <summary>
        /// The penalty that is applied for each additional step in the tonal adjustment process
        /// A higher number makes the algorithm favour keeping the note close to where it was placed vertically
        /// A value of 1 means there is no penalty for moving away from the placed pitch
        /// </summary>
        public double PitchStepPenalty { get; set; }



        public ContourWarper(Phrase contour, Rational beatsPerSample, int maxTonalAdjustment, double pitchStepPenalty)
        {
            Contour = contour;
            BeatsPerSample = beatsPerSample;
            MaxTonalAdjustment = maxTonalAdjustment;
            PitchStepPenalty = pitchStepPenalty;
        }



        public Phrase Run(Phrase input)
        {
            Phrase contour = CommonMethods.MultiplyPhraseTempo(Contour, input.Tempo).MapBeatsPerBar(input.BeatsPerBar);

            CantusFirmus inputCantus = new CantusFirmus(input, BeatsPerSample);
            CantusFirmus contourCantus = new CantusFirmus(contour, BeatsPerSample);
            
            Phrase output = new Phrase();
            output.CopyProperties(input);
            input.Chords.ForEach(x => output.AddChord(new Chord(x.ChordDef, x.Root, x.StartBeat, x.Duration)));

            Random random = new Random();
            
            foreach(Note note in inputCantus.Notes)
            {
                Interval adjustment = contourCantus.GetPitchAt(note.StartBeat) - note.Pitch;

                foreach(Note inputNote in input.Notes.Where(x => x.StartBeat >= note.StartBeat && x.StartBeat < note.EndBeat))
                {
                    Note shiftedNote = new Note(inputNote.Pitch + adjustment, inputNote.StartBeat, inputNote.Duration, inputNote.Velocity);
                    List<Chord> noteChords = output.GetChordsInBeatRange(shiftedNote.StartBeat, shiftedNote.EndBeat);
                    double targetDistance = CommonMethods.NoteChordsDistance(inputNote, noteChords);

                    Note newNote = CommonMethods.ReharmoniseNote(shiftedNote, noteChords, output.Scale, targetDistance, MaxTonalAdjustment, PitchStepPenalty);
                    output.AddNote(newNote);
                }
            }

            return output;
        }

    }
}
