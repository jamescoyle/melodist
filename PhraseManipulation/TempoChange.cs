﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation
{
    public class TempoChange
    {

        private Rational _beat;
        public event EventHandler BeatChanged;
        public Rational Beat
        {
            get { return _beat; }
            set
            {
                if(_beat != value)
                {
                    _beat = value;
                    BeatChanged?.Invoke(this, null);
                }
            }
        }


        private int _tempo;
        public event EventHandler TempoChanged;
        public int Tempo
        {
            get { return _tempo; }
            set
            {
                if(_tempo != value)
                {
                    _tempo = value;
                    TempoChanged?.Invoke(this, null);
                }
            }
        }



        public TempoChange(Rational beat, int tempo)
        {
            Beat = beat;
            Tempo = tempo;
        }

    }


    public delegate void TempoChangeEventHandler(object sender, TempoChangeEventArgs args);

    public class TempoChangeEventArgs : EventArgs
    {
        public TempoChange TempoChange { get; private set; }

        public TempoChangeEventArgs(TempoChange tempoChange)
        {
            TempoChange = tempoChange;
        }
    }



    public class TempoChangeParse
    {
        public RationalParse Beat;
        public int Tempo;

        public TempoChangeParse()
        {
        }

        public TempoChangeParse(TempoChange tempoChange)
        {
            Beat = new RationalParse(tempoChange.Beat);
            Tempo = tempoChange.Tempo;
        }

        public TempoChange Load()
        {
            return new TempoChange(Beat.Load(), Tempo);
        }
    }
}
