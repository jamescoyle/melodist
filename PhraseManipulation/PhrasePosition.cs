﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyEngine;
using MathPlus;

namespace PhraseManipulation
{
    public class PhrasePosition : FiniteTimeObject
    {

        public Phrase Phrase { get; private set; }

        
        /// <summary>
        /// The number of beats that the phrase is offset from 0 by
        /// </summary>
        public Rational Offset
        {
            get
            {
                return StartBeat - Phrase.StartBeat;
            }
            set
            {
                StartBeat = value + Phrase.StartBeat;
            }
        }

        
        public Rational LoopCount
        {
            get
            {
                return Duration / Phrase.Duration;
            }
            set
            {
                Duration = value * Phrase.Duration;
            }
        }



        public PhrasePosition(Phrase phrase)
            : base(phrase.StartBeat, phrase.Duration)
        {
            Init(phrase, 0, 1);
        }

        public PhrasePosition(Phrase phrase, Rational offset)
            : base(phrase.StartBeat + offset, phrase.Duration)
        {
            Init(phrase, offset, 1);
        }

        public PhrasePosition(Phrase phrase, Rational offset, Rational loopCount)
            : base(phrase.StartBeat + offset, phrase.Duration * loopCount)
        {
            Init(phrase, offset, loopCount);
        }

        private void Init(Phrase phrase, Rational offset, Rational loopCount)
        {
            Phrase = phrase;
            Offset = offset;
            LoopCount = loopCount;
            StartBeatChanged += (sender, args) =>
            {
                Offset = StartBeat - Phrase.StartBeat;
            };
        }



        /// <summary>
        /// Get a list of notes, that reflect both their positions within the phrase and the position of the phrase
        /// </summary>
        public List<Note> GetNotes()
        {
            List<Note> notes = new List<Note>();
            Rational endBeat = EndBeat;

            for(int loopNum = 0; loopNum < LoopCount; loopNum++)
            {
                Rational loopOffset = Offset + (loopNum * Phrase.Duration);
                
                foreach (Note note in Phrase.Notes)
                {
                    Note newNote = new Note(note.Pitch, note.StartBeat + loopOffset, note.Duration, note.Velocity);
                    if (newNote.StartBeat < endBeat)
                    {
                        newNote.EndBeat = Rational.Min(newNote.EndBeat, endBeat);
                        notes.Add(newNote);
                    }
                }
            }
            return notes;
        }


        /// <summary>
        /// Get a list of chords, that reflect both their positions within the phrase and the position of the phrase
        /// </summary>
        /// <returns></returns>
        public List<Chord> GetChords()
        {
            List<Chord> chords = new List<Chord>();
            Rational endBeat = EndBeat;

            for (int loopNum = 0; loopNum < LoopCount; loopNum++)
            {
                Rational loopOffset = Offset + (loopNum * Phrase.Duration);

                foreach(Chord chord in Phrase.Chords)
                {
                    Chord newChord = new Chord(chord.ChordDef, chord.Root, chord.StartBeat + loopOffset, chord.Duration);
                    if(newChord.StartBeat < endBeat)
                    {
                        newChord.EndBeat = Rational.Min(newChord.EndBeat, endBeat);
                        chords.Add(newChord);
                    }
                }
            }
            return chords;
        }

    }



    public class PhrasePositionParse
    {
        public PhraseParse Phrase;
        public RationalParse Offset;
        public RationalParse LoopCount;

        public PhrasePositionParse()
        {
        }

        public PhrasePositionParse(PhrasePosition phrasePosition)
        {
            Phrase = new PhraseParse(phrasePosition.Phrase);
            if (!string.IsNullOrWhiteSpace(Phrase.FilePath))
            {
                Phrase.Notes.Clear();
                Phrase.Chords.Clear();
            }
            Offset = new RationalParse(phrasePosition.Offset);
            LoopCount = new RationalParse(phrasePosition.LoopCount);
        }

        public PhrasePosition Load()
        {
            Phrase phrase = null;
            if (!string.IsNullOrWhiteSpace(Phrase.FilePath))
                phrase = PhraseManipulation.Phrase.Load(Phrase.FilePath);
            else
                phrase = PhraseManipulation.Phrase.Load(Phrase);

            return new PhrasePosition(phrase, Offset.Load(), LoopCount.Load());
        }
    }
}
