﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpExtras;
using MathPlus;

namespace PhraseManipulation
{
    public class Phrase : FiniteTimeObject
    {

        private static MemberListManager<Phrase> _phraseFilesManager = new MemberListManager<Phrase>();
        public static MemberList<Phrase> PhraseFiles { get { return _phraseFilesManager.List; } }


        protected MemberListManager<Note> NotesManager;
        public MemberList<Note> Notes { get { return NotesManager.List; } }


        protected MemberListManager<Chord> ChordsManager;
        public MemberList<Chord> Chords { get { return ChordsManager.List; } }


        private int _beatsPerBar;
        public event EventHandler BeatsPerBarChanged;
        public int BeatsPerBar
        {
            get { lock (Lock) { return _beatsPerBar; } }
            set
            {
                if (value <= 0)
                    throw PhraseException.InvalidBeatsPerBar();
                if(_beatsPerBar != value)
                {
                    lock (Lock) { _beatsPerBar = value; }
                    OnPropertyChanged("BeatsPerBar");
                    BeatsPerBarChanged?.Invoke(this, null);
                }
            }
        }


        private int _tempo;
        public event EventHandler TempoChanged;
        public int Tempo
        {
            get
            {
                lock (Lock) { return _tempo; }
            }
            set
            {
                if (value <= 0)
                    throw PhraseException.InvalidTempo();
                if (_tempo != value)
                {
                    lock (Lock) { _tempo = value; }
                    OnPropertyChanged("Tempo");
                    TempoChanged?.Invoke(this, null);
                }
            }
        }


        /// <summary>
        /// Is there any point where more than one note is playing simulaneously
        /// </summary>
        public bool IsPolyphonic
        {
            get
            {
                List<Note> notes = Notes.ToList();
                for (int i = 0; i < notes.Count; i++)
                {
                    Note note1 = notes[i];
                    for (int j = i + 1; j < notes.Count; j++)
                    {
                        Note note2 = notes[j];
                        if (note1.EndBeat > note2.StartBeat && note1.StartBeat < note2.EndBeat)
                            return true;
                    }
                }
                return false;
            }
        }


        /// <summary>
        /// Doesn't define the conventional meaning of polyrhythms, instead, determines whether there are notes that play over each other but with different start & end times
        /// </summary>
        public bool IsPolyRhythmic
        {
            get
            {
                List<Note> notes = Notes.ToList();
                for (int i = 0; i < notes.Count; i++)
                {
                    Note note1 = notes[i];
                    for (int j = i + 1; j < notes.Count; j++)
                    {
                        Note note2 = notes[j];
                        if (note1.EndBeat > note2.StartBeat && note1.StartBeat < note2.EndBeat && note1.StartBeat != note2.StartBeat && note1.EndBeat != note2.EndBeat)
                            return true;
                    }
                }
                return false;
            }
        }


        private string _name;
        public event EventHandler NameChanged;
        public string Name
        {
            get { return _name; }
            set
            {
                if(_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                    NameChanged?.Invoke(this, null);
                }
            }
        }


        private string _filePath;
        public event EventHandler FilePathChanged;
        public string FilePath
        {
            get
            {
                return _filePath;
            }
            //Changes to the file path of the phrase can only be made at instantiation or saving, at all other times this property is read-only
            private set
            {
                if (string.IsNullOrEmpty(Name) && !string.IsNullOrWhiteSpace(value))
                    Name = System.IO.Path.GetFileName(value).Replace(".phr", "");

                if (_filePath != value)
                {
                    _filePath = value;
                    OnPropertyChanged("FilePath");
                    FilePathChanged?.Invoke(this, null);
                }
            }
        }


        private Scale _scale = null;
        public Scale Scale
        {
            get
            {
                if (_scale == null)
                    _scale = CalculateScale();
                return _scale;
            }
            set
            {
                _scale = value;
            }
        }



        public Phrase()
            : base(0, 16)
        {
            NotesManager = new MemberListManager<Note>();
            ChordsManager = new MemberListManager<Chord>();
            BeatsPerBar = 4;
            Tempo = 120;
        }


        public Phrase(int beatsPerBar, int tempo)
            : base(0, beatsPerBar * 4)
        {
            NotesManager = new MemberListManager<Note>();
            ChordsManager = new MemberListManager<Chord>();
            BeatsPerBar = beatsPerBar;
            Tempo = tempo;
        }


        public static Phrase Load(string filePath)
        {
            Phrase phrase = PhraseFiles.FirstOrDefault(x => x.FilePath == filePath);
            if (phrase != null)
                return phrase;

            string fileContents = System.IO.File.ReadAllText(filePath);
            PhraseParse parse = Newtonsoft.Json.JsonConvert.DeserializeObject<PhraseParse>(fileContents);
            parse.FilePath = filePath;
            return Load(parse);
        }


        public static Phrase Load(PhraseParse phraseParse)
        {
            Phrase phrase = new Phrase()
            {
                Name = phraseParse.Name,
                FilePath = phraseParse.FilePath,
                Tempo = phraseParse.Tempo,
                BeatsPerBar = phraseParse.BeatsPerBar,
                StartBeat = phraseParse.StartBeat.Load(),
                Duration = phraseParse.Duration.Load()
            };
            phrase.NotesManager = new MemberListManager<Note>(phraseParse.Notes.Select(x => x.Load()));
            phrase.ChordsManager = new MemberListManager<Chord>(phraseParse.Chords.Select(x => x.Load()));
            _phraseFilesManager.Add(phrase);
            return phrase;
        }


        /// <summary>
        /// Copy the properties (though not the notes or chords) from the source phrase into the calling phrase
        /// </summary>
        /// <param name="source"></param>
        public void CopyProperties(Phrase source)
        {
            StartBeat = source.StartBeat;
            Duration = source.Duration;
            BeatsPerBar = source.BeatsPerBar;
            Tempo = source.Tempo;
        }


        /// <summary>
        /// Creates a new phrase which is a copy of this one, but repeated some n number of times
        /// </summary>
        /// <param name="loopCount"></param>
        /// <returns></returns>
        public Phrase CreateLoop(int loopCount)
        {
            if (loopCount <= 0)
                throw new ArgumentException();

            Phrase newPhrase = new Phrase();
            newPhrase.CopyProperties(this);
            newPhrase.EndBeat = StartBeat + (Duration * loopCount);

            for (int i = 0; i < loopCount; i++)
            {
                foreach (Note note in Notes)
                {
                    newPhrase.AddNote(new Note(
                        note.Pitch,
                        (i * Duration) + note.StartBeat,
                        note.Duration, 
                        note.Velocity
                    ));
                }
                foreach (Chord chord in Chords)
                {
                    newPhrase.AddChord(new Chord(
                        chord.ChordDef,
                        chord.Root,
                        (i * Duration) + chord.StartBeat,
                        chord.Duration
                    ));
                }
            }
            return newPhrase;
        }


        /// <summary>
        /// Returns a copy of the phrase, but represented within a different time signature
        /// For example, if the phrase had 4 beats per bar, calling MapBeatsPerBar(3) would create a copy phrase, with all notes 3/4 shorter and with 3 beats per bar
        /// </summary>
        /// <param name="beatsPerBar"></param>
        /// <returns></returns>
        public Phrase MapBeatsPerBar(int beatsPerBar)
        {
            Rational multiplier = new Rational(beatsPerBar, BeatsPerBar);

            Phrase phrase = new Phrase();
            phrase.CopyProperties(this);

            phrase.StartBeat = phrase.StartBeat * multiplier;
            phrase.EndBeat = phrase.EndBeat * multiplier;

            Notes.ForEach(x => phrase.AddNote(new Note(x.Pitch, x.StartBeat * multiplier, x.Duration * multiplier, x.Velocity)));

            Chords.ForEach(x => phrase.AddChord(new Chord(x.ChordDef, x.Root, x.StartBeat * multiplier, x.Duration * multiplier)));

            return phrase;
        }


        public void AddNote(Note note)
        {
            lock (Lock) { NotesManager.Add(note); }
        }

        public void RemoveNote(Note note)
        {
            lock (Lock) { NotesManager.Remove(note); }
        }

        public void ClearNotes()
        {
            lock (Lock) { NotesManager.Clear(); }
        }


        public void AddChord(Chord chord)
        {
            lock (Lock) { ChordsManager.Add(chord); }
        }

        public void RemoveChord(Chord chord)
        {
            lock (Lock) { ChordsManager.Remove(chord); }
        }

        public void ClearChords()
        {
            lock (Lock) { ChordsManager.Clear(); }
        }


        public Scale CalculateScale()
        {
            List<Note> notes = Notes.ToList();
            foreach (Chord chord in Chords)
            {
                foreach (Pitch pitch in chord.Pitches)
                    notes.Add(new Note(pitch, chord.StartBeat, chord.Duration));
            }
            return Scale.GetBestFit(notes);
        }


        public List<Note> GetNotesInBeatRange(Rational startBeat, Rational endBeat)
        {
            startBeat = MapBeat(startBeat);
            endBeat = MapBeat(endBeat);

            if (startBeat < endBeat)
                return Notes.Where(x => x.EndBeat > startBeat && x.StartBeat < endBeat).ToList();

            return Notes.Where(x => x.EndBeat > startBeat || x.StartBeat < endBeat).ToList();
        }


        public List<Chord> GetChordsInBeatRange(Rational startBeat, Rational endBeat)
        {
            startBeat = MapBeat(startBeat);
            endBeat = MapBeat(endBeat);

            if (startBeat < endBeat)
                return Chords.Where(x => x.EndBeat > startBeat && x.StartBeat < endBeat).ToList();

            return Chords.Where(x => x.EndBeat > startBeat || x.StartBeat < endBeat).ToList();
        }


        public void Save(string filePath = null)
        {
            //Make sure have got a file path to save to
            filePath = (string.IsNullOrWhiteSpace(filePath)) ? FilePath : filePath;
            if (string.IsNullOrWhiteSpace(filePath))
                throw PhraseException.InvalidFilePath();

            //Make sure the save won't result in us overwriting the file of another phrase in use by the program
            if(filePath != FilePath)
            {
                if (PhraseFiles.Any(x => x.FilePath == filePath))
                    throw PhraseException.InvalidOverwriteAttempt();
            }

            string oldFilePath = FilePath;
            try
            {
                FilePath = filePath;
                PhraseParse parse = new PhraseParse(this);
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(FilePath));
                System.IO.File.WriteAllText(FilePath, Newtonsoft.Json.JsonConvert.SerializeObject(parse));
                if (!PhraseFiles.Contains(this))
                    _phraseFilesManager.Add(this);
            }
            catch(Exception ex)
            {
                FilePath = oldFilePath;
                throw;
            }
        }


        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Name))
                return Name;
            if (!string.IsNullOrEmpty(FilePath))
                return FilePath;
            return "Unnamed phrase";
        }

    }



    public delegate void PhraseEventHandler(object sender, PhraseEventArgs e);

    public class PhraseEventArgs : EventArgs
    {
        public Phrase Phrase { get; private set; }

        public PhraseEventArgs(Phrase phrase)
        {
            Phrase = phrase;
        }
    }



    public class PhraseParse : FiniteTimeObjectParse
    {
        public List<NoteParse> Notes;
        public List<ChordParse> Chords;
        public int BeatsPerBar;
        public int Tempo;
        public string Name;
        public string FilePath;

        public PhraseParse()
        {
        }

        public PhraseParse(Phrase phrase)
            : base(phrase)
        {
            Notes = new List<NoteParse>();
            phrase.Notes.ForEach(x => Notes.Add(new NoteParse(x)));

            Chords = new List<ChordParse>();
            phrase.Chords.ForEach(x => Chords.Add(new ChordParse(x)));

            BeatsPerBar = phrase.BeatsPerBar;
            Tempo = phrase.Tempo;
            Name = phrase.Name;
            FilePath = phrase.FilePath;
        }

        /// <summary>
        /// This method is not implemented, to extract the phrase from this object, call Phrase.Load()
        /// </summary>
        /// <returns></returns>
        public new Phrase Load()
        {
            throw new NotImplementedException();
        }
    }
}
