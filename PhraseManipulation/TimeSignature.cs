﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation
{
    public class TimeSignature
    {

        private Rational _beat;
        public event EventHandler BeatChanged;
        public Rational Beat
        {
            get { return _beat; }
            set
            {
                if(_beat != value)
                {
                    _beat = value;
                    BeatChanged?.Invoke(this, null);
                }
            }
        }


        private int _numerator;
        public event EventHandler NumeratorChanged;
        public int Numerator
        {
            get { return _numerator; }
            set
            {
                if (value <= 0)
                    throw PhraseException.InvalidTimeSignature();

                if (_numerator != value)
                {
                    _numerator = value;
                    NumeratorChanged?.Invoke(this, null);
                }
            }
        }


        private int _denominator;
        public event EventHandler DenominatorChanged;
        public int Denominator
        {
            get { return _denominator; }
            set
            {
                //If value <= 0 or value isn't a power of 2 then throw exception
                if (value <= 0 || (value & (value - 1)) != 0)
                    throw PhraseException.InvalidTimeSignature();

                if (_denominator != value)
                {
                    _denominator = value;
                    DenominatorChanged?.Invoke(this, null);
                }
            }
        }


        public Rational QuarterBeatsPerBar
        {
            get
            {
                return new Rational(Numerator, Denominator / 4);
            }
        }


        public TimeSignature(Rational beat, int numerator, int denominator)
        {
            Beat = beat;
            Numerator = numerator;
            Denominator = denominator;
        }

    }



    public delegate void TimeSignatureEventHandler(object sender, TimeSignatureEventArgs args);

    public class TimeSignatureEventArgs : EventArgs
    {
        public TimeSignature TimeSignature { get; private set; }

        public TimeSignatureEventArgs(TimeSignature timeSignature)
        {
            TimeSignature = timeSignature;
        }
    }



    public class TimeSignatureParse
    {
        public RationalParse Beat;
        public int Numerator;
        public int Denominator;

        public TimeSignatureParse()
        {
        }

        public TimeSignatureParse(TimeSignature timeSignature)
        {
            Beat = new RationalParse(timeSignature.Beat);
            Numerator = timeSignature.Numerator;
            Denominator = timeSignature.Denominator;
        }

        public TimeSignature Load()
        {
            return new TimeSignature(Beat.Load(), Numerator, Denominator);
        }
    }
}
