﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation
{
    public interface IPhraseWarper
    {

        Phrase Run(Phrase input);

    }
}
