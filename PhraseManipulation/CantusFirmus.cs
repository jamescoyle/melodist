﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation
{
    public class CantusFirmus : Phrase
    {

        public Rational BeatsPerNote { get; private set; }



        public CantusFirmus(Rational beatsPerNote, int beatsPerBar, int tempo)
            : base(beatsPerBar, tempo)
        {
            BeatsPerNote = beatsPerNote;
        }


        /// <summary>
        /// Create a new cantus firmus using the given phrase notes as a template
        /// </summary>
        /// <param name="phrase">The phrase to base the cantus firmus on</param>
        /// <param name="beatsPerNote">How many beats each note should occupy</param>
        public CantusFirmus(Phrase phrase, Rational beatsPerNote)
            : base(phrase.BeatsPerBar, phrase.Tempo)
        {
            if (phrase.Notes.Count == 0)
                throw new ArgumentException("Cannot construct cantus firmus for phrase with no notes");

            CopyProperties(phrase);
            BeatsPerNote = beatsPerNote;

            //Copy the source phrase's notes and organise them so there are no gaps between them
            List<Note> newNotes = new List<Note>();
            phrase.Notes.ForEach(x => newNotes.Add(new Note(x.Pitch, x.StartBeat, x.Duration, x.Velocity)));

            newNotes = newNotes.OrderByDescending(x => x.EndBeat).ToList();
            newNotes.Where(x => x.EndBeat == newNotes[0].EndBeat).ToList().ForEach(x => x.EndBeat = phrase.EndBeat);

            newNotes = newNotes.OrderBy(x => x.StartBeat).ToList();
            newNotes.Where(x => x.StartBeat == newNotes[0].StartBeat).ToList().ForEach(x => x.StartBeat = phrase.StartBeat);

            for(int i = 0; i < newNotes.Count - 1; i++)
            {
                if (newNotes[i].EndBeat < newNotes[i + 1].StartBeat)
                    newNotes.Where(x => x.EndBeat == newNotes[i].EndBeat).ToList().ForEach(x => x.EndBeat = newNotes[i + 1].StartBeat);
            }

            //Use startBeat as an iterator to loop through the phrase, getting blocks of notes
            Rational startBeat = 0;
            while (startBeat > StartBeat)
                startBeat -= BeatsPerNote;
            for (; startBeat < EndBeat; startBeat += BeatsPerNote)
            {
                Rational endBeat = Rational.Min(startBeat + BeatsPerNote, EndBeat);

                List<Note> notes = newNotes.Where(x => x.StartBeat < endBeat && x.EndBeat > startBeat).ToList();

                //Construct the weights of pitches, for each note in the range, add the duration it played over the range x how the note start was to the range start
                double[] pitchWeights = new double[128];
                foreach (Note note in notes)
                    pitchWeights[note.Pitch] +=
                        (Rational.Min(endBeat, note.EndBeat) - Rational.Max(startBeat, note.StartBeat)) *
                        Rational.Max(1, (BeatsPerNote - (note.StartBeat - startBeat).Abs())) *
                        ((note.StartBeat >= startBeat && note.StartBeat < endBeat) ? 1.5 : 1);

                //Get the maximum pitch weight and get all pitches that had this maximum weight
                double maxWeight = pitchWeights.Max();
                List<Pitch> bestPitches = new List<Pitch>();
                for (int j = 0; j < pitchWeights.Length; j++)
                {
                    if (pitchWeights[j] == maxWeight)
                        bestPitches.Add(j);
                }

                //Get all the notes in the region which had one of the maximum duration pitches
                List<Note> bestNotes = new List<Note>();
                foreach (Note note in notes)
                {
                    if (bestPitches.Contains(note.Pitch))
                        bestNotes.Add(note);
                }

                //Add a new note, taking the pitch of the first note that had a maximal duration pitch
                AddNote(new Note(
                    bestNotes.OrderBy(x => x.StartBeat).ThenBy(x => x.Pitch.Value).First().Pitch,
                    Rational.Max(startBeat, StartBeat),
                    BeatsPerNote,
                    (int)Math.Round(notes.Average(x => x.Velocity))
                ));
            }
        }



        public Pitch GetPitchAt(Rational beat)
        {
            beat = MapBeat(beat);
            return Notes.First(x => x.StartBeat <= beat && x.EndBeat > beat).Pitch;
        }

    }
}
