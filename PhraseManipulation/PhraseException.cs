﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseManipulation
{
    public class PhraseException : Exception
    {

        private PhraseException(string message)
            : base(message)
        {
        }



        public static PhraseException InvalidBeatsPerBar()
        {
            return new PhraseException("Invalid beats per bar, must be greater than zero");
        }


        public static PhraseException InvalidTempo()
        {
            return new PhraseException("Invalid tempo, must be greater than zero");
        }


        public static PhraseException InvalidFilePath()
        {
            return new PhraseException("Invalid file path for phrase");
        }


        public static PhraseException InvalidOverwriteAttempt()
        {
            return new PhraseException("Attempted to overwrite a phrase file currently in use by Melodist");
        }


        public static PhraseException InvalidLoopCount()
        {
            return new PhraseException("Invalid loop count, must be greater than zero");
        }


        public static PhraseException InvalidTimeSignature()
        {
            return new PhraseException("Invalid time signature");
        }


        public static PhraseException ExpectedMonophonicPhrase()
        {
            return new PhraseException("Expected a monophonic phrase");
        }

    }
}
