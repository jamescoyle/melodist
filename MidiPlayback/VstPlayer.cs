﻿using CSharpExtras;
using MathPlus;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSTHosting;

namespace MidiPlayback
{
    public class VstPlayer : WaveProvider32
    {

        private WaveOut _waveOut;

        private MemberListManager<VSTWaveProvider> WaveProvidersMngr;
        public MemberList<VSTWaveProvider> WaveProviders { get { return WaveProvidersMngr.List; } }


        public Rational Playhead
        {
            get { return WaveProviders[0].Playhead; }
            set
            {
                WaveProviders.ForEach(x => x.Playhead = value);
            }
        }


        public Rational PreviousPlayhead { get { return WaveProviders[0].PreviousPlayhead; } }


        private DateTime _lastReadTime = DateTime.MinValue;


        public Rational Tempo { get { return WaveProviders[0].Tempo; } }



        public VstPlayer(params VSTWaveProvider[] waveProviders)
        {
            _waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
            SetWaveFormat(waveProviders[0].WaveFormat.SampleRate, waveProviders[0].WaveFormat.Channels);
            WaveProvidersMngr = new MemberListManager<VSTWaveProvider>(waveProviders);
            _waveOut.Volume = 0.8F;
            _waveOut.Init(this);
        }


        public void Play()
        {
            _waveOut.Play();
        }


        public void Stop()
        {
            _waveOut.Stop();
            Playhead = 0;
            _waveOut = null;
            WaveProvidersMngr.Clear();
        }

        
        public override int Read(float[] buffer, int offset, int sampleCount)
        {
            if(WaveProviders.Count == 1)
            {
                WaveProvidersMngr[0].Read(buffer, offset, sampleCount);
            }
            else if(WaveProviders.Count > 1)
            {
                float[] bufferCopy = new float[buffer.Length];

                foreach (VSTWaveProvider waveProvider in WaveProviders)
                {
                    waveProvider.Read(bufferCopy, offset, sampleCount);
                    for(int i = 0; i < bufferCopy.Length; i++)
                        buffer[i] += bufferCopy[i];
                    _lastReadTime = DateTime.Now;
                }
            }
            _lastReadTime = DateTime.Now;
            return sampleCount;
        }


        public Rational CalculateCurrentPlayhead()
        {
            if (_lastReadTime == DateTime.MinValue || PreviousPlayhead == Playhead)
                return Playhead;

            TimeSpan sinceLastRead = DateTime.Now - _lastReadTime;
            Rational elapsedSeconds = new Rational(sinceLastRead.Milliseconds, 1000);
            Rational elapsedBeats = ((elapsedSeconds / 60) * Tempo).Round(100000);

            return Rational.Min(Playhead, PreviousPlayhead + elapsedBeats);
        }

    }
}
