﻿using GeneralMidi;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MidiPlayback
{
    public abstract class GMPlayer
    {

        protected MidiDevice OutputDevice { get; private set; }

        private Timer _timer;

        private Stopwatch _stopwatch;


        public abstract bool Loop { get; }

        public abstract Rational LoopStart { get; }
        
        public abstract Rational LoopEnd { get; }

        public Rational Playhead { get; set; }

        public abstract Rational Tempo { get; }

        public Rational TempoAdjustment { get; set; }



        public GMPlayer(MidiDevice outputDevice = null)
        {
            OutputDevice = (outputDevice == null) ? MidiDevice.Default : outputDevice;
            _timer = new Timer(2);
            _timer.Elapsed += timer_Elapsed;
            _stopwatch = new Stopwatch();
        }


        public void Play()
        {
            _timer.Start();
            _stopwatch.Start();
        }


        public virtual void Stop()
        {
            _timer.Stop();
            _stopwatch.Stop();
            Playhead = 0;
            OutputDevice.SendAllNotesOff();
            OutputDevice.Dispose();
            OutputDevice = null;
            _timer = null;
            _stopwatch = null;
        }


        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _stopwatch.Stop();
            long elapsedTicks = _stopwatch.ElapsedTicks;
            _stopwatch.Restart();

            Rational elapsedSeconds = new Rational(elapsedTicks, Stopwatch.Frequency);
            Rational elapsedBeats = ((elapsedSeconds / 60) * (Tempo + TempoAdjustment)).Round(100000);

            Rational startBeat = Playhead;
            Rational endBeat = startBeat + elapsedBeats;
            if(Loop && endBeat > LoopEnd && LoopEnd > LoopStart)
                endBeat -= (LoopEnd - LoopStart);

            Playhead = endBeat;
            ReadStarting?.Invoke(this, null);

            Read(startBeat, endBeat);
        }


        public event EventHandler ReadStarting;

        protected abstract void Read(Rational startBeat, Rational endBeat);

    }
}
