﻿using GeneralMidi;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiPlayback
{
    public class PlayerManager
    {

        public VstPlayer VstPlayer { get; private set; }

        public GMPlayer GMPlayer { get; private set; }

        public Rational Playhead
        {
            get
            {
                if (GMPlayer != null)
                    return GMPlayer.Playhead;

                return VstPlayer.Playhead;
            }
        }



        public PlayerManager(VstPlayer vstPlayer = null, GMPlayer gmPlayer = null)
        {
            VstPlayer = vstPlayer;
            GMPlayer = gmPlayer;
            if(VstPlayer != null && GMPlayer != null)
                GMPlayer.ReadStarting += OnGMReadStarting;
        }


        public void Play()
        {
            VstPlayer?.Play();
            GMPlayer?.Play();
        }


        public void Stop()
        {
            VstPlayer?.Stop();
            GMPlayer?.Stop();
            VstPlayer = null;
            GMPlayer = null;
        }


        private Rational _previousCatchupRequired = 0;
        private Rational _catchupRequired = 0;
        private void OnGMReadStarting(object sender, EventArgs e)
        {
            Rational playhead = GMPlayer.Playhead;
            Rational targetPlayhead = VstPlayer.CalculateCurrentPlayhead();
            targetPlayhead += ((Rational.FromDecimal(MidiDevice.Latency) / 60) * GMPlayer.Tempo).Round(100000);

            _previousCatchupRequired = _catchupRequired;
            _catchupRequired = CalculateCatchupRequired(playhead, targetPlayhead);

            if (_catchupRequired * _previousCatchupRequired < 0)
            {
                GMPlayer.TempoAdjustment = 0;
            }
            else
            {
                Rational nextCatchupRequired = _catchupRequired + (_catchupRequired - _previousCatchupRequired);
                if (nextCatchupRequired * _catchupRequired < 0)
                {
                    GMPlayer.TempoAdjustment = 0;
                }
                else
                {
                    GMPlayer.TempoAdjustment += _catchupRequired * 5;
                }
            }
        }


        private Rational CalculateCatchupRequired(Rational playhead, Rational targetPlayhead)
        {
            if (playhead == targetPlayhead)
                return 0;

            if (!GMPlayer.Loop)
                return targetPlayhead - playhead;

            Rational wrapDistance = Rational.Min(
                (GMPlayer.LoopEnd - playhead) + (targetPlayhead - GMPlayer.LoopStart),
                (playhead - GMPlayer.LoopStart) + (GMPlayer.LoopEnd - targetPlayhead)
            );
            Rational nonWrapDistance = targetPlayhead - playhead;

            return (wrapDistance.Abs() < nonWrapDistance.Abs()) ? wrapDistance : nonWrapDistance;
        }

    }
}
