# Melodist

Melodist is a project that I created a few years ago and ended up winning the Open University 2017 innovation award, run in collaboration with Santander bank.

I'd been interested in the field of computer composition for a while, and had for a long time had the idea for a tool which, rather than relying on machine learning, takes melodic inputs from the user which it then remixes together in a new way.

In the attached video demo available [here](demo vid.mp4), you can see this in action. 

Between 0:00 - 1:06, the rhythm of Bach's 'Minuet in G major' is shown to be modified by the verse rhythm of the Beatles' 'I Want To Hold Your Hand'.

Between 1:06 - 1:47, the result of the above combination is then reharmonised using the chord progression of Tim Minchin's 'Peace Anthem For Palestine'.

Finally, the resultant melody is reshaped to more closely fit the melodic contour of Beethoven's 'Fur Elise'.

Sadly the project is currently not in active development, as the focus of the competition was to showcase commercially viable products, and the pressure of bringing this up to a standard where it could be commercially viable was too much work alongside my degree and full time job. Though I do have plans to rework it at some point in the future, not worrying about commercial viability, and taking on board some of the better development practices that I've learnt in the 2+ years since building this.