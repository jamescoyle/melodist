﻿using Jacobi.Vst.Core;
using Jacobi.Vst.Core.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTHosting
{
    public class HostCommandStub : IVstHostCommandStub
    {

        public IVstPluginContext PluginContext { get; set; }

        private VstTimeInfo _vstTimeInfo;


        public HostCommandStub()
        {
            _vstTimeInfo = new VstTimeInfo()
            {
                SamplePosition = 0,
                SampleRate = GetSampleRate(),
                NanoSeconds = 0,
                PpqPosition = 0,
                Tempo = 120,
                BarStartPosition = 0,
                CycleStartPosition = 0,
                CycleEndPosition = 0,
                TimeSignatureNumerator = 4,
                TimeSignatureDenominator = 4,
                SmpteOffset = 0,
                SmpteFrameRate = VstSmpteFrameRate.Smpte25fps,
                SamplesToNearestClock = 0,
                Flags = 0
            };

            BlockSize = 1024;
        }



        #region IVstHostCommands20 Members
        public bool BeginEdit(int index)
        {
            //RaisePluginCalled("BeginEdit(" + index + ")");
            return false;
        }

        public VstCanDoResult CanDo(string cando)
        {
            //RaisePluginCalled("CanDo(" + cando + ")");
            bool result = (
                cando == "sendVstEvents" ||
                cando == "sendVstMidiEvent" ||
                cando == "receiveVstEvents" ||
                cando == "receiveVstMidiEvent" ||
                cando == "sizeWindow" ||
                cando == "sendVstMidiEventFlagIsRealtime"
            );
            return result ? VstCanDoResult.Yes : VstCanDoResult.No;
        }

        public bool CloseFileSelector(VstFileSelect fileSelect)
        {
            //RaisePluginCalled("CloseFileSelector(" + fileSelect.Command + ")");
            return false;
        }

        public bool EndEdit(int index)
        {
            //RaisePluginCalled("EndEdit(" + index + ")");
            return false;
        }

        public VstAutomationStates GetAutomationState()
        {
            //RaisePluginCalled("GetAutomationState()");
            return VstAutomationStates.Off;
        }

        public int BlockSize { get; set; }
        public int GetBlockSize()
        {
            //RaisePluginCalled("GetBlockSize()");
            return BlockSize;
        }

        public string GetDirectory()
        {
            //RaisePluginCalled("GetDirectory()");
            return null;
        }

        public int GetInputLatency()
        {
            //RaisePluginCalled("GetInputLatency()");
            return 0;
        }

        public VstHostLanguage GetLanguage()
        {
            //RaisePluginCalled("GetLanguage()");
            return VstHostLanguage.NotSupported;
        }

        public int GetOutputLatency()
        {
            //RaisePluginCalled("GetOutputLatency()");
            return 0;
        }

        public VstProcessLevels GetProcessLevel()
        {
            //RaisePluginCalled("GetProcessLevel()");
            return VstProcessLevels.Unknown;
        }

        public string GetProductString()
        {
            //RaisePluginCalled("GetProductString()");
            return "Melodist";
        }

        public float GetSampleRate()
        {
            //RaisePluginCalled("GetSampleRate()");
            return 44.1f;
        }

        public VstTimeInfo GetTimeInfo(VstTimeInfoFlags filterFlags)
        {
            //RaisePluginCalled("GetTimeInfo(" + filterFlags + ")");
            return _vstTimeInfo;
        }

        public string GetVendorString()
        {
            //RaisePluginCalled("GetVendorString()");
            return "James Coyle";
        }

        public int GetVendorVersion()
        {
            //RaisePluginCalled("GetVendorVersion()");
            return 1;
        }

        public bool IoChanged()
        {
            //RaisePluginCalled("IoChanged()");
            return false;
        }

        public bool OpenFileSelector(VstFileSelect fileSelect)
        {
            //RaisePluginCalled("OpenFileSelector(" + fileSelect.Command + ")");
            return false;
        }

        public bool ProcessEvents(VstEvent[] events)
        {
            //RaisePluginCalled("ProcessEvents(" + events.Length + ")");
            return false;
        }

        public bool SizeWindow(int width, int height)
        {
            //RaisePluginCalled("SizeWindow(" + width + ", " + height + ")");
            return false;
        }

        public bool UpdateDisplay()
        {
            //RaisePluginCalled("UpdateDisplay()");
            return false;
        }
        #endregion



        #region IVstHostCommands10 Members
        public int GetCurrentPluginID()
        {
            //RaisePluginCalled("GetCurrentPluginID()");
            return PluginContext.PluginInfo.PluginID;
        }

        public int GetVersion()
        {
            //RaisePluginCalled("GetVersion()");
            return 1;
        }

        public void ProcessIdle()
        {
            //RaisePluginCalled("ProcessIdle()");
        }

        public void SetParameterAutomated(int index, float value)
        {
            //RaisePluginCalled("SetParameterAutomated(" + index + ", " + value + ")");
        }
        #endregion



        public event EventHandler<PluginCalledEventArgs> PluginCalled;

        private void RaisePluginCalled(string message)
        {
            PluginCalled?.Invoke(this, new PluginCalledEventArgs(message));
        }

    }



    public class PluginCalledEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public PluginCalledEventArgs(string message)
        {
            Message = message;
        }
    }
}
