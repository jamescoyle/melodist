﻿using Jacobi.Vst.Core;
using Jacobi.Vst.Core.Host;
using Jacobi.Vst.Interop.Host;
using MathPlus;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTHosting
{
    public abstract class VSTWaveProvider : WaveProvider32
    {

        public VstPluginContext VstContext { get; private set; }

        public HostCommandStub VstHostCommandStub { get; private set; }

        public string PluginPath { get; private set; }

        private VstAudioBufferManager _inputBuffers;

        private VstAudioBufferManager _outputBuffers;

        
        public Rational Playhead { get; set; }

        public Rational PreviousPlayhead { get; protected set; }

        public abstract Rational LoopStart { get; }

        public abstract Rational LoopEnd { get; }

        public abstract bool Loop { get; }

        public abstract Rational Tempo { get; }



        public VSTWaveProvider(string pluginPath, int sampleRate, int channels)
            : base(sampleRate, channels)
        {
            if (!string.IsNullOrEmpty(pluginPath))
                VstContext = OpenPlugin(pluginPath);
            PluginPath = pluginPath;
        }


        private VstPluginContext OpenPlugin(string pluginPath)
        {
            VstHostCommandStub = new HostCommandStub();
            VstHostCommandStub.PluginCalled += (sender, args) => Debug.WriteLine(args.Message);

            return OpenPlugin(pluginPath, VstHostCommandStub);
        }


        public static VstPluginContext OpenPlugin(string pluginPath, IVstHostCommandStub hostCommandStub)
        {
            VstPluginContext ctx = VstPluginContext.Create(pluginPath, hostCommandStub);

            // add custom data to the context
            ctx.Set("PluginPath", pluginPath);
            ctx.Set("HostCmdStub", hostCommandStub);

            // actually open the plugin itself
            ctx.PluginCommandStub.Open();
            ctx.PluginCommandStub.MainsChanged(true);

            return ctx;
        }



        public VstMidiEvent NoteOnEvent(byte channel, byte pitch, byte velocity, int noteLength, int deltaFrames = 0)
        {
            return new VstMidiEvent(deltaFrames, noteLength, deltaFrames, new byte[] { (byte)(144 + channel), pitch, velocity, 0 }, 0, 0);
        }


        public VstMidiEvent NoteOffEvent(byte channel, byte pitch, int deltaFrames = 0)
        {
            return new VstMidiEvent(deltaFrames, 0, deltaFrames, new byte[] { (byte)(144 + channel), pitch, 0, 0 }, 0, 0);
        }



        public override int Read(float[] outputBuffer, int offset, int count)
        {
            int samplesPerTrack = count / VstContext.PluginInfo.AudioOutputCount;
            VstHostCommandStub.BlockSize = samplesPerTrack;

            VstEvent[] vstEvents = GetVstEvents(offset, samplesPerTrack);
            
            if (_inputBuffers == null)
                _inputBuffers = new VstAudioBufferManager(VstContext.PluginInfo.AudioInputCount, samplesPerTrack);

            if(_outputBuffers == null)
                _outputBuffers = new VstAudioBufferManager(VstContext.PluginInfo.AudioOutputCount, _inputBuffers.BufferSize);

            VstContext.PluginCommandStub.EditorIdle();

            VstContext.PluginCommandStub.StartProcess();
            if (vstEvents.Length > 0)
                VstContext.PluginCommandStub.ProcessEvents(vstEvents);
            VstContext.PluginCommandStub.ProcessReplacing(_inputBuffers.ToArray(), _outputBuffers.ToArray());
            VstContext.PluginCommandStub.StopProcess();

            int bufferIndex = offset;
            for(int i = 0; i < samplesPerTrack; i++)
            {
                foreach (VstAudioBuffer vstBuffer in _outputBuffers)
                {
                    outputBuffer[bufferIndex] = vstBuffer[i];
                    bufferIndex++;
                }
            }

            //Do this just to smooth over any rounding issues in the sample count to get a less crackly output
            while(bufferIndex < count)
            {
                outputBuffer[bufferIndex] = outputBuffer[bufferIndex - 1];
                bufferIndex++;
            }

            return count;
        }


        protected abstract VstEvent[] GetVstEvents(int offset, int count);

    }
}
