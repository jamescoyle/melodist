﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralMidi
{
    public class GeneralMidiException : Exception
    {

        public GeneralMidiException(string message)
            : base(message)
        {
        }


        public static GeneralMidiException UnrecognisedPlaybackDevice()
        {
            return new GeneralMidiException("Unrecognised playback device");
        }

    }
}
