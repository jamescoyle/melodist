﻿using Sanford.Multimedia.Midi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralMidi
{
    public class MidiDevice : IDisposable
    {

        private OutputDevice _outputDevice;
        
        private static MidiDevice _default;
        public static MidiDevice Default
        {
            get
            {
                if (_default == null || _default._outputDevice.IsDisposed)
                    _default = new MidiDevice(0);
                return _default;
            }
            set
            {
                _default = value;
            }
        }

        public static int DeviceCount { get { return OutputDevice.DeviceCount; } }



        public static decimal Latency { get; set; }



        public MidiDevice(int deviceID)
        {
            _outputDevice = new OutputDevice(deviceID);
        }

        public MidiDevice(string str)
        {
            InitMidiDevice(str);
        }

        private void InitMidiDevice(string deviceName)
        {
            int count = DeviceCount;
            for (int i = 0; i < count; i++)
            {
                if (OutputDevice.GetDeviceCapabilities(i).name == deviceName)
                {
                    _outputDevice = new OutputDevice(i);
                    break;
                }
            }
            if (_outputDevice == null)
                throw GeneralMidiException.UnrecognisedPlaybackDevice();
        }



        public static List<string> GetDeviceNames()
        {
            int count = DeviceCount;
            List<string> output = new List<string>();
            for (int i = 0; i < count; i++)
                output.Add(OutputDevice.GetDeviceCapabilities(i).name);
            return output;
        }



        public void SendNoteOn(int channel, int pitch, int velocity = 104)
        {
            _outputDevice.Send(new ChannelMessage(ChannelCommand.NoteOn, channel, pitch, velocity));
        }


        public void SendNoteOff(int channel, int pitch, int velocity = 104)
        {
            _outputDevice.Send(new ChannelMessage(ChannelCommand.NoteOff, channel, pitch, velocity));
        }


        public void SendProgramChange(int channel, int programNumber)
        {
            _outputDevice.Send(new ChannelMessage(ChannelCommand.ProgramChange, channel, programNumber));
        }


        public void SendAllNotesOff(int channel)
        {
            _outputDevice.Send(new ChannelMessage(ChannelCommand.Controller, channel, 123));
        }


        public void SendAllNotesOff()
        {
            for (int i = 0; i <= 15; i++)
                SendAllNotesOff(i);
        }


        public void Dispose()
        {
            if (_outputDevice != null && !_outputDevice.IsDisposed)
                _outputDevice.Dispose();
        }

    }
}
