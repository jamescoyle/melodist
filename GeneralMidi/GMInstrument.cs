﻿using CSharpExtras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralMidi
{
    public class GMInstrument
    {

        private static MemberListManager<GMInstrument> InstrumentsManager;
        public static MemberList<GMInstrument> Instruments { get { return InstrumentsManager.List; } }

        
        public static GMInstrument Get(int index)
        {
            return Instruments.FirstOrDefault(x => x.Index == index);
        }

        public static GMInstrument Get(string name)
        {
            return Instruments.FirstOrDefault(x => x.Name == name);
        }


        static GMInstrument()
        {
            List<string> names = new List<string>()
            {
                "Acoustic Grand Piano",     "Bright Acoustic Piano",    "Electric Grand Piano",     "Honky-tonk Piano",         "Electric Piano 1",
                "Electric Piano 2",         "Harpsichord",              "Clavinet",                 "Celesta",                  "Glockenspiel",
                "Music Box",                "Vibraphone",               "Marimba",                  "Xylophone",                "Tubular Bells",
                "Dulcimer",                 "Drawbar Organ",            "Percussive Organ",         "Rock Organ",               "Church Organ",
                "Reed Organ",               "Accordion",                "Harmonica",                "Tango Accordion",          "Acoustic Guitar (nylon)",
                "Acoustic Guitar (steel)",  "Electric Guitar (jazz)",   "Electric Guitar (clean)",  "Electric Guitar (muted)",  "Overdriven Guitar",
                "Distortion Guitar",        "Guitar Harmonics",         "Acoustic Bass",            "Electric Bass (finger)",   "Electric Bass (pick)",
                "Fretless Bass",            "Slap Bass 1",              "Slap Bass 2",              "Synth Bass 1",             "Synth Bass 2",
                "Violin",                   "Viola",                    "Cello",                    "Contrabass",               "Tremolo Strings",
                "Pizzicato Strings",        "Orchestral Harp",          "Timpani",                  "String Ensemble 1",        "String Ensemble 2",
                "Synth Strings 1",          "Synth Strings 2",          "Choir Aahs",               "Voice Oohs",               "Synth Choir",
                "Orchestra Hit",            "Trumpet",                  "Trombone",                 "Tuba",                     "Muted Trumpet",
                "French Horn",              "Brass Section",            "Synth Brass 1",            "Synth Brass 2",            "Soprano Sax",
                "Alto Sax",                 "Tenor Sax",                "Baritone Sax",             "Oboe",                     "English Horn",
                "Bassoon",                  "Clarinet",                 "Piccolo",                  "Flute",                    "Recorder",
                "Pan Flute",                "Blown bottle",             "Shakuhachi",               "Whistle",                  "Ocarina",
                "Lead 1 (square)",          "Lead 2 (sawtooth)",        "Lead 3 (calliope)",        "Lead 4 (chiff)",           "Lead 5 (charang)",
                "Lead 6 (voice)",           "Lead 7 (fifths)",          "Lead 8 (bass + lead)",     "Pad 1 (new age)",          "Pad 2 (warm)",
                "Pad 3 (polysynth)",        "Pad 4 (choir)",            "Pad 5 (bowed)",            "Pad 6 (metallic)",         "Pad 7 (halo)",
                "Pad 8 (sweep)",            "FX 1 (rain)",              "FX 2 (soundtrack)",        "FX 3 (crystal)",           "FX 4 (atmosphere)",
                "FX 5 (brightness)",        "FX 6 (goblins)",           "FX 7 (echoes)",            "FX 8 (sci-fi)",            "Sitar",
                "Banjo",                    "Shamisen",                 "Koto",                     "Kalimba",                  "Bagpipe",
                "Fiddle",                   "Shanai",                   "Tinkle Bell",              "Agogo",                    "Steel Drums",
                "Woodblock",                "Taiko Drum",               "Melodic Tom",              "Synth Drum",               "Reverse Cymbal",
                "Guitar Fret Noise",        "Breath Noise",             "Seashore",                 "Bird Tweet",               "Telephone Ring",
                "Helicopter",               "Applause",                 "Gunshot"
            };

            InstrumentsManager = new MemberListManager<GMInstrument>();
            for (int i = 0; i < names.Count; i++)
                InstrumentsManager.Add(new GMInstrument(i, names[i]));
        }



        public int Index { get; protected set; }


        public string Name { get; protected set; }



        protected GMInstrument(int index, string name)
        {
            Index = index;
            Name = name;
        }


        public override string ToString()
        {
            return Name;
        }

    }
}
