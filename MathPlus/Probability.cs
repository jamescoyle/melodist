﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathPlus
{
    public class Probability
    {

        /// <summary>
        /// Returns a point on a normal distribution curve
        /// </summary>
        /// <param name="x">The x co-ordinate of the point to return</param>
        /// <param name="mean">The mean x of the curve</param>
        /// <param name="std">The standard deviation of the curve</param>
        /// <returns></returns>
        public static double NormalDistribution(double x, double mean, double std)
        {
            double distance = Math.Abs(x - mean);
            double returnVal = -Math.Pow(distance, 2) / (2 * std * std);
            returnVal = Math.Pow(Math.E, returnVal);
            returnVal /= std * Math.Sqrt(2 * Math.PI);
            return returnVal;
        }

    }
}
