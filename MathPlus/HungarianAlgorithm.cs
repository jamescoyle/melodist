﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathPlus
{
    public static class HungarianAlgorithm
    {

        public static List<HungarianPair> Solve(double[,] costMatrix)
        {
            double[,] costs = GetSquareCostMatrix(costMatrix);
            double[] rowReductions = ReduceRows(costs);
            double[] columnReductions = ReduceColumns(costs);
            while (true)
            {
                List<HungarianPair> pairs = GetOptimalPairings(costs);
                RemoveMinusOnes(costs);
                Tuple<bool[], bool[]> lines = DrawLines(costs, pairs);
                bool[] rowLines = lines.Item1;
                bool[] columnLines = lines.Item2;
                int lineCount = rowLines.Count(x => x) + columnLines.Count(x => x);
                if (lineCount == costs.GetUpperBound(0) + 1)
                    return pairs.Where(x => x.Row <= costMatrix.GetUpperBound(0) && x.Column <= costMatrix.GetUpperBound(1)).OrderBy(x => x.Row).ToList();

                ReduceUncoveredCosts(costs, rowLines, columnLines);
            }
        }



        private static double[,] GetSquareCostMatrix(double[,] costs)
        {
            int rowCount = costs.GetUpperBound(0) + 1;
            int columnCount = costs.GetUpperBound(1) + 1;

            double maxCost = 0;
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                    maxCost = Math.Max(maxCost, costs[i, j]);
            }

            int outputSize = Math.Max(rowCount, columnCount);
            double[,] output = new double[outputSize, outputSize];
            for (int i = 0; i < outputSize; i++)
            {
                for (int j = 0; j < outputSize; j++)
                {
                    output[i, j] = (i >= rowCount || j >= columnCount) ?
                        maxCost :
                        costs[i, j];
                }
            }
            return output;
        }



        private static double[] ReduceRows(double[,] costs)
        {
            double[] reductions = new double[costs.GetUpperBound(0) + 1];

            for (int i = reductions.Length - 1; i >= 0; i--)
            {
                double minCost = double.MaxValue;
                for (int j = reductions.Length - 1; j >= 0; j--)
                    minCost = Math.Min(minCost, costs[i, j]);

                for (int j = reductions.Length - 1; j >= 0; j--)
                    costs[i, j] -= minCost;

                reductions[i] = minCost;
            }
            return reductions;
        }



        private static double[] ReduceColumns(double[,] costs)
        {
            double[] reductions = new double[costs.GetUpperBound(0) + 1];

            for (int i = reductions.Length - 1; i >= 0; i--)
            {
                double minCost = double.MaxValue;
                for (int j = reductions.Length - 1; j >= 0; j--)
                    minCost = Math.Min(minCost, costs[j, i]);

                for (int j = reductions.Length - 1; j >= 0; j--)
                    costs[j, i] -= minCost;

                reductions[i] = minCost;
            }
            return reductions;
        }



        private static Tuple<bool[], bool[]> DrawLines(double[,] costs, List<HungarianPair> pairs)
        {
            bool[] rowMarks = new bool[costs.GetUpperBound(0) + 1];
            bool[] columnMarks = new bool[costs.GetUpperBound(0) + 1];

            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                rowMarks[i] = false;
                columnMarks[i] = false;
            }

            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                HungarianPair pair = pairs.FirstOrDefault(x => x.Row == i);
                if (pair == null)
                {
                    rowMarks[i] = true;
                    MarkColumns(costs, pairs, rowMarks, columnMarks, i);
                }
            }

            for (int i = costs.GetUpperBound(0); i >= 0; i--)
                rowMarks[i] = !rowMarks[i];

            return new Tuple<bool[], bool[]>(rowMarks, columnMarks);
        }



        private static void MarkColumns(double[,] costs, List<HungarianPair> pairs, bool[] rowMarks, bool[] columnMarks, int markedRow)
        {
            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                if (!columnMarks[i] && costs[markedRow, i] == 0)
                {
                    columnMarks[i] = true;
                    MarkRows(costs, pairs, rowMarks, columnMarks, i);
                }
            }
        }



        private static void MarkRows(double[,] costs, List<HungarianPair> pairs, bool[] rowMarks, bool[] columnMarks, int markedColumn)
        {
            foreach (HungarianPair pair in pairs)
            {
                if (pair.Column == markedColumn && !rowMarks[pair.Row])
                {
                    rowMarks[pair.Row] = true;
                    MarkColumns(costs, pairs, rowMarks, columnMarks, pair.Row);
                }
            }
        }



        private static void ReduceUncoveredCosts(double[,] costs, bool[] rowLines, bool[] columnLines)
        {
            double minUncovered = double.MaxValue;
            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                if (rowLines[i])
                    continue;

                for (int j = costs.GetUpperBound(0); j >= 0; j--)
                {
                    if (columnLines[j])
                        continue;

                    minUncovered = Math.Min(minUncovered, costs[i, j]);
                }
            }

            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                if (rowLines[i])
                    continue;

                for (int j = costs.GetUpperBound(0); j >= 0; j--)
                {
                    if (columnLines[j])
                        continue;

                    costs[i, j] -= minUncovered;
                }
            }
        }



        private static List<HungarianPair> GetOptimalPairings(double[,] costs)
        {
            int[] rowZeros = null;
            int[] columnZeros = null;

            List<HungarianPair> pairs = new List<HungarianPair>();
            bool recalculateZeroCounts = true;
            while (pairs.Count < costs.GetUpperBound(0) + 1)
            {
                bool progressMade = false;
                for (int i = costs.GetUpperBound(0); i >= 0; i--)
                {
                    if (recalculateZeroCounts)
                    {
                        rowZeros = new int[costs.GetUpperBound(0) + 1];
                        columnZeros = new int[costs.GetUpperBound(0) + 1];

                        for (int j = costs.GetUpperBound(0); j >= 0; j--)
                        {
                            for (int k = costs.GetUpperBound(0); k >= 0; k--)
                            {
                                if (costs[j, k] == 0)
                                {
                                    rowZeros[j]++;
                                    columnZeros[k]++;
                                }
                            }
                        }
                        recalculateZeroCounts = false;
                    }

                    int oneCount = rowZeros.Count(x => x == 1) + columnZeros.Count(x => x == 1);

                    if ((rowZeros[i] == 1 && pairs.Count(x => x.Row == i) == 0) || (oneCount == 0 && rowZeros[i] > 0))
                    {
                        for (int j = costs.GetUpperBound(0); j >= 0; j--)
                        {
                            if (costs[i, j] == 0)
                            {
                                pairs.Add(new HungarianPair(i, j));
                                for (int k = costs.GetUpperBound(0); k >= 0; k--)
                                {
                                    if (costs[i, k] == 0) { costs[i, k] = -1; }
                                    if (costs[k, j] == 0) { costs[k, j] = -1; }
                                }
                                recalculateZeroCounts = true;
                                progressMade = true;
                                break;
                            }
                        }
                    }
                    else if ((columnZeros[i] == 1 && pairs.Count(x => x.Column == i) == 0) || (oneCount == 0 && columnZeros[i] > 0))
                    {
                        for (int j = costs.GetUpperBound(0); j >= 0; j--)
                        {
                            if (costs[j, i] == 0)
                            {
                                pairs.Add(new HungarianPair(j, i));
                                for (int k = costs.GetUpperBound(0); k >= 0; k--)
                                {
                                    if (costs[j, k] == 0) { costs[j, k] = -1; }
                                    if (costs[k, i] == 0) { costs[k, i] = -1; }
                                }
                                recalculateZeroCounts = true;
                                progressMade = true;
                                break;
                            }
                        }
                    }
                }
                if (!progressMade)
                    return pairs;
            }
            return pairs;
        }



        private static void RemoveMinusOnes(double[,] costs)
        {
            for (int i = costs.GetUpperBound(0); i >= 0; i--)
            {
                for (int j = costs.GetUpperBound(0); j >= 0; j--)
                {
                    if (costs[i, j] < 0)
                        costs[i, j] = 0;
                }
            }
        }

    }



    public class HungarianPair
    {
        public int Row { get; private set; }
        public int Column { get; private set; }

        public HungarianPair(int rowArg, int columnArg)
        {
            Row = rowArg;
            Column = columnArg;
        }
    }
}
