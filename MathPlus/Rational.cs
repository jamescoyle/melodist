﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathPlus
{
    public struct Rational : IComparable, IComparable<Rational>
    {

        #region Getters
        private long _a;
        public long A
        {
            get
            {
                if (_a == 0 && _b == 0)
                    _b = 1;
                return _a;
            }
        }

        private long _b;
        public long B
        {
            get
            {
                if (_a == 0 && _b == 0)
                    _b = 1;
                return _b;
            }
        }
        #endregion Getters/Setters



        public Rational(long a, long b)
        {
            _a = a;
            _b = b;
            Simplify();
        }


        #region Public Methods
        public Rational Abs()
        {
            return new Rational(Math.Abs(A), Math.Abs(B));
        }


        public int Round()
        {
            return (int)Math.Round((float)this);
        }


        public Rational Round(long denominator)
        {
            return new Rational((long)Math.Round(((double)this) * denominator), denominator);
        }


        public int Ceiling()
        {
            return (int)Math.Ceiling((float)this);
        }


        public int Floor()
        {
            return (int)Math.Floor((float)this);
        }


        public static Rational Max(Rational a, Rational b, params Rational[] rest)
        {
            Rational best = (a > b) ? a : b;
            foreach (Rational rational in rest)
            {
                if (rational > best)
                    best = rational;
            }
            return best;
        }


        public static Rational Min(Rational a, Rational b, params Rational[] rest)
        {
            Rational best = (a < b) ? a : b;
            foreach (Rational rational in rest)
            {
                if (rational < best)
                    best = rational;
            }
            return best;
        }


        /// <summary>
        /// Convert a decimal to a rational number, with required precision in number of decimal places
        /// </summary>
        /// <param name="n"></param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public static Rational FromDecimal(decimal n, int precision = 10)
        {
            if (n < 0)
                return ConvertFromDecimal(-n, precision) * -1;

            else
                return ConvertFromDecimal(n, precision);
        }

        private static Rational ConvertFromDecimal(decimal n, int precision = 8)
        {
            //Refer here for more info https://7137d208-a-62cb3a1a-s-sites.googlegroups.com/site/johnkennedyshome/home/downloadable-papers/dec2frac.pdf?attachauth=ANoY7co6YIPzshVVtHoLLubOggUAwYjdrojVF0KDyWQ87XpbCiGLiyaPt24s0OZI4utyZyggNbBd-Jf3YS7EqmRnq_-y9jf7u21003btTXOVYkqClXpxi4QSeXDgGw2djS8yO2VfjcdRG57IphGfPWZROUcw5AEmulENXvTZ2BJ_PFPqiiVk0xYvoCQjNnygaO70lpvkqJoTRfwxjAJX5EbF-IifF9APEbeKqRdKEyFV1l0J6aU6Sc-T91ihTEpkrEezGuq-UIPV&attredirects=0
            decimal filter = (decimal)Math.Pow(0.1, precision);
            decimal Z = n;
            int D_2 = 0;
            int D_1 = 0;
            int D = 1;
            int N = (int)Math.Round(n * D);

            //If the algorithm breaks due to an overflow exception, we don't want to return N / D since there's the possibility that D has been calculated one time more than N, which would mean the wrong value is returned
            int confirmedD = D;
            int confirmedN = N;
            try
            {
                while (true)
                {
                    decimal modZ = Z % 1;
                    if (Math.Abs(modZ) <= filter)
                        return new Rational(confirmedN, confirmedD);
                    Z = 1 / modZ;
                    D_2 = D_1;
                    D_1 = D;
                    D = (D_1 * (int)Math.Floor(Z)) + D_2;
                    N = (int)Math.Round(n * D);
                    confirmedD = D;
                    confirmedN = N;
                }
            }
            catch
            {
                return new Rational(confirmedN, confirmedD);
            }
        }
        #endregion Public Methods



        #region Private Methods
        /// <summary>
        /// Ensures that the rational is always kept in the correct format ie. in simplest form and with any negative always occuring on the numerator
        /// </summary>
        private void Simplify()
        {
            if (A == 1 || B == 1)
            {
            }
            else
            {
                //Remove any common factors from both sides
                long hcf = NumberTheory.HCF(A, B);
                _a /= hcf;
                _b /= hcf;
            }

            //Make sure any negatives are always on the top
            if (B < 0)
            {
                _a = -A;
                _b = -B;
            }
        }
        #endregion Private Methods



        #region Object Overrides
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj is Rational)
                return Equals((Rational)obj);

            return false;
        }


        public bool Equals(Rational rational)
        {
            return (A == rational.A && B == rational.B);
        }


        public override int GetHashCode()
        {
            return (int)(A ^ B);
        }


        public override string ToString()
        {
            return A.ToString() + "/" + B.ToString() + " (" + ((double)this).ToString() + ")";
        }
        #endregion Object Overrides



        #region IComparable Interface
        public int CompareTo(object obj)
        {
            Rational other = (Rational)obj;
            return CompareTo(other);
        }

        public int CompareTo(Rational other)
        {
            if (this < other)
                return -1;
            if (this > other)
                return 1;
            return 0;
        }
        #endregion IComparable Interface



        #region Operator Overloads
        public static bool operator ==(Rational r1, Rational r2)
        {
            return r1.Equals(r2);
        }

        public static bool operator !=(Rational r1, Rational r2)
        {
            return !r1.Equals(r2);
        }


        public static Rational operator +(Rational r, int x)
        {
            return new Rational(r.A + (r.B * x), r.B);
        }

        public static Rational operator +(int x, Rational r)
        {
            return r + x;
        }

        public static Rational operator +(Rational r1, Rational r2)
        {
            long lcm = NumberTheory.LCM(r1.B, r2.B);
            return new Rational((int)(((lcm / r1.B) * r1.A) + ((lcm / r2.B) * r2.A)), (int)lcm);
        }


        public static Rational operator -(Rational r, int x)
        {
            return new Rational(r.A - (r.B * x), r.B);
        }

        public static Rational operator -(int x, Rational r)
        {
            return new Rational((r.B * x) - r.A, r.B);
        }

        public static Rational operator -(Rational r1, Rational r2)
        {
            long lcm = NumberTheory.LCM(r1.B, r2.B);
            return new Rational((int)(((lcm / r1.B) * r1.A) - ((lcm / r2.B) * r2.A)), (int)lcm);
        }


        public static Rational operator *(Rational r, int x)
        {
            return new Rational(r.A * x, r.B);
        }

        public static Rational operator *(int x, Rational r)
        {
            return r * x;
        }

        public static Rational operator *(Rational r1, Rational r2)
        {
            return new Rational(r1.A * r2.A, r1.B * r2.B);
        }


        public static Rational operator /(Rational r, int x)
        {
            return new Rational(r.A, r.B * x);
        }

        public static Rational operator /(int x, Rational r)
        {
            return new Rational(r.B * x, r.A);
        }

        public static Rational operator /(Rational r1, Rational r2)
        {
            return new Rational(r1.A * r2.B, r1.B * r2.A);
        }


        public static Rational operator %(Rational r, int x)
        {
            return new Rational(r.A % (r.B * x), r.B);
        }

        public static Rational operator %(int x, Rational r)
        {
            return new Rational((r.B * x) % r.A, r.B);
        }

        public static Rational operator %(Rational r1, Rational r2)
        {
            long lcm = NumberTheory.LCM(r1.B, r2.B);
            return new Rational((int)(((lcm / r1.B) * r1.A) % ((lcm / r2.B) * r2.A)), (int)lcm);
        }


        public static bool operator <(Rational r, int x)
        {
            return ((float)r < x);
        }
        public static bool operator >(Rational r, int x)
        {
            return ((float)r > x);
        }

        public static bool operator <(Rational r, float x)
        {
            return ((float)r < x);
        }
        public static bool operator >(Rational r, float x)
        {
            return ((float)r > x);
        }

        public static bool operator <(Rational r, double x)
        {
            return ((double)r < x);
        }
        public static bool operator >(Rational r, double x)
        {
            return ((double)r > x);
        }

        public static bool operator <(Rational r, decimal x)
        {
            return ((decimal)r < x);
        }
        public static bool operator >(Rational r, decimal x)
        {
            return ((decimal)r > x);
        }

        public static bool operator <(Rational r1, Rational r2)
        {
            return ((double)r1 < (double)r2);
        }
        public static bool operator >(Rational r1, Rational r2)
        {
            return ((double)r1 > (double)r2);
        }


        public static bool operator <=(Rational r, int x)
        {
            return ((float)r <= x);
        }
        public static bool operator >=(Rational r, int x)
        {
            return ((float)r >= x);
        }

        public static bool operator <=(Rational r, float x)
        {
            return ((float)r <= x);
        }
        public static bool operator >=(Rational r, float x)
        {
            return ((float)r >= x);
        }

        public static bool operator <=(Rational r, double x)
        {
            return ((double)r <= x);
        }
        public static bool operator >=(Rational r, double x)
        {
            return ((double)r >= x);
        }

        public static bool operator <=(Rational r, decimal x)
        {
            return ((decimal)r <= x);
        }
        public static bool operator >=(Rational r, decimal x)
        {
            return ((decimal)r >= x);
        }

        public static bool operator <=(Rational r1, Rational r2)
        {
            return ((double)r1 <= (double)r2);
        }
        public static bool operator >=(Rational r1, Rational r2)
        {
            return ((double)r1 >= (double)r2);
        }
        #endregion Operator Overloads



        #region Type Conversions
        public static implicit operator decimal(Rational r)
        {
            return (decimal)r.A / r.B;
        }

        public static implicit operator double(Rational r)
        {
            return (double)r.A / r.B;
        }

        public static implicit operator float(Rational r)
        {
            return (float)r.A / r.B;
        }

        public static implicit operator Rational(int i)
        {
            return new Rational(i, 1);
        }
        #endregion Type Conversions

    }
}
