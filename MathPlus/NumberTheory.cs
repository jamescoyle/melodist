﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathPlus
{
    public class NumberTheory
    {

        /// <summary>
        /// The highest common factor of 2 integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int HCF(int a, int b)
        {
            if (a < b)
                return HCF(b, a);
            while (b != 0)
            {
                int c = a % b;
                a = b;
                b = c;
            }
            return a;
        }
        
        /// <summary>
        /// The least common multiple of 2 integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long LCM(int a, int b)
        {
            return (long)a * (long)b / HCF(a, b);
        }



        /// <summary>
        /// The highest common factor of 2 integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long HCF(long a, long b)
        {
            if (a < b)
                return HCF(b, a);
            while (b != 0)
            {
                long c = a % b;
                a = b;
                b = c;
            }
            return a;
        }

        /// <summary>
        /// The least common multiple of 2 integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long LCM(long a, long b)
        {
            return a * b / HCF(a, b);
        }



        /// <summary>
        /// The highest common factor of 2 rationals
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Rational HCF(Rational a, Rational b)
        {
            long denominatorLcm = LCM(a.B, b.B);
            long numeratorHcf = HCF(a.A * denominatorLcm, b.A * denominatorLcm);
            return new Rational((int)(numeratorHcf / denominatorLcm), (int)denominatorLcm);
        }

        /// <summary>
        /// The highest common factor of any number of rationals
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="rest"></param>
        /// <returns></returns>
        public static Rational HCF(Rational a, Rational b, Rational c, params Rational[] rest)
        {
            Rational hcf = HCF(HCF(a, b), c);
            foreach (Rational r in rest)
                hcf = HCF(hcf, r);
            return hcf;
        }

        /// <summary>
        /// The highest common factor of any number of rationals
        /// </summary>
        /// <param name="rationals"></param>
        /// <returns></returns>
        public static Rational HCF(List<Rational> rationals)
        {
            Rational hcf = HCF(rationals[0], rationals[1]);
            for (int i = 2; i < rationals.Count; i++)
                hcf = HCF(hcf, rationals[i]);
            return hcf;
        }

        /// <summary>
        /// The least common multiple of 2 rationals
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Rational LCM(Rational a, Rational b)
        {
            return a * b / HCF(a, b);
        }

        /// <summary>
        /// The least common multiple of any number of rationals
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="rest"></param>
        /// <returns></returns>
        public static Rational LCM(Rational a, Rational b, Rational c, params Rational[] rest)
        {
            Rational lcm = LCM(a, b);
            lcm = LCM(lcm, c);
            foreach (Rational r in rest)
                lcm = LCM(lcm, r);
            return lcm;
        }

        /// <summary>
        /// The least common multiple of any number of rationals
        /// </summary>
        /// <param name="rationals"></param>
        /// <returns></returns>
        public static Rational LCM(List<Rational> rationals)
        {
            Rational lcm = LCM(rationals[0], rationals[1]);
            for (int i = 2; i < rationals.Count; i++)
                lcm = LCM(lcm, rationals[i]);
            return lcm;
        }

    }
}
