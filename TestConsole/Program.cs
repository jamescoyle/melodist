﻿using CSCore.CoreAudioAPI;
using GeneralMidi;
using HarmonyEngine;
using MidiPlayback;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            LatencyTest();
            Console.Read();
        }


        private static void DissonanceTest()
        {
            for(int i = 0; i < 128; i++)
            {
                Console.WriteLine(i.ToString() + "," + new Interval(i).Dissonance(false).ToString());
            }
        }


        private static void WarpTest()
        {
            Phrase manWhoSoldTheWorld = Phrase.Load(@"C:\Users\james\Documents\Melodist Projects\New Acoustic\Man Who Sold The World Verse 2.phr");
            Phrase cannonball = Phrase.Load(@"C:\Users\james\Documents\Melodist Projects\New Acoustic\Damian Rice Cannonball Verse.phr");

            ContourWarper contourWarp = new ContourWarper(manWhoSoldTheWorld, 2, 2, 1.3);
            HarmonyWarper harmonyWarp = new HarmonyWarper(manWhoSoldTheWorld, 2, 1.3);
            RhythmWarper rhythmWarp = new RhythmWarper(manWhoSoldTheWorld, 3);

            Phrase output = cannonball;
            rhythmWarp.UseRecommendedDistanceParameters(output);

            List<IPhraseWarper> warps = new List<IPhraseWarper>() { rhythmWarp, contourWarp, harmonyWarp };
            foreach (IPhraseWarper warp in warps)
                output = warp.Run(output);

            output.Save(@"C:\Users\james\Documents\Melodist Projects\New Acoustic\Man Who Sold Cannonballs 5.phr");
        }


        public static void LatencyTest()
        {
            using (MidiDevice inputDevice = MidiDevice.Default)
            {
                using (MMDevice outputDevice = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Console))
                {
                    using (AudioMeterInformation outputMeter = AudioMeterInformation.FromDevice(outputDevice))
                    {
                        Stopwatch stopwatch = new Stopwatch();

                        inputDevice.SendProgramChange(1, 0);
                        inputDevice.SendNoteOn(1, 60);

                        stopwatch.Start();
                        while (stopwatch.ElapsedMilliseconds < 2000)
                        {
                            float outputPeak = outputMeter.PeakValue;
                            Console.WriteLine("Output audio peak: " + outputPeak);
                            if (outputPeak > 0)
                            {
                                stopwatch.Stop();
                                break;
                            }
                        }

                        inputDevice.SendAllNotesOff(1);

                        Console.WriteLine("Elapsed milliseconds: " + stopwatch.ElapsedMilliseconds);
                    }
                }
            }
        }
    }
}
