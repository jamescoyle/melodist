﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.MusicViewerActions
{
    public class RightResizeAction : MusicViewerAction, IUndoable
    {

        private Rational _dragStartBeat;
        private Rational _totalBeatDrag;

        private List<FiniteTimeObject> _objects;

        public bool IsUndone { get; private set; }



        public RightResizeAction(MusicViewer musicViewer)
            : base(musicViewer)
        {
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objects != null)
                return;

            _dragStartBeat = MusicViewer.GetMouseBeat();
            _totalBeatDrag = 0;

            _objects = DefaultInitialSelector();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            Rational dragBeat = MusicViewer.GetMouseBeat();

            Rational newTotalBeatDrag = MusicViewer.Quantise(dragBeat - _dragStartBeat);

            //Block the resize from making any note have negative duration or extend past the phrase end beat
            for (int i = 0; i < _objects.Count; i++)
            {
                Rational newNoteEndBeat = _objects[i].EndBeat + newTotalBeatDrag - _totalBeatDrag;
                if (newNoteEndBeat < _objects[i].StartBeat || newNoteEndBeat > MusicViewer.EndBeat)
                    return;
            }

            //Apply the changes to the notes
            for (int i = 0; i < _objects.Count; i++)
                _objects[i].EndBeat += newTotalBeatDrag - _totalBeatDrag;

            _totalBeatDrag = newTotalBeatDrag;
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            Finished = true;
        }



        public void Undo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
                finiteTimeObject.EndBeat -= _totalBeatDrag;

            IsUndone = true;
        }


        public void Redo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
                finiteTimeObject.EndBeat += _totalBeatDrag;

            IsUndone = true;
        }

    }
}
