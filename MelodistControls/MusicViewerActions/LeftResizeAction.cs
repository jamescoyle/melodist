﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.MusicViewerActions
{
    public class LeftResizeAction : MusicViewerAction, IUndoable
    {

        private Rational _dragStartBeat;
        private Rational _totalBeatDrag;

        private List<FiniteTimeObject> _objects;

        public bool IsUndone { get; private set; }



        public LeftResizeAction(MusicViewer musicViewer)
            : base(musicViewer)
        {
            IsUndone = false;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objects != null)
                return;

            _dragStartBeat = MusicViewer.GetMouseBeat();
            _totalBeatDrag = 0;

            _objects = DefaultInitialSelector();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            Rational dragBeat = MusicViewer.GetMouseBeat();

            Rational newTotalBeatDrag = MusicViewer.Quantise(dragBeat - _dragStartBeat);

            //Block the resize from making any note have negative duration or extend past the piano roll start beat
            for (int i = 0; i < _objects.Count; i++)
            {
                Rational newNoteStartBeat = _objects[i].StartBeat + newTotalBeatDrag - _totalBeatDrag;
                if (newNoteStartBeat > _objects[i].EndBeat || newNoteStartBeat < MusicViewer.StartBeat)
                    return;
            }

            //Apply the changes to the notes
            for (int i = 0; i < _objects.Count; i++)
            {
                _objects[i].StartBeat += newTotalBeatDrag - _totalBeatDrag;
                _objects[i].Duration -= newTotalBeatDrag - _totalBeatDrag;
            }

            _totalBeatDrag = newTotalBeatDrag;
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            Finished = true;
        }



        public void Undo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
            {
                finiteTimeObject.StartBeat -= _totalBeatDrag;
                finiteTimeObject.Duration += _totalBeatDrag;
            }
            IsUndone = true;
        }


        public void Redo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
            {
                finiteTimeObject.StartBeat += _totalBeatDrag;
                finiteTimeObject.Duration -= _totalBeatDrag;
            }
            IsUndone = true;
        }

    }
}
