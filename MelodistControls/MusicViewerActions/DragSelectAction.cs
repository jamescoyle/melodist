﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.MusicViewerActions
{
    public class DragSelectAction : MusicViewerAction
    {

        private Point _dragStartPoint;

        private Rational _dragStartBeat;

        private Rectangle _rect;

        private List<FiniteTimeObjectViewer> _objectViewers;



        public DragSelectAction(MusicViewer musicViewer)
            : base(musicViewer)
        {
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objectViewers != null)
                return;

            _dragStartPoint = e.GetPosition(MusicViewer.ForegroundCanvas);
            _dragStartBeat = MusicViewer.GetMouseBeat();

            _objectViewers = new List<FiniteTimeObjectViewer>();
            UpdateSelectedObjects();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            UpdateSelectedObjects();

            Point dragEndPoint = e.GetPosition(MusicViewer.ForegroundCanvas);

            if (_rect == null)
            {
                _rect = new Rectangle()
                {
                    IsHitTestVisible = false,
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = 2,
                    Fill = new SolidColorBrush(Colors.Transparent)
                };
                MusicViewer.ForegroundCanvas.Children.Add(_rect);
            }

            _rect.Width = Math.Abs(dragEndPoint.X - _dragStartPoint.X);
            _rect.Height = Math.Abs(dragEndPoint.Y - _dragStartPoint.Y);
            Canvas.SetLeft(_rect, Math.Min(_dragStartPoint.X, dragEndPoint.X));
            Canvas.SetTop(_rect, Math.Min(_dragStartPoint.Y, dragEndPoint.Y));
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (_rect != null)
                MusicViewer.ForegroundCanvas.Children.Remove(_rect);

            _rect = null;
            Finished = true;
        }


        private void UpdateSelectedObjects()
        {
            Rational dragEndBeat = MusicViewer.GetMouseBeat();

            Rational beat1 = Rational.Min(_dragStartBeat, dragEndBeat);
            Rational beat2 = Rational.Max(_dragStartBeat, dragEndBeat);

            List<FiniteTimeObjectViewer> finiteTimeObjectViewers = MusicViewer.GetFiniteTimeObjectViewers();

            foreach (FiniteTimeObjectViewer ftov in finiteTimeObjectViewers)
            {
                if (!ftov.Selected)
                {
                    if (IsObjectInDragArea(ftov, beat1, beat2))
                    {
                        _objectViewers.Add(ftov);
                        ftov.Selected = true;
                    }
                }
            }

            for (int i = _objectViewers.Count - 1; i >= 0; i--)
            {
                FiniteTimeObjectViewer ftov = _objectViewers[i];
                if (!IsObjectInDragArea(ftov, beat1, beat2))
                {
                    ftov.Selected = false;
                    _objectViewers.RemoveAt(i);
                }
            }
        }


        private bool IsObjectInDragArea(FiniteTimeObjectViewer ftov, Rational beat1, Rational beat2)
        {
            Rational minBeat = Rational.Min(beat1, beat2);
            Rational maxBeat = Rational.Max(beat1, beat2);

            return !(maxBeat < ftov.StartBeat || minBeat > ftov.EndBeat);
        }

    }
}
