﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.MusicViewerActions
{
    public class MoveAction : MusicViewerAction, IUndoable
    {

        private Rational _dragStartBeat;
        private Rational _totalBeatDrag;

        private Rational _cumulativeBeatDrag;
        private DateTime _startTime;

        private List<FiniteTimeObject> _objects;

        public bool IsUndone { get; private set; }



        public MoveAction(MusicViewer musicViewer)
            : base(musicViewer)
        {
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objects != null)
                return;

            _dragStartBeat = MusicViewer.GetMouseBeat();
            _totalBeatDrag = 0;

            _cumulativeBeatDrag = 0;
            _startTime = DateTime.Now;

            _objects = DefaultInitialSelector();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            Rational dragBeat = MusicViewer.GetMouseBeat();

            Rational newTotalBeatDrag = MusicViewer.Quantise(dragBeat - _dragStartBeat);

            //Block the resize from making any note have negative duration, extend past the piano roll end beat or have an out of range pitch
            for (int i = 0; i < _objects.Count; i++)
            {
                Rational newNoteStartBeat = _objects[i].StartBeat + newTotalBeatDrag - _totalBeatDrag;
                Rational newNoteEndBeat = _objects[i].EndBeat + newTotalBeatDrag - _totalBeatDrag;
                if (newNoteStartBeat < MusicViewer.StartBeat || newNoteEndBeat > MusicViewer.EndBeat)
                    newTotalBeatDrag = _totalBeatDrag;
            }

            //Apply the changes to the notes
            for (int i = 0; i < _objects.Count; i++)
                _objects[i].StartBeat += newTotalBeatDrag - _totalBeatDrag;

            _cumulativeBeatDrag += (_totalBeatDrag - newTotalBeatDrag).Abs();
            _totalBeatDrag = newTotalBeatDrag;
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            //If there has been absolutely no movement, then deselect all objects except the one that the mouse is over
            if (_cumulativeBeatDrag == 0 && DateTime.Now.Subtract(_startTime).Milliseconds <= 250)
            {
                List<FiniteTimeObjectViewer> objectsUnderMouse = MusicViewer.GetFiniteTimeObjectViewersUnderMouse();
                if (objectsUnderMouse.Count > 0)
                {
                    FiniteTimeObjectViewer objectUnderMouse = objectsUnderMouse.Last();
                    foreach (FiniteTimeObjectViewer selectedObject in MusicViewer.GetSelectedFiniteTimeObjectViewers())
                    {
                        if (selectedObject != objectUnderMouse)
                            selectedObject.Selected = false;
                    }
                }
            }

            Finished = true;
        }



        public void Undo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
            {
                finiteTimeObject.StartBeat -= _totalBeatDrag;
            }
            IsUndone = true;
        }


        public void Redo()
        {
            foreach (FiniteTimeObject finiteTimeObject in _objects)
            {
                finiteTimeObject.StartBeat += _totalBeatDrag;
            }
            IsUndone = true;
        }

    }
}
