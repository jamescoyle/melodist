﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.MusicViewerActions
{
    public class CopyAction : MusicViewerAction
    {

        public static List<FiniteTimeObject> Clipboard { get; private set; }


        public CopyAction(MusicViewer musicViewer)
            : base(musicViewer)
        {
            List<FiniteTimeObject> selectedObjects = musicViewer.GetSelectedFiniteTimeObjectViewers().Select(x => x.FiniteTimeObject).ToList();
            Clipboard = selectedObjects;
            Finished = true;
        }


        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }

    }
}