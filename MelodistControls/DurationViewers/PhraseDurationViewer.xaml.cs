﻿using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MelodistControls.DurationViewers
{
    public partial class PhraseDurationViewer : DurationViewer
    {

        private Phrase _phrase;
        public Phrase Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                if (_phrase != null)
                {
                    _phrase.BeatsPerBarChanged -= OnBeatsPerBarChanged;
                    _phrase.StartBeatChanged -= OnPhraseStartBeatChanged;
                    _phrase.DurationChanged -= OnPhraseDurationChanged;
                }

                _phrase = value;

                StartBeat = _phrase.StartBeat.Floor();
                EndBeat = _phrase.EndBeat.Ceiling();

                BeatRangeViewer.BeatRange.StartBeat = _phrase.StartBeat;
                BeatRangeViewer.BeatRange.EndBeat = _phrase.EndBeat;

                _phrase.BeatsPerBarChanged += OnBeatsPerBarChanged;
                OnBeatsPerBarChanged(null, null);

                _phrase.StartBeatChanged += OnPhraseStartBeatChanged;
                _phrase.DurationChanged += OnPhraseDurationChanged;
            }
        }



        public PhraseDurationViewer()
            : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();

            BeatRangeViewer.BeatRange.StartBeatChanged += (sender, e) =>
            {
                if (Phrase != null)
                    Phrase.StartBeat = BeatRangeViewer.BeatRange.StartBeat;
            };

            BeatRangeViewer.BeatRange.EndBeatChanged += (sender, e) =>
            {
                if (Phrase != null)
                    Phrase.EndBeat = BeatRangeViewer.BeatRange.EndBeat;
            };
            
            StartBeatChanged += (sender, e) =>
            {
                if (Phrase != null)
                    Phrase.StartBeat = Rational.Max(Phrase.StartBeat, StartBeat);
            };

            BeatDurationChanged += (sender, e) =>
            {
                if (Phrase != null)
                    Phrase.EndBeat = Rational.Min(Phrase.EndBeat, EndBeat);
            };
        }


        private void OnBeatsPerBarChanged(object sender, EventArgs e)
        {
            TimeSignaturesManager.Clear();
            TimeSignaturesManager.Add(new TimeSignature(0, _phrase.BeatsPerBar, 4));
        }


        private void OnPhraseStartBeatChanged(object sender, EventArgs e)
        {
            BeatRangeViewer.BeatRange.StartBeat = Phrase.StartBeat;
        }


        private void OnPhraseDurationChanged(object sender, EventArgs e)
        {
            BeatRangeViewer.BeatRange.EndBeat = Phrase.EndBeat;
        }

    }
}
