﻿using HarmonyEngine;
using MathPlus;
using MelodistControls.MusicViewerActions;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.DurationViewers
{
    public class DurationViewer : MusicViewer
    {

        public BeatRangeViewer BeatRangeViewer { get; private set; }



        public DurationViewer()
        {
        }


        protected override void Init()
        {
            Painter = new DurationViewerPainter(this);

            StartBeatChanged += OnBeatRangePropertyChanged;
            BeatWidthChanged += OnBeatRangePropertyChanged;

            BeatRangeViewer = new BeatRangeViewer(new FiniteTimeObject(0, 1));
            ForegroundCanvas.Children.Add(BeatRangeViewer);

            BackgroundCanvas.Background = new SolidColorBrush(Colors.LightGray);

            BeatRangeViewer.BeatRange.StartBeatChanged += OnBeatRangePropertyChanged;
            BeatRangeViewer.BeatRange.DurationChanged += OnBeatRangePropertyChanged;
            OnBeatRangePropertyChanged(BeatRangeViewer.BeatRange, null);

            Binding binding = new Binding("ActualHeight") { Source = this, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
            BindingOperations.SetBinding(BeatRangeViewer, HeightProperty, binding);

            ForegroundCanvas.MouseDown += OnMouseDown;
            ForegroundCanvas.MouseMove += OnMouseMove;
            ForegroundCanvas.MouseUp += OnMouseUp;

            base.Init();
        }


        private void OnBeatRangePropertyChanged(object sender, EventArgs e)
        {
            BeatRangeViewer.BeatRange.StartBeat = Quantise(BeatRangeViewer.BeatRange.StartBeat);
            BeatRangeViewer.BeatRange.Duration = Quantise(BeatRangeViewer.BeatRange.Duration);
            Canvas.SetLeft(BeatRangeViewer, (BeatRangeViewer.BeatRange.StartBeat - StartBeat) * BeatWidth);
            BeatRangeViewer.Width = BeatRangeViewer.BeatRange.Duration * BeatWidth;
        }



        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewers()
        {
            return new List<FiniteTimeObjectViewer>() { BeatRangeViewer };
        }


        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewersUnderMouse()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            Rational mouseBeat = GetMouseBeat();
            if (BeatRangeViewer.StartBeat <= mouseBeat && BeatRangeViewer.EndBeat >= mouseBeat)
                output.Add(BeatRangeViewer);
            return output;
        }


        public override List<FiniteTimeObjectViewer> GetSelectedFiniteTimeObjectViewers()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            if (BeatRangeViewer.Selected)
                output.Add(BeatRangeViewer);
            return output;
        }



        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Focus();

            Rational mouseBeat = GetMouseBeat();
            if (mouseBeat < StartBeat || mouseBeat > EndBeat)
                return;

            if (CurrentAction == null)
            {
                if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && e.LeftButton == MouseButtonState.Pressed)
                    CurrentAction = new DragSelectAction(this);
                else
                {
                    if (ReadOnly)
                        return;

                    List<FiniteTimeObjectViewer> objectsUnderMouse = GetFiniteTimeObjectViewersUnderMouse();
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        if (objectsUnderMouse.Count == 0)
                            BeatRangeViewer.Selected = false;
                        else
                        {
                            FiniteTimeObjectViewer objectUnderMouse = objectsUnderMouse.Last();

                            if ((mouseBeat - objectUnderMouse.EndBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new RightResizeAction(this);

                            else if ((mouseBeat - objectUnderMouse.StartBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new LeftResizeAction(this);

                            else
                                CurrentAction = new MoveAction(this);
                        }
                    }
                }
            }

            CurrentAction_MouseDown(e);
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            DefaultMouseMoveBehaviour(sender, e);
        }


        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseUpBehaviour(sender, e);
        }

    }



    public class DurationViewerPainter : MusicViewerPainter
    {

        protected List<Label> BarLabels { get; private set; }


        public DurationViewerPainter(DurationViewer durationViewer)
            : base(durationViewer)
        {
            BarLineStyle = new Style(typeof(Line));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.Black)));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));

            BarLabels = new List<Label>();
        }


        public override void PaintBack()
        {
            //Remove all previously drawn bar labels
            BarLabels.ForEach(x => MusicViewer.BackgroundCanvas.Children.Remove(x));
            BarLabels.Clear();

            //Define the iterator i, make sure it is positioned at the position where it will start drawing from
            Rational i = 0;
            while (i < MusicViewer.StartBeat)
                i += 1;
            while (i > MusicViewer.StartBeat)
                i -= 1;
            if (i < MusicViewer.StartBeat)
                i += 1;

            //Start looping through, drawing bar line labels
            for (; i <= MusicViewer.EndBeat; i += 1)
            {
                if (BarLineStyle != null && i % 1 == 0)
                {
                    TimeSignature timeSig = MusicViewer.GetTimeSignatureAt(i);
                    if (timeSig != null)
                    {
                        if (
                            (i < 0 && (i - timeSig.Beat) % timeSig.QuarterBeatsPerBar == 0) ||
                            (i >= 0 && (i - Rational.Max(0, timeSig.Beat)) % timeSig.QuarterBeatsPerBar == 0)
                        )
                        {
                            Label label = new Label()
                            {
                                Content = ((decimal)i).ToString(),
                                FontFamily = new FontFamily("Calibri"),
                                FontSize = 14f,
                                FontStyle = FontStyles.Normal,
                                FontWeight = FontWeights.Bold,
                                Foreground = new SolidColorBrush(Color.FromRgb(50, 50, 50))
                            };
                            Canvas.SetLeft(label, (i - MusicViewer.StartBeat) * MusicViewer.BeatWidth);
                            MusicViewer.BackgroundCanvas.Children.Add(label);
                            BarLabels.Add(label);
                        }
                    }
                }
            }
        }

    }
}
