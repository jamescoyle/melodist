﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls.DurationViewers
{
    /// <summary>
    /// Interaction logic for BeatRangeViewer.xaml
    /// </summary>
    public partial class BeatRangeViewer : FiniteTimeObjectViewer
    {

        public FiniteTimeObject BeatRange { get; private set; }



        public BeatRangeViewer(FiniteTimeObject beatRange, Color? colour = null)
            : base(beatRange, (colour == null) ? Color.FromArgb(136, 136, 204, 136) : colour.Value)
        {
            InitializeComponent();
            BeatRange = beatRange;
        }

    }
}
