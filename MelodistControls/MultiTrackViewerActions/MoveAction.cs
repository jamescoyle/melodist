﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.MultiTrackViewerActions
{
    public class MoveAction<T> : MultiTrackViewerAction where T : FiniteTimeObject
    {

        private Rational _dragStartBeat;
        private Rational _totalBeatDrag;

        private int _dragStartTrack;
        private int _totalTrackDrag;

        private Rational _cumulativeBeatDrag;
        private int _cumulativeTrackDrag;
        private DateTime _startTime;

        private List<T> _objects;

        public bool IsUndone { get; private set; }

        private Func<T, int> GetTrack;

        private Action<T, int> SetTrack;



        public MoveAction(MultiTrackViewer multiTrackViewer, Func<T, int> getTrack, Action<T, int> setTrack)
            : base(multiTrackViewer)
        {
            GetTrack = getTrack;
            SetTrack = setTrack;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objects != null)
                return;

            _dragStartBeat = MultiTrackViewer.GetMouseBeat();
            _totalBeatDrag = 0;

            _dragStartTrack = MultiTrackViewer.GetMouseTrack();
            _totalTrackDrag = 0;

            _cumulativeBeatDrag = 0;
            _startTime = DateTime.Now;

            _objects = DefaultInitialSelector().OfType<T>().ToList();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            Rational dragBeat = MultiTrackViewer.GetMouseBeat();
            int dragTrack = MultiTrackViewer.GetMouseTrack();

            Rational newTotalBeatDrag = MultiTrackViewer.Quantise(dragBeat - _dragStartBeat);
            int newTotalTrackDrag = dragTrack - _dragStartTrack;

            //Block the move from making any object start before the start beat, or end after the end beat
            for (int i = 0; i < _objects.Count; i++)
            {
                Rational newStartBeat = _objects[i].StartBeat + newTotalBeatDrag - _totalBeatDrag;
                Rational newEndBeat = _objects[i].EndBeat + newTotalBeatDrag - _totalBeatDrag;
                if (newStartBeat < MultiTrackViewer.StartBeat || newEndBeat > MultiTrackViewer.EndBeat)
                    newTotalBeatDrag = _totalBeatDrag;

                int newTrack = GetTrack(_objects[i]) + newTotalTrackDrag - _totalTrackDrag;
                if (newTrack < 0 || newTrack >= MultiTrackViewer.TrackCount)
                    newTotalTrackDrag = _totalTrackDrag;
            }

            //Apply the changes to the objects
            for (int i = 0; i < _objects.Count; i++)
            {
                _objects[i].StartBeat += newTotalBeatDrag - _totalBeatDrag;
                SetTrack(_objects[i], GetTrack(_objects[i]) + newTotalTrackDrag - _totalTrackDrag);
            }

            _cumulativeBeatDrag += (_totalBeatDrag - newTotalBeatDrag).Abs();
            _cumulativeTrackDrag += Math.Abs(_totalTrackDrag - newTotalTrackDrag);

            _totalBeatDrag = newTotalBeatDrag;
            _totalTrackDrag = newTotalTrackDrag;
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            //If there has been absolutely no movement, then deselect all objects except the one that the mouse is over
            if (_cumulativeBeatDrag == 0 && _cumulativeTrackDrag == 0 && DateTime.Now.Subtract(_startTime).Milliseconds <= 250)
            {
                List<FiniteTimeTrackObjectViewer> objectsUnderMouse = MultiTrackViewer.GetFiniteTimeTrackObjectViewersUnderMouse();
                if (objectsUnderMouse.Count > 0)
                {
                    FiniteTimeTrackObjectViewer objectUnderMouse = objectsUnderMouse.Last();
                    foreach (FiniteTimeTrackObjectViewer selectedObject in MultiTrackViewer.GetSelectedFiniteTimeTrackObjectViewers())
                    {
                        if (selectedObject != objectUnderMouse)
                            selectedObject.Selected = false;
                    }
                }
            }

            Finished = true;
        }



        public void Undo()
        {
            foreach (T obj in _objects)
            {
                obj.StartBeat -= _totalBeatDrag;
                SetTrack(obj, GetTrack(obj) - _totalTrackDrag);
            }
            IsUndone = true;
        }


        public void Redo()
        {
            foreach (T obj in _objects)
            {
                obj.StartBeat += _totalBeatDrag;
                SetTrack(obj, GetTrack(obj) + _totalTrackDrag);
            }
            IsUndone = true;
        }

    }
}
