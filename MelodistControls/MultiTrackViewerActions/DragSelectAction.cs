﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.MultiTrackViewerActions
{
    public class DragSelectAction : MultiTrackViewerAction
    {

        private Point _dragStartPoint;

        private Rational _dragStartBeat;

        private int _dragStartTrack;

        private Rectangle _rect;

        private List<FiniteTimeTrackObjectViewer> _objectViewers;



        public DragSelectAction(MultiTrackViewer multiTrackViewer)
            : base(multiTrackViewer)
        {
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_objectViewers != null)
                return;

            _dragStartPoint = e.GetPosition(MusicViewer.ForegroundCanvas);
            _dragStartBeat = MusicViewer.GetMouseBeat();
            _dragStartTrack = MultiTrackViewer.GetMouseTrack();

            _objectViewers = new List<FiniteTimeTrackObjectViewer>();
            UpdateSelectedObjects();
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            UpdateSelectedObjects();

            Point dragEndPoint = e.GetPosition(MusicViewer.ForegroundCanvas);

            if (_rect == null)
            {
                _rect = new Rectangle()
                {
                    IsHitTestVisible = false,
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = 2,
                    Fill = new SolidColorBrush(Colors.Transparent)
                };
                MusicViewer.ForegroundCanvas.Children.Add(_rect);
            }

            _rect.Width = Math.Abs(dragEndPoint.X - _dragStartPoint.X);
            _rect.Height = Math.Abs(dragEndPoint.Y - _dragStartPoint.Y);
            Canvas.SetLeft(_rect, Math.Min(_dragStartPoint.X, dragEndPoint.X));
            Canvas.SetTop(_rect, Math.Min(_dragStartPoint.Y, dragEndPoint.Y));
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (_rect != null)
                MusicViewer.ForegroundCanvas.Children.Remove(_rect);

            _rect = null;
            Finished = true;
        }


        private void UpdateSelectedObjects()
        {
            Rational dragEndBeat = MusicViewer.GetMouseBeat();
            int dragEndTrack = MultiTrackViewer.GetMouseTrack();

            List<FiniteTimeTrackObjectViewer> finiteTimeObjectViewers = MultiTrackViewer.GetFiniteTimeTrackObjectViewers();

            foreach (FiniteTimeTrackObjectViewer ftov in finiteTimeObjectViewers)
            {
                if (!ftov.Selected)
                {
                    if (IsObjectInDragArea(ftov, _dragStartBeat, dragEndBeat, _dragStartTrack, dragEndTrack))
                    {
                        _objectViewers.Add(ftov);
                        ftov.Selected = true;
                    }
                }
            }

            for (int i = _objectViewers.Count - 1; i >= 0; i--)
            {
                FiniteTimeTrackObjectViewer ftov = _objectViewers[i];
                if (!IsObjectInDragArea(ftov, _dragStartBeat, dragEndBeat, _dragStartTrack, dragEndTrack))
                {
                    ftov.Selected = false;
                    _objectViewers.RemoveAt(i);
                }
            }
        }


        private bool IsObjectInDragArea(FiniteTimeTrackObjectViewer ftov, Rational beat1, Rational beat2, int track1, int track2)
        {
            Rational minBeat = Rational.Min(beat1, beat2);
            Rational maxBeat = Rational.Max(beat1, beat2);

            int minTrack = Math.Min(track1, track2);
            int maxTrack = Math.Max(track1, track2);

            return !(maxBeat < ftov.StartBeat || minBeat > ftov.EndBeat || maxTrack < ftov.Track || minTrack > ftov.Track);
        }

    }
}
