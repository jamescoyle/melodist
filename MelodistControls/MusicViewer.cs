﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using MelodistControls.MusicViewerActions;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls
{
    /// <summary>
    /// Interaction logic for MusicViewer.xaml
    /// </summary>
    public abstract class MusicViewer : UserControl, INotifyPropertyChanged
    {

        #region Public Properties
        public Canvas BackgroundCanvas { get; protected set; }

        public Canvas ForegroundCanvas { get; protected set; }


        public event EventHandler BeatWidthChanged;
        public static DependencyProperty BeatWidthProperty = DependencyProperty.Register("BeatWidth", typeof(int), typeof(MusicViewer), new PropertyMetadata(80, OnBeatWidthDPChanged));
        private static void OnBeatWidthDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MusicViewer instance = (MusicViewer)d;
            instance.SetBeatWidthSideEffects();
        }
        public int BeatWidth
        {
            get
            {
                return (int)GetValue(BeatWidthProperty);
            }
            set
            {
                if (value != BeatWidth)
                {
                    SetValue(BeatWidthProperty, value);
                    SetBeatWidthSideEffects();
                }
            }
        }
        private void SetBeatWidthSideEffects()
        {
            BackgroundCanvas.Width = BeatDuration * BeatWidth;
            OnPropertyChanged("BeatWidth");
            BeatWidthChanged?.Invoke(this, null);
        }



        public event EventHandler UseFlatsChanged;
        public static DependencyProperty UseFlatsProperty = DependencyProperty.Register("UseFlats", typeof(bool), typeof(MusicViewer), new PropertyMetadata(false, OnUseFlatsDPChanged));
        private static void OnUseFlatsDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MusicViewer instance = (MusicViewer)d;
            instance.SetUseFlatsSideEffects();
        }
        public bool UseFlats
        {
            get
            {
                return (bool)GetValue(UseFlatsProperty);
            }
            set
            {
                if (value != UseFlats)
                {
                    SetValue(UseFlatsProperty, value);
                    SetUseFlatsSideEffects();
                }
            }
        }
        private void SetUseFlatsSideEffects()
        {
            OnPropertyChanged("UseFlats");
            UseFlatsChanged?.Invoke(this, null);
        }



        public event EventHandler BeatDivisionsChanged;
        public static DependencyProperty BeatDivisionsProperty = DependencyProperty.Register("BeatDivisions", typeof(int), typeof(MusicViewer), new PropertyMetadata(24, OnBeatDivisionsDPChanged));
        private static void OnBeatDivisionsDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MusicViewer instance = (MusicViewer)d;
            instance.SetBeatDivisionsSideEffects();
        }
        public int BeatDivisions
        {
            get
            {
                return (int)GetValue(BeatDivisionsProperty);
            }
            set
            {
                if (value != BeatDivisions)
                {
                    SetValue(BeatDivisionsProperty, value);
                    SetBeatDivisionsSideEffects();
                }
            }
        }
        private void SetBeatDivisionsSideEffects()
        {
            OnPropertyChanged("BeatDivisions");
            BeatDivisionsChanged?.Invoke(this, null);
        }



        public event EventHandler StartBeatChanged;
        public static DependencyProperty StartBeatProperty = DependencyProperty.Register("StartBeat", typeof(Rational), typeof(MusicViewer), new PropertyMetadata(new Rational(0, 1), OnStartBeatDPChanged));
        private static void OnStartBeatDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MusicViewer instance = (MusicViewer)d;
            instance.SetStartBeatChangedSideEffects();
        }
        public Rational StartBeat
        {
            get
            {
                return (Rational)GetValue(StartBeatProperty);
            }
            set
            {
                if (value != StartBeat)
                {
                    SetValue(StartBeatProperty, value);
                    SetStartBeatChangedSideEffects();
                }
            }
        }
        private void SetStartBeatChangedSideEffects()
        {
            OnPropertyChanged("StartBeat");
            StartBeatChanged?.Invoke(this, null);
            OnPropertyChanged("EndBeat");
            EndBeatChanged?.Invoke(this, null);
        }



        public event EventHandler BeatDurationChanged;
        public static DependencyProperty BeatDurationProperty = DependencyProperty.Register("BeatDuration", typeof(Rational), typeof(MusicViewer), new PropertyMetadata(new Rational(1, 1), OnBeatDurationDPChanged));
        private static void OnBeatDurationDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MusicViewer instance = (MusicViewer)d;
            instance.SetBeatDurationSideEffects();
        }
        public Rational BeatDuration
        {
            get
            {
                return (Rational)GetValue(BeatDurationProperty);
            }
            set
            {
                Rational newBeatDuration = Rational.Max(0, value);
                if (newBeatDuration != BeatDuration)
                {
                    SetValue(BeatDurationProperty, newBeatDuration);
                    SetBeatDurationSideEffects();
                }
            }
        }
        private void SetBeatDurationSideEffects()
        {
            BackgroundCanvas.Width = BeatDuration * BeatWidth;
            OnPropertyChanged("BeatDuration");
            BeatDurationChanged?.Invoke(this, null);
            OnPropertyChanged("EndBeat");
            EndBeatChanged?.Invoke(this, null);
        }



        public event EventHandler EndBeatChanged;
        public Rational EndBeat
        {
            get
            {
                return StartBeat + BeatDuration;
            }
            set
            {
                BeatDuration = value - StartBeat;
            }
        }



        protected MemberListManager<TimeSignature> TimeSignaturesManager { get; private set; }
        public MemberList<TimeSignature> TimeSignatures { get { return TimeSignaturesManager.List; } }



        public MusicViewerPainter Painter { get; set; }


        public UndoableStack PreviousActions { get; private set; }

        public MusicViewerAction CurrentAction { get; protected set; }

        public bool ReadOnly { get; set; }
        #endregion Public Properties



        public MusicViewer()
        {
            PreviousActions = new UndoableStack();

            TimeSignaturesManager = new MemberListManager<TimeSignature>();

            ReadOnly = false;

            BeatWidthChanged += OnPaintRequired;
            StartBeatChanged += OnPaintRequired;
            BeatDurationChanged += OnPaintRequired;
            TimeSignatures.Added += OnPaintRequired;
            TimeSignatures.Removed += OnPaintRequired;
        }


        protected virtual void Init()
        {
        }



        private void OnPaintRequired(object sender, EventArgs e)
        {
            Painter?.Paint();
        }



        #region Public Methods
        public Rational Quantise(Rational value)
        {
            return ((Rational)(value * BeatDivisions).Round()) / BeatDivisions;
        }


        public Rational GetMouseBeat()
        {
            Point p = Mouse.GetPosition(ForegroundCanvas);
            return Rational.FromDecimal((decimal)(Math.Round(p.X) / BeatWidth)) + StartBeat;
        }


        public TimeSignature GetTimeSignatureAt(Rational beat)
        {
            TimeSignature timeSig = TimeSignatures.LastOrDefault(x => x.Beat < beat);
            if (timeSig != null)
                return timeSig;
            return (TimeSignatures.Count == 0) ? null : TimeSignatures[0];
        }


        public abstract List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewers();

        public abstract List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewersUnderMouse();

        public abstract List<FiniteTimeObjectViewer> GetSelectedFiniteTimeObjectViewers();


        public virtual MusicViewerAction NewCreateAction()
        {
            return null;
        }

        public virtual MusicViewerAction NewEditAction()
        {
            return null;
        }

        public virtual MusicViewerAction NewDeleteAction()
        {
            return null;
        }

        public virtual MusicViewerAction NewPasteAction()
        {
            return null;
        }
        #endregion Public Methods



        #region User Interaction Event Handlers
        protected virtual void DefaultMouseDownBehaviour(object sender, MouseButtonEventArgs e)
        {
            Focus();

            Rational mouseBeat = GetMouseBeat();
            if (mouseBeat < StartBeat || mouseBeat > EndBeat)
                return;

            if (CurrentAction == null)
            {
                if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && e.LeftButton == MouseButtonState.Pressed)
                    CurrentAction = new DragSelectAction(this);
                else
                {
                    List<FiniteTimeObjectViewer> objectsUnderMouse = GetFiniteTimeObjectViewersUnderMouse();
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        if (objectsUnderMouse.Count == 0)
                        {
                            if (!ReadOnly)
                            {
                                CurrentAction = NewCreateAction();
                                if (CurrentAction == null)
                                    GetSelectedFiniteTimeObjectViewers().ForEach(x => x.Selected = false);
                            }
                            else
                                GetSelectedFiniteTimeObjectViewers().ForEach(x => x.Selected = false);
                        }
                        else if (!ReadOnly)
                        {
                            FiniteTimeObjectViewer objectUnderMouse = objectsUnderMouse.Last();

                            if ((mouseBeat - objectUnderMouse.EndBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new RightResizeAction(this);

                            else if ((mouseBeat - objectUnderMouse.StartBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new LeftResizeAction(this);

                            else
                                CurrentAction = new MoveAction(this);
                        }
                    }
                    else if (e.RightButton == MouseButtonState.Pressed && !ReadOnly)
                    {
                        if (objectsUnderMouse.Count > 0)
                            CurrentAction = NewEditAction();
                    }
                }
            }

            CurrentAction_MouseDown(e);
        }


        protected void CurrentAction_MouseDown(MouseButtonEventArgs e)
        {
            if (CurrentAction != null)
            {
                CurrentAction.OnMouseDown(e);
                if (CurrentAction.Finished)
                {
                    if (CurrentAction is IUndoable)
                        PreviousActions.Push((IUndoable)CurrentAction);
                    CurrentAction = null;
                }
            }
        }


        protected virtual void DefaultMouseMoveBehaviour(object sender, MouseEventArgs e)
        {
            CurrentAction_MouseMove(e);
        }


        protected void CurrentAction_MouseMove(MouseEventArgs e)
        {
            if (CurrentAction != null)
            {
                CurrentAction.OnMouseMove(e);
                if (CurrentAction.Finished)
                {
                    if (CurrentAction is IUndoable)
                        PreviousActions.Push((IUndoable)CurrentAction);
                    CurrentAction = null;
                }
            }
        }


        protected virtual void DefaultMouseUpBehaviour(object sender, MouseButtonEventArgs e)
        {
            Focus();
            CurrentAction_MouseUp(e);
        }


        protected void CurrentAction_MouseUp(MouseButtonEventArgs e)
        {
            if (CurrentAction != null)
            {
                CurrentAction.OnMouseUp(e);
                if (CurrentAction.Finished)
                {
                    if (CurrentAction is IUndoable)
                        PreviousActions.Push((IUndoable)CurrentAction);
                    CurrentAction = null;
                }
            }
        }


        protected virtual void DefaultKeyDownBehaviour(object sender, KeyEventArgs e)
        {
            if (ReadOnly)
                return;

            if (CurrentAction == null)
            {
                if (e.Key == Key.Delete)
                    CurrentAction = NewDeleteAction();

                else if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                {
                    if (e.Key == Key.C)
                        CurrentAction = new CopyAction(this);

                    else if (e.Key == Key.V)
                        CurrentAction = NewPasteAction();
                }
            }
            CurrentAction_CheckIfFinished();
        }


        protected void CurrentAction_CheckIfFinished()
        {
            if (CurrentAction != null && CurrentAction.Finished)
            {
                if (CurrentAction is IUndoable)
                    PreviousActions.Push((IUndoable)CurrentAction);
                CurrentAction = null;
            }
        }
        #endregion User Interaction Event Handlers



        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion INotifyPropertyChanged implementation

    }



    public abstract class MusicViewerAction
    {

        public MusicViewer MusicViewer { get; private set; }

        public bool Finished { get; protected set; }


        public MusicViewerAction(MusicViewer musicViewer)
        {
            MusicViewer = musicViewer;
        }


        public abstract void OnMouseDown(MouseButtonEventArgs e);

        public abstract void OnMouseMove(MouseEventArgs e);

        public abstract void OnMouseUp(MouseButtonEventArgs e);



        protected virtual List<FiniteTimeObject> DefaultInitialSelector()
        {
            //If there aren't any objects under the mouse, just deselect all objects and exit
            List<FiniteTimeObjectViewer> objectsUnderMouse = MusicViewer.GetFiniteTimeObjectViewersUnderMouse();
            if (objectsUnderMouse.Count == 0)
            {
                MusicViewer.GetSelectedFiniteTimeObjectViewers().ForEach(x => x.Selected = false);
                return new List<FiniteTimeObject>();
            }

            //Set all selected objects as the objects to act upon
            List<FiniteTimeObjectViewer> selectedObjects = MusicViewer.GetSelectedFiniteTimeObjectViewers();

            //If the object that's under the mouse is not in the selected objects, deselect all the selected objects, select the object under the mouse and make that the only object to act upon
            FiniteTimeObjectViewer objectUnderMouse = objectsUnderMouse.Last();
            if (selectedObjects.Count == 0 || !selectedObjects.Contains(objectUnderMouse))
            {
                selectedObjects.ForEach(x => x.Selected = false);
                selectedObjects = new List<FiniteTimeObjectViewer>() { objectUnderMouse };
                objectUnderMouse.Selected = true;
            }
            return selectedObjects.Select(x => x.FiniteTimeObject).ToList();
        }

    }
}
