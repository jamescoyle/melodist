﻿using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls
{
    /// <summary>
    /// Interaction logic for PhraseWarpActionFrame.xaml
    /// </summary>
    public partial class PhraseWarpActionFrame : UserControl
    {

        public bool Active
        {
            get { return ActiveSwitch.IsChecked; }
            set { ActiveSwitch.IsChecked = value; }
        }


        public bool IsMinimized { get; private set; }


        public UIElement WarpActionControl
        {
            get { return ContentParent.Child; }
            set { ContentParent.Child = value; }
        }


        public string WarpActionName
        {
            get { return NameText.Text; }
            set { NameText.Text = value; }
        }


        public IPhraseWarper WarpAction
        {
            get
            {
                if (WarpActionControl is IPhraseWarper)
                    return (IPhraseWarper)WarpActionControl;
                return null;
            }
        }


        public PhraseWarpActionFrame()
        {
            InitializeComponent();
            IsMinimized = false;
        }


        public event EventHandler UpArrowClick;
        private void UpArrowBtn_Click(object sender, EventArgs e)
        {
            if (UpArrowClick != null)
                UpArrowClick.Invoke(this, null);
        }


        public event EventHandler DownArrowClick;
        private void DownArrowBtn_Click(object sender, EventArgs e)
        {
            if (DownArrowClick != null)
                DownArrowClick.Invoke(this, null);
        }


        public event EventHandler Minimize;
        private void MinimizeBtn_Click(object sender, EventArgs e)
        {
            IsMinimized = !IsMinimized;
            if (IsMinimized)
                ContentRowDefinition.Height = new GridLength(0);
            else
                ContentRowDefinition.Height = GridLength.Auto;

            if (Minimize != null)
                Minimize.Invoke(this, null);
        }


        public event EventHandler Close;
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            if (Close != null)
                Close.Invoke(this, null);
        }


        private void ActiveSwitch_Checked(object sender, RoutedEventArgs e)
        {
        }


        private void NameText_SelectionChanged(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (NameText.SelectionLength != 0)
                NameText.SelectionLength = 0;
        }

    }
}
