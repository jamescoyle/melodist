﻿using HarmonyEngine;
using MathPlus;
using MelodistControls.MusicViewerActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MelodistControls
{
    public abstract class MultiTrackViewer : MusicViewer
    {

        #region Public Properties
        public event EventHandler TrackHeightChanged;
        public static DependencyProperty TrackHeightProperty = DependencyProperty.Register("TrackHeight", typeof(int), typeof(MultiTrackViewer), new PropertyMetadata(50, OnTrackHeightDPChanged));
        private static void OnTrackHeightDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiTrackViewer instance = (MultiTrackViewer)d;
            instance.SetTrackHeightSideEffects();
        }
        public int TrackHeight
        {
            get
            {
                return (int)GetValue(TrackHeightProperty);
            }
            set
            {
                if (value != TrackHeight)
                {
                    SetValue(TrackHeightProperty, value);
                    SetTrackHeightSideEffects();
                }
            }
        }
        private void SetTrackHeightSideEffects()
        {
            BackgroundCanvas.Height = TrackHeight * TrackCount;
            OnPropertyChanged("TrackHeight");
            TrackHeightChanged?.Invoke(this, null);
        }



        public event EventHandler TrackCountChanged;
        public static DependencyProperty TrackCountProperty = DependencyProperty.Register("TrackCount", typeof(int), typeof(MultiTrackViewer), new PropertyMetadata(4, OnTrackCountDPChanged));
        private static void OnTrackCountDPChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiTrackViewer instance = (MultiTrackViewer)d;
            instance.SetTrackCountSideEffects();
        }
        public int TrackCount
        {
            get
            {
                return (int)GetValue(TrackCountProperty);
            }
            set
            {
                if (value != TrackCount)
                {
                    SetValue(TrackCountProperty, value);
                    SetTrackCountSideEffects();
                }
            }
        }
        private void SetTrackCountSideEffects()
        {
            BackgroundCanvas.Height = TrackHeight * TrackCount;
            OnPropertyChanged("TrackCount");
            TrackCountChanged?.Invoke(this, null);
        }
        #endregion Public Properties



        public MultiTrackViewer()
            : base()
        {
        }


        protected override void Init()
        {
            BackgroundCanvas.VerticalAlignment = VerticalAlignment.Top;
            BackgroundCanvas.Height = TrackHeight * TrackCount;
            TrackHeightChanged += (sender, e) => Painter.Paint();
            TrackCountChanged += (sender, e) => Painter.Paint();

            base.Init();
        }



        #region Public Methods
        public int GetMouseTrack()
        {
            Point p = Mouse.GetPosition(ForegroundCanvas);
            return (int)(p.Y / TrackHeight);
        }


        public abstract List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewers();

        public abstract List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewersUnderMouse();

        public abstract List<FiniteTimeTrackObjectViewer> GetSelectedFiniteTimeTrackObjectViewers();


        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewers()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            GetFiniteTimeTrackObjectViewers().ForEach(x => output.Add(x));
            return output;
        }

        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewersUnderMouse()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            GetFiniteTimeTrackObjectViewersUnderMouse().ForEach(x => output.Add(x));
            return output;
        }

        public override List<FiniteTimeObjectViewer> GetSelectedFiniteTimeObjectViewers()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            GetSelectedFiniteTimeTrackObjectViewers().ForEach(x => output.Add(x));
            return output;
        }


        public virtual MultiTrackViewerAction NewMoveAction()
        {
            return null;
        }
        #endregion Public Methods


        protected override void DefaultMouseDownBehaviour(object sender, MouseButtonEventArgs e)
        {
            Focus();

            Rational mouseBeat = GetMouseBeat();
            if (mouseBeat < StartBeat || mouseBeat > EndBeat)
                return;

            if (CurrentAction == null)
            {
                if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && e.LeftButton == MouseButtonState.Pressed)
                    CurrentAction = new MultiTrackViewerActions.DragSelectAction(this);
                else
                {
                    List<FiniteTimeTrackObjectViewer> objectsUnderMouse = GetFiniteTimeTrackObjectViewersUnderMouse();
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        if (objectsUnderMouse.Count == 0)
                        {
                            if (!ReadOnly)
                            {
                                CurrentAction = NewCreateAction();
                                if (CurrentAction == null)
                                    GetSelectedFiniteTimeTrackObjectViewers().ForEach(x => x.Selected = false);
                            }
                            else
                                GetSelectedFiniteTimeTrackObjectViewers().ForEach(x => x.Selected = false);
                        }
                        else if (!ReadOnly)
                        {
                            FiniteTimeObjectViewer objectUnderMouse = objectsUnderMouse.Last();

                            if ((mouseBeat - objectUnderMouse.EndBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new RightResizeAction(this);

                            else if ((mouseBeat - objectUnderMouse.StartBeat).Abs() <= Rational.Min(objectUnderMouse.Duration / 4, new Rational(3, 20)))
                                CurrentAction = new LeftResizeAction(this);

                            else
                                CurrentAction = NewMoveAction();
                        }
                    }
                    else if (e.RightButton == MouseButtonState.Pressed && !ReadOnly)
                    {
                        if (objectsUnderMouse.Count > 0)
                            CurrentAction = NewEditAction();
                    }
                }
            }

            CurrentAction_MouseDown(e);
        }

    }



    public abstract class MultiTrackViewerAction : MusicViewerAction
    {

        public MultiTrackViewer MultiTrackViewer { get; private set; }


        public MultiTrackViewerAction(MultiTrackViewer multiTrackViewer)
            : base(multiTrackViewer)
        {
            MultiTrackViewer = multiTrackViewer;
        }


        protected override List<FiniteTimeObject> DefaultInitialSelector()
        {
            //If there aren't any objects under the mouse, just deselect all objects and exit
            List<FiniteTimeTrackObjectViewer> objectsUnderMouse = MultiTrackViewer.GetFiniteTimeTrackObjectViewersUnderMouse();
            if (objectsUnderMouse.Count == 0)
            {
                MultiTrackViewer.GetSelectedFiniteTimeTrackObjectViewers().ForEach(x => x.Selected = false);
                return new List<FiniteTimeObject>();
            }

            //Set all selected objects as the objects to act upon
            List<FiniteTimeTrackObjectViewer> selectedObjects = MultiTrackViewer.GetSelectedFiniteTimeTrackObjectViewers();

            //If the object that's under the mouse is not in the selected objects, deselect all the selected objects, select the object under the mouse and make that the only object to act upon
            FiniteTimeTrackObjectViewer objectUnderMouse = objectsUnderMouse.Last();
            if (selectedObjects.Count == 0 || !selectedObjects.Contains(objectUnderMouse))
            {
                selectedObjects.ForEach(x => x.Selected = false);
                selectedObjects = new List<FiniteTimeTrackObjectViewer>() { objectUnderMouse };
                objectUnderMouse.Selected = true;
            }
            return selectedObjects.Select(x => x.FiniteTimeObject).ToList();
        }

    }



    public abstract class MultiTrackViewerPainter : MusicViewerPainter
    {

        public MultiTrackViewer MultiTrackViewer { get; private set; }


        public MultiTrackViewerPainter(MultiTrackViewer multiTrackViewer)
            : base(multiTrackViewer)
        {
            MultiTrackViewer = multiTrackViewer;
        }

    }
}
