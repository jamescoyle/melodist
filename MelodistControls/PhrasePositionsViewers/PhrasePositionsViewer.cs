﻿using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.PhrasePositionsViewers
{
    public class PhrasePositionsViewer : MultiTrackViewer
    {

        public List<PhrasePositionViewer> PhrasePositionViewers { get; private set; }



        public PhrasePositionsViewer()
        {
        }


        protected override void Init()
        {
            BackgroundCanvas.Background = new SolidColorBrush(Colors.Gray);

            PhrasePositionViewers = new List<PhrasePositionViewer>();

            Painter = new PhrasePositionsViewerPainter(this);

            TrackHeight = 60;
            TrackCount = 4;

            BeatWidth = 30;
            BeatDivisions = 6;

            StartBeatChanged += OnPhrasePositionsUpdateRequired;
            BeatWidthChanged += OnPhrasePositionsUpdateRequired;
            TrackHeightChanged += OnPhrasePositionsUpdateRequired;

            KeyDown += OnKeyDown;
            ForegroundCanvas.MouseDown += OnMouseDown;
            ForegroundCanvas.MouseMove += OnMouseMove;
            ForegroundCanvas.MouseUp += OnMouseUp;

            base.Init();
        }



        #region PhrasePosition Management Logic
        private void OnPhrasePositionsUpdateRequired(object sender, EventArgs e)
        {
            foreach (PhrasePositionViewer phrasePositionViewer in PhrasePositionViewers)
                UpdatePhrasePositionViewer(phrasePositionViewer);
        }


        protected void OnPhrasePositionPropertyChanged(object sender, EventArgs e)
        {
            PhrasePosition phrasePosition = (PhrasePosition)sender;
            phrasePosition.StartBeat = Quantise(phrasePosition.StartBeat);
            phrasePosition.Duration = Quantise(phrasePosition.Duration);

            PhrasePositionViewer phrasePositionViewer = PhrasePositionViewers.FirstOrDefault(x => x.PhrasePosition == phrasePosition);
            if (phrasePositionViewer != null)
                UpdatePhrasePositionViewer(phrasePositionViewer);
        }


        protected void AddPhrasePositionViewer(PhrasePosition phrasePosition, Func<PhrasePosition, int> trackGetter)
        {
            PhrasePositionViewer phrasePositionViewer = new PhrasePositionViewer(phrasePosition, this, trackGetter);
            ForegroundCanvas.Children.Add(phrasePositionViewer);
            PhrasePositionViewers.Add(phrasePositionViewer);

            phrasePosition.StartBeatChanged += OnPhrasePositionPropertyChanged;
            phrasePosition.DurationChanged += OnPhrasePositionPropertyChanged;
            OnPhrasePositionPropertyChanged(phrasePosition, null);
        }


        protected void UpdatePhrasePositionViewer(PhrasePositionViewer phrasePositionViewer)
        {
            Canvas.SetLeft(phrasePositionViewer, (phrasePositionViewer.PhrasePosition.StartBeat - StartBeat) * BeatWidth);
            Canvas.SetTop(phrasePositionViewer, phrasePositionViewer.Track * TrackHeight);
            phrasePositionViewer.Height = TrackHeight;
        }


        protected void RemovePhrasePositionViewer(PhrasePosition phrasePosition)
        {
            PhrasePositionViewer phrasePositionViewer = PhrasePositionViewers.FirstOrDefault(x => x.PhrasePosition == phrasePosition);
            if(phrasePositionViewer != null)
            {
                PhrasePositionViewers.Remove(phrasePositionViewer);
                ForegroundCanvas.Children.Remove(phrasePositionViewer);
                phrasePositionViewer.Destroy();

                phrasePosition.StartBeatChanged -= OnPhrasePositionPropertyChanged;
                phrasePosition.DurationChanged -= OnPhrasePositionPropertyChanged;
            }
        }
        #endregion PhrasePosition Management Logic



        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewers()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            PhrasePositionViewers.ForEach(x => output.Add(x));
            return output;
        }


        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewersUnderMouse()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            GetPhrasePositionViewersUnderMouse().ForEach(x => output.Add(x));
            return output;
        }

        public List<PhrasePositionViewer> GetPhrasePositionViewersUnderMouse()
        {
            Rational mouseBeat = GetMouseBeat();
            int mouseTrack = GetMouseTrack();
            return PhrasePositionViewers.Where(x => x.StartBeat <= mouseBeat && x.EndBeat >= mouseBeat && x.Track == mouseTrack).ToList();
        }


        public override List<FiniteTimeTrackObjectViewer> GetSelectedFiniteTimeTrackObjectViewers()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            GetSelectedPhrasePositionViewers().ForEach(x => output.Add(x));
            return output;
        }

        public List<PhrasePositionViewer> GetSelectedPhrasePositionViewers()
        {
            return PhrasePositionViewers.Where(x => x.Selected).ToList();
        }



        #region Action Handlers
        protected virtual void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseDownBehaviour(sender, e);
        }


        protected virtual void OnMouseMove(object sender, MouseEventArgs e)
        {
            DefaultMouseMoveBehaviour(sender, e);
        }


        protected virtual void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseUpBehaviour(sender, e);
        }


        protected virtual void OnKeyDown(object sender, KeyEventArgs e)
        {
            DefaultKeyDownBehaviour(sender, e);
        }
        #endregion Action Handlers

    }



    public class PhrasePositionsViewerPainter : MultiTrackViewerPainter
    {

        private List<Rectangle> _backgroundRects;

        public PhrasePositionsViewer PhrasePositionsViewer { get; private set; }


        public PhrasePositionsViewerPainter(PhrasePositionsViewer phrasePositionsViewer)
            : base(phrasePositionsViewer)
        {
            PhrasePositionsViewer = phrasePositionsViewer;

            _backgroundRects = new List<Rectangle>();

            Style beatLineStyle = new Style(typeof(Line));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.LightSteelBlue)));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(1, beatLineStyle);

            BarLineStyle = new Style(typeof(Line));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.Black)));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
        }


        public override void Paint()
        {
            base.Paint();
        }


        public override void PaintBack()
        {
            foreach (Rectangle rect in _backgroundRects)
                MultiTrackViewer.BackgroundCanvas.Children.Remove(rect);

            for (int i = 1; i < PhrasePositionsViewer.TrackCount; i += 2)
            {
                Rectangle rect = new Rectangle()
                {
                    Fill = new SolidColorBrush(Color.FromRgb(115, 115, 115)),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top
                };
                _backgroundRects.Add(rect);
                MultiTrackViewer.BackgroundCanvas.Children.Add(rect);

                Binding binding = new Binding("ActualWidth") { Source = MultiTrackViewer.BackgroundCanvas };
                BindingOperations.SetBinding(rect, Rectangle.WidthProperty, binding);

                Canvas.SetTop(rect, i * MultiTrackViewer.TrackHeight);
                rect.Height = MultiTrackViewer.TrackHeight;
            }
        }

    }
}
