﻿using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls.PhrasePositionsViewers
{
    /// <summary>
    /// Interaction logic for PhrasePositionViewer.xaml
    /// </summary>
    public partial class PhrasePositionViewer : FiniteTimeTrackObjectViewer
    {

        public PhrasePosition PhrasePosition { get; private set; }

        public PhrasePositionsViewer PhrasePositionsViewer { get; private set; }


        public Func<PhrasePosition, int> TrackGetter { get; private set; }

        public override int Track { get { return TrackGetter(PhrasePosition); } }

        private List<Line> _loopLines = new List<Line>();


        public PhrasePositionViewer(PhrasePosition phrasePosition, PhrasePositionsViewer phrasePositionsViewer, Func<PhrasePosition, int> trackGetter, Color? colour = null)
            : base(phrasePosition, (colour == null) ? Color.FromRgb(Colors.SeaGreen.R, Colors.SeaGreen.G, Colors.SeaGreen.B) : colour.Value)
        {
            InitializeComponent();
            PhrasePosition = phrasePosition;
            PhrasePositionsViewer = phrasePositionsViewer;
            TrackGetter = trackGetter;
            DataContext = PhrasePosition;
            PhraseNameText.Text = phrasePosition.Phrase.Name;

            PhrasePosition.Phrase.StartBeatChanged += UpdateDrawing;
            PhrasePosition.DurationChanged += UpdateDrawing;
            PhrasePositionsViewer.BeatWidthChanged += UpdateDrawing;
            UpdateDrawing();
        }


        private void UpdateDrawing(object sender = null, EventArgs e = null)
        {
            Rational zeroLine = PhrasePositionsViewer.BeatWidth * PhrasePosition.Phrase.StartBeat;
            if (zeroLine != 0)
                zeroLine += 1;
            Canvas.SetLeft(ZeroLine, 0 - zeroLine);
            Width = PhrasePosition.Duration * PhrasePositionsViewer.BeatWidth;
            Rational loopCount = PhrasePosition.LoopCount;

            _loopLines.ForEach(x => Canvas.Children.Remove(x));
            _loopLines.Clear();
            for(int i = 1; i < loopCount; i++)
            {
                Line line = new Line()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    StrokeThickness = 1,
                    Stroke = new SolidColorBrush(Colors.Black)
                };
                line.X1 = line.X2 = (i * PhrasePosition.Phrase.Duration * PhrasePositionsViewer.BeatWidth) - 1;
                Binding binding = new Binding("ActualHeight") { Source = Canvas };
                BindingOperations.SetBinding(line, Line.Y2Property, binding);
                Canvas.Children.Add(line);
                _loopLines.Add(line);
            }
        }


        public void Destroy()
        {
            PhrasePosition.Phrase.StartBeatChanged -= UpdateDrawing;
            PhrasePosition.DurationChanged -= UpdateDrawing;
            PhrasePositionsViewer.BeatWidthChanged -= UpdateDrawing;
            PhrasePosition = null;
            PhrasePositionsViewer = null;
            TrackGetter = null;
        }

    }
}
