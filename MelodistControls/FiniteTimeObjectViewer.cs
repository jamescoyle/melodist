﻿using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace MelodistControls
{
    public abstract class FiniteTimeObjectViewer : UserControl, INotifyPropertyChanged
    {

        public FiniteTimeObject FiniteTimeObject { get; private set; }


        public Rational StartBeat { get { return FiniteTimeObject.StartBeat; } }

        public Rational Duration { get { return FiniteTimeObject.Duration; } }

        public Rational EndBeat { get { return FiniteTimeObject.EndBeat; } }


        public event EventHandler SelectedChanged;
        private bool _selected;
        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    OnPropertyChanged("Selected");
                    OnPropertyChanged("BackgroundBrush");
                    SelectedChanged?.Invoke(this, null);
                }
            }
        }


        private Color _colour;
        public Color Colour
        {
            get
            {
                return _colour;
            }
            set
            {
                if (_colour != value)
                {
                    _colour = value;
                    _selectedColour = new Color()
                    {
                        A = _colour.A,
                        R = (byte)(_colour.R + ((256 - _colour.R) / 2)),
                        G = (byte)(_colour.G + ((256 - _colour.G) / 2)),
                        B = (byte)(_colour.B + ((256 - _colour.B) / 2))
                    };
                    OnPropertyChanged("Colour");
                    OnPropertyChanged("BackgroundBrush");
                }
            }
        }


        private Color _selectedColour;
        public Color SelectedColour
        {
            get
            {
                return _selectedColour;
            }
            set
            {
                if (_selectedColour != value)
                {
                    _selectedColour = value;
                    OnPropertyChanged("SelectedColour");
                    if (Selected)
                        OnPropertyChanged("BackgroundBrush");
                }
            }
        }


        public Brush BackgroundBrush
        {
            get
            {
                return new SolidColorBrush(Selected ? SelectedColour : Colour);
            }
        }



        public FiniteTimeObjectViewer(FiniteTimeObject finiteTimeObject, Color colour)
        {
            FiniteTimeObject = finiteTimeObject;
            Colour = colour;
        }

        public FiniteTimeObjectViewer()
        {
            FiniteTimeObject = new FiniteTimeObject(0, 1);
            Colour = Color.FromArgb(255, 255, 255, 255);
        }



        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion INotifyPropertyChanged implementation

    }
}
