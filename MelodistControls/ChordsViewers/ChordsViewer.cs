﻿using HarmonyEngine;
using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.ChordsViewers
{
    public class ChordsViewer : MusicViewer
    {

        public List<ChordViewer> ChordViewers { get; private set; }



        public ChordsViewer()
        {
        }


        protected override void Init()
        {
            ChordViewers = new List<ChordViewer>();

            if(Painter == null)
                Painter = new ChordsViewerPainter(this);

            StartBeatChanged += OnChordsUpdateRequired;
            BeatWidthChanged += OnChordsUpdateRequired;

            KeyDown += OnKeyDown;
            ForegroundCanvas.MouseDown += OnMouseDown;
            ForegroundCanvas.MouseMove += OnMouseMove;
            ForegroundCanvas.MouseUp += OnMouseUp;

            base.Init();
        }



        #region Chord Management Logic
        private void OnChordsUpdateRequired(object sender, EventArgs e)
        {
            foreach (ChordViewer chordViewer in ChordViewers)
                UpdateChordViewer(chordViewer);
        }


        protected void OnChordPropertyChanged(object sender, EventArgs e)
        {
            Chord chord = (Chord)sender;
            chord.StartBeat = Quantise(chord.StartBeat);
            chord.Duration = Quantise(chord.Duration);

            ChordViewer chordViewer = ChordViewers.FirstOrDefault(x => x.Chord == chord);
            if (chordViewer != null)
                UpdateChordViewer(chordViewer);
        }


        protected void AddChordViewer(Chord chord)
        {
            ChordViewer chordViewer = new ChordViewer(chord);
            ForegroundCanvas.Children.Add(chordViewer);
            ChordViewers.Add(chordViewer);

            chord.StartBeatChanged += OnChordPropertyChanged;
            chord.DurationChanged += OnChordPropertyChanged;
            OnChordPropertyChanged(chord, null);

            Binding binding = new Binding("ActualHeight") { Source = this, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
            BindingOperations.SetBinding(chordViewer, HeightProperty, binding);
        }


        protected void UpdateChordViewer(ChordViewer chordViewer)
        {
            Canvas.SetLeft(chordViewer, (chordViewer.Chord.StartBeat - StartBeat) * BeatWidth);
            chordViewer.Width = chordViewer.Chord.Duration * BeatWidth;
        }


        protected void RemoveChordViewer(Chord chord)
        {
            ChordViewer chordViewer = ChordViewers.FirstOrDefault(x => x.Chord == chord);
            if (chordViewer != null)
            {
                ChordViewers.Remove(chordViewer);
                ForegroundCanvas.Children.Remove(chordViewer);

                chordViewer.Chord.StartBeatChanged -= OnChordPropertyChanged;
                chordViewer.Chord.DurationChanged -= OnChordPropertyChanged;

                BindingOperations.ClearAllBindings(chordViewer);
            }
        }
        #endregion Chord Management Logic



        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewers()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            ChordViewers.ForEach(x => output.Add(x));
            return output;
        }


        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewersUnderMouse()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            Rational mouseBeat = GetMouseBeat();
            ChordViewers.Where(x => x.StartBeat <= mouseBeat && x.EndBeat >= mouseBeat).ToList().ForEach(x => output.Add(x));
            return output;
        }

        public List<ChordViewer> GetChordViewersUnderMouse()
        {
            Rational mouseBeat = GetMouseBeat();
            return ChordViewers.Where(x => x.StartBeat <= mouseBeat && x.EndBeat >= mouseBeat).ToList();
        }


        public override List<FiniteTimeObjectViewer> GetSelectedFiniteTimeObjectViewers()
        {
            List<FiniteTimeObjectViewer> output = new List<FiniteTimeObjectViewer>();
            ChordViewers.Where(x => x.Selected).ToList().ForEach(x => output.Add(x));
            return output;
        }

        public List<ChordViewer> GetSelectedChordViewers()
        {
            return ChordViewers.Where(x => x.Selected).ToList();
        }



        #region Action Handlers
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseDownBehaviour(sender, e);
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            DefaultMouseMoveBehaviour(sender, e);
        }


        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseUpBehaviour(sender, e);
        }


        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            DefaultKeyDownBehaviour(sender, e);
        }
        #endregion Action Handlers

    }



    public class ChordsViewerPainter : MusicViewerPainter
    {

        public ChordsViewerPainter(ChordsViewer chordsViewer)
            : base(chordsViewer)
        {
            Style beatLineStyle = new Style(typeof(Line));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.LightGray)));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(1, beatLineStyle);

            Style semiQuaverLineStyle = new Style(typeof(Line));
            semiQuaverLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.SlateGray)));
            semiQuaverLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(new Rational(1, 4), semiQuaverLineStyle);

            BarLineStyle = new Style(typeof(Line));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.Black)));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
        }


        public override void PaintBack()
        {
        }

    }



    public abstract class ChordsViewerAction : MusicViewerAction
    {

        public ChordsViewer ChordsViewer { get; private set; }


        public ChordsViewerAction(ChordsViewer chordsViewer)
            : base(chordsViewer)
        {
            ChordsViewer = chordsViewer;
        }

    }
}

