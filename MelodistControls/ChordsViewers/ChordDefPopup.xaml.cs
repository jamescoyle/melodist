﻿using HarmonyEngine;
using PhraseManipulation;
using PhraseManipulation.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls.ChordsViewers
{
    /// <summary>
    /// Interaction logic for ChordDefPopup.xaml
    /// </summary>
    public partial class ChordDefPopup : Popup
    {

        public Chord Chord { get; private set; }

        public Phrase Phrase { get; private set; }

        public Pitch? SelectedRoot { get; private set; }

        public ChordDef SelectedChordDef { get; private set; }


        private bool _rootSelectedIndexChanged = false;
        private bool _chordDefSelectedIndexChanged = false;



        public ChordDefPopup(Chord chord, Phrase phrase)
        {
            Chord = chord;
            Phrase = phrase;
            InitializeComponent();

            RootList.Items.Add(new Pitch(0));
            RootList.Items.Add(new Pitch(1));
            RootList.Items.Add(new Pitch(2));
            RootList.Items.Add(new Pitch(3));
            RootList.Items.Add(new Pitch(4));
            RootList.Items.Add(new Pitch(5));
            RootList.Items.Add(new Pitch(6));
            RootList.Items.Add(new Pitch(7));
            RootList.Items.Add(new Pitch(8));
            RootList.Items.Add(new Pitch(9));
            RootList.Items.Add(new Pitch(10));
            RootList.Items.Add(new Pitch(11));

            List<ChordDef> chordDefs = ChordDef.ChordDefs;
            foreach (ChordDef chordDef in chordDefs)
                ChordTypeList.Items.Add(chordDef);
        }


        private void RootList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedRoot = (Pitch?)RootList.SelectedItem;
            _rootSelectedIndexChanged = true;
            if (SelectedChordDef != null && _rootSelectedIndexChanged && _chordDefSelectedIndexChanged)
                IsOpen = false;
        }


        private void ChordTypeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedChordDef = (ChordDef)ChordTypeList.SelectedItem;
            _chordDefSelectedIndexChanged = true;
            if (SelectedRoot != null && _rootSelectedIndexChanged && _chordDefSelectedIndexChanged)
                IsOpen = false;
        }


        private void ChromaticismSlider_RangeSelectionChanged(object sender, MahApps.Metro.Controls.RangeSelectionChangedEventArgs e)
        {
            RecalculateSuggestions();
        }

        private void DissonanceSlider_RangeSelectionChanged(object sender, MahApps.Metro.Controls.RangeSelectionChangedEventArgs e)
        {
            RecalculateSuggestions();
        }


        private void RecalculateSuggestions()
        {
            if (DissonanceSlider == null || ChromaticismSlider == null)
                return;

            List<ChordSuggestion2> suggestions = new ChordSuggester2(DissonanceSlider.LowerValue, DissonanceSlider.UpperValue, ChromaticismSlider.LowerValue, ChromaticismSlider.UpperValue).Run(Phrase, Chord);
            SuggestionList.Items.Clear();
            foreach (ChordSuggestion2 suggestion in suggestions)
                SuggestionList.Items.Add(suggestion.Chord);
        }


        private void SuggestionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Chord chord = (Chord)e.AddedItems[0];
            SelectedRoot = chord.Root;
            SelectedChordDef = chord.ChordDef;
            IsOpen = false;
        }
    }
}
