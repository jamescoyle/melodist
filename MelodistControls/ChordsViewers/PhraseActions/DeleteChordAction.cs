﻿using CSharpExtras;
using HarmonyEngine;
using MelodistControls.ChordsViewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.PhraseActions
{
    public class DeleteChordAction : PhraseChordsViewerAction, IUndoable
    {

        public List<Chord> _chords = new List<Chord>();

        public bool IsUndone { get; private set; }



        public DeleteChordAction(PhraseChordsViewer phraseChordViewer)
            : base(phraseChordViewer)
        {
            _chords = PhraseChordsViewer.GetSelectedChordViewers().Select(x => x.Chord).ToList();
            Redo();
            Finished = true;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }


        public void Undo()
        {
            _chords.ForEach(x => PhraseChordsViewer.Phrase.AddChord(x));
            IsUndone = true;
        }

        public void Redo()
        {
            _chords.ForEach(x => PhraseChordsViewer.Phrase.RemoveChord(x));
            IsUndone = false;
        }

    }
}
