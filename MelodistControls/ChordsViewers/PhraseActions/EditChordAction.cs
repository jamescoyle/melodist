﻿using CSharpExtras;
using HarmonyEngine;
using MelodistControls.ChordsViewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MelodistControls.PhraseActions
{
    public class EditChordAction : PhraseChordsViewerAction, IUndoable
    {

        private Chord _oldChord;

        private Chord _newChord;

        public bool IsUndone { get; private set; }



        public EditChordAction(PhraseChordsViewer phraseChordViewer)
            : base(phraseChordViewer)
        {
            IsUndone = false;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_oldChord != null || Finished)
            {
                Finished = true;
                return;
            }

            PhraseChordsViewer.GetSelectedFiniteTimeObjectViewers().ForEach(x => x.Selected = false);

            List<ChordViewer> chordViewers = PhraseChordsViewer.GetChordViewersUnderMouse();
            if (chordViewers.Count != 1)
            {
                MessageBox.Show("Please select one chord to edit", "Edit chord");
                Finished = true;
                return;
            }

            _oldChord = chordViewers[0].Chord;
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (Finished)
                return;

            //Create a new chord that's a duplicate of the old one, pass it in to the popup window
            _newChord = new Chord(_oldChord.ChordDef, _oldChord.Root, _oldChord.StartBeat, _oldChord.Duration);
            ChordDefPopup popup = new ChordDefPopup(_newChord, PhraseChordsViewer.Phrase);
            popup.Closed += Popup_Closed;
            popup.IsOpen = true;
        }


        private void Popup_Closed(object sender, EventArgs e)
        {
            ChordDefPopup popup = (ChordDefPopup)sender;
            popup.Closed -= Popup_Closed;

            if (popup.SelectedRoot == null || popup.SelectedChordDef == null)
            {
                _newChord = null;
                Finished = true;
                return;
            }

            _newChord.Root = (Pitch)popup.SelectedRoot;
            _newChord.ChordDef = popup.SelectedChordDef;
            
            PhraseChordsViewer.Phrase.RemoveChord(_oldChord);
            PhraseChordsViewer.Phrase.AddChord(_newChord);

            Finished = true;
        }



        public void Undo()
        {
            if (_newChord == null)
                return;
            
            PhraseChordsViewer.Phrase.RemoveChord(_newChord);
            PhraseChordsViewer.Phrase.AddChord(_oldChord);
            IsUndone = true;
        }

        public void Redo()
        {
            if (_newChord == null)
                return;
            
            PhraseChordsViewer.Phrase.RemoveChord(_oldChord);
            PhraseChordsViewer.Phrase.AddChord(_newChord);
            IsUndone = false;
        }

    }
}
