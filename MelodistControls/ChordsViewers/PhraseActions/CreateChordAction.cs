﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using MelodistControls.ChordsViewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.PhraseActions
{
    public class CreateChordAction : PhraseChordsViewerAction, IUndoable
    {

        private Chord _chord;

        private Rational _dragStartBeat;

        public bool IsUndone { get; private set; }



        public CreateChordAction(PhraseChordsViewer phraseChordViewer)
            : base(phraseChordViewer)
        {
            IsUndone = false;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_chord != null || Finished)
            {
                Finished = true;
                return;
            }

            PhraseChordsViewer.GetSelectedChordViewers().ForEach(x => x.Selected = false);

            _chord = new Chord(ChordDef.Retrieve("M"), new Pitch(0), PhraseChordsViewer.Quantise(PhraseChordsViewer.GetMouseBeat()), 0);
            _dragStartBeat = _chord.StartBeat;
            PhraseChordsViewer.Phrase.AddChord(_chord);
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            if (Finished)
                return;

            Rational mouseBeat = PhraseChordsViewer.GetMouseBeat();
            _chord.StartBeat = Rational.Max(Rational.Min(mouseBeat, _dragStartBeat), PhraseChordsViewer.StartBeat);
            _chord.EndBeat = Rational.Min(Rational.Max(mouseBeat, _dragStartBeat), PhraseChordsViewer.EndBeat);
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (Finished)
                return;

            if (_chord.Duration == 0)
            {
                PhraseChordsViewer.Phrase.RemoveChord(_chord);
                _chord = null;
                Finished = true;
                return;
            }

            ChordDefPopup popup = new ChordDefPopup(_chord, PhraseChordsViewer.Phrase);
            popup.Closed += Popup_Closed;
            popup.IsOpen = true;
        }


        private void Popup_Closed(object sender, EventArgs e)
        {
            ChordDefPopup popup = (ChordDefPopup)sender;
            popup.Closed -= Popup_Closed;

            if (popup.SelectedRoot == null || popup.SelectedChordDef == null)
            {
                PhraseChordsViewer.Phrase.RemoveChord(_chord);
                _chord = null;
                Finished = true;
                return;
            }

            _chord.Root = (Pitch)popup.SelectedRoot;
            _chord.ChordDef = popup.SelectedChordDef;

            Finished = true;
        }



        public void Undo()
        {
            if (_chord == null)
                return;

            PhraseChordsViewer.Phrase.RemoveChord(_chord);
            IsUndone = true;
        }


        public void Redo()
        {
            PhraseChordsViewer.Phrase.AddChord(_chord);
            IsUndone = false;
        }

    }
}
