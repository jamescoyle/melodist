﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls.ChordsViewers
{
    /// <summary>
    /// Interaction logic for ChordViewer.xaml
    /// </summary>
    public partial class ChordViewer : FiniteTimeObjectViewer
    {

        public Chord Chord { get; private set; }



        public ChordViewer(Chord chord, Color? colour = null)
            : base(chord, (colour == null) ? Color.FromArgb(180, Colors.SeaGreen.R, Colors.SeaGreen.G, Colors.SeaGreen.B) : colour.Value)
        {
            Chord = chord;

            InitializeComponent();

            Chord.RootChanged += OnChordStructureChanged;
            Chord.ChordDefChanged += OnChordStructureChanged;
            OnChordStructureChanged(null, null);
        }


        private void OnChordStructureChanged(object sender, EventArgs e)
        {
            ChordText.Text = Chord.ToString();
        }

    }
}
