﻿using CSharpExtras;
using HarmonyEngine;
using MelodistControls.PhraseActions;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MelodistControls.ChordsViewers
{
    public partial class PhraseChordsViewer : ChordsViewer
    {

        private Phrase _phrase;
        public Phrase Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                if (_phrase != value)
                {
                    if (_phrase != null)
                    {
                        _phrase.Chords.Added -= PhraseChords_Added;
                        _phrase.Chords.Removed -= PhraseChords_Removed;
                        _phrase.Chords.ForEach(x => RemoveChordViewer(x));
                        BindingOperations.ClearAllBindings(BackgroundCanvas);
                        _phrase.BeatsPerBarChanged -= OnBeatsPerBarChanged;
                    }

                    _phrase = value;

                    StartBeat = _phrase.StartBeat.Floor();
                    EndBeat = _phrase.EndBeat.Ceiling();

                    //Set up listeners on the phrase for new chords being added, add chords already in the phrase
                    _phrase.Chords.Added += PhraseChords_Added;
                    _phrase.Chords.Removed += PhraseChords_Removed;
                    _phrase.Chords.ForEach(x => AddChordViewer(x));
                    _phrase.BeatsPerBarChanged += OnBeatsPerBarChanged;
                    OnBeatsPerBarChanged(null, null);
                }
            }
        }



        public PhraseChordsViewer() : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();
        }


        private void OnBeatsPerBarChanged(object sender, EventArgs e)
        {
            TimeSignaturesManager.Clear();
            TimeSignaturesManager.Add(new TimeSignature(0, _phrase.BeatsPerBar, 4));
        }


        private void PhraseChords_Added(object sender, MemberListEventArgs<Chord> args)
        {
            AddChordViewer(args.Item);
        }

        private void PhraseChords_Removed(object sender, MemberListEventArgs<Chord> args)
        {
            RemoveChordViewer(args.Item);
        }


        public override MusicViewerAction NewCreateAction()
        {
            return new CreateChordAction(this);
        }

        public override MusicViewerAction NewEditAction()
        {
            return new EditChordAction(this);
        }

        public override MusicViewerAction NewDeleteAction()
        {
            return new DeleteChordAction(this);
        }

    }



    public abstract class PhraseChordsViewerAction : MusicViewerAction
    {

        public PhraseChordsViewer PhraseChordsViewer { get; private set; }


        public PhraseChordsViewerAction(PhraseChordsViewer phraseChordsViewer)
            : base(phraseChordsViewer)
        {
            PhraseChordsViewer = phraseChordsViewer;
        }

    }
}
