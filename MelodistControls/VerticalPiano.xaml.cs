﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows;

namespace MelodistControls
{
    public partial class VerticalPiano : MultiTrackViewer
    {

        public VerticalPiano()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();

            ForegroundCanvas.Background = new SolidColorBrush(Colors.White);

            Painter = new VerticalPianoPainter(this);

            TrackHeight = 10;
            TrackCount = 128;

            ForegroundCanvas.Loaded += (sender, e) => Painter.Paint();
        }



        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewers()
        {
            return new List<FiniteTimeTrackObjectViewer>();
        }

        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewersUnderMouse()
        {
            return new List<FiniteTimeTrackObjectViewer>();
        }

        public override List<FiniteTimeTrackObjectViewer> GetSelectedFiniteTimeTrackObjectViewers()
        {
            return new List<FiniteTimeTrackObjectViewer>();
        }

    }



    public class VerticalPianoPainter : MultiTrackViewerPainter
    {

        private List<Rectangle> _blackRects;

        private List<Label> _labels;


        public VerticalPianoPainter(VerticalPiano verticalPiano)
            : base(verticalPiano)
        {
            _blackRects = new List<Rectangle>();
            _labels = new List<Label>();
        }


        public override void PaintBack()
        {
            foreach (Rectangle rect in _blackRects)
                MultiTrackViewer.ForegroundCanvas.Children.Remove(rect);
            _blackRects.Clear();

            foreach (Label label in _labels)
                MultiTrackViewer.ForegroundCanvas.Children.Remove(label);
            _labels.Clear();

            List<int> blackPitches = new List<int>() { 1, 3, 6, 8, 10 };
            for (int i = 0; blackPitches[i] + 12 < 128; i++)
                blackPitches.Add(blackPitches[i] + 12);

            foreach (int pitch in blackPitches)
            {
                Rectangle rect = new Rectangle()
                {
                    Fill = new SolidColorBrush(Colors.Black),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top
                };
                _blackRects.Add(rect);
                MultiTrackViewer.ForegroundCanvas.Children.Add(rect);

                Binding binding = new Binding("ActualWidth") { Source = MultiTrackViewer.ForegroundCanvas };
                BindingOperations.SetBinding(rect, Rectangle.WidthProperty, binding);

                Canvas.SetTop(rect, (127 - pitch) * MultiTrackViewer.TrackHeight);
                rect.Height = MultiTrackViewer.TrackHeight;
            }

            for (int i = 0; i < 128; i += 12)
            {
                Label label = new Label();
                label.Content = "C" + (i / 12).ToString();
                label.FontFamily = new FontFamily("Calibri");
                label.FontSize = 11f;
                label.FontStyle = FontStyles.Normal;
                label.FontWeight = FontWeights.Bold;
                label.Foreground = new SolidColorBrush(Color.FromRgb(50, 50, 50));
                _labels.Add(label);

                Canvas.SetTop(label, ((127 - i) * MultiTrackViewer.TrackHeight) - label.ActualHeight);
                MultiTrackViewer.ForegroundCanvas.Children.Add(label);

                label.SizeChanged += OnLabelSizeChanged;
            }
        }


        private void OnLabelSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.HeightChanged)
            {
                Label label = (Label)sender;
                Canvas.SetTop(label, Canvas.GetTop(label) - ((label.ActualHeight - MultiTrackViewer.TrackHeight) / 2));
                label.SizeChanged -= OnLabelSizeChanged;
            }
        }

    }
}
