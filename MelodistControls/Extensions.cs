﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MelodistControls
{
    public static class Extensions
    {

        public static bool IsChildFilePathOf(this string child, string parent)
        {
            string[] parentSplit = parent.Split('\\');
            string[] childSplit = child.Split('\\');

            if (childSplit.Length < parentSplit.Length)
                return false;

            for(int i = 0; i < parentSplit.Length; i++)
            {
                if (parentSplit[i] != childSplit[i])
                    return false;
            }
            return true;
        }

    }
}
