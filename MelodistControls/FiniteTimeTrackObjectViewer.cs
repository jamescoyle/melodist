﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MelodistControls
{
    public abstract class FiniteTimeTrackObjectViewer : FiniteTimeObjectViewer
    {

        public abstract int Track { get; }



        public FiniteTimeTrackObjectViewer(FiniteTimeObject finiteTimeObject, Color colour)
            : base(finiteTimeObject, colour)
        {
        }

        public FiniteTimeTrackObjectViewer()
            : base()
        {
        }

    }
}
