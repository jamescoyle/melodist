﻿using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls.NotesViewers
{
    /// <summary>
    /// Interaction logic for NoteViewer.xaml
    /// </summary>
    public partial class NoteViewer : FiniteTimeTrackObjectViewer
    {

        public Note Note { get; private set; }


        public Pitch Pitch { get { return Note.Pitch; } }


        public override int Track { get { return 127 - Note.Pitch.Value; } }



        public NoteViewer(Note note, Color? colour = null)
            : base(note, (colour == null) ? Colors.SeaGreen : colour.Value)
        {
            InitializeComponent();
            Note = note;
        }

    }



    public delegate void NoteViewerEventHandler(object sender, NoteViewerEventArgs e);

    public class NoteViewerEventArgs : EventArgs
    {
        public NoteViewer NoteViewer { get; private set; }

        public NoteViewerEventArgs(NoteViewer noteViewer)
        {
            NoteViewer = noteViewer;
        }
    }
}
