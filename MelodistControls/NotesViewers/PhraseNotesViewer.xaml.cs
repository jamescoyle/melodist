﻿using CSharpExtras;
using HarmonyEngine;
using MelodistControls.NotesViewers.PhraseActions;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MelodistControls.NotesViewers
{
    public partial class PhraseNotesViewer : NotesViewer
    {

        private Phrase _phrase;
        public Phrase Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                if (_phrase != value)
                {
                    if (_phrase != null)
                    {
                        _phrase.Notes.Added -= PhraseNotes_Added;
                        _phrase.Notes.Removed -= PhraseNotes_Removed;
                        _phrase.Notes.ForEach(x => RemoveNoteViewer(x));
                        BindingOperations.ClearAllBindings(BackgroundCanvas);
                        _phrase.BeatsPerBarChanged -= OnBeatsPerBarChanged;
                    }

                    _phrase = value;

                    StartBeat = _phrase.StartBeat.Floor();
                    EndBeat = _phrase.EndBeat.Ceiling();

                    OnBeatsPerBarChanged(null, null);

                    //Set up listeners on the phrase for new chords being added, add chords already in the phrase
                    _phrase.Notes.Added += PhraseNotes_Added;
                    _phrase.Notes.Removed += PhraseNotes_Removed;
                    _phrase.Notes.ForEach(x => AddNoteViewer(x));
                    _phrase.BeatsPerBarChanged += OnBeatsPerBarChanged;
                }
            }
        }



        public PhraseNotesViewer()
            : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();
        }


        private void OnBeatsPerBarChanged(object sender, EventArgs e)
        {
            TimeSignaturesManager.Clear();
            TimeSignaturesManager.Add(new TimeSignature(0, _phrase.BeatsPerBar, 4));
        }


        private void PhraseNotes_Added(object sender, MemberListEventArgs<Note> args)
        {
            AddNoteViewer(args.Item);
        }

        private void PhraseNotes_Removed(object sender, MemberListEventArgs<Note> args)
        {
            RemoveNoteViewer(args.Item);
        }


        public override MusicViewerAction NewCreateAction()
        {
            return new CreateNoteAction(this);
        }

        public override MusicViewerAction NewDeleteAction()
        {
            return new DeleteNoteAction(this);
        }

        public override MusicViewerAction NewPasteAction()
        {
            return new PasteAction(this);
        }

    }



    public abstract class PhraseNotesViewerAction : MultiTrackViewerAction
    {

        public PhraseNotesViewer PhraseNotesViewer { get; private set; }


        public PhraseNotesViewerAction(PhraseNotesViewer phraseNotesViewer)
            : base(phraseNotesViewer)
        {
            PhraseNotesViewer = phraseNotesViewer;
        }

    }
}
