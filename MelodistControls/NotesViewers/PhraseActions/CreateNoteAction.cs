﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.NotesViewers.PhraseActions
{
    public class CreateNoteAction : PhraseNotesViewerAction, IUndoable
    {

        private Note _note;

        private Rational _dragStartBeat;

        public bool IsUndone { get; private set; }



        public CreateNoteAction(PhraseNotesViewer phraseNoteViewer)
            : base(phraseNoteViewer)
        {
            IsUndone = false;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_note != null)
                return;

            PhraseNotesViewer.GetSelectedNoteViewers().ForEach(x => x.Selected = false);

            _note = new Note(PhraseNotesViewer.GetMousePitch(), PhraseNotesViewer.Quantise(PhraseNotesViewer.GetMouseBeat()), 0);
            _dragStartBeat = _note.StartBeat;
            PhraseNotesViewer.Phrase.AddNote(_note);
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            Rational mouseBeat = PhraseNotesViewer.GetMouseBeat();
            _note.StartBeat = Rational.Max(Rational.Min(mouseBeat, _dragStartBeat), PhraseNotesViewer.StartBeat);
            _note.EndBeat = Rational.Min(Rational.Max(mouseBeat, _dragStartBeat), PhraseNotesViewer.EndBeat);
        }


        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            Finished = true;
            if (_note.Duration == 0)
            {
                PhraseNotesViewer.Phrase.RemoveNote(_note);
                _note = null;
            }
        }



        public void Undo()
        {
            if (_note == null)
                return;

            PhraseNotesViewer.Phrase.RemoveNote(_note);
            IsUndone = true;
        }


        public void Redo()
        {
            PhraseNotesViewer.Phrase.AddNote(_note);
            IsUndone = false;
        }

    }
}
