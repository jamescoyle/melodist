﻿using CSharpExtras;
using HarmonyEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.NotesViewers.PhraseActions
{
    public class DeleteNoteAction : PhraseNotesViewerAction, IUndoable
    {

        private List<Note> _notes = new List<Note>();

        public bool IsUndone { get; private set; }



        public DeleteNoteAction(PhraseNotesViewer phraseNoteViewer)
            : base(phraseNoteViewer)
        {
            _notes = PhraseNotesViewer.GetSelectedNoteViewers().Select(x => x.Note).ToList();
            Redo();
            Finished = true;
        }



        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }


        public void Undo()
        {
            _notes.ForEach(x => PhraseNotesViewer.Phrase.AddNote(x));
            IsUndone = true;
        }

        public void Redo()
        {
            _notes.ForEach(x => PhraseNotesViewer.Phrase.RemoveNote(x));
            IsUndone = false;
        }

    }
}
