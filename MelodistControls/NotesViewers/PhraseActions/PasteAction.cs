﻿using CSharpExtras;
using HarmonyEngine;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MelodistControls.NotesViewers.PhraseActions
{
    public class PasteAction : PhraseNotesViewerAction, IUndoable
    {

        private List<Note> _notes;

        public bool IsUndone { get; private set; }


        public PasteAction(PhraseNotesViewer phraseNoteViewer)
            : base(phraseNoteViewer)
        {
            IsUndone = false;

            List<Note> notes = MusicViewerActions.CopyAction.Clipboard.OfType<Note>().ToList();
            if (notes.Count == 0)
            {
                Finished = true;
                return;
            }

            Rational pasteStartBeat = PhraseNotesViewer.GetMouseBeat();
            Rational notesStartBeat = notes.Min(x => x.StartBeat);
            Rational pasteNotesDuration = notes.Max(x => x.EndBeat) - notesStartBeat;
            pasteStartBeat = Rational.Max(pasteStartBeat, PhraseNotesViewer.Phrase.StartBeat);
            pasteStartBeat = Rational.Min(pasteStartBeat, PhraseNotesViewer.Phrase.EndBeat - pasteNotesDuration);
            Rational pasteBeatOffset = pasteStartBeat - notesStartBeat;

            Pitch pasteStartPitch = PhraseNotesViewer.GetMousePitch();
            Pitch notesStartPitch = notes.Where(x => x.StartBeat == notesStartBeat).First().Pitch;
            Pitch minPitch = notes.Min(x => x.Pitch.Value);
            Pitch maxPitch = notes.Max(x => x.Pitch.Value);
            pasteStartPitch = Math.Max(pasteStartPitch, notesStartPitch.Value - minPitch.Value);
            pasteStartPitch = Math.Min(pasteStartPitch, 127 - (maxPitch.Value - notesStartPitch.Value));
            int pastePitchOffset = pasteStartPitch.Value - notesStartPitch.Value;

            _notes = new List<Note>();
            foreach (Note note in notes)
                _notes.Add(new Note(note.Pitch.Value + pastePitchOffset, note.StartBeat + pasteBeatOffset, note.Duration));

            Redo();
            Finished = true;
        }


        public override void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
        }



        public void Undo()
        {
            if (_notes == null)
                return;

            foreach (Note note in _notes)
                PhraseNotesViewer.Phrase.RemoveNote(note);
            IsUndone = true;
        }


        public void Redo()
        {
            PhraseNotesViewer.NoteViewers.ForEach(x => x.Selected = false);
            foreach (Note note in _notes)
            {
                PhraseNotesViewer.Phrase.AddNote(note);
                NoteViewer noteViewer = PhraseNotesViewer.NoteViewers.FirstOrDefault(x => x.Note == note);
                if (noteViewer != null)
                    noteViewer.Selected = true;
            }
            IsUndone = false;
        }

    }
}
