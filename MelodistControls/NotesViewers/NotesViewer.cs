﻿using HarmonyEngine;
using MathPlus;
using MelodistControls.MultiTrackViewerActions;
using MelodistControls.MusicViewerActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.NotesViewers
{
    public class NotesViewer : MultiTrackViewer
    {

        public List<NoteViewer> NoteViewers { get; private set; }



        public NotesViewer()
        {
        }


        protected override void Init()
        {
            BackgroundCanvas.Background = new SolidColorBrush(Colors.Gray);

            NoteViewers = new List<NoteViewer>();

            Painter = new NotesViewerPainter(this);

            TrackHeight = 10;
            TrackCount = 128;

            StartBeatChanged += OnNotesUpdateRequired;
            BeatWidthChanged += OnNotesUpdateRequired;
            TrackHeightChanged += OnNotesUpdateRequired;

            KeyDown += OnKeyDown;
            ForegroundCanvas.MouseDown += OnMouseDown;
            ForegroundCanvas.MouseMove += OnMouseMove;
            ForegroundCanvas.MouseUp += OnMouseUp;

            base.Init();
        }



        #region Note Management Logic
        private void OnNotesUpdateRequired(object sender, EventArgs e)
        {
            foreach (NoteViewer noteViewer in NoteViewers)
                UpdateNoteViewer(noteViewer);
        }


        protected void OnNotePropertyChanged(object sender, EventArgs e)
        {
            Note note = (Note)sender;
            note.StartBeat = Quantise(note.StartBeat);
            note.Duration = Quantise(note.Duration);

            NoteViewer noteViewer = NoteViewers.FirstOrDefault(x => x.Note == note);
            if (noteViewer != null)
                UpdateNoteViewer(noteViewer);
        }


        public event NoteViewerEventHandler NoteViewerAdded;
        protected void AddNoteViewer(Note note)
        {
            NoteViewer noteViewer = new NoteViewer(note);
            ForegroundCanvas.Children.Add(noteViewer);
            NoteViewers.Add(noteViewer);

            note.StartBeatChanged += OnNotePropertyChanged;
            note.DurationChanged += OnNotePropertyChanged;
            note.PitchChanged += OnNotePropertyChanged;
            OnNotePropertyChanged(note, null);
            NoteViewerAdded?.Invoke(this, new NoteViewerEventArgs(noteViewer));
        }


        protected void UpdateNoteViewer(NoteViewer noteViewer)
        {
            Canvas.SetLeft(noteViewer, (noteViewer.Note.StartBeat - StartBeat) * BeatWidth);
            noteViewer.Width = noteViewer.Note.Duration * BeatWidth;
            Canvas.SetTop(noteViewer, noteViewer.Track * TrackHeight);
            noteViewer.Height = TrackHeight;
        }


        public event NoteViewerEventHandler NoteViewerRemoved;
        protected void RemoveNoteViewer(Note note)
        {
            NoteViewer noteViewer = NoteViewers.FirstOrDefault(x => x.Note == note);
            if (noteViewer != null)
            {
                NoteViewers.Remove(noteViewer);
                ForegroundCanvas.Children.Remove(noteViewer);

                noteViewer.Note.StartBeatChanged -= OnNotePropertyChanged;
                noteViewer.Note.DurationChanged -= OnNotePropertyChanged;
                noteViewer.Note.PitchChanged -= OnNotePropertyChanged;
                NoteViewerRemoved?.Invoke(this, new NoteViewerEventArgs(noteViewer));
            }
        }
        #endregion Note Management Logic



        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewers()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            NoteViewers.ForEach(x => output.Add(x));
            return output;
        }


        public override List<FiniteTimeTrackObjectViewer> GetFiniteTimeTrackObjectViewersUnderMouse()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            GetNoteViewersUnderMouse().ForEach(x => output.Add(x));
            return output;
        }

        public List<NoteViewer> GetNoteViewersUnderMouse()
        {
            Rational mouseBeat = GetMouseBeat();
            int mouseTrack = GetMouseTrack();
            return NoteViewers.Where(x => x.StartBeat <= mouseBeat && x.EndBeat >= mouseBeat && x.Track == mouseTrack).ToList();
        }


        public override List<FiniteTimeTrackObjectViewer> GetSelectedFiniteTimeTrackObjectViewers()
        {
            List<FiniteTimeTrackObjectViewer> output = new List<FiniteTimeTrackObjectViewer>();
            GetSelectedNoteViewers().ForEach(x => output.Add(x));
            return output;
        }

        public List<NoteViewer> GetSelectedNoteViewers()
        {
            return NoteViewers.Where(x => x.Selected).ToList();
        }



        public Pitch GetMousePitch()
        {
            return new Pitch(127 - GetMouseTrack());
        }


        public override MultiTrackViewerAction NewMoveAction()
        {
            return new MoveAction<Note>(
                this,
                (note) => 127 - note.Pitch.Value,
                (note, track) => note.Pitch = 127 - track
            );
        }



        #region Action Handlers
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseDownBehaviour(sender, e);
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            DefaultMouseMoveBehaviour(sender, e);
        }


        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            DefaultMouseUpBehaviour(sender, e);
        }


        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            DefaultKeyDownBehaviour(sender, e);
        }
        #endregion Action Handlers

    }



    public class NotesViewerPainter : MultiTrackViewerPainter
    {

        private List<Rectangle> _blackRects;


        public NotesViewerPainter(NotesViewer notesViewer)
            : base(notesViewer)
        {
            _blackRects = new List<Rectangle>();

            Style beatLineStyle = new Style(typeof(Line));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.LightGray)));
            beatLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(1, beatLineStyle);

            Style semiQuaverLineStyle = new Style(typeof(Line));
            semiQuaverLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.SlateGray)));
            semiQuaverLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
            VerticalLineStyles.Add(new Rational(1, 4), semiQuaverLineStyle);

            BarLineStyle = new Style(typeof(Line));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Colors.Black)));
            BarLineStyle.Setters.Add(new Setter(Line.StrokeThicknessProperty, 1.0));
        }


        public override void PaintBack()
        {
            foreach (Rectangle rect in _blackRects)
                MultiTrackViewer.BackgroundCanvas.Children.Remove(rect);

            List<int> blackPitches = new List<int>() { 1, 3, 6, 8, 10 };
            for (int i = 0; blackPitches[i] + 12 < 128; i++)
                blackPitches.Add(blackPitches[i] + 12);

            foreach (int pitch in blackPitches)
            {
                Rectangle rect = new Rectangle()
                {
                    Fill = new SolidColorBrush(Colors.DimGray),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top
                };
                _blackRects.Add(rect);
                MultiTrackViewer.BackgroundCanvas.Children.Add(rect);

                Binding binding = new Binding("ActualWidth") { Source = MultiTrackViewer.BackgroundCanvas };
                BindingOperations.SetBinding(rect, Rectangle.WidthProperty, binding);

                Canvas.SetTop(rect, (127 - pitch) * MultiTrackViewer.TrackHeight);
                rect.Height = MultiTrackViewer.TrackHeight;
            }
        }

    }



    public abstract class NotesViewerAction : MultiTrackViewerAction
    {

        public NotesViewer NotesViewer { get; private set; }


        public NotesViewerAction(NotesViewer notesViewer)
            : base(notesViewer)
        {
            NotesViewer = notesViewer;
        }

    }
}
