﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls
{
    /// <summary>
    /// Interaction logic for FileTreeView.xaml
    /// </summary>
    public partial class FileTreeView : TreeView
    {

        private FileSystemWatcher _fileWatcher;

        private FileSystemWatcher _folderWatcher;


        private string _rootPath;
        public string RootPath
        {
            get { return _rootPath; }
            set
            {
                value = System.IO.Path.GetDirectoryName(value);
                if (value == _rootPath)
                    return;

                _rootPath = value;
                Items.Clear();
                foreach(string str in Directory.GetDirectories(_rootPath))
                    AddItem(str, false);

                foreach(string str in Directory.GetFiles(_rootPath).Where(x => x.EndsWith(".phr")))
                    AddItem(str, true);

                if(_fileWatcher != null)
                {
                    _fileWatcher.EnableRaisingEvents = false;
                    _fileWatcher.Created -= _fileWatcher_Created;
                    _fileWatcher.Renamed -= _fileWatcher_Renamed;
                    _fileWatcher.Deleted -= _fileWatcher_Deleted;
                    _fileWatcher.Dispose();
                }
                _fileWatcher = new FileSystemWatcher(_rootPath, "*.phr");
                _fileWatcher.IncludeSubdirectories = true;
                _fileWatcher.NotifyFilter = NotifyFilters.FileName;
                _fileWatcher.Created += _fileWatcher_Created;
                _fileWatcher.Renamed += _fileWatcher_Renamed;
                _fileWatcher.Deleted += _fileWatcher_Deleted;
                _fileWatcher.EnableRaisingEvents = true;

                if (_folderWatcher != null)
                {
                    _folderWatcher.EnableRaisingEvents = false;
                    _folderWatcher.Created -= _folderWatcher_Created;
                    _folderWatcher.Renamed -= _folderWatcher_Renamed;
                    _folderWatcher.Deleted -= _folderWatcher_Deleted;
                    _folderWatcher.Dispose();
                }
                _folderWatcher = new FileSystemWatcher(_rootPath);
                _folderWatcher.IncludeSubdirectories = true;
                _folderWatcher.NotifyFilter = NotifyFilters.DirectoryName;
                _folderWatcher.Created += _folderWatcher_Created;
                _folderWatcher.Renamed += _folderWatcher_Renamed;
                _folderWatcher.Deleted += _folderWatcher_Deleted;
                _folderWatcher.EnableRaisingEvents = true;
            }
        }


        public FileTreeView()
        {
            InitializeComponent();
        }


        private void SubItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if(item.Items.Count == 1 && IsDummyNode(item.Items[0]))
            {
                item.Items.Clear();
                foreach (string str in Directory.GetDirectories(item.Tag.ToString()))
                    AddItem(str, false, item.Items);

                foreach (string str in Directory.GetFiles(item.Tag.ToString()).Where(x => x.EndsWith(".phr")))
                    AddItem(str, true, item.Items);
            }
        }


        private void _fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Dispatcher.Invoke(() => AddItem(e.FullPath, true));
        }

        private void _fileWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            Dispatcher.Invoke(() => RenameItem(e.OldFullPath, e.FullPath));
        }
        
        private void _fileWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            Dispatcher.Invoke(() => RemoveItem(e.FullPath));
        }
        
        private void _folderWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Dispatcher.Invoke(() => AddItem(e.FullPath, false));
        }

        private void _folderWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            Dispatcher.Invoke(() => RenameItem(e.OldFullPath, e.FullPath));
        }

        private void _folderWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            Dispatcher.Invoke(() => RemoveItem(e.FullPath));
        }


        private void AddItem(string path, bool isFile, ItemCollection parent = null)
        {
            if (parent == null)
                parent = Items;

            if (parent.Count == 1 && IsDummyNode(parent[0]))
                return;

            foreach (TreeViewItem item in parent)
            {
                if (path.IsChildFilePathOf(item.Tag.ToString()))
                {
                    AddItem(path, isFile, item.Items);
                    return;
                }
            }

            TreeViewItem subItem = isFile ? new TreeViewDragItem() : new TreeViewItem();
            subItem.Header = path.Substring(path.LastIndexOf("\\") + 1);
            subItem.Tag = path;
            subItem.FontWeight = FontWeights.Normal;
            if (!isFile)
            {
                subItem.Items.Add(NewDummyNode());
                subItem.Expanded += SubItem_Expanded;
            }
            parent.Add(subItem);
        }


        private void RenameItem(string oldPath, string newPath, ItemCollection parent = null)
        {
            if (parent == null)
                parent = Items;

            foreach(TreeViewItem item in parent)
            {
                if (oldPath.IsChildFilePathOf(item.Tag.ToString()))
                {
                    if (oldPath == item.Tag.ToString())
                    {
                        item.Header = newPath.Substring(newPath.LastIndexOf("\\") + 1);
                        item.Tag = newPath;
                    }
                    else
                        RenameItem(oldPath, newPath, item.Items);
                    return;
                }
            }
        }


        private void RemoveItem(string path, ItemCollection parent = null)
        {
            if (parent == null)
                parent = Items;

            foreach(TreeViewItem item in parent)
            {
                if(path.IsChildFilePathOf(item.Tag.ToString()))
                {
                    if (path == item.Tag.ToString())
                        parent.Remove(item);
                    else
                        RemoveItem(path, item.Items);
                    return;
                }
            }
        }


        private TreeViewItem NewDummyNode()
        {
            return new TreeViewItem()
            {
                Tag = "DUMMY"
            };
        }


        private bool IsDummyNode(object item)
        {
            if (item is TreeViewItem)
                return IsDummyNode((TreeViewItem)item);
            return false;
        }

        private bool IsDummyNode(TreeViewItem item)
        {
            return (item.Tag.ToString() == "DUMMY");
        }

    }



    public class TreeViewDragItem : TreeViewItem
    {

        public TreeViewDragItem()
            : base()
        {
            PreviewMouseLeftButtonDown += (sender, args) =>
            {
                DragDrop.DoDragDrop(this, Tag.ToString(), DragDropEffects.Copy);
            };
        }

    }
}
