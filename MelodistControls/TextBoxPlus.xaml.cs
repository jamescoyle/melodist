﻿using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MelodistControls
{
    /// <summary>
    /// Interaction logic for TextBoxPlus.xaml
    /// </summary>
    public partial class TextBoxPlus : TextBox
    {
        public TextBoxPlus()
        {
            InitializeComponent();
        }


        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            SelectAll();
        }


        protected override void OnDragOver(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;

            if (
                e.Data.GetDataPresent(typeof(Phrase)) || 
                e.Data.GetDataPresent(DataFormats.StringFormat) ||
                e.Data.GetDataPresent(typeof(PhraseParse))
            )
            {
                e.Effects = DragDropEffects.Copy;
            }
        }


        protected override void OnDrop(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length > 0)
                    Text = files[0];
            }
            else if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                Text = e.Data.GetData(typeof(string)).ToString();
                DataContext = null;
            }

            else if (e.Data.GetDataPresent(typeof(Phrase)))
            {
                Phrase phrase = e.Data.GetData(typeof(Phrase)) as Phrase;
                Text = phrase.ToString();
                e.Handled = true;
                DataContext = phrase;
            }
            else if (e.Data.GetDataPresent(typeof(PhraseParse)))
            {
                PhraseParse phraseParse = e.Data.GetData(typeof(PhraseParse)) as PhraseParse;
                Text = phraseParse.Name;
                e.Handled = true;
                DataContext = phraseParse;
            }
        }


        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            if(DataContext != null)
            {
                if (DataContext is Phrase && ((Phrase)DataContext).Name != Text)
                    DataContext = null;

                else if (DataContext is PhraseParse && ((PhraseParse)DataContext).Name != Text)
                    DataContext = null;
            }
        }


        public Phrase GetPhrase()
        {
            if(DataContext != null)
            {
                if (DataContext is Phrase)
                    return (Phrase)DataContext;

                if(DataContext is PhraseParse)
                    return Phrase.Load(((PhraseParse)DataContext).FilePath);
            }
            return null;
        }
    }
}
