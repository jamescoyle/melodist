﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MelodistControls.PlayheadViewers
{
    public class PlayheadViewer : MusicViewer
    {

        public PlayheadViewer()
        {
        }


        protected override void Init()
        {
            BackgroundCanvas.Background = new SolidColorBrush(Colors.Transparent);

            base.Init();
        }



        private Line _playhead = null;

        private Rational _lastBeatUpdate = -1;

        private DateTime _lastUpdateTime = DateTime.MinValue;

        private double _beatsPerMillisecond = -1;


        public void UpdatePlayhead(Rational beat)
        {
            if (_lastBeatUpdate == -1 && _lastUpdateTime == DateTime.MinValue)
            {
                _lastBeatUpdate = beat;
                _lastUpdateTime = DateTime.Now;
            }

            double newBeatValue = beat;
            if (beat != _lastBeatUpdate)
                _beatsPerMillisecond = (beat - _lastBeatUpdate) / DateTime.Now.Subtract(_lastUpdateTime).TotalMilliseconds;
            else if (_beatsPerMillisecond > 0)
                newBeatValue += DateTime.Now.Subtract(_lastUpdateTime).TotalMilliseconds * _beatsPerMillisecond;
            if (beat != _lastBeatUpdate)
            {
                _lastBeatUpdate = beat;
                _lastUpdateTime = DateTime.Now;
            }

            double playheadX = (newBeatValue - StartBeat) * BeatWidth;
            if (_playhead == null)
            {
                _playhead = new Line()
                {
                    X1 = playheadX,
                    Y1 = 0,
                    X2 = playheadX,
                    Y2 = ActualHeight,
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = 2
                };
                ForegroundCanvas.Children.Add(_playhead);
            }
            else
                _playhead.X1 = _playhead.X2 = playheadX;
        }


        public void RemovePlayhead()
        {
            if (_playhead != null)
            {
                ForegroundCanvas.Children.Remove(_playhead);
                _playhead = null;
            }
            _lastBeatUpdate = -1;
            _lastUpdateTime = DateTime.MinValue;
            _beatsPerMillisecond = -1;
        }


        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewers()
        {
            return new List<FiniteTimeObjectViewer>();
        }

        public override List<FiniteTimeObjectViewer> GetFiniteTimeObjectViewersUnderMouse()
        {
            return new List<FiniteTimeObjectViewer>();
        }

        public override List<FiniteTimeObjectViewer> GetSelectedFiniteTimeObjectViewers()
        {
            return new List<FiniteTimeObjectViewer>();
        }

    }
}
