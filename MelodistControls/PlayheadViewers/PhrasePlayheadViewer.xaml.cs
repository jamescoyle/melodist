﻿using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MelodistControls.PlayheadViewers
{
    public partial class PhrasePlayheadViewer : PlayheadViewer
    {

        private Phrase _phrase;
        public Phrase Phrase
        {
            get
            {
                return _phrase;
            }
            set
            {
                _phrase = value;

                StartBeat = _phrase.StartBeat.Floor();
                EndBeat = _phrase.EndBeat.Ceiling();
            }
        }



        public PhrasePlayheadViewer()
            : base()
        {
            InitializeComponent();
            BackgroundCanvas = _backgroundCanvas;
            ForegroundCanvas = _foregroundCanvas;
            Init();
        }

    }
}
