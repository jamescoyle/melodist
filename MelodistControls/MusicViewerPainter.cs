﻿using MathPlus;
using PhraseManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Shapes;

namespace MelodistControls
{
    public abstract class MusicViewerPainter
    {

        public Dictionary<Rational, Style> VerticalLineStyles { get; private set; }

        public Style BarLineStyle { get; protected set; }

        protected List<Line> VerticalLines { get; private set; }


        public MusicViewer MusicViewer { get; private set; }



        public MusicViewerPainter(MusicViewer musicViewer)
        {
            MusicViewer = musicViewer;
            VerticalLineStyles = new Dictionary<Rational, Style>();
            VerticalLines = new List<Line>();
        }


        public virtual void Paint()
        {
            PaintBack();
            PaintLines();
        }


        public abstract void PaintBack();



        public virtual void PaintLines()
        {
            if (VerticalLineStyles.Count == 0 && BarLineStyle == null)
                return;

            //Remove all current vertical lines
            foreach (Line verticalLine in VerticalLines)
                MusicViewer.BackgroundCanvas.Children.Remove(verticalLine);
            VerticalLines.Clear();

            //Compile a list of line styles, arranged by how often they occur
            List<KeyValuePair<Rational, Style>> orderedLineStyles = new List<KeyValuePair<Rational, Style>>();
            foreach (KeyValuePair<Rational, Style> lineStyle in VerticalLineStyles)
                orderedLineStyles.Add(lineStyle);
            orderedLineStyles = orderedLineStyles.OrderByDescending(x => x.Key).ToList();

            //Calculate the highest common factor in line style beats to get the increment for iterating
            Rational increment = (orderedLineStyles.Count == 0) ? 1 : orderedLineStyles[0].Key;
            if (orderedLineStyles.Count > 1)
                increment = NumberTheory.HCF(orderedLineStyles.Select(x => x.Key).ToList());

            //Define the iterator i, make sure it is positioned at the position where it will start drawing from
            Rational i = 0;
            while (i < MusicViewer.StartBeat)
                i += increment;
            while (i > MusicViewer.StartBeat)
                i -= increment;
            if (i < MusicViewer.StartBeat)
                i += increment;

            //Start looping through, drawing new vertical lines
            for (; i <= MusicViewer.EndBeat; i += increment)
            {
                if (BarLineStyle != null && i % 1 == 0)
                {
                    TimeSignature timeSig = MusicViewer.GetTimeSignatureAt(i);
                    if (timeSig != null)
                    {
                        if (
                            (i < 0 && (i - timeSig.Beat) % timeSig.QuarterBeatsPerBar == 0) ||
                            (i >= 0 && (i - Rational.Max(0, timeSig.Beat)) % timeSig.QuarterBeatsPerBar == 0)
                        )
                        {
                            Line line = new Line()
                            {
                                HorizontalAlignment = HorizontalAlignment.Left,
                                Style = BarLineStyle,
                                Y1 = 0
                            };
                            line.X1 = line.X2 = (i - MusicViewer.StartBeat) * MusicViewer.BeatWidth;
                            Binding binding = new Binding("ActualHeight") { Source = MusicViewer.BackgroundCanvas };
                            BindingOperations.SetBinding(line, Line.Y2Property, binding);
                            MusicViewer.BackgroundCanvas.Children.Add(line);
                            VerticalLines.Add(line);
                            continue;
                        }
                    }
                }

                foreach (KeyValuePair<Rational, Style> lineStyle in orderedLineStyles)
                {
                    if (i % lineStyle.Key == 0)
                    {
                        Line line = new Line()
                        {
                            HorizontalAlignment = HorizontalAlignment.Left,
                            Style = lineStyle.Value,
                            Y1 = 0
                        };
                        line.X1 = line.X2 = (i - MusicViewer.StartBeat) * MusicViewer.BeatWidth;
                        Binding binding = new Binding("ActualHeight") { Source = MusicViewer.BackgroundCanvas };
                        BindingOperations.SetBinding(line, Line.Y2Property, binding);
                        MusicViewer.BackgroundCanvas.Children.Add(line);
                        VerticalLines.Add(line);
                        break;
                    }
                }
            }
        }

    }
}
