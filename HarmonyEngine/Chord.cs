﻿using CSharpExtras;
using HarmonyEngine.Algorithms;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public class Chord : FiniteTimeObject
    {

        private MemberListManager<Pitch> _pitchesMngr;
        public MemberList<Pitch> Pitches { get { return _pitchesMngr.List; } }

        private void UpdatePitches()
        {
            if (_chordDef == null)
                return;

            List<Pitch> pitches = new List<Pitch>();
            _chordDef.Structure.ForEach(x => pitches.Add(x + _root));
            _pitchesMngr = new MemberListManager<Pitch>(pitches);
        }


        private Pitch _root;
        public event EventHandler RootChanged;
        public Pitch Root
        {
            get { lock (Lock) { return _root; } }
            set
            {
                if(_root != value)
                {
                    lock (Lock)
                    {
                        _root = value;
                        UpdatePitches();
                    }
                    OnPropertyChanged("Root");
                    RootChanged?.Invoke(this, null);
                }
            }
        }


        private ChordDef _chordDef;
        public event EventHandler ChordDefChanged;
        public ChordDef ChordDef
        {
            get { lock (Lock) { return _chordDef; } }
            set
            {
                if(_chordDef != value)
                {
                    lock (Lock)
                    {
                        _chordDef = value;
                        UpdatePitches();
                    }
                    OnPropertyChanged("ChordDef");
                    ChordDefChanged?.Invoke(this, null);
                }
            }
        }
        

        public double Dissonance
        {
            get { return ChordDef.Dissonance; }
        }



        public Chord(ChordDef chordDef, Pitch root, Rational startBeat, Rational duration)
            : base(startBeat, duration)
        {
            Root = root;
            ChordDef = chordDef;
        }


        public bool Contains(Pitch pitch)
        {
            foreach(Pitch p in Pitches)
            {
                if ((pitch - p).Value == 0)
                    return true;
            }
            return false;
        }


        public double Distance(Scale scale)
        {
            return ChordScaleDistance.Distance(this, scale);
        }

        public double Distance(Chord chord)
        {
            return ChordChordDistance.Distance(this, chord);
        }

        public double Distance(ChordDef chordDef, Pitch root)
        {
            return Distance(new Chord(chordDef, root, StartBeat, Duration));
        }

        public double Distance(Pitch pitch)
        {
            return ChordPitchDistance.Distance(this, pitch);
        }


        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool useFlats)
        {
            return Root.ToString(useFlats) + ChordDef.ToString();
        }



        public Chord Copy()
        {
            return new Chord(ChordDef, Root, StartBeat, Duration);
        }



        public static Chord Retrieve(List<Pitch> pitches, Rational startBeat, Rational duration)
        {
            Chord chord = ChordDef.Retrieve(pitches);
            if(chord != null)
            {
                chord.StartBeat = startBeat;
                chord.Duration = duration;
            }
            return chord;
        }

    }



    public class ChordParse : FiniteTimeObjectParse
    {
        public int Root;
        public string Suffix;

        public ChordParse()
        {
        }

        public ChordParse(Chord chord)
            : base(chord)
        {
            Root = chord.Root;
            Suffix = chord.ChordDef.Suffix;
        }

        public new Chord Load()
        {
            return new Chord(ChordDef.Retrieve(Suffix), Root, StartBeat.Load(), Duration.Load());
        }
    }
}
