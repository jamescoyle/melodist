﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public class HarmonyEngineException : Exception
    {

        private HarmonyEngineException(string message)
            : base(message)
        {
        }



        public static HarmonyEngineException PitchOutOfRange()
        {
            return new HarmonyEngineException("Pitch value defined out of allowable range [0, 127]");
        }


        public static HarmonyEngineException IntervalOutOfRange()
        {
            return new HarmonyEngineException("Interval value defined out of allowable range [-127, 127]");
        }


        public static HarmonyEngineException VelocityOutOfRange()
        {
            return new HarmonyEngineException("Velocity value defined out of allowable range [0, 127]");
        }


        public static HarmonyEngineException NegativeDuration()
        {
            return new HarmonyEngineException("Duration must take a non-negative value");
        }


        public static HarmonyEngineException UnrecognisedChordDef(string suffix)
        {
            return new HarmonyEngineException("Chord type '" + suffix + "' not recognised");
        }


        public static HarmonyEngineException UnrecognisedChordDef(List<int> structure)
        {
            string structureStr = string.Empty;
            for (int i = 0; i < structure.Count; i++)
            {
                if (i > 0)
                    structureStr += ",";
                structureStr += structure[i].ToString();
            }
            return new HarmonyEngineException("Chord structure [" + structureStr + "] not recognised");
        }

    }
}
