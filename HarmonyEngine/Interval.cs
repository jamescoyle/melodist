﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public struct Interval
    {

        public int Value { get; private set; }


        public int Octaves { get { return Math.Abs(Value) / 12; } }


        public bool Perfect
        {
            get
            {
                int absValue = Math.Abs(Value) % 12;
                return (absValue == 0 || absValue == 5 || absValue == 7);
            }
        }


        private static double[] _nonShrinkageDissonances;
        private static double[] _shrinkageDissonances;

        private static List<Rational> JustRatios;


        private const double EXPONENT_PARAM = 0.05;

        private const double OCTAVE_SHRINK_PARAM = 0.9;

        private const double SHRINK_PARAM = 1.1;



        public Interval(int value)
        {
            if (value < -127 || value > 127)
                throw HarmonyEngineException.IntervalOutOfRange();
            Value = value;
        }


        static Interval()
        {
            _nonShrinkageDissonances = new double[128];
            _shrinkageDissonances = new double[128];

            for (int i = 0; i < 128; i++)
            {
                _nonShrinkageDissonances[i] = -1;
                _shrinkageDissonances[i] = -1;
            }

            JustRatios = new List<Rational>()
            {
                new Rational(1, 1),
                new Rational(16, 15),
                new Rational(9, 8),
                new Rational(6, 5),
                new Rational(5, 4),
                new Rational(4, 3),
                new Rational(45, 32),
                new Rational(3, 2),
                new Rational(8, 5),
                new Rational(5, 3),
                new Rational(9, 5),
                new Rational(15, 8)
            };
        }



        /// <summary>
        /// Gets a value representing the amount of dissonance in the interval
        /// </summary>
        /// <param name="octaveShrinkage">Should the dissonance become smaller for larger intervals?</param>
        /// <returns></returns>
        public double Dissonance(bool octaveShrinkage)
        {
            int interval = Math.Abs(Value);

            double output = (octaveShrinkage) ? _shrinkageDissonances[interval] : _nonShrinkageDissonances[interval];
            if (output >= 0)
                return output;

            //Get the just ratio for the interval
            Rational justRatio = JustRatios[interval % 12];

            //Get the value of the just ratio, this is used to figure out how close the interval is to an octave / unison
            //This has range [1, 2)
            double justDouble = (double)justRatio;

            //Get the sum of the just ratio numerator & denominator, this is used to get an idea of how consonant the interval is
            //Since intervals with larger fractional components sound more dissonant
            long justSum = justRatio.A + justRatio.B;

            //Figure out how close or far the interval is from octave/unison
            //Taken across all possible intervals this produces an upside-down V shape, with peak at (justDouble = 0.5, distanceFromUnity = 0.5)
            double distanceFromUnity = Math.Abs(justDouble - Math.Round(justDouble));

            //Raise the distanceFromUnity by some small exponent to round out the shape and bring up the values closer to 1
            output = Math.Pow(distanceFromUnity, EXPONENT_PARAM);

            //Invert the distance for any interval except those of octave/unison, since the intervals near octaves are generally the more dissonant ones
            //Taken across the whole interval range, this creates a bowl shape like .\_/ (where the dot is the point of unison
            output = (output == 0) ? 0 : 1 - distanceFromUnity;

            //Multiply the distance by the sum of the just ratio numerator & denominator, since more consonant intervals have smaller fractional representations
            //This creates exaggarated spikes at points of dissonance
            output *= justRatio.A + justRatio.B;

            if (octaveShrinkage)
            {
                //Multiply the distance by octave shrink param (some number in the range (0, 1), preferably closer to 1) raised by the number of octaves in the interval
                //This is based on the observation that dissonant intervals stretched by an octave tend to lose some of their dissonance
                output *= Math.Pow(OCTAVE_SHRINK_PARAM, Octaves);
            }

            //Gets set close to 1 for intervals close to unison, increases to upper bound of SHRINK_PARAM for intervals nearing octave
            //Used to trim off a little bit of the dissonance for 7ths, which are far less dissonant than their corresponding 2nds
            double shrinkFactor = Math.Pow(SHRINK_PARAM, (interval % 12) / 12);
            output /= shrinkFactor;

            if (octaveShrinkage)
                _shrinkageDissonances[interval] = output;
            else
                _nonShrinkageDissonances[interval] = output;

            return output;
        }

    }
}
