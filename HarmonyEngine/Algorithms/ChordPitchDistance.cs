﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ChordPitchDistance
    {

        internal static double Distance(Chord chord, Pitch pitch)
        {
            //Return 0 if the chord contains the pitch
            if (chord.Contains(pitch))
                return 0;

            //Return the average distance between the pitch and each of the pitches in the chord
            double output = 0;
            chord.Pitches.ForEach(x => output += (x - pitch).Dissonance(false));
            return output / chord.Pitches.Count;
        }

    }
}
