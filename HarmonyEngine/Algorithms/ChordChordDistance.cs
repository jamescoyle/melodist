﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ChordChordDistance
    {

        internal static double Distance(Chord chord1, Chord chord2)
        {
            double output = 0;

            //Calculate the average distance between each note from one chord to the other
            chord1.Pitches.ForEach(x => output += chord2.Distance(x));
            chord2.Pitches.ForEach(x => output += chord1.Distance(x));
            output /= chord1.Pitches.Count + chord2.Pitches.Count;

            //Calculate the difference in dissonance between the 2 chords
            double bigDissonance = Math.Max(chord1.Dissonance, chord2.Dissonance);
            double smallDissonance = Math.Min(chord1.Dissonance, chord2.Dissonance);
            double dissonanceMultiplier = Math.Sqrt(bigDissonance / smallDissonance);

            return output * dissonanceMultiplier;
        }

    }
}
