﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ChordDefDissonance
    {

        internal static double Dissonance(ChordDef chordDef)
        {
            if (chordDef.Structure.Count <= 1)
                return 0;
            
            //Take the average of the dissonances between each possible pair of pitches within the chord def
            double dissonance = 0;
            for (int i = 0; i < chordDef.Structure.Count; i++)
            {
                for (int j = i + 1; j < chordDef.Structure.Count; j++)
                    dissonance += new Interval(chordDef.Structure[i] - chordDef.Structure[j]).Dissonance(false);
            }
            dissonance /= (chordDef.Structure.Count) * (chordDef.Structure.Count - 1) / 2;

            //Special rule that had to be built in since we hear augmented triads as much more dissonant than the dissonance calculations says they should be
            if (CountAugmenteds(chordDef) > 0)
                dissonance += 1.5;

            return dissonance;
        }


        internal static int CountAugmenteds(ChordDef chordDef)
        {
            //There are only 4 possible augmented chords, create a list, with one position for each
            List<int> augs = new List<int> { 1, 1, 1, 1 };

            //For each note in the chord, find which of the 4 augmented chords it would belong to, multiply the associated array element by a prime marker
            for (int i = 0; i < chordDef.Structure.Count; i++)
            {
                //The interval from the root to the pitch
                int interval = chordDef.Structure[i] % 12;
                //Which of the 4 possible augmented chords the pitch would belong to
                int augIndex = interval % 4;
                //Multiply one of the 4 items in the list by either 2, 3 or 5, depending on whether the interval is in the 0-3, 4-7 or 8-11 range respectively
                augs[augIndex] *= ((interval - augIndex == 0) ? 2 : ((interval - augIndex == 4) ? 3 : 5));
            }

            //Count how many of the 4 array elements have all 3 prime markers on them
            return augs.Count(x => x % 30 == 0);
        }

    }
}
