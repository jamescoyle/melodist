﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ChordScaleDistance
    {

        internal static double Distance(Chord chord, Scale scale)
        {
            //Return the distance between the scale and the closest scale to it that contains the chord
            return ScaleScaleDistance.Distance(scale, scale.GetClosestScaleWithChord(chord));
        }

    }
}
