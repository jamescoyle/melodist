﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ScaleDissonance
    {

        internal static double Dissonance(Scale scale)
        {
            //Sum together the dissonances of all possible triads contained within the scale
            ChordDef dimChord = ChordDef.Retrieve(0, 3, 6);
            ChordDef minChord = ChordDef.Retrieve(0, 3, 7);
            ChordDef majChord = ChordDef.Retrieve(0, 4, 7);
            ChordDef augChord = ChordDef.Retrieve(0, 4, 8);

            double sum = 0;
            int count = 0;
            for (int i = 0; i < 12; i++)
            {
                if (scale.Contains(i))
                {
                    if (scale.Contains(i + 3))
                    {
                        if (scale.Contains(i + 6))
                        {
                            sum += dimChord.Dissonance;
                            count++;
                        }
                        if (scale.Contains(i + 7))
                        {
                            sum += minChord.Dissonance;
                            count++;
                        }
                    }
                    if (scale.Contains(i + 4))
                    {
                        if (scale.Contains(i + 7))
                        {
                            sum += majChord.Dissonance;
                            count++;
                        }
                        if (scale.Contains(i + 8))
                        {
                            sum += augChord.Dissonance;
                            count++;
                        }
                    }
                }
            }

            //Apply penalty multipliers for scales that don't contain certain key points, such as the dominant
            double penaltyMultiplier = 1;
            if (!scale.Contains(scale.Key.Value + 7))
                penaltyMultiplier *= 1.5;

            //Set the dissonance equal to the average dissonance of the chords it contains, times any penalty multiplier acquired
            return (sum / count) * penaltyMultiplier;
        }

    }
}
