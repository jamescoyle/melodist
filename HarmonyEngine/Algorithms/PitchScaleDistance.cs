﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class PitchScaleDistance
    {

        internal static double Distance(Pitch pitch, Scale scale)
        {
            //Return the distance between the scale and the closest scale to it that contains the pitch
            return ScaleScaleDistance.Distance(scale, scale.GetClosestScaleWithPitch(pitch));
        }

    }
}
