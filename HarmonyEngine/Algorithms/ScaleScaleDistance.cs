﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ScaleScaleDistance
    {

        internal static double Distance(Scale scale1, Scale scale2)
        {
            //Count the number of matching notes between the 2 scales
            int matchCount = 0;
            foreach (Pitch pitch in scale1.Pitches)
            {
                if (scale2.Contains(pitch))
                    matchCount++;
            }

            //From the number of matches, calculate the number of mismatches
            int mismatchCount = scale1.Pitches.Count + scale2.Pitches.Count - (matchCount * 2);

            //Calculate the proportional change in dissonance from one scale to the other
            double bigDissonance = Math.Max(scale1.Dissonance, scale2.Dissonance);
            double smallDissonance = Math.Min(scale1.Dissonance, scale2.Dissonance);
            double dissonanceMultiplier = Math.Sqrt(bigDissonance / smallDissonance);

            //Return the number of notes different between the scales, times the change in dissonance between them
            return mismatchCount * dissonanceMultiplier;
        }

    }
}
