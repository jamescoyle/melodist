﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class RhythmRhythmDistance
    {

        internal static double Distance(Rhythm rhythm1, Rhythm rhythm2)
        {
            //Make sure that the calling object is not the one with more pulses, for the sake of simplicity later on
            if (rhythm1.Pulses.Count > rhythm2.Pulses.Count)
                return Distance(rhythm2, rhythm1);

            //Calculate the average difference in duration between pulses from rhythm1 and rhythm2
            double averageDurationDifference = 0;
            for(int i = 0; i < rhythm1.Pulses.Count; i++)
            {
                for(int j = 0; j < rhythm2.Pulses.Count; j++)
                    averageDurationDifference += (rhythm1.Pulses[i].Duration - rhythm2.Pulses[j].Duration).Abs();
            }
            averageDurationDifference /= (rhythm1.Pulses.Count * rhythm2.Pulses.Count);

            //Construct a matrix of distances between rhythm pulses and find an optimal pairing of pulses from one rhythm to the other
            //Explanation of the distance calculation:
            //A pulse's strength is being defined as the position at which it starts in relation to a beat, for example, a note that starts on the beat is stronger than a note that starts on a 16th
            //To calculate the strength distance, you need to get the rationals of the 2 notes, for example a/b and c/d. The distance is simply d/2b where d >= b, otherwise b/2d
            //This results in quaver - semiquaver notes distance = 1, semiquaver - semisemiquaver notes distance = 1, quaver - quavertriplet = 1.5, quaver - semisemiquaver = 2 etc.
            //Added to this is the duration difference, which as the name implies is the difference in durations between 2 pulses...
            //the result is then divided by the average duration difference to get a more useful measurement
            double[,] matrix = new double[rhythm1.Pulses.Count, rhythm2.Pulses.Count];
            for (int i = 0; i < rhythm1.Pulses.Count; i++)
            {
                Rational rA = rhythm1.Pulses[i].StartBeat;
                for (int j = 0; j < rhythm2.Pulses.Count; j++)
                {
                    Rational rB = rhythm2.Pulses[j].StartBeat;

                    double strengthDistance;
                    if (rA.B == rB.B)
                        strengthDistance = 0;
                    else
                        strengthDistance = (rB.B >= rA.B) ? rB.B / (double)(2 * rA.B) : rA.B / (double)(2 * rB.B);

                    matrix[i, j] = strengthDistance;
                    if (averageDurationDifference != 0)
                        matrix[i, j] += ((rA - rhythm1.StartBeat) - (rB - rhythm2.StartBeat)).Abs() / averageDurationDifference;
                }
            }
            List<HungarianPair> pulsePairs = HungarianAlgorithm.Solve(matrix);

            //Cost to move a pulse / adjust its duration is the distance between it and the target, as calculated above
            //Cost to add or remove a pulse is 1, pulses are added with the average pulse length
            double distance = 0;

            //Simulate cost of moving pulses to match their paired up partners
            foreach (HungarianPair pair in pulsePairs)
                distance += matrix[pair.Row, pair.Column];

            //Simulate cost of adding all the remaining pulses at their partner's positions
            int pulseCountDifference = Math.Abs(rhythm2.Pulses.Count - rhythm1.Pulses.Count);
            distance += pulseCountDifference;

            return distance;
        }

    }
}
