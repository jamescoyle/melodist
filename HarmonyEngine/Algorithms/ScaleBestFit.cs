﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine.Algorithms
{
    internal class ScaleBestFit
    {

        /// <summary>
        /// Given an input chord progression, return the scale most likely they belong to
        /// </summary>
        /// <param name="chords"></param>
        /// <returns></returns>
        internal static Scale BestFit(List<Chord> chords)
        {
            //For each pitch, get proportion of time it's being played, to be used as weightings
            List<PitchWeight> pitchWeights = new List<PitchWeight>();
            for (int i = 0; i < 12; i++)
                pitchWeights.Add(new PitchWeight(i, 0));

            foreach (Chord chord in chords)
                chord.Pitches.ForEach(x => pitchWeights[x % 12].Weight += chord.Duration);

            return BestFit(pitchWeights);
        }



        /// <summary>
        /// Given an input set of notes, return a scale as output that is likely to be the scale the notes are in
        /// </summary>
        /// <param name="notes"></param>
        /// <returns></returns>
        internal static Scale BestFit(List<Note> notes)
        {
            //For each pitch, get proportion of time it's being played, to be used as weightings
            List<PitchWeight> pitchWeights = new List<PitchWeight>();
            for (int i = 0; i < 12; i++)
                pitchWeights.Add(new PitchWeight(i, 0));

            foreach (Note note in notes)
                pitchWeights[note.Pitch % 12].Weight += note.Duration;

            return BestFit(pitchWeights);
        }



        private static Scale BestFit(List<PitchWeight> pitchWeights)
        {
            //Loop through all scales, getting the distance of each one and recording the closest
            List<Scale> bestScales = new List<Scale>();
            double bestDistance = double.MaxValue;
            foreach (Scale scale in Scale.Scales)
            {
                double scaleDistance = 0;
                foreach (PitchWeight pitchWeight in pitchWeights)
                {
                    //For each pitch in the list but not in the scale, add to the scale distance the pitch weight multiplied by the distance of the pitch from the scale
                    if (!scale.Contains(pitchWeight.Pitch))
                        scaleDistance += pitchWeight.Weight * scale.Distance(pitchWeight.Pitch);
                }
                if (scaleDistance < bestDistance)
                {
                    bestScales = new List<Scale> { scale };
                    bestDistance = scaleDistance;
                }
                else if (scaleDistance == bestDistance)
                    bestScales.Add(scale);
            }
            if (bestScales.Count == 1)
                return bestScales[0];

            //If multiple scales returned as best option, use weight of tonic to decide the best
            double bestTonicWeight = -1;
            Scale bestScale = null;
            foreach (Scale scale in bestScales)
            {
                double tonicWeight = pitchWeights[scale.Key.Value % 12].Weight;
                if (tonicWeight > bestTonicWeight)
                {
                    bestTonicWeight = tonicWeight;
                    bestScale = scale;
                }
            }
            return bestScale;
        }



        private class PitchWeight
        {
            public Pitch Pitch { get; private set; }

            public double Weight { get; set; }

            public PitchWeight(Pitch pitch, double weight)
            {
                Pitch = pitch;
                Weight = weight;
            }
        }

    }
}
