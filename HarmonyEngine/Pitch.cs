﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public struct Pitch
    {

        public int Value { get; private set; }


        public int Octave { get { return (int)(Value / 12); } }



        public Pitch(int value)
        {
            if (value < 0 || value >= 128)
                throw HarmonyEngineException.PitchOutOfRange();
            Value = value;
        }



        public override string ToString()
        {
            return ToString(false);
        }

        private static List<string> _sharpNames = new List<string>() { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
        private static List<string> _flatNames = new List<string>() { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };
        public string ToString(bool useFlats, bool showOctave = false)
        {
            string pitchStr = (useFlats) ? _flatNames[Value % 12] : _sharpNames[Value % 12];

            if (showOctave)
                pitchStr += Octave.ToString();

            return pitchStr;
        }


        public override bool Equals(object obj)
        {
            if (obj is Pitch)
                return Equals((Pitch)obj);
            return false;
        }

        public bool Equals(Pitch obj)
        {
            return Value == obj.Value;
        }


        public override int GetHashCode()
        {
            return Value;
        }


        public static Interval operator -(Pitch p1, Pitch p2)
        {
            return new Interval(p1.Value - p2.Value);
        }


        public static Pitch operator +(Pitch p, Interval i)
        {
            return new Pitch(p.Value + i.Value);
        }

        public static Pitch operator + (Interval i, Pitch p)
        {
            return new Pitch(p.Value + i.Value);
        }



        #region Type Conversions
        public static implicit operator int(Pitch pitch)
        {
            return pitch.Value;
        }

        public static implicit operator Pitch(int value)
        {
            return new Pitch(value);
        }
        #endregion Type Conversions

    }
}
