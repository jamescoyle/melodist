﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public class FiniteTimeObject : INotifyPropertyChanged
    {

        protected object Lock = new object();



        private Rational _startBeat;
        public event EventHandler StartBeatChanged;
        public Rational StartBeat
        {
            get
            {
                lock (Lock) { return _startBeat; }
            }
            set
            {
                if (_startBeat != value)
                {
                    lock (Lock)
                    {
                        _startBeat = value;
                    }
                    OnPropertyChanged("StartBeat");
                    StartBeatChanged?.Invoke(this, null);
                }
            }
        }


        private Rational _duration;
        public event EventHandler DurationChanged;
        public Rational Duration
        {
            get
            {
                lock (Lock) { return _duration; }
            }
            set
            {
                if (value < 0)
                    throw HarmonyEngineException.NegativeDuration();
                if (_duration != value)
                {
                    lock (Lock)
                    {
                        _duration = value;
                    }
                    OnPropertyChanged("Duration");
                    DurationChanged?.Invoke(this, null);
                }
            }
        }


        public event EventHandler EndBeatChanged;
        public Rational EndBeat
        {
            get
            {
                lock (Lock) { return StartBeat + Duration; }
            }
            set
            {
                Rational newDuration = value - StartBeat;
                if (Duration != newDuration)
                {
                    lock (Lock)
                    {
                        Duration = newDuration;
                    }
                    OnPropertyChanged("EndBeat");
                    EndBeatChanged?.Invoke(this, null);
                }
            }
        }



        public FiniteTimeObject(Rational startBeat, Rational duration)
        {
            StartBeat = startBeat;
            Duration = duration;
        }


        /// <summary>
        /// Acts like a modulus to move the beat parameter by some multiple of Duration to be within the StartBeat - EndBeat range
        /// </summary>
        /// <param name="beat"></param>
        /// <returns></returns>
        public Rational MapBeat(Rational beat)
        {
            if (beat < StartBeat)
                beat += Duration * ((StartBeat - beat) / Duration).Ceiling();

            return ((beat - StartBeat) % Duration) + StartBeat;
        }


        public Rational BeatOverlap(FiniteTimeObject fto)
        {
            return Rational.Max(0, Rational.Min(EndBeat, fto.EndBeat) - Rational.Max(StartBeat, fto.StartBeat));
        }



        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion INotifyPropertyChanged implementation


    }



    public class RationalParse
    {
        public long A;
        public long B;

        public RationalParse()
        {
        }

        public RationalParse(Rational rational)
        {
            A = rational.A;
            B = rational.B;
        }

        public Rational Load()
        {
            return new Rational(A, B);
        }
    }



    public class FiniteTimeObjectParse
    {
        public RationalParse StartBeat;
        public RationalParse Duration;

        public FiniteTimeObjectParse()
        {
        }

        public FiniteTimeObjectParse(FiniteTimeObject finiteTimeObject)
        {
            StartBeat = new RationalParse(finiteTimeObject.StartBeat);
            Duration = new RationalParse(finiteTimeObject.Duration);
        }

        public FiniteTimeObject Load()
        {
            return new FiniteTimeObject(StartBeat.Load(), Duration.Load());
        }
    }
}
