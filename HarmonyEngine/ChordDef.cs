﻿using CSharpExtras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyEngine.Algorithms;

namespace HarmonyEngine
{
    public class ChordDef
    {

        private MemberListManager<int> _structureMngr;
        public MemberList<int> Structure { get { return _structureMngr.List; } }


        public string Suffix { get; private set; }


        private double _dissonance = -1;
        public double Dissonance
        {
            get
            {
                if (_dissonance < 0)
                    _dissonance = ChordDefDissonance.Dissonance(this);
                return _dissonance;
            }
        }


        public ChordDef(string suffix, params int[] structure)
        {
            Suffix = suffix;
            _structureMngr = new MemberListManager<int>(structure.ToList());
        }

        public ChordDef(string suffix, List<int> structure)
        {
            Suffix = suffix;
            _structureMngr = new MemberListManager<int>(structure);
        }


        public override string ToString()
        {
            return Suffix;
        }


        public bool Contains(ChordDef chordDef)
        {
            foreach(int struc in chordDef.Structure)
            {
                if (!Structure.Contains(struc))
                    return false;
            }
            return true;
        }


        public override bool Equals(object obj)
        {
            if (obj is ChordDef)
                return Equals((ChordDef)obj);
            return false;
        }

        public bool Equals(ChordDef obj)
        {
            return Structure.Count == obj.Structure.Count && Contains(obj);
        }


        public override int GetHashCode()
        {
            int output = 0;
            unchecked
            {
                foreach (int struc in Structure)
                    output += 2 << struc;   //Same as output += (int)Math.Pow(2, struc) but cleaner and faster
            }
            return output;
        }



        #region Static Properties & Methods
        public static List<ChordDef> ChordDefs { get; private set; }


        static ChordDef()
        {
            ChordDefs = new List<ChordDef>();
            ChordDefs.Add(new ChordDef("M", 0, 4, 7));
            ChordDefs.Add(new ChordDef("m", 0, 3, 7));
            ChordDefs.Add(new ChordDef("sus2", 0, 2, 7));
            ChordDefs.Add(new ChordDef("sus4", 0, 5, 7));
            ChordDefs.Add(new ChordDef("dim", 0, 3, 6));
            ChordDefs.Add(new ChordDef("+", 0, 4, 8));

            ChordDefs.Add(new ChordDef("M7", 0, 4, 7, 11));
            ChordDefs.Add(new ChordDef("7", 0, 4, 7, 10));
            ChordDefs.Add(new ChordDef("m7", 0, 3, 7, 10));
            ChordDefs.Add(new ChordDef("dim7", 0, 3, 6, 9));
            ChordDefs.Add(new ChordDef("m7b5", 0, 3, 6, 10));
        }


        public static ChordDef Retrieve(string suffix)
        {
            ChordDef result = ChordDefs.FirstOrDefault(x => x.Suffix == suffix);
            if (result == null)
                throw HarmonyEngineException.UnrecognisedChordDef(suffix);
            return result;
        }


        public static ChordDef Retrieve(params int[] structure)
        {
            return Retrieve(structure.ToList());
        }

        public static ChordDef Retrieve(List<int> structure)
        {
            ChordDef chordDef = _retrieve(structure);
            if (chordDef == null)
                throw HarmonyEngineException.UnrecognisedChordDef(structure);
            return chordDef;
        }

        private static ChordDef _retrieve(List<int> structure)
        {
            ChordDef structDef = new ChordDef("", structure);
            foreach (ChordDef chordDef in ChordDefs)
            {
                if (chordDef.Equals(structDef))
                    return chordDef;
            }
            return null;
        }

        public static Chord Retrieve(List<Pitch> pitches)
        {
            foreach(Pitch pitch in pitches)
            {
                //Subtract all pitches by the chosen chord root to get a chorddef structure
                int root = pitch.Value;
                List<int> pitchInts = new List<int>();
                pitches.ForEach(x => pitchInts.Add((x.Value - root + 240) % 12));
                pitchInts.Sort();

                //Try to get a ChordDef from the structure
                ChordDef chordDef = _retrieve(pitchInts);
                if (chordDef != null)
                    return new Chord(chordDef, pitch, 0, 0);
            }
            return null;
        }
        #endregion Static Properties & Methods

    }
}
