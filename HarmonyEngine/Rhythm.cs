﻿using CSharpExtras;
using HarmonyEngine.Algorithms;
using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public class Rhythm : FiniteTimeObject
    {

        private MemberListManager<FiniteTimeObject> _pulsesMngr;
        public MemberList<FiniteTimeObject> Pulses { get { return _pulsesMngr.List; } }



        public Rhythm(List<FiniteTimeObject> pulses)
            : base(pulses.Min(x => x.StartBeat), pulses.Max(x => x.EndBeat) - pulses.Min(x => x.StartBeat))
        {
            _pulsesMngr = new MemberListManager<FiniteTimeObject>(pulses.OrderBy(x => x.StartBeat));
        }


        public override bool Equals(object obj)
        {
            if (obj == null || (obj is Rhythm) == false)
                return false;
            return Equals((Rhythm)obj);
        }

        public bool Equals(Rhythm rhythm)
        {
            if (Pulses.Count != rhythm.Pulses.Count || StartBeat != rhythm.StartBeat || EndBeat != rhythm.EndBeat)
                return false;

            for(int i = 0; i < Pulses.Count; i++)
            {
                if (Pulses[i].StartBeat != rhythm.Pulses[i].StartBeat || Pulses[i].Duration != rhythm.Pulses[i].Duration)
                    return false;
            }
            return true;
        }


        public override int GetHashCode()
        {
            return Pulses.Count ^ EndBeat.GetHashCode();
        }


        public double Distance(Rhythm rhythm)
        {
            return RhythmRhythmDistance.Distance(this, rhythm);
        }



        /// <summary>
        /// Create a list of sub-rhythms
        /// </summary>
        /// <param name="removeDuplicates"></param>
        /// <param name="minimumRhythmNoteLength"></param>
        /// <returns></returns>
        public List<Rhythm> GenerateSubRhythms(bool removeDuplicates, int minimumRhythmNoteLength = 3)
        {
            List<Rhythm> subRhythms = new List<Rhythm>();
            for (int i = 0; i <= Pulses.Count - minimumRhythmNoteLength; i++)
            {
                for (int j = i + minimumRhythmNoteLength; j <= Pulses.Count; j++)
                {
                    Rhythm subRhythm = subRhythm = new Rhythm(Pulses.GetRange(i, j - i + 1));

                    if(!removeDuplicates && subRhythms.Any(x => x == subRhythm) == false)
                        subRhythms.Add(subRhythm);
                }
            }
            return subRhythms;
        }

    }
}
