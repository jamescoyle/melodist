﻿using CSharpExtras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyEngine.Algorithms;

namespace HarmonyEngine
{
    public enum ScaleType { Major, HarmonicMinor, Diminished, WholeTone }



    public class Scale
    {

        public ScaleType ScaleType { get; private set; }


        private MemberListManager<Pitch> _pitchesMngr;
        public MemberList<Pitch> Pitches { get { return _pitchesMngr.List; } }


        public Pitch Key { get; private set; }


        private double _dissonance = -1;
        public double Dissonance
        {
            get
            {
                if (_dissonance < 0)
                    _dissonance = ScaleDissonance.Dissonance(this);
                return _dissonance;
            }
        }



        public Scale(ScaleType type, Pitch key)
        {
            ScaleType = type;
            List<int> scalePattern = ScalePatterns[ScaleType];

            _pitchesMngr = new MemberListManager<Pitch>();
            scalePattern.ForEach(x => _pitchesMngr.Add(new Pitch((key.Value + x) % 12)));

            Key = key;
        }


        public bool Contains(Pitch pitch)
        {
            foreach (Pitch p in Pitches)
            {
                if ((pitch - p).Value % 12 == 0)
                    return true;
            }
            return false;
        }


        public bool Contains(Chord chord)
        {
            foreach (Pitch chordPitch in chord.Pitches)
            {
                if (!Contains(chordPitch))
                    return false;
            }
            return true;
        }


        public double Distance(Scale scale)
        {
            return ScaleScaleDistance.Distance(this, scale);
        }


        public double Distance(Chord chord)
        {
            return ChordScaleDistance.Distance(chord, this);
        }


        public double Distance(Pitch pitch)
        {
            return PitchScaleDistance.Distance(pitch, this);
        }


        /// <summary>
        /// Get the scale closest to this one which contains the pitch
        /// </summary>
        /// <param name="pitch"></param>
        /// <returns></returns>
        public Scale GetClosestScaleWithPitch(Pitch pitch)
        {
            Scale bestScale = null;
            double bestDistance = double.MaxValue;
            foreach (Scale scale in Scales)
            {
                if (scale.Contains(pitch))
                {
                    double scaleDistance = Distance(scale);
                    if (scaleDistance < bestDistance)
                    {
                        bestScale = scale;
                        bestDistance = scaleDistance;
                    }
                }
            }
            return bestScale;
        }


        /// <summary>
        /// Get the scale closest to this one which contains the chord
        /// </summary>
        /// <param name="chord"></param>
        /// <returns></returns>
        public Scale GetClosestScaleWithChord(Chord chord)
        {
            Scale bestScale = null;
            double bestDistance = double.MaxValue;
            foreach (Scale scale in Scales)
            {
                if (scale.Contains(chord))
                {
                    double scaleDistance = Distance(scale);
                    if (scaleDistance < bestDistance)
                    {
                        bestScale = scale;
                        bestDistance = scaleDistance;
                    }
                }
            }
            return bestScale;
        }


        /// <summary>
        /// Return the scale key name, plus the scale type, eg. C# Diminished
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToString(false);
        }

        /// <summary>
        /// Return the scale key name, plus the scale type, eg. C# Diminished
        /// </summary>
        /// <returns></returns>
        public string ToString(bool useFlats)
        {
            return Key.ToString(useFlats) + " " + ScaleType.ToString();
        }


        #region Static Properties & Methods
        public static List<Scale> Scales { get; private set; }

        public static Dictionary<ScaleType, List<int>> ScalePatterns;


        //Static constructor
        static Scale()
        {
            //Create a lookup of scale patterns
            ScalePatterns = new Dictionary<ScaleType, List<int>>()
            {
                { ScaleType.Major, new List<int> { 0, 2, 4, 5, 7, 9, 11 } },
                { ScaleType.HarmonicMinor, new List<int> { 0, 2, 3, 5, 7, 8, 11 } },
                { ScaleType.Diminished, new List<int> { 0, 2, 3, 5, 6, 8, 9, 11 } },
                { ScaleType.WholeTone, new List<int> { 0, 2, 4, 6, 8, 10 } }
            };

            //Create every possible scale
            Scales = new List<Scale>();
            for (int i = 0; i < 12; i++)
            {
                Scales.Add(new Scale(ScaleType.Major, new Pitch(i)));

                Scales.Add(new Scale(ScaleType.HarmonicMinor, new Pitch(i)));

                if (i < 3)
                    Scales.Add(new Scale(ScaleType.Diminished, new Pitch(i)));

                if (i < 2)
                    Scales.Add(new Scale(ScaleType.WholeTone, new Pitch(i)));
            }
        }


        /// <summary>
        /// Returns the scale that best fits the list of passed in chords
        /// </summary>
        /// <param name="chords"></param>
        /// <returns></returns>
        public static Scale GetBestFit(List<Chord> chords)
        {
            return ScaleBestFit.BestFit(chords);
        }


        /// <summary>
        /// Returns the scale that best fits the list of passed in notes
        /// </summary>
        /// <param name="notes"></param>
        /// <returns></returns>
        public static Scale GetBestFit(List<Note> notes)
        {
            return ScaleBestFit.BestFit(notes);
        }
        #endregion Static Properties & Methods

    }
}
