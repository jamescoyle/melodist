﻿using MathPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarmonyEngine
{
    public class Note : FiniteTimeObject
    {

        private Pitch _pitch;
        public event EventHandler PitchChanged;
        public Pitch Pitch
        {
            get { lock (Lock) { return _pitch; } }
            set
            {
                if(_pitch != value)
                {
                    lock (Lock) { _pitch = value; }
                    OnPropertyChanged("Pitch");
                    PitchChanged?.Invoke(this, null);
                }
            }
        }


        private int _velocity;
        public event EventHandler VelocityChanged;
        public int Velocity
        {
            get { return _velocity; }
            set
            {
                if (value < 0 || value > 127)
                    throw HarmonyEngineException.VelocityOutOfRange();
                if (_velocity != value)
                {
                    _velocity = value;
                    OnPropertyChanged("Velocity");
                    VelocityChanged?.Invoke(this, null);
                }
            }
        }



        public Note(Pitch pitch, Rational startBeat, Rational duration, int velocity = 100)
            : base(startBeat, duration)
        {
            Pitch = pitch;
            Velocity = velocity;
        }


        public static Interval operator -(Note n1, Note n2)
        {
            return n1.Pitch - n2.Pitch;
        }


        public override string ToString()
        {
            return Pitch.ToString(false, true);
        }

        public string ToString(bool useFlats, bool showOctave = false)
        {
            return Pitch.ToString(useFlats, showOctave);
        }



        public Note Copy()
        {
            return new Note(Pitch, StartBeat, Duration, Velocity);
        }

    }



    public class NoteParse : FiniteTimeObjectParse
    {
        public int Pitch;
        public int Velocity;

        public NoteParse()
        {
        }

        public NoteParse(Note note)
            : base(note)
        {
            Pitch = note.Pitch;
            Velocity = note.Velocity;
        }

        public new Note Load()
        {
            return new Note(Pitch, StartBeat.Load(), Duration.Load(), Velocity);
        }
    }
}
