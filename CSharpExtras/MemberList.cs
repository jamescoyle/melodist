﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtras
{
    public class MemberList<T> : IEnumerable<T>
    {

        private List<T> _list;

        private object _lock = new object();


        public event MemberListEventHandler<T> Added;

        public event MemberListEventHandler<T> Removed;



        internal MemberList()
        {
            _list = new List<T>();
        }

        internal MemberList(IEnumerable<T> collection)
        {
            _list = collection.ToList();
        }



        public int Capacity
        {
            get { lock (_lock) { return _list.Capacity; } }
            internal set { lock (_lock) { _list.Capacity = value; } }
        }

        public int Count
        {
            get { lock (_lock) { return _list.Count; } }
        }

        public T this[int index]
        {
            get { lock (_lock) { return _list[index]; } }
            internal set { lock (_lock) { _list[index] = value; } }
        }

        internal void Add(T item)
        {
            lock (_lock) { _list.Add(item); }
            Added?.Invoke(this, new MemberListEventArgs<T>(item));
        }

        internal void AddRange(IEnumerable<T> collection)
        {
            lock (_lock) { _list.AddRange(collection); }
            if (Added != null)
            {
                foreach (T item in collection)
                    Added.Invoke(this, new MemberListEventArgs<T>(item));
            }
        }

        public ReadOnlyCollection<T> AsReadOnly()
        {
            lock (_lock) { return _list.AsReadOnly(); }
        }

        public int BinarySearch(T item)
        {
            lock (_lock) { return _list.BinarySearch(item); }
        }

        public int BinarySearch(T item, IComparer<T> comparer)
        {
            lock (_lock) { return _list.BinarySearch(item, comparer); }
        }

        public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
        {
            lock (_lock) { return _list.BinarySearch(index, count, item, comparer); }
        }

        internal void Clear()
        {
            if (Removed != null)
            {
                List<T> copy = null;
                lock (_lock)
                {
                    copy = _list.ToList();
                    _list.Clear();
                }
                copy.ForEach(x => Removed.Invoke(this, new MemberListEventArgs<T>(x)));
            }
            else
                lock (_lock) { _list.Clear(); }
        }

        public bool Contains(T item)
        {
            lock (_lock) { return _list.Contains(item); }
        }

        public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
        {
            lock (_lock) { return _list.ConvertAll<TOutput>(converter); }
        }

        public void CopyTo(T[] array)
        {
            lock (_lock) { _list.CopyTo(array); }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (_lock) { _list.CopyTo(array, arrayIndex); }
        }

        public void CopyTo(int index, T[] array, int arrayIndex, int count)
        {
            lock (_lock) { _list.CopyTo(index, array, arrayIndex, count); }
        }

        public bool Exists(Predicate<T> match)
        {
            lock (_lock) { return _list.Exists(match); }
        }

        public T Find(Predicate<T> match)
        {
            lock (_lock) { return _list.Find(match); }
        }

        public List<T> FindAll(Predicate<T> match)
        {
            lock (_lock) { return _list.FindAll(match); }
        }

        public int FindIndex(int startIndex, int count, Predicate<T> match)
        {
            lock (_lock) { return _list.FindIndex(startIndex, count, match); }
        }

        public int FindIndex(int startIndex, Predicate<T> match)
        {
            lock (_lock) { return _list.FindIndex(startIndex, match); }
        }

        public int FindIndex(Predicate<T> match)
        {
            lock (_lock) { return _list.FindIndex(match); }
        }

        public T FindLast(Predicate<T> match)
        {
            lock (_lock) { return _list.FindLast(match); }
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T> match)
        {
            lock (_lock) { return _list.FindLastIndex(startIndex, count, match); }
        }

        public int FindLastIndex(int startIndex, Predicate<T> match)
        {
            lock (_lock) { return _list.FindLastIndex(startIndex, match); }
        }

        public int FindLastIndex(Predicate<T> match)
        {
            lock (_lock) { return _list.FindLastIndex(match); }
        }

        public void ForEach(Action<T> action)
        {
            lock (_lock) { _list.ForEach(action); }
        }

        public IEnumerator<T> GetEnumerator()
        {
            lock (_lock) { return _list.GetEnumerator(); }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (_lock) { return _list.GetEnumerator(); }
        }

        public List<T> GetRange(int index, int count)
        {
            lock (_lock) { return _list.GetRange(index, count); }
        }

        public int IndexOf(T item)
        {
            lock (_lock) { return _list.IndexOf(item); }
        }

        public int IndexOf(T item, int index)
        {
            lock (_lock) { return _list.IndexOf(item, index); }
        }

        public int IndexOf(T item, int index, int count)
        {
            lock (_lock) { return _list.IndexOf(item, index, count); }
        }

        internal void Insert(int index, T item)
        {
            lock (_lock) { _list.Insert(index, item); }
            Added?.Invoke(this, new MemberListEventArgs<T>(item));
        }

        internal void InsertRange(int index, IEnumerable<T> collection)
        {
            lock (_lock) { _list.InsertRange(index, collection); }
            if (Added != null)
            {
                foreach (T item in collection)
                    Added.Invoke(this, new MemberListEventArgs<T>(item));
            }
        }

        public int LastIndexOf(T item)
        {
            lock (_lock) { return _list.LastIndexOf(item); }
        }

        public int LastIndexOf(T item, int index)
        {
            lock (_lock) { return _list.LastIndexOf(item, index); }
        }

        public int LastIndexOf(T item, int index, int count)
        {
            lock (_lock) { return _list.LastIndexOf(item, index, count); }
        }

        internal bool Remove(T item)
        {
            bool returnValue = false;
            lock (_lock) { returnValue = _list.Remove(item); }
            if (returnValue)
            {
                Removed?.Invoke(this, new MemberListEventArgs<T>(item));
                return true;
            }
            return false;
        }

        internal int RemoveAll(Predicate<T> match)
        {
            if (Removed != null)
            {
                List<T> items = FindAll(match);
                int returnValue = 0;
                lock (_lock) { returnValue = _list.RemoveAll(match); }
                items.ForEach(x => Removed.Invoke(this, new MemberListEventArgs<T>(x)));
                return returnValue;
            }
            lock (_lock) { return _list.RemoveAll(match); }
        }

        internal void RemoveAt(int index)
        {
            if (Removed != null)
            {
                T item = _list[index];
                lock (_lock) { _list.RemoveAt(index); }
                Removed?.Invoke(this, new MemberListEventArgs<T>(item));
            }
            else
                lock (_lock) { _list.RemoveAt(index); }
        }

        internal void RemoveRange(int index, int count)
        {
            if (Removed != null)
            {
                List<T> items = _list.GetRange(index, count);
                lock (_lock) { _list.RemoveRange(index, count); }
                items.ForEach(x => Removed.Invoke(this, new MemberListEventArgs<T>(x)));
            }
            else
                lock (_lock) { _list.RemoveRange(index, count); }
        }

        internal void Reverse()
        {
            lock (_lock) { _list.Reverse(); }
        }

        internal void Reverse(int index, int count)
        {
            lock (_lock) { _list.Reverse(index, count); }
        }

        internal void Sort()
        {
            lock (_lock) { _list.Sort(); }
        }

        internal void Sort(Comparison<T> comparison)
        {
            lock (_lock) { _list.Sort(comparison); }
        }

        internal void Sort(IComparer<T> comparer)
        {
            lock (_lock) { _list.Sort(comparer); }
        }

        internal void Sort(int index, int count, IComparer<T> comparer)
        {
            lock (_lock) { _list.Sort(index, count, comparer); }
        }

        public T[] ToArray()
        {
            lock (_lock) { return _list.ToArray(); }
        }

        internal void TrimExcess()
        {
            lock (_lock) { _list.TrimExcess(); }
        }

        public bool TrueForAll(Predicate<T> match)
        {
            lock (_lock) { return _list.TrueForAll(match); }
        }

    }



    public delegate void MemberListEventHandler<T>(object sender, MemberListEventArgs<T> args);

    public class MemberListEventArgs<T> : EventArgs
    {

        public T Item { get; private set; }

        public MemberListEventArgs(T item)
        {
            Item = item;
        }

    }



    public class MemberListManager<T>
    {

        public MemberList<T> List { get; private set; }



        public MemberListManager()
        {
            List = new MemberList<T>();
        }

        public MemberListManager(IEnumerable<T> collection)
        {
            List = new MemberList<T>(collection);
        }



        public int Capacity
        {
            get { return List.Capacity; }
            set { List.Capacity = value; }
        }

        public T this[int index]
        {
            get { return List[index]; }
            set { List[index] = value; }
        }

        public void Add(T item)
        {
            List.Add(item);
        }

        public void AddRange(IEnumerable<T> collection)
        {
            List.AddRange(collection);
        }

        public void Clear()
        {
            List.Clear();
        }

        public void Insert(int index, T item)
        {
            List.Insert(index, item);
        }

        public void InsertRange(int index, IEnumerable<T> collection)
        {
            List.InsertRange(index, collection);
        }

        public bool Remove(T item)
        {
            return List.Remove(item);
        }

        public int RemoveAll(Predicate<T> match)
        {
            return List.RemoveAll(match);
        }

        public void RemoveAt(int index)
        {
            List.RemoveAt(index);
        }

        public void RemoveRange(int index, int count)
        {
            List.RemoveRange(index, count);
        }

        public void Reverse()
        {
            List.Reverse();
        }

        public void Reverse(int index, int count)
        {
            List.Reverse(index, count);
        }

        public void Sort()
        {
            List.Sort();
        }

        public void Sort(Comparison<T> comparison)
        {
            List.Sort(comparison);
        }

        public void Sort(IComparer<T> comparer)
        {
            List.Sort(comparer);
        }

        public void Sort(int index, int count, IComparer<T> comparer)
        {
            List.Sort(index, count, comparer);
        }

        public void TrimExcess()
        {
            List.TrimExcess();
        }

    }
}
