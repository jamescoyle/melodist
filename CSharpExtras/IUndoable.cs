﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtras
{
    public interface IUndoable
    {

        bool IsUndone { get; }

        void Undo();

        void Redo();

    }



    public class UndoableStack : IUndoable
    {

        private List<IUndoable> _outstandingUndos = new List<IUndoable>();

        private Stack<IUndoable> _successfulUndos = new Stack<IUndoable>();


        public bool IsUndone { get; private set; }

        public int MaxLength { get; private set; }



        public UndoableStack(int maxLength = 100)
            : base()
        {
            if (maxLength <= 0)
                throw new ArgumentException();

            IsUndone = false;
            MaxLength = maxLength;
        }



        public event UndoableStackEventHandler Pushed;
        public void Push(IUndoable item)
        {
            IsUndone = false;
            _successfulUndos.Clear();
            _outstandingUndos.Add(item);
            if (_outstandingUndos.Count > MaxLength)
                _outstandingUndos = _outstandingUndos.GetRange(_outstandingUndos.Count - MaxLength, MaxLength);
            Pushed?.Invoke(this, new UndoableStackEventArgs(item));
        }


        public event EventHandler Cleared;
        public void Clear()
        {
            _outstandingUndos.Clear();
            _successfulUndos.Clear();
            Cleared?.Invoke(this, new EventArgs());
        }


        public event UndoableStackEventHandler Undone;
        public void Undo()
        {
            while(_outstandingUndos.Count > 0)
            {
                IUndoable undo = _outstandingUndos.Last();
                _outstandingUndos.RemoveAt(_outstandingUndos.Count - 1);
                undo.Undo();
                if (undo.IsUndone)
                {
                    _successfulUndos.Push(undo);
                    Undone?.Invoke(this, new UndoableStackEventArgs(undo));
                    break;
                }
            }
        }


        public event UndoableStackEventHandler Redone;
        public void Redo()
        {
            while(_successfulUndos.Count > 0)
            {
                IUndoable undo = _successfulUndos.Pop();
                undo.Redo();
                if (!undo.IsUndone)
                {
                    _outstandingUndos.Add(undo);
                    Redone?.Invoke(this, new UndoableStackEventArgs(undo));
                    break;
                }
            }
        }

    }



    public delegate void UndoableStackEventHandler(object sender, UndoableStackEventArgs args);

    public class UndoableStackEventArgs : EventArgs
    {
        public IUndoable Item { get; private set; }

        public UndoableStackEventArgs(IUndoable item)
        {
            Item = item;
        }
    }
}
